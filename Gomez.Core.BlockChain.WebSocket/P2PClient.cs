﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using WS = WebSocketSharp;

namespace Gomez.Core.BlockChain.WebSocket
{
    public class P2PClient<T>
    {
        private readonly IDictionary<string, WS.WebSocket> _wsDict;
        private readonly ClientHandler<T> _handler;

        public P2PClient(Blockchain<T> chain)
        {
            _wsDict = new Dictionary<string, WS.WebSocket>();
            _handler = new ClientHandler<T>
            {
                CurrentChain = chain
            };
        }

        public void Connect(string url)
        {
            if (!_wsDict.ContainsKey(url))
            {
                var ws = new WS.WebSocket(url);
                ws.OnMessage += (sender, e) =>
                {
                    if (e.Data == "Hi Client")
                    {
                        Console.WriteLine(e.Data);
                    }
                    else
                    {
                        var serverChain = JsonConvert.DeserializeObject<Blockchain<T>>(e.Data);
                        _handler.TrySetChain(serverChain);
                    }
                };

                ws.Connect();
                ws.Send("Hi Server");
                ws.Send(JsonConvert.SerializeObject(_handler.CurrentChain));
                _wsDict.Add(url, ws);
            }
        }

        public void Send(string url, string data)
        {
            foreach (var item in _wsDict)
            {
                if (item.Key == url)
                {
                    item.Value.Send(data);
                }
            }
        }

        public void Broadcast(string data)
        {
            foreach (var item in _wsDict)
            {
                item.Value.Send(data);
            }
        }

        public IList<string> GetServers()
        {
            IList<string> servers = new List<string>();
            foreach (var item in _wsDict)
            {
                servers.Add(item.Key);
            }
            return servers;
        }

        public void Close()
        {
            foreach (var item in _wsDict)
            {
                item.Value.Close();
            }
        }
    }
}