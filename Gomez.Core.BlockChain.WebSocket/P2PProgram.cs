﻿namespace Gomez.Core.BlockChain.WebSocket
{
    public class P2PProgram<T>
    {
        private readonly int _port = 0;
        public Blockchain<T> Chain { get; private set; }
        private readonly P2PClient<T> _client;
        public P2PServer<T> Server { get; private set; } = null;

        public P2PProgram(Blockchain<T> chain, int port)
        {
            _port = port;
            _client = new P2PClient<T>(Chain);
            Chain = chain;
            Main();
        }

        //public string name = "Unknown";

        private void Main()
        {
            //if (args.Length >= 1)
            //    _port = int.Parse(args[0]);
            //if (args.Length >= 2)
            //    name = args[1];

            if (_port > 0)
            {
                Server = new P2PServer<T>(_port, Chain);
                Server.Start();
                return;
            }

            //todo stream the blockchain per block.

            //if (name != "Unkown")
            //{
            //    Console.WriteLine($"Current user is {name}");
            //}

            //Console.WriteLine("=========================");
            //Console.WriteLine("1. Connect to a server");
            //Console.WriteLine("2. Add a transaction");
            //Console.WriteLine("3. Display Blockchain");
            //Console.WriteLine("4. Exit");
            //Console.WriteLine("=========================");

            //int selection = 0;
            //while (selection != 4)
            //{
            //    switch (selection)
            //    {
            //        case 1:
            //            Console.WriteLine("Please enter the server URL");
            //            string serverURL = Console.ReadLine();
            //            _client.Connect($"{serverURL}/Blockchain");
            //            break;
            //        case 2:
            //            Console.WriteLine("Please enter the receiver name");
            //            string receiverName = Console.ReadLine();
            //            Console.WriteLine("Please enter the amount");
            //            string amount = Console.ReadLine();
            //            Chain.CreateTransaction(new Transaction<T>(name, receiverName, int.Parse(amount)));
            //            Chain.ProcessPendingTransactions(name);
            //            _client.Broadcast(JsonConvert.SerializeObject(PhillyCoin));
            //            break;
            //        case 3:
            //            Console.WriteLine("Blockchain");
            //            Console.WriteLine(JsonConvert.SerializeObject(PhillyCoin, Formatting.Indented));
            //            break;

            //    }

            //    Console.WriteLine("Please select an action");
            //    string action = Console.ReadLine();
            //    selection = int.Parse(action);
            //}

            _client.Close();
        }
    }
}