﻿using Newtonsoft.Json;
using System;
using WebSocketSharp;
using WebSocketSharp.Server;

namespace Gomez.Core.BlockChain.WebSocket
{
    public class P2PServer<T> : WebSocketBehavior
    {
        private WebSocketServer wss = null;
        private readonly int _port;
        private readonly ServerHandler<T> _handler;

        public P2PServer()
        {
        }

        public P2PServer(int port, Blockchain<T> chain) : this(port)
        {
            _handler = new ServerHandler<T>
            {
                CurrentChain = chain
            };
        }

        protected P2PServer(int port) : this()
        {
            _port = port;
        }

        public void Start()
        {
            wss = new WebSocketServer($"ws://127.0.0.1:{_port}");
            wss.AddWebSocketService<P2PServer<T>>("/Blockchain");
            wss.Start();
            Console.WriteLine($"Started server at ws://127.0.0.1:{_port}");
        }

        public void Stop()
        {
            wss.Stop();
        }

        protected override void OnMessage(MessageEventArgs e)
        {
            if (e.Data == "Hi Server")
            {
                Console.WriteLine(e.Data);
                Send("Hi Client");
            }
            else
            {
                Blockchain<T> clientChain = JsonConvert.DeserializeObject<Blockchain<T>>(e.Data);
                var newChain = _handler.TryGetChain(clientChain);
                if (newChain != null)
                {
                    Send(JsonConvert.SerializeObject(newChain));
                }
            }
        }
    }
}