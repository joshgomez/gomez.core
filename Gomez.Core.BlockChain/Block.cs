﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Gomez.Core.BlockChain
{
    public class Block<T>
    {
        public IList<Transaction<T>> Transactions { get; set; }
        public int Index { get; set; }
        public DateTime TimeStamp { get; set; }
        public string PreviousHash { get; set; }
        public string Hash { get; set; }

        public int Nonce { get; set; } = 0;

        public Block(string previousHash, IList<Transaction<T>> transactions)
        {
            Index = 0;
            TimeStamp = DateTime.UtcNow;
            PreviousHash = previousHash;
            Transactions = transactions;
            Hash = CalculateHash();
        }

        public string CalculateHash()
        {
            using var sha256 = SHA256.Create();
            var strJson = JsonConvert.SerializeObject(Transactions);
            byte[] inputBytes = Encoding.ASCII.GetBytes($"{TimeStamp}-{PreviousHash ?? ""}-{strJson}-{Nonce}");
            byte[] outputBytes = sha256.ComputeHash(inputBytes);

            return Convert.ToBase64String(outputBytes);
        }

        /// <summary>
        /// The implementation of Proof of Work is called mining. Just like coal mining. Mining is a process of finding something valuable with a
        /// lot of effort. In Blockchain, mining is to find the right hash with a lot of computing time.
        /// </summary>
        /// <param name="difficulty"></param>
        public void Mine(int difficulty)
        {
            /*
             * In the end, a new method, Mine, is added to accept difficulty as a parameter.
             * The difficulty is an integer that indicates the number of leading zeros required
             * for a generated hash. The Mine method tries to find a hash that matches with difficulty.
             * If a generated hash doesn’t meet the difficulty,
             * then it increases nonce to generate a new one.
             * The process will be ended when a qualified hash is found.
             */
            var leadingZeros = new string('0', difficulty);
            while (Hash == null || Hash.Substring(0, difficulty) != leadingZeros)
            {
                Nonce++;
                Hash = CalculateHash();
            }
        }
    }
}