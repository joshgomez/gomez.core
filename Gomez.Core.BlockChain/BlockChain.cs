﻿using System;
using System.Collections.Generic;

namespace Gomez.Core.BlockChain
{
    public class Blockchain<T>
    {
        private readonly IList<Block<T>> _chain;

        /// <summary>
        /// The new block generating process is a time-consuming process.But, a transaction
        /// can be submitted anytime, so we need to have a place to store transactions before they are processed.
        /// Therefore, we added a new field, PendingTransactions, to store newly added transactions.
        /// </summary>
        public IList<Transaction<T>> PendingTransactions { get; set; } = new List<Transaction<T>>();

        public int Difficulty { set; get; } = 2;
        public T Reward { get; set; }
        public int Count => _chain.Count;

        public Blockchain()
        {
            _chain = new List<Block<T>>();
            AddGenesisBlock();
        }

        private Block<T> CreateGenesisBlock()
        {
            return new Block<T>(null, default);
        }

        private void AddGenesisBlock()
        {
            _chain.Add(CreateGenesisBlock());
        }

        private Block<T> GetLatestBlock()
        {
            return _chain[_chain.Count - 1];
        }

        private void Add(IList<Transaction<T>> transactions)
        {
            Block<T> latestBlock = GetLatestBlock();
            var block = new Block<T>(latestBlock.PreviousHash, transactions)
            {
                Index = latestBlock.Index + 1,
                PreviousHash = latestBlock.Hash
            };
            block.Mine(Difficulty);
            _chain.Add(block);
        }

        public void CreateTransaction(Transaction<T> transaction)
        {
            PendingTransactions.Add(transaction);
        }

        /// <summary>
        /// We need to update Blockchain and add a ProcessPendingTransactions method. This method needs a miner address as the parameter.
        /// </summary>
        /// <param name="minerAddress"></param>
        public void ProcessPendingTransactions(string minerAddress)
        {
            Add(PendingTransactions);
            PendingTransactions = new List<Transaction<T>>();
            CreateTransaction(new Transaction<T>(null, minerAddress, Reward));
        }

        public bool IsValid()
        {
            int middle = (int)Math.Ceiling(_chain.Count / 2d);

            for (int i = 1; i < middle; i++)
            {
                if (!IsValid(i))
                {
                    return false;
                }

                int j = _chain.Count - i;
                if (j > middle && !IsValid(j))
                {
                    return false;
                }
            }

            return true;
        }

        private bool IsValid(int i)
        {
            Block<T> currentBlock = _chain[i];
            Block<T> previousBlock = _chain[i - 1];

            if (currentBlock.Hash != currentBlock.CalculateHash())
            {
                return false;
            }

            if (currentBlock.PreviousHash != previousBlock.CalculateHash())
            {
                return false;
            }

            return true;
        }
    }
}