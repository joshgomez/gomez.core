﻿using System.Collections.Generic;

namespace Gomez.Core.BlockChain
{
    public class ClientHandler<T>
    {
        public Blockchain<T> CurrentChain { get; set; }

        public void TrySetChain(Blockchain<T> serverChain)
        {
            if (serverChain.IsValid() && serverChain.Count > CurrentChain.Count)
            {
                var newTransactions = new List<Transaction<T>>();
                newTransactions.AddRange(serverChain.PendingTransactions);
                newTransactions.AddRange(CurrentChain.PendingTransactions);

                serverChain.PendingTransactions = newTransactions;
                CurrentChain = serverChain;
            }
        }
    }
}