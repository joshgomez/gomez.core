﻿using System.Collections.Generic;

namespace Gomez.Core.BlockChain
{
    public class ServerHandler<T>
    {
        private bool chainSynched = false;
        public Blockchain<T> CurrentChain { get; set; }

        public Blockchain<T> TryGetChain(Blockchain<T> clientChain)
        {
            if (clientChain.IsValid() && clientChain.Count > CurrentChain.Count)
            {
                var newTransactions = new List<Transaction<T>>();
                newTransactions.AddRange(clientChain.PendingTransactions);
                newTransactions.AddRange(CurrentChain.PendingTransactions);
                clientChain.PendingTransactions = newTransactions;
                CurrentChain = clientChain;
            }

            if (!chainSynched)
            {
                chainSynched = true;
                return CurrentChain;
            }

            return null;
        }
    }
}