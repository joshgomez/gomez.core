﻿namespace Gomez.Core.BlockChain
{
    public class Transaction<T>
    {
        public string FromAddress { get; set; }
        public string ToAddress { get; set; }
        public T Data { get; set; }

        public Transaction(string fromAddress, string toAddress, T data)
        {
            FromAddress = fromAddress;
            ToAddress = toAddress;
            Data = data;
        }
    }
}