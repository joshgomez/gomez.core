﻿using Gomez.Core.Cache.LinqCache.Containers;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Primitives;
using System;
using System.Threading;

namespace Gomez.Core.Cache.Containers.Memory
{
    /// <summary>
    /// Cache container wrapper around System.Runtime.Caching.MemoryCache providing a fast in-process memory cache.
    /// </summary>
    public sealed class MemoryCacheContainer : Container
    {
        /// <summary>
        /// Internal instance of MemoryCache.
        /// </summary>
        private IMemoryCache _memoryCache;

        /// <summary>
        /// Used to reset the cache
        /// </summary>
        private CancellationTokenSource _resetCacheToken = new CancellationTokenSource();

        /// <summary>
        /// Initializes a new instance of the MemoryCacheContainer.
        /// </summary>
        public MemoryCacheContainer() : this(new MemoryCache(new MemoryCacheOptions()))
        {
        }

        /// <summary>
        /// Initializes a new instance of the MemoryCacheContainer.
        /// </summary>
        /// <param name="memoryCache">Custom MemoryCache instance.</param>
        public MemoryCacheContainer(IMemoryCache memoryCache)
        {
            SupportsDurationInvalidation = true;
            _memoryCache = memoryCache;
        }

        private MemoryCacheEntryOptions GetEntryOption(TimeSpan? duration)
        {
            var options = new MemoryCacheEntryOptions().SetPriority(CacheItemPriority.Normal);
            if (duration != null)
            {
                options.SetAbsoluteExpiration(duration.Value);
            }

            options.AddExpirationToken(new CancellationChangeToken(_resetCacheToken.Token));
            return options;
        }

        public override void Set(string key, object value)
        {
            _memoryCache.Set(key, value, GetEntryOption(null));
        }

        public override void Set(string key, object value, TimeSpan duration)
        {
            _memoryCache.Set(key, value, GetEntryOption(duration));
        }

        public override bool Get(string key, out object value)
        {
            var cacheItem = _memoryCache.Get(key);

            var isCached = cacheItem != null;

            value = isCached ? cacheItem : null;
            return isCached;
        }

        public override void Delete(string key)
        {
            _memoryCache.Remove(key);
        }

        public override void Clear()
        {
            if (_resetCacheToken != null && !_resetCacheToken.IsCancellationRequested && _resetCacheToken.Token.CanBeCanceled)
            {
                _resetCacheToken.Cancel();
                _resetCacheToken.Dispose();
            }

            _resetCacheToken = new CancellationTokenSource();
        }

        #region IDisposable Support

        private bool disposedValue = false; // To detect redundant calls

        protected override void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (_memoryCache != null)
                    {
                        _memoryCache.Dispose();
                        _memoryCache = null;
                    }

                    if (_resetCacheToken != null)
                    {
                        _resetCacheToken.Dispose();
                        _resetCacheToken = null;
                    }
                }

                disposedValue = true;
            }
        }

        #endregion IDisposable Support
    }
}