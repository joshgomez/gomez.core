﻿using System;

namespace Gomez.Core.Cache.LinqCache
{
    internal static class ArgumentValidator
    {
        public static void IsNotNull<TType>(TType argument, string name) where TType : class
        {
            if (argument == null)
            {
                throw new ArgumentNullException(name);
            }
        }
    }
}