﻿using Gomez.Core.Cache.LinqCache.Containers;
using System;
using System.Linq;

namespace Gomez.Core.Cache.LinqCache.Invalidators
{
    /// <summary>
    /// Abstract base class for cache invalidators.
    /// </summary>
    public abstract class Invalidator
    {
        /// <summary>
        /// On initialization of the invalidator.
        /// </summary>
        protected internal virtual void OnInit(Container container, IQueryable query, string key)
        {
        }

        /// <summary>
        /// After a cache hit has occured.
        /// </summary>
        protected internal virtual void OnCacheHit(Container container, IQueryable query, string key, object cachedValue)
        {
        }

        /// <summary>
        /// After a cache miss has occured.
        /// </summary>
        protected internal virtual void OnCacheMiss(Container container, IQueryable query, string key)
        {
        }

        /// <summary>
        /// After a value has been refreshed.
        /// </summary>
        protected internal virtual void OnCacheRefresh(Container container, IQueryable query, string key, object value)
        {
        }

        /// <summary>
        /// If the invalidator uses duration.
        /// </summary>
        protected internal bool UsesDuration { get; protected set; }

        /// <summary>
        /// Duration until invalidation occurs if invalidator uses duration.
        /// </summary>
        protected internal TimeSpan Duration { get; protected set; }

        internal bool IsInitialized { get; set; }
    }
}