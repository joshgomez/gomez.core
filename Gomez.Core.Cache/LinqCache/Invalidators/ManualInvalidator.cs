﻿namespace Gomez.Core.Cache.LinqCache.Invalidators
{
    /// <summary>
    /// Manual invalidator.
    /// </summary>
    public class ManualInvalidator : Invalidator
    {
    }
}