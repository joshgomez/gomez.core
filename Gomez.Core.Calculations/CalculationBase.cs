﻿using Gomez.Core.Models.Entity;
using System;
using System.Collections.Generic;

namespace Gomez.Core.Calculations
{
    public abstract class CalculationBase
    {
        public Dictionary<string, byte[]> RowVersions { get; private set; }

        protected CalculationBase()
        {
            RowVersions = new Dictionary<string, byte[]>();
        }

        protected T GetProperty<T>(ISimplePropertyCollection collection, string key)
        {
            var prop = collection.GetProperty<T>(key, out byte[] rowVersion);
            RowVersions[key] = rowVersion;
            return prop;
        }

        public virtual void Init(ISimplePropertyCollection collection)
        {
            throw new NotImplementedException();
        }
    }
}