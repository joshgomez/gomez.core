﻿using Gomez.Core.Storage;
using Gomez.Core.Utilities;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Gomez.Core.Compression
{
    public class CompressionProvider : ICompressionProvider, IDisposable
    {
        private static readonly ILockManager _fileLockManager = new LockManager();
        private readonly IStorageProvider _storage;
        private readonly string _filePath;
        private ZipFile _zipFile;
        public string FilePath { get { return _filePath; } }

        public CompressionProvider(string filePath, IStorageProvider storage)
        {
            _filePath = filePath;
            _storage = storage;
        }

        protected void CreateIfNull()
        {
            if (_zipFile != null) return;
            _zipFile = ZipFile.Create(_filePath);
        }

        private async Task AddAndUpdateAsync(string containerName, string blobName, string prefix)
        {
            if (!await _storage.BlobExistsAsync(containerName, blobName))
            {
                return;
            }

            using var stream = await _storage.GetBlobStreamAsync(containerName, blobName);
            string fileName = Path.Combine(prefix, containerName, blobName);
            AddAndUpdate(stream, fileName);
        }

        public void AddAndUpdate(string containerName, string blobName, string prefix = "")
        {
            _fileLockManager.GetLock($"{nameof(AddAndUpdate)}/{containerName}/{blobName}", () =>
            {
                AsyncPump.Run(async delegate
                {
                    await AddAndUpdateAsync(containerName, blobName, prefix);
                });
            });
        }

        public void AddAndUpdate(Stream stream, string filePath)
        {
            if (stream == null || stream.Length == 0) return;

            CreateIfNull();
            _zipFile.BeginUpdate();
            _zipFile.Add(new CustomStaticDataSource(stream), ZipEntry.CleanName(filePath));
            _zipFile.CommitUpdate();
        }

        public void Extract(Stream stream, string password = "")
        {
            _zipFile = new ZipFile(stream);
            if (!string.IsNullOrEmpty(password))
            {
                _zipFile.Password = password;     // AES encrypted entries are handled automatically
            }

            foreach (ZipEntry zipEntry in _zipFile)
            {
                if (!zipEntry.IsFile)
                {
                    continue;           // Ignore directories
                }

                string entryFileName = zipEntry.Name;
                // Optionally match entrynames against a selection list here to skip as desired.
                // The unpacked length is available in the zipEntry.Size property.

                byte[] buffer = new byte[4096];     // 4K is optimum
                Stream zipStream = _zipFile.GetInputStream(zipEntry);

                // Manipulate the output filename here as desired.
                string fullZipToPath = Path.Combine(_filePath, entryFileName);
                string directoryName = Path.GetDirectoryName(fullZipToPath);
                if (directoryName.Length > 0)
                    Directory.CreateDirectory(directoryName);

                // Unzip file in buffered chunks. This is just as fast as unpacking to a buffer the full size
                // of the file, but does not waste memory.
                // The "using" will close the stream even if an exception occurs.
                using FileStream streamWriter = File.Create(fullZipToPath);
                StreamUtils.Copy(zipStream, streamWriter, buffer);
            }
        }

        #region IDisposable Support

        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing && _zipFile != null)
                {
                    _zipFile.Close();
                    _zipFile = null;
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion IDisposable Support
    }
}