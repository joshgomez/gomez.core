﻿using System.IO;

namespace Gomez.Core.Compression
{
    public class CustomStaticDataSource : ICustomerStaticDataSource
    {
        private Stream _stream;

        public Stream GetSource()
        {
            return _stream;
        }

        public CustomStaticDataSource()
        {
        }

        public CustomStaticDataSource(Stream inputStream)
        {
            SetStream(inputStream);
        }

        public void SetStream(Stream inputStream)
        {
            _stream = inputStream;
            _stream.Position = 0;
        }
    }
}