﻿using System.IO;

namespace Gomez.Core.Compression
{
    public interface ICompressionProvider
    {
        void Extract(Stream stream, string password = "");
    }
}