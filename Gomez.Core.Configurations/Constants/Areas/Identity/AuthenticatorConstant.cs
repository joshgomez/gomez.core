﻿namespace Gomez.Core.Configurations.Constants.Areas.Identity
{
    public static class AuthenticatorConstant
    {
        private const string V = "otpauth://totp/{0}:{1}?secret={2}&issuer={0}&digits=6";
        public const string EnableUriFormat = V;
        private const string V1 = "otpauth://totp/{0}:{1}?secret={2}&issuer={0}";
        public const string UriFormat = V1;
    }
}