﻿namespace Gomez.Core.Configurations.Constants.Areas.Identity
{
    public static class RoleConstant
    {
        /// <summary>
        /// Somebody who has access to all the administration features and system settings.
        /// </summary>
        public const string SUPER_ADMINISTRATOR = "SuperAdministrator";

        /// <summary>
        /// Somebody who has access to all the administration features.
        /// </summary>
        public const string ADMINISTRATOR = "Administrator";

        /// <summary>
        /// Somebody who can publish and manage content including the content of other users.
        /// </summary>
        public const string MODERATOR = "Moderator";

        /// <summary>
        /// Somebody who can publish and manage their own content.
        /// </summary>
        public const string ORIGINATOR = "Originator";

        /// <summary>
        /// Somebody who can create content but cannot publish them.
        /// </summary>
        public const string CONTRIBUTOR = "Contributor";

        /// <summary>
        /// Sombody who have access to the content assigned to the user.
        /// </summary>
        public const string USER = "User";
    }
}