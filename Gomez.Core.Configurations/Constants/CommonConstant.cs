﻿namespace Gomez.Core.Configurations.Constants
{
    public static class CommonConstant
    {
        public const string KEY_COORDINATE_X = "Coordinate.X";
        public const string KEY_COORDINATE_Y = "Coordinate.Y";

        public const string KEY_NAME = "Name";
        public const string KEY_DESCRIPTION = "Description";
        public const string KEY_TYPE = "Type";
        public const string KEY_IMAGE = "Image";
    }
}