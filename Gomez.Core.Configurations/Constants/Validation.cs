﻿namespace Gomez.Core.Configurations.Constants
{
    public static class Validation
    {
        public const int NAME_MIN_LENGHT = 2;
        public const int NAME_MAX_LENGHT = 256;

        public const int SMALL_DESCRIPTION_MIN_LENGHT = 3;
        public const int SMALL_DESCRIPTION_MAX_LENGHT = 4000;

        public const int SHORT_NAME_MIN_LENGHT = 2;
        public const int SHORT_NAME_MAX_LENGHT = 32;

        public const int MEDIUM_NAME_MIN_LENGHT = 2;
        public const int MEDIUM_NAME_MAX_LENGHT = 64;
    }
}