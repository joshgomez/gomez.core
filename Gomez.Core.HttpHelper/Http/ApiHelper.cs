﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Gomez.Core.Net.Http
{
    public class ApiHelper : IDisposable
    {
        private string _token;

        public HttpClient Client { get; set; }

        public string GrantType { get; private set; }
        public string GrantId { get; private set; }
        private string GrantSecret { get; set; }
        public string HostUrl { get; set; }
        public string TokenUrl { get; set; }

        public Dictionary<string, string> Headers { get; set; }

        public ApiHelper(string hostUrl, string tokenUrl, string grantId, string grantSecret, string grantType = "client_credentials")
        {
            GrantType = grantType;
            HostUrl = hostUrl.TrimEnd('/') + '/';
            TokenUrl = tokenUrl.TrimEnd('/');
            GrantId = grantId;
            GrantSecret = grantSecret;

            Headers = new Dictionary<string, string>();
            Cookies = new CookieContainer();

            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
        }

        private string ToQueryString(NameValueCollection nvc)
        {
            if (nvc == null || nvc.Count == 0)
            {
                return string.Empty;
            }

            var array = (from key in nvc.AllKeys
                         from value in nvc.GetValues(key)
                         select string.Format("{0}={1}", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(value)))
                .ToArray();

            return "?" + string.Join("&", array);
        }

        public async Task<HttpResponseMessage> GetRequestAsync(string route, NameValueCollection query = null)
        {
            var url = HostUrl + route + ToQueryString(query);
            Client = await GetHttpClientAsync();
            return await Client.GetAsync(url).ConfigureAwait(false);
        }

        public async Task<HttpResponseMessage> GetRequestAsync(string route, CancellationToken ct, NameValueCollection query = null)
        {
            var url = HostUrl + route + ToQueryString(query);
            Client = await GetHttpClientAsync();
            return await Client.GetAsync(url, ct).ConfigureAwait(false);
        }

        public HttpResponseMessage GetRequest(string route, NameValueCollection query = null)
        {
            return GetRequestAsync(route, query).Result;
        }

        public async static Task<T> ToObjAsync<T>(HttpContent content)
        {
            if (content == null)
            {
                return default;
            }

            var strContent = await content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(strContent);
        }

        public async static Task<T> ToObjFromStreamAsync<T>(HttpContent content)
        {
            if (content == null)
            {
                return default;
            }

            var stream = await content.ReadAsStreamAsync();
            using var sr = new StreamReader(stream);
            using JsonReader reader = new JsonTextReader(sr);
            JsonSerializer serializer = new JsonSerializer();
            return serializer.Deserialize<T>(reader);
        }

        public async static Task ToFileFromStreamAsync(HttpContent content, string filePath)
        {
            using var stream = await content.ReadAsStreamAsync();
            using Stream file = File.Create(filePath);
            await stream.CopyToAsync(file);
        }

        public async static Task ToFileFromStringAsync(HttpContent content, string filePath)
        {
            string value = await content.ReadAsStringAsync();
            File.WriteAllText(filePath, value, Encoding.UTF8);
        }

        public static T ToObj<T>(HttpContent content)
        {
            return ToObjAsync<T>(content).Result;
        }

        public async Task<HttpResponseMessage> PostRequestAsync<T>(string route, T model)
        {
            Client = await GetHttpClientAsync();
            var content = new StringContent(JsonConvert.SerializeObject(model), new UTF8Encoding(), "application/json");
            return await Client.PostAsync(HostUrl + route, content);
        }

        public async Task<HttpResponseMessage> PutRequestAsync<T>(string route, T model)
        {
            Client = await GetHttpClientAsync();
            var content = new StringContent(JsonConvert.SerializeObject(model), new UTF8Encoding(), "application/json");
            return await Client.PutAsync(HostUrl + route, content);
        }

        public async Task<HttpResponseMessage> DeleteRequestAsync(string route, NameValueCollection query = null)
        {
            Client = await GetHttpClientAsync();
            return await Client.DeleteAsync(HostUrl + route + ToQueryString(query));
        }

        public HttpResponseMessage DeleteRequest(string route, NameValueCollection query = null)
        {
            return DeleteRequestAsync(route, query).Result;
        }

        private HttpClientHandler HttpHandler
        {
            get
            {
                return new HttpClientHandler()
                {
                    AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate,
                    ////AllowAutoRedirect = true,
                    //UseCookies = true,
                    //CookieContainer = this.Cookies,
                    ClientCertificateOptions = ClientCertificateOption.Automatic,
                    //UseProxy = false,
                    //Proxy = null
                };
            }
        }

        public async Task<string> GetTokenAsync()
        {
            await Task.Yield();

            Client = new HttpClient(HttpHandler);
            string id;
            string secret;
            switch (GrantType)
            {
                case "client_credentials":
                    id = "client_id";
                    secret = "client_secret";
                    break;

                case "password":
                    id = "username";
                    secret = "password";
                    break;

                default:
                    throw new ApiException("Invalid grant type.");
            }

            try
            {
                var response = await
                    Client.PostAsync(TokenUrl,
                        new FormUrlEncodedContent(new Dictionary<string, string>
                        {
                            {"grant_type", GrantType},
                            {id, GrantId},
                            {secret, GrantSecret}
                        }));

                var stream = await response.Content.ReadAsStreamAsync();
                using var reader = new StreamReader(stream);
                var obj = JObject.Parse(reader.ReadToEnd());
                return obj.GetValue("access_token").ToString();
            }
            catch (Exception ex)
            {
                string errMessage = ex.Message;
                if (ex.InnerException != null)
                {
                    errMessage += ",INNER: " + ex.InnerException.Message;
                }

                throw new ApiException(errMessage);
            }
        }

        private async Task<HttpClient> GetHttpClientAsync()
        {
            await Task.Yield();

            if (_token == null)
                _token = await GetTokenAsync();

            if (Client == null)
                Client = new HttpClient(HttpHandler);

            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _token);

            foreach (var header in Headers)
            {
                if (Client.DefaultRequestHeaders.Contains(header.Key))
                {
                    Client.DefaultRequestHeaders.Remove(header.Key);
                }

                Client.DefaultRequestHeaders.Add(header.Key, header.Value);
            }

            return Client;
        }

        public CookieContainer Cookies
        {
            get;
            private set;
        }

        #region IDisposable Support

        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    Cookies = null;
                    if (Client != null)
                    {
                        Client.Dispose();
                        Client = null;
                    }
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion IDisposable Support
    }
}