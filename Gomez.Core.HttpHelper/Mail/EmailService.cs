﻿using Gomez.Core.Utilities;
using MailKit.Net.Smtp;
using MimeKit;
using System;
using System.Threading.Tasks;

namespace Gomez.Core.Net.Mail
{
    public class EmailService : IEmailService
    {
        private readonly EmailSettings _emailSettings;

        public EmailService(EmailSettings emailSettings)
        {
            _emailSettings = emailSettings;
        }

        private MimeMessage CreateMessage(string email, string subject, string htmlMessage)
        {
            var mimeMessage = new MimeMessage();
            mimeMessage.From.Add(new MailboxAddress(_emailSettings.SenderName, _emailSettings.Sender));
            mimeMessage.To.Add(new MailboxAddress(StringUtility.GetNamesFromEmail(email), email));
            mimeMessage.Subject = subject;
            mimeMessage.Body = new TextPart("html")
            {
                Text = htmlMessage
            };

            return mimeMessage;
        }

        public void SendEmail(string email, string subject, string htmlMessage)
        {
            AsyncPump.Run(async () => { await SendEmailAsync(email, subject, htmlMessage); });
        }

        public async Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            try
            {
                var mimeMessage = CreateMessage(email, subject, htmlMessage);
                using var client = new SmtpClient
                {
                    // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                    ServerCertificateValidationCallback = (s, c, h, e) =>
                    {
                        if (e != System.Net.Security.SslPolicyErrors.None)
                        {
                            return false;
                        }

                        return true;
                    }
                };
                await client.ConnectAsync(_emailSettings.MailServer, _emailSettings.MailPort, _emailSettings.UseSsl);
                await AuthenticateAsync(client);
                await client.SendAsync(mimeMessage);
                await client.DisconnectAsync(true);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(ex.Message, ex);
            }
        }

        /// <summary>
        /// needed if the SMTP server requires authentication
        /// </summary>
        /// <param name="client"></param>
        private async Task AuthenticateAsync(SmtpClient client)
        {
            // Note: only needed if the SMTP server requires authentication
            if (!string.IsNullOrEmpty(_emailSettings.Password))
            {
                await client.AuthenticateAsync(_emailSettings.UserName, _emailSettings.Password);
            }
        }
    }
}