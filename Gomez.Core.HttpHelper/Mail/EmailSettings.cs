﻿namespace Gomez.Core.Net.Mail
{
    public class EmailSettings
    {
        public string MailServer { get; set; }
        public int MailPort { get; set; }
        public bool UseSsl { get; set; }
        public string SenderName { get; set; }

        private string _sender;

        public string Sender
        {
            get { return string.IsNullOrEmpty(_sender) ? UserName : _sender; }
            set { _sender = value; }
        }

        public string UserName { get; set; }
        public string Password { get; set; }
    }
}