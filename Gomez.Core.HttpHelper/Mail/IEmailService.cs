﻿using System.Threading.Tasks;

namespace Gomez.Core.Net.Mail
{
    public interface IEmailService
    {
        Task SendEmailAsync(string email, string subject, string htmlMessage);

        void SendEmail(string email, string subject, string htmlMessage);
    }
}