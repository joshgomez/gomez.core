﻿using Microsoft.Extensions.Configuration;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;

namespace Gomez.Core.KeyVault
{
    public class AppSettingsKeyVaultProvider : IKeyVault
    {
        protected NameValueCollection _store;

        public AppSettingsKeyVaultProvider(IConfiguration config)
        {
            _store = new NameValueCollection();
            foreach (var c in config.GetChildren().Where(x => !x.GetChildren().Any()))
            {
                _store[c.Key] = c.Value;
            }

            foreach (var c in config.GetSection("ConnectionStrings").GetChildren())
            {
                _store[c.Key] = c.Value;
            }
        }

        public string GetKey(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return string.Empty;
            }

            if (_store[name] != null)
            {
                return _store[name];
            }

            return string.Empty;
        }

        public async Task<string> GetKeyAsync(string name)
        {
            return await Task.Run(() => GetKey(name));
        }
    }
}