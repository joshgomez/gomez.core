﻿using System.Threading.Tasks;

namespace Gomez.Core.KeyVault
{
    public interface IKeyVault
    {
        string GetKey(string name);

        Task<string> GetKeyAsync(string name);
    }
}