﻿using System.Threading.Tasks;

namespace Gomez.Core.KeyVault
{
    public class KeyVaultService : IKeyVault
    {
        private readonly IKeyVault _provider;

        public KeyVaultService(IKeyVault provider)
        {
            _provider = provider;
        }

        public string GetKey(string name)
        {
            return _provider.GetKey(name);
        }

        public async Task<string> GetKeyAsync(string name)
        {
            return await _provider.GetKeyAsync(name);
        }
    }
}