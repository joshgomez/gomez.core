﻿using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace Gomez.Core.Localization
{
    public class LocalizationHelper
    {
        private readonly IStringLocalizerFactory _localizerFactory;
        private readonly Type _type;

        public LocalizationHelper(IStringLocalizerFactory localizerFactory, Type type)
        {
            _localizerFactory = localizerFactory;
            _type = type;
        }

        public Dictionary<string, string[]> LoadJson(string basePath, string collection)
        {
            var filePath = Path.Combine(basePath, Path.GetFileNameWithoutExtension(collection)) + ".json";
            if (File.Exists(filePath))
            {
                var localization = _localizerFactory.Create(collection, _type.Assembly.FullName);
                using StreamReader file = File.OpenText(filePath);
                JsonSerializer serializer = new JsonSerializer();
                Dictionary<string, string[]> translations =
                    (Dictionary<string, string[]>)serializer.Deserialize(file, typeof(Dictionary<string, string[]>));

                foreach (var translation in translations)
                {
                    if (translations.Values == null)
                    {
                        continue;
                    }

                    for (int i = 0; i < translation.Value.Length; i++)
                    {
                        translation.Value[i] = localization[translation.Value[i]];
                    }
                }

                return translations;
            }

            return new Dictionary<string, string[]>();
        }
    }
}