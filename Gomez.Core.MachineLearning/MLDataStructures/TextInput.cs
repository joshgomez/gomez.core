﻿using Microsoft.ML.Data;

namespace Gomez.Core.MachineLearning.MLDataStructures
{
    internal class TextInput
    {
        [LoadColumn(0)]
        public string Label { get; set; }

        [LoadColumn(1)]
        public string Message { get; set; }
    }
}