﻿using Microsoft.ML.Data;

namespace Gomez.Core.MachineLearning.MLDataStructures
{
    internal class TextPrediction
    {
        [ColumnName("PredictedLabel")]
        public string Is { get; set; }
    }
}