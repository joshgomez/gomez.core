﻿using Gomez.Core.MachineLearning.MLDataStructures;
using Microsoft.ML;
using Microsoft.ML.Data;
using System;
using System.Collections.Generic;

namespace Gomez.Core.MachineLearning
{
    public class SentenceDetection : IDisposable
    {
        private readonly MLContext _mlContext;
        private readonly string _filePath;
        private IDataView _data;
        private DataViewSchema _scheme;
        private ITransformer _model = null;
        private PredictionEngine<TextInput, TextPrediction> _engine;
        private readonly string _match;

        public SentenceDetection(string filePath, string match)
        {
            _filePath = filePath;
            _mlContext = new MLContext();
            _match = match;
            LoadTrainedModel();
        }

        public void SetData(string filePath)
        {
            // Specify the schema for spam data and read it into DataView.
            _data = _mlContext.Data.LoadFromTextFile<TextInput>(path: filePath, hasHeader: true, separatorChar: '\t');
            _scheme = _data.Schema;
        }

        public void LoadTrainedModel()
        {
            if (!System.IO.File.Exists(_filePath))
            {
                return;
            }

            // Specify the schema for spam data and read it into DataView.
            _model = _mlContext.Model.Load(_filePath, out _scheme);
        }

        public void SetEngine()
        {
            //Create a PredictionFunction from our model
            _engine = _mlContext.Model.CreatePredictionEngine<TextInput, TextPrediction>(_model);
        }

        public IReadOnlyList<TrainCatalogBase.CrossValidationResult<MulticlassClassificationMetrics>>
            Train(int numberOfIterations = 10, int numberOfFolds = 5)
        {
            // Create the estimator which converts the text label to boolean, featurizes the text, and adds a linear trainer.
            // Data process configuration with pipeline data transformations
            var dataProcessPipeline = _mlContext.Transforms.Conversion.MapValueToKey("Label", "Label")
                                      .Append(_mlContext.Transforms.Text.FeaturizeText("FeaturesText", new Microsoft.ML.Transforms.Text.TextFeaturizingEstimator.Options
                                      {
                                          WordFeatureExtractor = new Microsoft.ML.Transforms.Text.WordBagEstimator.Options { NgramLength = 2, UseAllLengths = true },
                                          CharFeatureExtractor = new Microsoft.ML.Transforms.Text.WordBagEstimator.Options { NgramLength = 3, UseAllLengths = false },
                                      }, "Message"))
                                      .Append(_mlContext.Transforms.CopyColumns("Features", "FeaturesText"))
                                      .Append(_mlContext.Transforms.NormalizeLpNorm("Features", "Features"))
                                      .AppendCacheCheckpoint(_mlContext);

            // Set the training algorithm
            var trainer = _mlContext.MulticlassClassification.Trainers.OneVersusAll(_mlContext.BinaryClassification.Trainers.AveragedPerceptron(labelColumnName: "Label", numberOfIterations: numberOfIterations, featureColumnName: "Features"), labelColumnName: "Label")
                                      .Append(_mlContext.Transforms.Conversion.MapKeyToValue("PredictedLabel", "PredictedLabel"));
            var trainingPipeLine = dataProcessPipeline.Append(trainer);

            // Evaluate the model using cross-validation.
            // Cross-validation splits our dataset into 'folds', trains a model on some folds and
            // evaluates it on the remaining fold. We are using 5 folds so we get back 5 sets of scores.
            // Let's compute the average AUC, which should be between 0.5 and 1 (higher is better).
            var crossValidationResults = _mlContext.MulticlassClassification.CrossValidate(data: _data, estimator: trainingPipeLine, numberOfFolds: numberOfFolds);

            // Now let's train a model on the full dataset to help us get better results
            _model = trainingPipeLine.Fit(_data);
            _mlContext.Model.Save(_model, _scheme, _filePath);
            return crossValidationResults;
        }

        public bool Predict(string message)
        {
            var input = new TextInput { Message = message };
            var prediction = _engine.Predict(input);

            return prediction.Is == _match;
        }

        #region IDisposable Support

        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing && _engine != null)
                {
                    _engine.Dispose();
                    _engine = null;
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion IDisposable Support
    }
}