﻿namespace Gomez.Core.MachineLearning
{
    public class SpamDetection : SentenceDetection
    {
        public SpamDetection(string filePath) : base(filePath, "spam")
        {
        }
    }
}