﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gomez.Core.Models.Entity
{
    public abstract class EntityBase : IEntityBase
    {
        [NotMapped]
        public IDictionary<string, string> RuntimeStringProperties { get; set; } = new Dictionary<string, string>();

        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;
        public DateTime? UpdatedAt { get; set; }
        public DateTime? DeletedAt { get; set; }

        public int SortingOrder { get; set; } = 0;

        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}