﻿using System;

namespace Gomez.Core.Models.Entity
{
    public interface IEntityBase
    {
        DateTime CreatedAt { get; set; }
        DateTime? UpdatedAt { get; set; }
        DateTime? DeletedAt { get; set; }
        int SortingOrder { get; set; }
    }
}