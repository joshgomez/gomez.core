﻿using System.Collections.Generic;

namespace Gomez.Core.Models.Entity
{
    public interface IPagedResult<T> : IPagedResultBase where T : class
    {
        IList<T> Results { get; set; }
    }
}