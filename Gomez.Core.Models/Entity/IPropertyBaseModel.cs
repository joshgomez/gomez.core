﻿namespace Gomez.Core.Models.Entity
{
    public interface IPropertyBaseModel
    {
        int TypeNum { get; }
        string Key { get; }
        object Value { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>")]
        byte[] RowVersion { get; set; }
    }
}