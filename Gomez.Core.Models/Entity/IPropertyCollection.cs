﻿using System;

namespace Gomez.Core.Models.Entity
{
    public interface IPropertyCollection : ISimplePropertyCollection
    {
        bool DeleteProperty(string key);

        void SetProperty<T>(string key, T value);

        void SetProperty(int typeNum, string key, object value);

        bool SetProperty(Type type, string key, object value);
    }
}