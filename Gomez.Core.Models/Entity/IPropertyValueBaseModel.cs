﻿using System;

namespace Gomez.Core.Models.Entity
{
    public interface IPropertyValueBaseModel
    {
        int? ValueInt32 { get; set; }
        long? ValueInt64 { get; set; }
        float? ValueFloat { get; set; }
        double? ValueDouble { get; set; }
        decimal? ValueDecimal { get; set; }
        string ValueString { get; set; }
        bool? ValueBool { get; set; }
        DateTime? ValueDateTime { get; set; }
    }
}