﻿using System;

namespace Gomez.Core.Models.Entity
{
    public interface ISimplePropertyCollection
    {
        bool PropertyExists(string key);

        T GetProperty<T>(string key);

        object GetProperty(Type type, string key);

        object GetProperty(int typeNum, string key);

        T GetProperty<T>(string key, out byte[] rowVersion);

        object GetProperty(Type type, string key, out byte[] rowVersion);

        object GetProperty(int typeNum, string key, out byte[] rowVersion);

        byte[] GetRowVersion(string key);
    }
}