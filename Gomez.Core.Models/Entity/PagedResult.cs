﻿using System.Collections.Generic;

namespace Gomez.Core.Models.Entity
{
    public class PagedResult<T> : PagedResultBase, IPagedResult<T> where T : class
    {
        public IList<T> Results { get; set; }

        public PagedResult()
        {
            Results = new List<T>();
        }
    }
}