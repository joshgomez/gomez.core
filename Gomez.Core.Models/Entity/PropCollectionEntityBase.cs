﻿using System;
using System.Collections.Generic;

namespace Gomez.Core.Models.Entity
{
    /// <summary>
    /// Can be used when it is possible to set a parent class on a database entity
    /// </summary>
    /// <typeparam name="TListItem"></typeparam>
    public class PropCollectionEntityBase<TListItem> : EntityBase, IPropertyCollection where TListItem : IPropertyBaseModel
    {
        public ICollection<TListItem> Properties { get; set; }

        public bool DeleteProperty(string key)
        {
            return PropertyBaseModel.DeleteProperty(Properties, key);
        }

        public byte[] GetRowVersion(string key)
        {
            return PropertyBaseModel.GetRowVersion(Properties, key);
        }

        public T GetProperty<T>(string key)
        {
            return GetProperty<T>(key, out _);
        }

        public T GetProperty<T>(string key, out byte[] rowVersion)
        {
            object value = GetProperty(typeof(T), key, out rowVersion);
            if (value == null)
            {
                return default;
            }

            return (T)value;
        }

        public object GetProperty(int typeNum, string key)
        {
            return GetProperty(typeNum, key, out _);
        }

        public object GetProperty(int typeNum, string key, out byte[] rowVersion)
        {
            var type = PropertyBaseModel.GetPropertyType(typeNum);
            return GetProperty(type, key, out rowVersion);
        }

        public object GetProperty(Type type, string key)
        {
            return GetProperty(type, key, out _);
        }

        public object GetProperty(Type type, string key, out byte[] rowVersion)
        {
            return PropertyBaseModel.GetProperty(Properties, type, key, out rowVersion);
        }

        public void SetProperty<T>(string key, T value)
        {
            SetProperty(typeof(T), key, value);
        }

        public void SetProperty(int typeNum, string key, object value)
        {
            var type = PropertyBaseModel.GetPropertyType(typeNum);
            SetProperty(type, key, value);
        }

        public virtual bool SetProperty(Type type, string key, object value)
        {
            Properties ??= new List<TListItem>();
            if (!PropertyBaseModel.UpdateProperty(Properties, type, key, value))
            {
                //Must add property in derived class
                return false;
            }

            return true;
        }

        public bool PropertyExists(string key)
        {
            return PropertyBaseModel.PropertyExists(Properties, key);
        }
    }
}