﻿using Gomez.Core.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;

namespace Gomez.Core.Models.Entity
{
    /// <summary>
    /// A database key value store, add relation to a parent entity to get a collection of properties.
    /// </summary>
    public abstract class PropertyBaseModel : EntityBase, IPropertyBaseModel, IPropertyValueBaseModel
    {
        public const int NULL = 0;
        public const int INT32 = 1;
        public const int INT64 = 2;
        public const int FLOAT = 3;
        public const int DOUBLE = 4;
        public const int DECIMAL = 5;
        public const int STRING = 6;
        public const int BOOL = 7;
        public const int DATETIME = 8;

        public static bool PropertyExists<TListItem>(ICollection<TListItem> properties, string key) where TListItem : IPropertyBaseModel
        {
            return properties.Any(x => x.Key == key);
        }

        public static bool PropertyExists<T, TListItem>(ICollection<TListItem> properties, string key) where TListItem : IPropertyBaseModel
        {
            var type = GetPropertyTypeNum(typeof(T));
            return properties.Any(x => x.Key == key && x.TypeNum == type);
        }

        public static bool DeleteProperty<TListItem>(ICollection<TListItem> properties, string key) where TListItem : IPropertyBaseModel
        {
            var property = properties.FirstOrDefault(x => x.Key == key);
            if (property == null)
            {
                return false;
            }

            properties.Remove(property);
            return true;
        }

        public static T GetProperty<T, TListItem>(ICollection<TListItem> properties, string key) where TListItem : IPropertyBaseModel
        {
            return (T)GetProperty(properties, typeof(T), key);
        }

        public static object GetProperty<TListItem>(ICollection<TListItem> properties, Type type, string key) where TListItem : IPropertyBaseModel
        {
            return GetProperty(properties, type, key, out _);
        }

        public static object GetProperty<TListItem>(ICollection<TListItem> properties, Type type, string key, out byte[] rowVersion) where TListItem : IPropertyBaseModel
        {
            var typeNum = PropertyBaseModel.GetPropertyTypeNum(type);
            var property = properties.FirstOrDefault(x => x.Key == key && x.TypeNum == typeNum);
            if (property == null)
            {
                rowVersion = Array.Empty<byte>();
                return TypeUtility.GetDefault(type);
            }

            rowVersion = property.RowVersion;
            return property.Value;
        }

        public static byte[] GetRowVersion<TListItem>(ICollection<TListItem> properties, string key) where TListItem : IPropertyBaseModel
        {
            var property = properties.FirstOrDefault(x => x.Key == key);
            if (property == null)
            {
                return Array.Empty<byte>();
            }

            return property.RowVersion;
        }

        public static bool UpdateProperty<T, TListItem>(ICollection<TListItem> properties, string key, T value) where TListItem : IPropertyBaseModel
        {
            return UpdateProperty(properties, typeof(T), key, value);
        }

        public static bool UpdateProperty<TListItem>(ICollection<TListItem> properties, Type type, string key, object value) where TListItem : IPropertyBaseModel
        {
            int typeNum = PropertyBaseModel.GetPropertyTypeNum(type);
            var property = properties.FirstOrDefault(x => x.Key == key);
            if (property == null)
            {
                return false;
            }

            if (property.TypeNum != typeNum)
            {
                properties.Remove(property);
                return false;
            }

            property.Value = value;
            return true;
        }

        protected PropertyBaseModel() : base()
        {
        }

        protected PropertyBaseModel(Type type, string key, object value = null) : this()
        {
            Key = key;
            TypeNum = GetPropertyTypeNum(type);
            SetValue(value);
        }

        protected PropertyBaseModel(string key, object value) : this(value.GetType(), key, value)
        {
        }

        [Required]
        public virtual string Key { get; set; }

        [Required]
        public int TypeNum { get; set; }

        public int? ValueInt32 { get; set; }
        public long? ValueInt64 { get; set; }
        public float? ValueFloat { get; set; }
        public double? ValueDouble { get; set; }
        public decimal? ValueDecimal { get; set; }
        public string ValueString { get; set; }
        public bool? ValueBool { get; set; }
        public DateTime? ValueDateTime { get; set; }

        protected void SetToNull()
        {
            this.SetValuesToNull();
        }

        public static int GetPropertyTypeNum(Type type)
        {
            var underlyingType = Nullable.GetUnderlyingType(type);
            if (underlyingType != null)
            {
                type = underlyingType;
            }

            return (System.Type.GetTypeCode(type)) switch
            {
                TypeCode.Int32 => PropertyBaseModel.INT32,
                TypeCode.Int64 => PropertyBaseModel.INT64,
                TypeCode.Single => PropertyBaseModel.FLOAT,
                TypeCode.Double => PropertyBaseModel.DOUBLE,
                TypeCode.Decimal => PropertyBaseModel.DECIMAL,
                TypeCode.Boolean => PropertyBaseModel.BOOL,
                TypeCode.DateTime => PropertyBaseModel.DATETIME,
                TypeCode.String => PropertyBaseModel.STRING,
                _ => PropertyBaseModel.NULL,
            };
        }

        public static Type GetPropertyType(int typeNum)
        {
            return typeNum switch
            {
                PropertyBaseModel.INT32 => typeof(int),
                PropertyBaseModel.INT64 => typeof(long),
                PropertyBaseModel.FLOAT => typeof(float),
                PropertyBaseModel.DOUBLE => typeof(double),
                PropertyBaseModel.DECIMAL => typeof(decimal),
                PropertyBaseModel.BOOL => typeof(bool),
                PropertyBaseModel.DATETIME => typeof(DateTime),
                PropertyBaseModel.STRING => typeof(string),
                _ => typeof(object),
            };
        }

        protected object ReturnValue()
        {
            return this.ReturnValue(TypeNum);
        }

        protected void SetValue(object data)
        {
            this.SetValue(TypeNum, data);
        }

        [Newtonsoft.Json.JsonIgnore]
        [System.Xml.Serialization.XmlIgnore]
        [IgnoreDataMember]
        [NotMapped]
        public object Value
        {
            get
            {
                return ReturnValue();
            }
            set
            {
                if (value == null)
                {
                    SetToNull();
                }

                SetValue(value);
            }
        }
    }
}