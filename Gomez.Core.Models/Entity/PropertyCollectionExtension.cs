﻿using System;
using System.Collections.Generic;

namespace Gomez.Core.Models.Entity
{
    /// <summary>
    /// Used if not possible to inherit PropCollectionEntityBase.cs
    /// </summary>
    public static class PropertyCollectionExtension
    {
        public static bool PropertyExists<T, TListItem>(this ICollection<TListItem> properties, string key) where TListItem : IPropertyBaseModel
        {
            return PropertyBaseModel.PropertyExists<T, TListItem>(properties, key);
        }

        public static bool DeleteProperty<TListItem>(this ICollection<TListItem> properties, string key) where TListItem : IPropertyBaseModel
        {
            return PropertyBaseModel.DeleteProperty(properties, key);
        }

        public static T GetProperty<T, TListItem>(this ICollection<TListItem> properties, string key) where TListItem : IPropertyBaseModel
        {
            return PropertyBaseModel.GetProperty<T, TListItem>(properties, key);
        }

        public static object GetProperty<TListItem>(this ICollection<TListItem> properties, Type type, string key) where TListItem : IPropertyBaseModel
        {
            return PropertyBaseModel.GetProperty(properties, type, key);
        }

        public static bool UpdateProperty<TListItem>(this ICollection<TListItem> properties, Type type, string key, object value) where TListItem : IPropertyBaseModel
        {
            return PropertyBaseModel.UpdateProperty(properties, type, key, value);
        }

        public static bool UpdateProperty<T, TListItem>(this ICollection<TListItem> properties, string key, T value) where TListItem : IPropertyBaseModel
        {
            return PropertyBaseModel.UpdateProperty(properties, typeof(T), key, value);
        }
    }
}