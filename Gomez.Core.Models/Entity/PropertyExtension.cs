﻿using System;

namespace Gomez.Core.Models.Entity
{
    public static class PropertyExtension
    {
        public static void SetValuesToNull(this IPropertyValueBaseModel model)
        {
            model.ValueInt32 = null;
            model.ValueInt64 = null;
            model.ValueFloat = null;
            model.ValueDouble = null;
            model.ValueDecimal = null;
            model.ValueString = null;
            model.ValueBool = null;
            model.ValueDateTime = null;
        }

        public static object ReturnValue(this IPropertyValueBaseModel model, int typeNum)
        {
            return typeNum switch
            {
                PropertyBaseModel.INT32 => model.ValueInt32,
                PropertyBaseModel.INT64 => model.ValueInt64,
                PropertyBaseModel.FLOAT => model.ValueFloat,
                PropertyBaseModel.DOUBLE => model.ValueDouble,
                PropertyBaseModel.DECIMAL => model.ValueDecimal,
                PropertyBaseModel.STRING => model.ValueString,
                PropertyBaseModel.BOOL => model.ValueBool,
                PropertyBaseModel.DATETIME => model.ValueDateTime,
                _ => null,
            };
        }

        public static void SetValue(this IPropertyValueBaseModel model, int typeNum, object data)
        {
            switch (typeNum)
            {
                case PropertyBaseModel.INT32:
                    model.ValueInt32 = data as int?;
                    break;

                case PropertyBaseModel.INT64:
                    model.ValueInt64 = data as long?;
                    break;

                case PropertyBaseModel.FLOAT:
                    model.ValueFloat = data as float?;
                    break;

                case PropertyBaseModel.DOUBLE:
                    model.ValueDouble = data as double?;
                    break;

                case PropertyBaseModel.DECIMAL:
                    model.ValueDecimal = data as decimal?;
                    break;

                case PropertyBaseModel.STRING:
                    model.ValueString = data as string;
                    break;

                case PropertyBaseModel.BOOL:
                    model.ValueBool = data as bool?;
                    break;

                case PropertyBaseModel.DATETIME:
                    model.ValueDateTime = data as DateTime?;
                    break;
            }
        }
    }
}