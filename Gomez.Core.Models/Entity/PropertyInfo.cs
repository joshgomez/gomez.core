﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Gomez.Core.Models.Entity
{
    public class PropertyInfo : PropertyBaseModel
    {
        public PropertyInfo() : base()
        {
        }

        public PropertyInfo(Type type, string key, object value = null) : this()
        {
            Key = key;
            TypeNum = GetPropertyTypeNum(type);
            SetValue(value);
        }

        [Required]
        public override string Key { get; set; }

        [Required]
        public string Context { get; set; }

        [MaxLength(512)]
        public string Label { get; set; }

        public string Description { get; set; }
    }
}