﻿using Gomez.Core.Models.Exceptions.Shared;
using System;
using System.Runtime.Serialization;

namespace Gomez.Core.Models.Exceptions
{
    [Serializable]
    public class BadRequestException : ExtendedException
    {
        public BadRequestException()
        {
        }

        public BadRequestException(string message) : base(message)
        {
        }

        public BadRequestException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public BadRequestException(string message, string codeMsg) : base(message, codeMsg)
        {
        }

        public BadRequestException(string message, string codeMsg, Exception innerException) : base(message, codeMsg, innerException)
        {
        }

        protected BadRequestException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}