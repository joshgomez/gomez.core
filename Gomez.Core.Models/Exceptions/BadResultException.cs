﻿using Gomez.Core.Models.Exceptions.Shared;
using System;
using System.Runtime.Serialization;

namespace Gomez.Core.Models.Exceptions
{
    [Serializable]
    public class BadResultException : ExtendedException
    {
        public BadResultException()
        {
        }

        public BadResultException(string message) : base(message)
        {
        }

        public BadResultException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public BadResultException(string message, string codeMsg) : base(message, codeMsg)
        {
        }

        public BadResultException(string message, string codeMsg, Exception innerException) : base(message, codeMsg, innerException)
        {
        }

        protected BadResultException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}