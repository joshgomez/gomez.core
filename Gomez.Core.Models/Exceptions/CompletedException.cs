﻿using Gomez.Core.Models.Exceptions.Shared;
using System;
using System.Runtime.Serialization;

namespace Gomez.Core.Models.Exceptions
{
    [Serializable]
    public class CompletedException : ExtendedException
    {
        public CompletedException()
        {
        }

        public CompletedException(string message) : base(message)
        {
        }

        public CompletedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public CompletedException(string message, string codeMsg) : base(message, codeMsg)
        {
        }

        public CompletedException(string message, string codeMsg, Exception innerException) : base(message, codeMsg, innerException)
        {
        }

        protected CompletedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}