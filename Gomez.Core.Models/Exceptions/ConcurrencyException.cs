﻿using Gomez.Core.Models.Exceptions.Shared;
using System;
using System.Runtime.Serialization;

namespace Gomez.Core.Models.Exceptions
{
    [Serializable]
    public class ConcurrencyException : ExtendedException
    {
        public ConcurrencyException()
        {
        }

        public ConcurrencyException(string message) : base(message)
        {
        }

        public ConcurrencyException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public ConcurrencyException(string message, string codeMsg) : base(message, codeMsg)
        {
        }

        public ConcurrencyException(string message, string codeMsg, Exception innerException) : base(message, codeMsg, innerException)
        {
        }

        protected ConcurrencyException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}