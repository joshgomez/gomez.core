﻿using Gomez.Core.Models.Exceptions.Shared;
using System;
using System.Runtime.Serialization;

namespace Gomez.Core.Models.Exceptions
{
    [Serializable]
    public class ExistsException : ExtendedException
    {
        public ExistsException()
        {
        }

        public ExistsException(string message) : base(message)
        {
        }

        public ExistsException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public ExistsException(string message, string codeMsg) : base(message, codeMsg)
        {
        }

        public ExistsException(string message, string codeMsg, Exception innerException) : base(message, codeMsg, innerException)
        {
        }

        protected ExistsException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}