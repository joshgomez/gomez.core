﻿using Gomez.Core.Models.Exceptions.Shared;
using System;
using System.Runtime.Serialization;

namespace Gomez.Core.Models.Exceptions
{
    [Serializable]
    public class InvalidException : ExtendedException
    {
        public InvalidException()
        {
        }

        public InvalidException(string message) : base(message)
        {
        }

        public InvalidException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public InvalidException(string message, string codeMsg) : base(message, codeMsg)
        {
        }

        public InvalidException(string message, string codeMsg, Exception innerException) : base(message, codeMsg, innerException)
        {
        }

        protected InvalidException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}