﻿using Gomez.Core.Models.Exceptions.Shared;
using System;
using System.Runtime.Serialization;

namespace Gomez.Core.Models.Exceptions
{
    [Serializable]
    public class NotEnoughException : ExtendedException
    {
        public NotEnoughException()
        {
        }

        public NotEnoughException(string message) : base(message)
        {
        }

        public NotEnoughException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public NotEnoughException(string message, string codeMsg) : base(message, codeMsg)
        {
        }

        public NotEnoughException(string message, string codeMsg, Exception innerException) : base(message, codeMsg, innerException)
        {
        }

        protected NotEnoughException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}