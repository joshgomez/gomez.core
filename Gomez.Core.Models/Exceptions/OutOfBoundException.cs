﻿using Gomez.Core.Models.Exceptions.Shared;
using System;
using System.Runtime.Serialization;

namespace Gomez.Core.Models.Exceptions
{
    [Serializable]
    public class OutOfBoundException : ExtendedException
    {
        public OutOfBoundException()
        {
        }

        public OutOfBoundException(string message) : base(message)
        {
        }

        public OutOfBoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public OutOfBoundException(string message, string codeMsg) : base(message, codeMsg)
        {
        }

        public OutOfBoundException(string message, string codeMsg, Exception innerException) : base(message, codeMsg, innerException)
        {
        }

        protected OutOfBoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}