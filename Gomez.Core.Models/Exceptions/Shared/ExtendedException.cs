﻿using System;
using System.Runtime.Serialization;

namespace Gomez.Core.Models.Exceptions.Shared
{
    [Serializable]
    public class ExtendedException : Exception
    {
        public string CodeMsg { get; private set; }
        public string FriendlyMessage { get; set; }

        protected ExtendedException()
        {
        }

        protected ExtendedException(string message) : base(message)
        {
        }

        protected ExtendedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public ExtendedException(string message, string codeMsg) : base(message)
        {
            CodeMsg = codeMsg;
        }

        public ExtendedException(string message, string codeMsg, Exception innerException) : base(message, innerException)
        {
            CodeMsg = codeMsg;
        }

        protected ExtendedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}