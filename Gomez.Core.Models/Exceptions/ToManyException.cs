﻿using Gomez.Core.Models.Exceptions.Shared;
using System;
using System.Runtime.Serialization;

namespace Gomez.Core.Models.Exceptions
{
    [Serializable]
    public class TooManyException : ExtendedException
    {
        public TooManyException()
        {
        }

        public TooManyException(string message) : base(message)
        {
        }

        public TooManyException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public TooManyException(string message, string codeMsg) : base(message, codeMsg)
        {
        }

        public TooManyException(string message, string codeMsg, Exception innerException) : base(message, codeMsg, innerException)
        {
        }

        protected TooManyException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}