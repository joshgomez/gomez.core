﻿using Gomez.Core.Models.Exceptions.Shared;
using System;
using System.Runtime.Serialization;

namespace Gomez.Core.Models.Exceptions
{
    [Serializable]
    public class UnavailableException : ExtendedException
    {
        public UnavailableException()
        {
        }

        public UnavailableException(string message) : base(message)
        {
        }

        public UnavailableException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public UnavailableException(string message, string codeMsg) : base(message, codeMsg)
        {
        }

        public UnavailableException(string message, string codeMsg, Exception innerException) : base(message, codeMsg, innerException)
        {
        }

        protected UnavailableException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}