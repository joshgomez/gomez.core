﻿using Gomez.Core.Models.Exceptions.Shared;
using System;
using System.Runtime.Serialization;

namespace Gomez.Core.Models.Exceptions
{
    [Serializable]
    public class UncompletedException : ExtendedException
    {
        public UncompletedException()
        {
        }

        public UncompletedException(string message) : base(message)
        {
        }

        public UncompletedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public UncompletedException(string message, string codeMsg) : base(message, codeMsg)
        {
        }

        public UncompletedException(string message, string codeMsg, Exception innerException) : base(message, codeMsg, innerException)
        {
        }

        protected UncompletedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}