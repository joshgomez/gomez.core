﻿using Gomez.Core.Models.Shared;
using NetTopologySuite.Geometries;

namespace Gomez.Core.Models.Helpers
{
    public static class NtsHelper
    {
        public static Point GetPoint(ICoordinate coord)
        {
            return new Point(coord.X, coord.Y);
        }

        public static double GetDistance(ICoordinate coordA, ICoordinate coordB)
        {
            return GetPoint(coordA).Distance(GetPoint(coordB));
        }
    }
}