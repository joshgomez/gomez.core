﻿namespace Gomez.Core.Models.Shared
{
    public interface ICoordinate
    {
        public double X { get; set; }
        public double Y { get; set; }
    }
}