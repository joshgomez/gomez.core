﻿namespace Gomez.Core.Models.Shared
{
    public interface ICoordinateModel
    {
        public ICoordinate Coordinate { get; set; }
    }
}