﻿namespace Gomez.Core.OpenXml
{
    public class AdvanceSyntax
    {
        public string Label { get; set; }
        public string ReplaceLabel { get { return "{{" + Label + "}}"; } }
        public string PrefixBefore { get; set; }
        public string PrefixAfter { get; set; }
        public string Original { get; set; }
    }
}