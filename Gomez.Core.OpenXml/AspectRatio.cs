﻿using System;

namespace Gomez.Core.OpenXml
{
    public class AspectRatio
    {
        public long Width
        {
            get { return _width; }
            set
            {
                _width = value;
                _height = Convert.ToInt64(_width / ratio);
            }
        }

        public long Height
        {
            get { return _height; }
            set
            {
                _height = value;
                _width = (long)(ratio * _height);
            }
        }

        private readonly double ratio;
        private long _width;
        private long _height;

        public AspectRatio(long width, long height)
        {
            _width = width;
            _height = height;
            ratio = (double)Width / (double)Height;
        }

        public double Get()
        {
            return ratio;
        }
    }
}