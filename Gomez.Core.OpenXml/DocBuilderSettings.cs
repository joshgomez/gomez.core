﻿using System.IO;

namespace Gomez.Core.OpenXml
{
    public class DocBuilderSettings
    {
        public Stream HeaderStream { get; set; }
        public Stream FooterStream { get; set; }
        public string TemplateFilePath { get; set; }

        public bool KeepTemplateHeader { get; set; }
        public bool KeepTemplateFooter { get; set; }

        public DocBuilderSettings()
        {
            KeepTemplateHeader = false;
            KeepTemplateFooter = false;
        }

        public WordHeaderSetting HeaderSettings { get; set; }
        public WordFooterSetting FooterSettings { get; set; }
    }
}