﻿namespace Gomez.Core.OpenXml
{
    public enum DocumentPropertyType
    {
        YesNo,
        Text,
        DateTime,
        NumberInteger,
        NumberDouble
    }
}