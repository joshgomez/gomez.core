﻿using System.IO;
using System.Threading.Tasks;

namespace Gomez.Core.OpenXml
{
    public class Fixer
    {
        protected Fixer()
        {
        }

        public static async Task ToTemplateTypeAsync(string fileName)
        {
            string ext = Path.GetExtension(fileName).ToLower();
            switch (ext)
            {
                case ".dotx":
                    await OpenXmlWordHelper.CopyToAsync(fileName, fileName, DocumentFormat.OpenXml.WordprocessingDocumentType.Template);
                    break;

                case ".potx":
                    await OpenXmlPowerPointHelper.CopyToAsync(fileName, fileName, DocumentFormat.OpenXml.PresentationDocumentType.Template);
                    break;

                case ".xltx":
                    await OpenXmlExcelHelper.CopyToAsync(fileName, fileName, DocumentFormat.OpenXml.SpreadsheetDocumentType.Template);
                    break;
            }
        }
    }
}