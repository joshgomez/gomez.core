﻿using System.Drawing;
using System.IO;

namespace Gomez.Core.OpenXml
{
    public static class ImageHelper
    {
        public static void GetEMUsForImage(string filename, out long width, out long height)
        {
            using (var fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                GetEMUsForImage(fs, out width, out height);
            }
        }

        public static void GetEMUsForImage(Stream stream, out long width, out long height)
        {
            using (var ms = new MemoryStream())
            {
                stream.Position = 0;
                stream.CopyTo(ms);
                ms.Position = 0;

                using (var img = new Bitmap(Image.FromStream(ms)))
                {
                    var widthPx = img.Width;
                    var heightPx = img.Height;
                    var horzRezDpi = img.HorizontalResolution;
                    var vertRezDpi = img.VerticalResolution;
                    const int emusPerInch = 914400;
                    var widthEmus = (long)(widthPx / horzRezDpi * emusPerInch);
                    var heightEmus = (long)(heightPx / vertRezDpi * emusPerInch);

                    width = widthEmus;
                    height = heightEmus;
                }
            }
        }
    }
}