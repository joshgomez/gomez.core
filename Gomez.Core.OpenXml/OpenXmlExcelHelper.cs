﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using X = DocumentFormat.OpenXml.Spreadsheet;

namespace Gomez.Core.OpenXml
{
    public class OpenXmlExcelHelper : IDisposable
    {
        public static void CopyTo(string templatePath, string docxPath, SpreadsheetDocumentType type = SpreadsheetDocumentType.Workbook)
        {
            MemoryStream documentStream;
            using (var fileStream = new FileStream(templatePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                documentStream = new MemoryStream((int)fileStream.Length);
                fileStream.CopyTo(documentStream);
                documentStream.Position = 0L;
            }

            using (SpreadsheetDocument template = SpreadsheetDocument.Open(documentStream, true))
            {
                template.ChangeDocumentType(type);
                var mainPart = template.WorkbookPart;
                mainPart.Workbook.Save();
            }

            using (var fileStream = new FileStream(docxPath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite))
            {
                documentStream.WriteTo(fileStream);
            }

            documentStream.Dispose();
        }

        public Dictionary<string, string> GetProperties()
        {
            return OpenXmlGenericHelper.GetProperties(SpreadsheetDoc.PackageProperties, SpreadsheetDoc.ExtendedFilePropertiesPart?.Properties);
        }

        public void SetProperties(Dictionary<string, string> values)
        {
            OpenXmlGenericHelper.SetProperties(SpreadsheetDoc.PackageProperties, SpreadsheetDoc.ExtendedFilePropertiesPart?.Properties, values);
        }

        public static async Task CopyToAsync(string templatePath, string docxPath, SpreadsheetDocumentType type = SpreadsheetDocumentType.Workbook)
        {
            using (var fileStream = new FileStream(templatePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                await CopyToAsync(fileStream, docxPath, type);
            }
        }

        public static async Task CopyToAsync(System.IO.Stream stream, string docxPath, SpreadsheetDocumentType type = SpreadsheetDocumentType.Workbook)
        {
            using var documentStream = await CopyToStreamAsync(stream, type);
            using var fileStream = new FileStream(docxPath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
            documentStream.WriteTo(fileStream);
        }

        private static async Task<MemoryStream> CopyToStreamAsync(System.IO.Stream stream, SpreadsheetDocumentType type = SpreadsheetDocumentType.Workbook)
        {
            var documentStream = new MemoryStream();
            await stream.CopyToAsync(documentStream);
            using (var helper = new OpenXmlExcelHelper(documentStream))
            {
                helper.SpreadsheetDoc.ChangeDocumentType(type);
                helper.SpreadsheetDoc.WorkbookPart.Workbook.Save();
            }

            return documentStream;
        }

        public SpreadsheetDocument SpreadsheetDoc { get; private set; }

        /// <summary>
        /// Replace all text from a dictionary, the key is the search term and the value the replacement.
        /// </summary>
        /// <param name="replacements"></param>
        public void SearchAndReplace(Dictionary<string, string> replacements)
        {
            if (SpreadsheetDoc == null) return;

            IEnumerable<X.Text> texts;

            // Replace shared strings
            SharedStringTablePart sharedStringsPart = SpreadsheetDoc.WorkbookPart.SharedStringTablePart;
            if (sharedStringsPart != null)
            {
                texts = sharedStringsPart.SharedStringTable.Descendants<X.Text>();
                DoReplace(texts, replacements);
            }

            // Replace inline strings
            IEnumerable<WorksheetPart> worksheetParts = SpreadsheetDoc.GetPartsOfType<WorksheetPart>();
            foreach (var worksheet in worksheetParts)
            {
                texts = worksheet.Worksheet.Descendants<X.Text>();
                DoReplace(texts, replacements);
            }
        }

        /// <summary>
        /// Given a document name, a property name/value, and the property type,
        /// add a custom property to a document. The method returns the original
        /// value, if it existed.
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="propertyValue"></param>
        /// <param name="propertyType"></param>
        /// <returns></returns>
        public string SetCustomProperty(string propertyName, object propertyValue, DocumentPropertyType propertyType)
        {
            var newProp = OpenXmlGenericHelper.SetCustomProperty(propertyValue, propertyType);

            // Now that you have handled the parameters, start
            // working on the document.
            newProp.FormatId = "{D5CDD505-2E9C-101B-9397-08002B2CF9AE}";
            newProp.Name = propertyName;

            var customProps = SpreadsheetDoc.CustomFilePropertiesPart;
            if (customProps == null)
            {
                // No custom properties? Add the part, and the
                // collection of properties now.
                customProps = SpreadsheetDoc.AddCustomFilePropertiesPart();
                customProps.Properties =
                    new DocumentFormat.OpenXml.CustomProperties.Properties();
            }

            return OpenXmlGenericHelper.SetCustomProperty(customProps, newProp, propertyName);
        }

        public void Save()
        {
            if (SpreadsheetDoc == null) return;
            SpreadsheetDoc.Save();
        }

        private void DoReplace(IEnumerable<X.Text> texts, Dictionary<string, string> replacements)
        {
            foreach (var text in texts.ToList())
            {
                foreach (var replacement in replacements)
                {
                    if (text.Text.Contains(replacement.Key))
                        text.Text = text.Text.Replace(replacement.Key, replacement.Value);
                }
            }
        }

        public OpenXmlExcelHelper(string filePath)
        {
            SpreadsheetDoc = SpreadsheetDocument.Open(filePath, true);
        }

        public OpenXmlExcelHelper(string filePath, bool isEditable)
        {
            SpreadsheetDoc = SpreadsheetDocument.Open(filePath, isEditable);
        }

        public OpenXmlExcelHelper(Stream stream)
        {
            SpreadsheetDoc = SpreadsheetDocument.Open(stream, true);
        }

        public OpenXmlExcelHelper(Stream stream, bool isEditable)
        {
            SpreadsheetDoc = SpreadsheetDocument.Open(stream, isEditable);
        }

        public IEnumerable<string> FindAllSyntaxes(string syntax = "{{((?:}(?!})|[^}])*)}}")
        {
            var innerText = new StringBuilder();
            if (SpreadsheetDoc.WorkbookPart.SharedStringTablePart != null)
            {
                innerText.Append(SpreadsheetDoc.WorkbookPart.SharedStringTablePart.SharedStringTable.InnerText);
            }

            foreach (WorksheetPart part in SpreadsheetDoc.GetPartsOfType<WorksheetPart>())
            {
                innerText.Append(part.Worksheet.InnerText);
            }

            MatchCollection matches = Regex.Matches(innerText.ToString(), syntax);
            foreach (Match match in matches)
            {
                yield return match.Value;
            }
        }

        #region IDisposable Support

        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                SpreadsheetDoc.Close();
                if (disposing)
                {
                    SpreadsheetDoc.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion IDisposable Support
    }
}