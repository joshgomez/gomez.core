﻿using DocumentFormat.OpenXml.CustomProperties;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.VariantTypes;
using Gomez.Core.Utilities.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Gomez.Core.OpenXml
{
    public static class OpenXmlGenericHelper
    {
        public static string GenerateUniqueRId()
        {
            return "R" + Guid.NewGuid().ToString().Replace("-", "");
        }

        public static string SetIfLogoMax(string altText, AspectRatio oldRatio, AspectRatio newRatio)
        {
            if (altText.EndsWith("_max"))
            {
                if (oldRatio.Get() > newRatio.Get()) altText = altText.Replace("_max", "_maxheight");
                else altText = altText.Replace("_max", "_maxwidth");
            }

            return altText;
        }

        public static Dictionary<string, string> GetProperties(System.IO.Packaging.PackageProperties props, DocumentFormat.OpenXml.ExtendedProperties.Properties extProps)
        {
            var ret = new Dictionary<string, string>();
            if (props != null)
            {
                System.Reflection.PropertyInfo[] properties = props.GetType().GetProperties();
                foreach (var p in properties.Where(x => x.PropertyType == typeof(string)))
                {
                    ret[p.Name] = p.GetValue(props)?.ToString();
                }
            }

            if (extProps != null)
            {
                System.Reflection.PropertyInfo[] properties = extProps.GetType().GetProperties();
                foreach (var p in properties.Where(x => x.PropertyType == typeof(string)))
                {
                    ret[p.Name] = p.GetValue(extProps)?.ToString();
                }
            }

            return ret;
        }

        public static void SetProperties(System.IO.Packaging.PackageProperties props, DocumentFormat.OpenXml.ExtendedProperties.Properties extProps, Dictionary<string, string> values)
        {
            if (props != null)
            {
                System.Reflection.PropertyInfo[] properties = props.GetType().GetProperties();
                foreach (var p in properties.Where(x => x.PropertyType == typeof(string) && values.ContainsKey(x.Name)))
                {
                    p.SetValue(props, values[p.Name]);
                }
            }

            if (extProps != null)
            {
                System.Reflection.PropertyInfo[] properties = extProps.GetType().GetProperties();
                foreach (var p in properties.Where(x => x.PropertyType == typeof(string) && values.ContainsKey(x.Name)))
                {
                    p.SetValue(extProps, values[p.Name]);
                }
            }
        }

        public static IEnumerable<string> GetAllAdvanceSyntaxReplaceLabels(IEnumerable<AdvanceSyntax> syntaxes)
        {
            foreach (var syntax in syntaxes)
            {
                yield return syntax.ReplaceLabel;
            }
        }

        public static IEnumerable<AdvanceSyntax> GetAllAdvanceSyntaxes(IEnumerable<string> syntaxes)
        {
            foreach (var syntax in syntaxes.Where(x => x.Contains(";")))
            {
                var syntaxParts = syntax.Split(';');
                var label = syntaxParts[0].Replace("{{", "").Trim();
                var before = syntaxParts.ValueOrNull(1);
                var after = syntaxParts.ValueOrNull(2);

                if (after != null)
                {
                    after = after.Replace("}}", "").Replace(@"\n", Environment.NewLine);
                }
                else if (before != null)
                {
                    before = before.Replace("}}", "").Replace(@"\n", Environment.NewLine);
                }

                yield return new AdvanceSyntax() { Label = label, PrefixBefore = before, PrefixAfter = after, Original = syntax };
            }
        }

        public static Dictionary<string, string> GetAdvanceReplacement(string[] syntaxesToReplace, Dictionary<string, string> replacements)
        {
            var dict = new Dictionary<string, string>();
            var advanceSyntaxes = OpenXmlGenericHelper.GetAllAdvanceSyntaxes(syntaxesToReplace);
            foreach (var syntax in advanceSyntaxes)
            {
                var value = replacements.ContainsKey(syntax.ReplaceLabel) ? replacements[syntax.ReplaceLabel] : null;
                if (!string.IsNullOrEmpty(value))
                {
                    value = $"{syntax.PrefixBefore ?? ""}{value}{syntax.PrefixAfter ?? ""}";
                    dict.Add(syntax.Original, value);
                }
            }

            return dict;
        }

        public static string SetCustomProperty(CustomFilePropertiesPart customProps, CustomDocumentProperty newProp, string propertyName)
        {
            string returnValue = null;
            var props = customProps.Properties;
            if (props != null)
            {
                // This will trigger an exception if the property's Name
                // property is null, but if that happens, the property is damaged,
                // and probably should raise an exception.
                var prop =
                    props.FirstOrDefault(p => ((CustomDocumentProperty)p).Name.Value
                       == propertyName);

                // Does the property exist? If so, get the return value,
                // and then delete the property.
                if (prop != null)
                {
                    returnValue = prop.InnerText;
                    prop.Remove();
                }

                // Append the new property, and
                // fix up all the property ID values.
                // The PropertyId value must start at 2.
                props.AppendChild(newProp);
                int pid = 2;
                foreach (var item in props.OfType<CustomDocumentProperty>())
                {
                    item.PropertyId = pid++;
                }
                props.Save();
            }

            return returnValue;
        }

        public static CustomDocumentProperty SetCustomProperty(object propertyValue, DocumentPropertyType propertyType)
        {
            var newProp = new CustomDocumentProperty();
            bool propSet = false;

            // Calculate the correct type.
            switch (propertyType)
            {
                case DocumentPropertyType.DateTime:

                    // Be sure you were passed a real date,
                    // and if so, format in the correct way.
                    // The date/time value passed in should
                    // represent a UTC date/time.
                    if ((propertyValue) is DateTime)
                    {
                        newProp.VTFileTime =
                            new VTFileTime(string.Format("{0:s}Z",
                                Convert.ToDateTime(propertyValue)));
                        propSet = true;
                    }

                    break;

                case DocumentPropertyType.NumberInteger:
                    if ((propertyValue) is int)
                    {
                        newProp.VTInt32 = new VTInt32(propertyValue.ToString());
                        propSet = true;
                    }

                    break;

                case DocumentPropertyType.NumberDouble:
                    if (propertyValue is double)
                    {
                        newProp.VTFloat = new VTFloat(propertyValue.ToString());
                        propSet = true;
                    }

                    break;

                case DocumentPropertyType.Text:
                    newProp.VTLPWSTR = new VTLPWSTR(propertyValue.ToString());
                    propSet = true;

                    break;

                case DocumentPropertyType.YesNo:
                    if (propertyValue is bool)
                    {
                        // Must be lowercase.
                        newProp.VTBool = new VTBool(
                          Convert.ToBoolean(propertyValue).ToString().ToLower());
                        propSet = true;
                    }
                    break;
            }

            if (!propSet)
            {
                // If the code was not able to convert the
                // property to a valid value, throw an exception.
                throw new InvalidDataException("propertyValue");
            }

            return newProp;
        }
    }
}