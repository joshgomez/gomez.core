﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Presentation;
using Gomez.Core.Utilities.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using D = DocumentFormat.OpenXml.Drawing;

namespace Gomez.Core.OpenXml
{
    public class PowerPointPicModel
    {
        private D.BlipFill _blipFill;

        public D.BlipFill BlipFill
        {
            get
            {
                if (_blipFill == null && ShapeProperties != null)
                {
                    _blipFill = ShapeProperties.Descendants<D.BlipFill>().FirstOrDefault();
                }

                return _blipFill;
            }
            set
            {
                _blipFill = value;
            }
        }

        public bool Valid
        {
            get
            {
                return BlipFillPresentation != null || BlipFill != null;
            }
        }

        public Shape Shape { get; set; }
        public Picture Picture { get; set; }
        public BlipFill BlipFillPresentation { get; set; }
        public ShapeProperties ShapeProperties { get; set; }
        public NonVisualShapeProperties NonVisualShapeProperties { get; set; }

        public void ScalePicture(string imagepath, string altText)
        {
            ImageHelper.GetEMUsForImage(imagepath, out long w, out long h);
            ScalePicture(w, h, altText);
        }

        public void ScalePicture(Stream stream, string altText)
        {
            ImageHelper.GetEMUsForImage(stream, out long w, out long h);
            ScalePicture(w, h, altText);
        }

        public void ScalePicture(long w, long h, string altText)
        {
            AspectRatio oldRatio = new AspectRatio(ShapeProperties.Transform2D.Extents.Cx, ShapeProperties.Transform2D.Extents.Cy);
            AspectRatio newRatio = new AspectRatio(w, h);

            altText = OpenXmlGenericHelper.SetIfLogoMax(altText, oldRatio, newRatio);

            if (altText.EndsWith("_maxheight"))
            {
                newRatio.Height = ShapeProperties.Transform2D.Extents.Cy;
                ShapeProperties.Transform2D.Extents.Cx = newRatio.Width;
                return;
            }

            if (altText.EndsWith("_maxwidth"))
            {
                newRatio.Width = ShapeProperties.Transform2D.Extents.Cx;
                ShapeProperties.Transform2D.Extents.Cy = newRatio.Height;
                return;
            }

            ShapeProperties.Transform2D.Extents.Cx = w;
            ShapeProperties.Transform2D.Extents.Cy = h;
        }

        public void SetBlipId(string id)
        {
            if (BlipFill != null)
                BlipFill.Blip.Embed = id;
            else if (BlipFillPresentation != null)
            {
                BlipFillPresentation.Blip.Embed = id;
            }
        }

        public void RemoveImage()
        {
            if (Picture != null)
            {
                Picture.Remove();
            }
            else
            {
                Shape.Remove();
            }
        }
    }

    public class OpenXmlPowerPointHelper : IDisposable
    {
        public static void CopyTo(string templatePath, string docxPath, PresentationDocumentType type = PresentationDocumentType.Presentation)
        {
            MemoryStream documentStream;
            using (var fileStream = new FileStream(templatePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                documentStream = new MemoryStream((int)fileStream.Length);
                fileStream.CopyTo(documentStream);
                documentStream.Position = 0L;
            }

            using (PresentationDocument template = PresentationDocument.Open(documentStream, true))
            {
                template.ChangeDocumentType(type);
                var mainPart = template.PresentationPart;
                mainPart.Presentation.Save();
            }

            using (var fileStream = new FileStream(docxPath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite))
            {
                documentStream.WriteTo(fileStream);
            }

            documentStream.Dispose();
        }

        public static async Task CopyToAsync(string templatePath, string docxPath, PresentationDocumentType type = PresentationDocumentType.Presentation)
        {
            using (var fileStream = new FileStream(templatePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                await CopyToAsync(fileStream, docxPath, type);
            }
        }

        public static async Task CopyToAsync(System.IO.Stream stream, string docxPath, PresentationDocumentType type = PresentationDocumentType.Presentation)
        {
            using (var documentStream = await CopyToStreamAsync(stream, type))
            {
                using (var fileStream = new FileStream(docxPath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite))
                {
                    documentStream.WriteTo(fileStream);
                }
            }
        }

        private static async Task<MemoryStream> CopyToStreamAsync(System.IO.Stream stream, PresentationDocumentType type = PresentationDocumentType.Presentation)
        {
            var documentStream = new MemoryStream();
            await stream.CopyToAsync(documentStream);
            using (var helper = new OpenXmlPowerPointHelper(documentStream))
            {
                helper.PresentationDoc.ChangeDocumentType(type);
                helper.PresentationDoc.PresentationPart.Presentation.Save();
            }

            return documentStream;
        }

        public PresentationDocument PresentationDoc { get; private set; }

        public OpenXmlPowerPointHelper(string filePath)
        {
            PresentationDoc = PresentationDocument.Open(filePath, true);
        }

        public OpenXmlPowerPointHelper(string filePath, bool isEditable)
        {
            PresentationDoc = PresentationDocument.Open(filePath, isEditable);
        }

        public OpenXmlPowerPointHelper(Stream stream)
        {
            PresentationDoc = PresentationDocument.Open(stream, true);
        }

        public OpenXmlPowerPointHelper(Stream stream, bool isEditable)
        {
            PresentationDoc = PresentationDocument.Open(stream, isEditable);
        }

        public Dictionary<string, string> GetProperties()
        {
            return OpenXmlGenericHelper.GetProperties(PresentationDoc.PackageProperties, PresentationDoc.ExtendedFilePropertiesPart?.Properties);
        }

        public void SetProperties(Dictionary<string, string> values)
        {
            OpenXmlGenericHelper.SetProperties(PresentationDoc.PackageProperties, PresentationDoc.ExtendedFilePropertiesPart?.Properties, values);
        }

        public void Save()
        {
            if (PresentationDoc == null) return;

            PresentationDoc.Save();
        }

        public IEnumerable<string> FindAllSyntaxes(string syntax = "{{((?:}(?!})|[^}])*)}}")
        {
            var innerText = new StringBuilder(PresentationDoc.PresentationPart.Presentation.InnerText);

            foreach (var part in PresentationDoc.PresentationPart.SlideMasterParts)
            {
                innerText.Append(part.SlideMaster.InnerText);
            }

            foreach (var part in PresentationDoc.PresentationPart.SlideParts)
            {
                innerText.Append(part.Slide.InnerText);
            }

            MatchCollection matches = Regex.Matches(innerText.ToString(), syntax);
            foreach (Match match in matches)
            {
                yield return match.Value;
            }
        }

        public void SwitchImage(string altText, string imagepath)
        {
            SwitchImage(PresentationDoc.PresentationPart, altText, imagepath);
        }

        public void SwitchImage(string altText, Stream stream)
        {
            SwitchImage(PresentationDoc.PresentationPart, altText, stream);
        }

        public void SwitchImage(PresentationPart presentationPart, string altText, Stream stream)
        {
            foreach (var masterPart in presentationPart.SlideMasterParts)
            {
                SlideMaster master = masterPart.SlideMaster;

                var pictures = GetAllPictures(master.CommonSlideData, altText);
                pictures = pictures.Union(GetAllBlips(master.CommonSlideData, altText));
                SetPicture(pictures, masterPart, altText, stream);

                foreach (var layoutPart in masterPart.SlideLayoutParts)
                {
                    SlideLayout layout = layoutPart.SlideLayout;
                    pictures = GetAllPictures(layout.CommonSlideData, altText);
                    pictures = pictures.Union(GetAllBlips(layout.CommonSlideData, altText));
                    SetPicture(pictures, layoutPart, altText, stream);
                }

                if (masterPart.SlideParts != null)
                {
                    foreach (var slidePart in masterPart.SlideParts)
                    {
                        var slide = slidePart.Slide;
                        if (slide != null)
                        {
                            pictures = GetAllPictures(slide.CommonSlideData, altText);
                            pictures = pictures.Union(GetAllBlips(slide.CommonSlideData, altText));
                            SetPicture(pictures, slidePart, altText, stream);
                        }
                    }
                }
            }
        }

        public void SwitchImage(PresentationPart presentationPart, string altText, string filepath)
        {
            foreach (var masterPart in presentationPart.SlideMasterParts)
            {
                SlideMaster master = masterPart.SlideMaster;

                var pictures = GetAllPictures(master.CommonSlideData, altText);
                pictures = pictures.Union(GetAllBlips(master.CommonSlideData, altText));
                SetPicture(pictures, masterPart, altText, filepath);

                foreach (var layoutPart in masterPart.SlideLayoutParts)
                {
                    SlideLayout layout = layoutPart.SlideLayout;
                    pictures = GetAllPictures(layout.CommonSlideData, altText);
                    pictures = pictures.Union(GetAllBlips(layout.CommonSlideData, altText));
                    SetPicture(pictures, layoutPart, altText, filepath);
                }

                if (masterPart.SlideParts != null)
                {
                    foreach (var slidePart in masterPart.SlideParts)
                    {
                        var slide = slidePart.Slide;
                        if (slide != null)
                        {
                            pictures = GetAllPictures(slide.CommonSlideData, altText);
                            pictures = pictures.Union(GetAllBlips(slide.CommonSlideData, altText));
                            SetPicture(pictures, slidePart, altText, filepath);
                        }
                    }
                }
            }
        }

        public void RemoveImage(string altText)
        {
            RemoveImage(PresentationDoc.PresentationPart, altText);
        }

        public void RemoveImage(PresentationPart presentationPart, string altText)
        {
            foreach (var masterPart in presentationPart.SlideMasterParts)
            {
                SlideMaster master = masterPart.SlideMaster;

                var pictures = GetAllPictures(master.CommonSlideData, altText);
                pictures = pictures.Union(GetAllBlips(master.CommonSlideData, altText));
                RemovePicture(pictures);

                foreach (var layoutPart in masterPart.SlideLayoutParts)
                {
                    SlideLayout layout = layoutPart.SlideLayout;
                    pictures = GetAllPictures(layout.CommonSlideData, altText);
                    pictures = pictures.Union(GetAllBlips(layout.CommonSlideData, altText));
                    RemovePicture(pictures);
                }
            }
        }

        private void RemovePicture(IEnumerable<PowerPointPicModel> models)
        {
            foreach (var model in models)
            {
                model.RemoveImage();
            }
        }

        private void SetPicture(IEnumerable<PowerPointPicModel> models, OpenXmlPart part, string altText, string filepath)
        {
            using (var fs = new FileStream(filepath, FileMode.Open))
            {
                SetPicture(models, part, altText, fs);
            }
        }

        private void SetPicture(IEnumerable<PowerPointPicModel> models, OpenXmlPart part, string altText, Stream stream)
        {
            dynamic dynPart = part;
            if (models.Any())
            {
                var layoutImagePart = dynPart.AddImagePart(ImagePartType.Png);
                using (var ms = new MemoryStream())
                {
                    stream.Position = 0;
                    stream.CopyTo(ms);
                    ms.Position = 0;
                    layoutImagePart.FeedData(ms);
                }

                foreach (PowerPointPicModel model in models)
                {
                    model.ScalePicture(stream, altText);
                    model.SetBlipId(part.GetIdOfPart(layoutImagePart));
                }
            }
        }

        private IEnumerable<PowerPointPicModel> GetAllBlips(CommonSlideData data, string altText)
        {
            return data.Descendants<Shape>()
                .Where
                (
                    x =>
                    x.NonVisualShapeProperties?.NonVisualDrawingProperties != null &&
                    (
                        (
                            x.NonVisualShapeProperties.NonVisualDrawingProperties.Title != null &&
                            x.NonVisualShapeProperties.NonVisualDrawingProperties.Title.Value.ExactMatch(altText)
                        ) ||
                        (
                            x.NonVisualShapeProperties.NonVisualDrawingProperties.Description != null &&
                            x.NonVisualShapeProperties.NonVisualDrawingProperties.Description.Value.ExactMatch(altText)
                        )
                    )
                )
                .Select
                (
                        x => new PowerPointPicModel
                        {
                            ShapeProperties = x.Descendants<ShapeProperties>().FirstOrDefault(),
                            NonVisualShapeProperties = x.NonVisualShapeProperties,
                            Shape = x
                        }
                );
        }

        private IEnumerable<PowerPointPicModel> GetAllPictures(CommonSlideData data, string altText)
        {
            return data.Descendants<Picture>().Where
            (
                    x =>
                    x.NonVisualPictureProperties?.NonVisualDrawingProperties != null &&
                    (
                        (
                            x.NonVisualPictureProperties.NonVisualDrawingProperties.Title != null &&
                            x.NonVisualPictureProperties.NonVisualDrawingProperties.Title.Value.ExactMatch(altText)
                        ) ||
                        (
                            x.NonVisualPictureProperties.NonVisualDrawingProperties.Description != null &&
                            x.NonVisualPictureProperties.NonVisualDrawingProperties.Description.Value.ExactMatch(altText)
                        )
                    )
            )
            .Select
            (
                x => new PowerPointPicModel()
                {
                    ShapeProperties = x.ShapeProperties,
                    BlipFillPresentation = x.BlipFill,
                    Picture = x
                }
            );
        }

        /// <summary>
        /// Given a document name, a property name/value, and the property type,
        /// add a custom property to a document. The method returns the original
        /// value, if it existed.
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="propertyValue"></param>
        /// <param name="propertyType"></param>
        /// <returns></returns>
        public string SetCustomProperty(string propertyName, object propertyValue, DocumentPropertyType propertyType)
        {
            var newProp = OpenXmlGenericHelper.SetCustomProperty(propertyValue, propertyType);

            // Now that you have handled the parameters, start
            // working on the document.
            newProp.FormatId = "{D5CDD505-2E9C-101B-9397-08002B2CF9AE}";
            newProp.Name = propertyName;

            var customProps = PresentationDoc.CustomFilePropertiesPart;
            if (customProps == null)
            {
                // No custom properties? Add the part, and the
                // collection of properties now.
                customProps = PresentationDoc.AddCustomFilePropertiesPart();
                customProps.Properties =
                    new DocumentFormat.OpenXml.CustomProperties.Properties();
            }

            return OpenXmlGenericHelper.SetCustomProperty(customProps, newProp, propertyName);
        }

        #region IDisposable Support

        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    PresentationDoc.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion IDisposable Support
    }
}