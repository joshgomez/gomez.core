﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Validation;
using DocumentFormat.OpenXml.Wordprocessing;
using Gomez.Core.Utilities;
using Gomez.Core.Utilities.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using A = DocumentFormat.OpenXml.Drawing;
using DW = DocumentFormat.OpenXml.Drawing.Wordprocessing;
using PIC = DocumentFormat.OpenXml.Drawing.Pictures;

namespace Gomez.Core.OpenXml
{
    public class OpenXmlWordHelper : IDisposable
    {
        /// <summary>
        /// to be used later
        /// </summary>
        /// <returns></returns>
        public bool IsValid()
        {
            var validator = new OpenXmlValidator();
            int count = 0;
            foreach (ValidationErrorInfo error in validator.Validate(WordDoc))
            {
                count++;
            }

            if (count > 0)
            {
                return false;
            }

            return true;
        }

        public static async Task CopyToAsync(string templatePath, string docxPath, WordprocessingDocumentType type = WordprocessingDocumentType.Document)
        {
            using (var fileStream = new FileStream(templatePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                await CopyToAsync(fileStream, docxPath, type);
            }
        }

        public static async Task CopyToAsync(System.IO.Stream stream, string docxPath, WordprocessingDocumentType type = WordprocessingDocumentType.Document)
        {
            using (var documentStream = await CopyToStreamAsync(stream, type))
            {
                using (var fileStream = new FileStream(docxPath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite))
                {
                    documentStream.WriteTo(fileStream);
                }
            }
        }

        private static async Task<MemoryStream> CopyToStreamAsync(System.IO.Stream stream, WordprocessingDocumentType type = WordprocessingDocumentType.Document)
        {
            var documentStream = new MemoryStream();
            await stream.CopyToAsync(documentStream);

            using (var helper = new OpenXmlWordHelper(documentStream))
            {
                helper.WordDoc.ChangeDocumentType(type);
                helper.WordDoc.MainDocumentPart.Document.Save();
            }

            return documentStream;
        }

        public Dictionary<string, string> GetProperties()
        {
            return OpenXmlGenericHelper.GetProperties(WordDoc.PackageProperties, WordDoc.ExtendedFilePropertiesPart?.Properties);
        }

        public void SetProperties(Dictionary<string, string> values)
        {
            OpenXmlGenericHelper.SetProperties(WordDoc.PackageProperties, WordDoc.ExtendedFilePropertiesPart?.Properties, values);
        }

        public WordprocessingDocument WordDoc { get; private set; }

        public OpenXmlWordHelper(string filePath)
        {
            WordDoc = WordprocessingDocument.Open(filePath, true);
        }

        public OpenXmlWordHelper(string filePath, bool isEditable)
        {
            WordDoc = WordprocessingDocument.Open(filePath, isEditable);
        }

        public OpenXmlWordHelper(Stream stream)
        {
            WordDoc = WordprocessingDocument.Open(stream, true);
        }

        public OpenXmlWordHelper(Stream stream, bool isEditable)
        {
            WordDoc = WordprocessingDocument.Open(stream, isEditable);
        }

        /// <summary>
        /// Given a document name, a property name/value, and the property type,
        /// add a custom property to a document. The method returns the original
        /// value, if it existed.
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="propertyValue"></param>
        /// <param name="propertyType"></param>
        /// <returns></returns>
        public string SetCustomProperty(string propertyName, object propertyValue, DocumentPropertyType propertyType)
        {
            var newProp = OpenXmlGenericHelper.SetCustomProperty(propertyValue, propertyType);

            // Now that you have handled the parameters, start
            // working on the document.
            newProp.FormatId = "{D5CDD505-2E9C-101B-9397-08002B2CF9AE}";
            newProp.Name = propertyName;

            var customProps = WordDoc.CustomFilePropertiesPart;
            if (customProps == null)
            {
                // No custom properties? Add the part, and the
                // collection of properties now.
                customProps = WordDoc.AddCustomFilePropertiesPart();
                customProps.Properties =
                    new DocumentFormat.OpenXml.CustomProperties.Properties();
            }

            return OpenXmlGenericHelper.SetCustomProperty(customProps, newProp, propertyName);
        }

        public PageMargin GetPageMargin()
        {
            var pm = WordDoc.MainDocumentPart.Document.Descendants<PageMargin>().FirstOrDefault();
            if (pm != null)
            {
                pm = pm.Clone() as PageMargin;
            }

            return pm;
        }

        public void SetPageMargin(PageMargin pageMargin)
        {
            if (pageMargin == null) return;

            var secProps = WordDoc.MainDocumentPart.Document.Descendants<SectionProperties>();
            if (secProps != null)
            {
                foreach (var secProp in secProps.ToList())
                {
                    var pm = secProp.Descendants<PageMargin>().FirstOrDefault();
                    if (pm != null)
                    {
                        //If repeated must be cloned again.
                        var clonedMargin = pageMargin.Clone() as PageMargin;
                        secProp.ReplaceChild<PageMargin>(clonedMargin, pm);
                    }
                }
            }
        }

        public IEnumerable<string> FindAllSyntaxes(string syntax = "{{((?:}(?!})|[^}])*)}}")
        {
            var innerText = new StringBuilder(WordDoc.MainDocumentPart.Document.InnerText);

            foreach (var part in WordDoc.MainDocumentPart.HeaderParts)
            {
                innerText.Append(part.Header.InnerText);
            }

            foreach (var part in WordDoc.MainDocumentPart.FooterParts)
            {
                innerText.Append(part.Footer.InnerText);
            }

            MatchCollection matches = Regex.Matches(innerText.ToString(), syntax);
            foreach (Match match in matches)
            {
                yield return match.Value;
            }
        }

        public void ReplaceNewLinesWithBreaks(Text textEl)
        {
            if (textEl.Text.Contains("\n"))
            {
                var linesOfText = textEl.Text.Split(new[] { "\n" }, StringSplitOptions.None);
                for (int i = 0; i < linesOfText.Length; i++)
                {
                    string rText = linesOfText[i];
                    if (i == 0)
                    {
                        textEl.Text = linesOfText[0];
                    }
                    else
                    {
                        if (i <= linesOfText.Length - 1)
                        {
                            textEl.Parent.AppendChild(new Break());
                            textEl.Parent.AppendChild(new Text(rText));
                        }
                    }
                }
            }
        }

        public void ReplaceContentController(Dictionary<string, string> replacements)
        {
            MainDocumentPart mainPart = WordDoc.MainDocumentPart;
            ReplaceContentController(replacements, mainPart.Document.Descendants<SdtElement>().ToList());
            foreach (var part in mainPart.FooterParts.ToList())
            {
                ReplaceContentController(replacements, part.Footer.Descendants<SdtElement>().ToList());
            }
        }

        public void ReplaceContentController(Dictionary<string, string> replacements, List<SdtElement> elements)
        {
            foreach (SdtElement sdt in elements)
            {
                Tag tag = sdt.Descendants<Tag>().FirstOrDefault();
                if (tag != null && tag.Val != null && !string.IsNullOrEmpty(tag.Val.Value))
                {
                    var replaceWith = replacements.FirstOrDefault(x => x.Key.Contains(tag.Val.Value));
                    if (replaceWith.Equals(default(KeyValuePair<string, string>)) || string.IsNullOrEmpty(replaceWith.Value)) continue;

                    var isPlainText = sdt.Descendants<SdtProperties>().FirstOrDefault()?.ChildElements.OfType<SdtContentText>().Any() ?? false;
                    if (isPlainText)
                    {
                        var run = sdt.Descendants<Run>().FirstOrDefault();
                        if (run != null)
                        {
                            run.Descendants<Text>().ToList().ForEach(x => x.Remove());
#pragma warning disable S3220 // Method calls should not resolve ambiguously to overloads with "params"
                            run.Append(newChildren: new Text(replaceWith.Value));
#pragma warning restore S3220 // Method calls should not resolve ambiguously to overloads with "params"
                        }
                    }
                }
            }
        }

        public void ReplaceNewLinesWithBreaks()
        {
            foreach (var headerPart in WordDoc.MainDocumentPart.HeaderParts)
            {
                foreach (var textEl in headerPart.Header.Descendants<Text>())
                {
                    ReplaceNewLinesWithBreaks(textEl);
                }
            }

            var body = WordDoc.MainDocumentPart.Document.Body;
            foreach (var textEl in body.Descendants<Text>())
            {
                ReplaceNewLinesWithBreaks(textEl);
            }

            foreach (var footerPart in WordDoc.MainDocumentPart.FooterParts)
            {
                foreach (var textEl in footerPart.Footer.Descendants<Text>())
                {
                    ReplaceNewLinesWithBreaks(textEl);
                }
            }
        }

        public void RemoveNoneReplacedSyntaxes()
        {
            foreach (var headerPart in WordDoc.MainDocumentPart.HeaderParts)
            {
                foreach (var runEl in headerPart.Header.Descendants<Run>().ToList())
                {
                    RemoveNoneReplacedSyntaxSimple(runEl);
                }
            }

            var body = WordDoc.MainDocumentPart.Document.Body;
            foreach (var runEl in body.Descendants<Run>().ToList())
            {
                RemoveNoneReplacedSyntaxSimple(runEl);
            }

            foreach (var footerPart in WordDoc.MainDocumentPart.FooterParts)
            {
                foreach (var runEl in footerPart.Footer.Descendants<Run>().ToList())
                {
                    RemoveNoneReplacedSyntaxSimple(runEl);
                }
            }
        }

        private void RemoveNoneReplacedSyntaxSimple(Run runEl)
        {
            if (runEl == null) return;
            var elm = runEl.Parent ?? runEl;
            elm.InnerXml = StringUtility.RemoveSyntaxes(elm.InnerXml);
        }

        /// <summary>
        /// Overload of SearchAndReplace to replace a single word.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="replacement"></param>
        public void SearchAndReplace(string input, string replacement)
        {
            var replace = new Dictionary<string, string>() { { input, replacement } };
            SearchAndReplace(replace);
        }

        /// <summary>
        /// Replace all text from a dictionary, the key is the search term and the value the replacement.
        /// </summary>
        /// <param name="replacements"></param>
        public void SearchAndReplace(Dictionary<string, string> replacements)
        {
            foreach (var replacement in replacements)
            {
                SearchAndReplacer.SearchAndReplace(WordDoc, replacement.Key, replacement.Value, false);
            }
        }

        public void Save()
        {
            WordDoc.Save();
        }

        /// <summary>
        /// Remove body from document
        /// </summary>
        public void RemoveEmptyParagraph()
        {
            RemoveEmptyParagraph(WordDoc);
        }

        /// <summary>
        /// Remove all of the headers from a document.
        /// </summary>
        public void RemoveHeaders()
        {
            RemoveHeaders(WordDoc);
        }

        /// <summary>
        /// Remove all of the footers from a document.
        /// </summary>
        public void RemoveFooters()
        {
            RemoveFooters(WordDoc);
        }

        public static void RemoveHeaders(WordprocessingDocument wpDoc)
        {
            var docPart = wpDoc.MainDocumentPart;
            if (docPart.HeaderParts.Any())
            {
                docPart.DeleteParts(docPart.HeaderParts);
                Document document = docPart.Document;

                var headers = document.Descendants<HeaderReference>().ToList();
                foreach (var header in headers)
                {
                    header.Remove();
                }

                document.Save();
            }
        }

        /// <summary>
        /// Remove all of the footers from a document.
        /// </summary>
        /// <param name="wpDoc"></param>
        public static void RemoveFooters(WordprocessingDocument wpDoc)
        {
            var docPart = wpDoc.MainDocumentPart;
            if (docPart.FooterParts.Any())
            {
                docPart.DeleteParts(docPart.FooterParts);
                Document document = docPart.Document;

                var footers = document.Descendants<FooterReference>().ToList();
                foreach (var footer in footers)
                {
                    footer.Remove();
                }

                document.Save();
            }
        }

        public static void RemoveEmptyParagraph(WordprocessingDocument wpDoc)
        {
            MainDocumentPart mainPart = wpDoc.MainDocumentPart;
            Document doc = mainPart.Document;

            foreach (Paragraph p in doc.Descendants<Paragraph>().ToList())
            {
                if (string.IsNullOrEmpty(p.InnerText))
                {
                    p.Remove();
                }
            }
        }

        public List<Run> GetAllRuns(OpenXmlPart part)
        {
            var runs = new List<Run>();
            if (part is HeaderPart)
            {
                var headerPart = part as HeaderPart;
                runs = headerPart.Header.Descendants<Run>().ToList();
            }
            if (part is FooterPart)
            {
                var footerPart = part as FooterPart;
                runs = footerPart.Footer.Descendants<Run>().ToList();
            }
            else if (part is MainDocumentPart)
            {
                var docPart = part as MainDocumentPart;
                runs = docPart.Document.Descendants<Run>().ToList();
            }

            return runs;
        }

        public void RemoveImage(string altText)
        {
            foreach (var headerPart in WordDoc.MainDocumentPart.HeaderParts.ToList())
            {
                RemoveImage(headerPart, altText);
            }

            RemoveImage(WordDoc.MainDocumentPart, altText);

            foreach (var footerPart in WordDoc.MainDocumentPart.FooterParts.ToList())
            {
                RemoveImage(footerPart, altText);
            }
        }

        public void RemoveImage(OpenXmlPart part, string altText)
        {
            var runs = GetAllRuns(part);
            foreach (var run in runs)
            {
                var blips = run.Descendants<A.Blip>();
                if (blips.Any())
                {
                    var docProps = run.Descendants<DW.DocProperties>().FirstOrDefault();
                    if (docProps == null)
                    {
                        continue;
                    }

                    string imgText = docProps.Title != null ? docProps.Title.Value : docProps.Description?.Value;
                    if (imgText != null && imgText.ExactMatch(altText))
                    {
                        var drawing = run.Descendants<Drawing>().FirstOrDefault();
                        if (drawing != null)
                        {
                            run.Remove();
                        }
                    }
                }
            }
        }

        public void SwitchImage(string altText, string filepath)
        {
            if (!File.Exists(filepath))
            {
                return;
            }

            foreach (var headerPart in WordDoc.MainDocumentPart.HeaderParts.ToList())
            {
                SwitchImage(headerPart, altText, filepath);
            }

            SwitchImage(WordDoc.MainDocumentPart, altText, filepath);

            foreach (var footerPart in WordDoc.MainDocumentPart.FooterParts.ToList())
            {
                SwitchImage(footerPart, altText, filepath);
            }
        }

        public void SwitchImage(string altText, Stream stream)
        {
            foreach (var headerPart in WordDoc.MainDocumentPart.HeaderParts.ToList())
            {
                SwitchImage(headerPart, altText, stream);
            }

            SwitchImage(WordDoc.MainDocumentPart, altText, stream);

            foreach (var footerPart in WordDoc.MainDocumentPart.FooterParts.ToList())
            {
                SwitchImage(footerPart, altText, stream);
            }
        }

        public void SwitchImage(OpenXmlPart part, string altText, string filepath)
        {
            using (var fs = new FileStream(filepath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                SwitchImage(part, altText, fs);
            }
        }

        public void SwitchImage(OpenXmlPart part, Run run, A.Blip blip, string altText, Stream stream)
        {
            dynamic dynPart = part;
            long w = 0, h = 0;

            var docProps = run.Descendants<DW.DocProperties>()
                .FirstOrDefault(x =>
                    (x.Title != null && x.Title.Value.ExactMatch(altText)) ||
                    (x.Description != null && x.Description.Value.ExactMatch(altText))
                );

            if (docProps != null)
            {
                var shapeprops = run.Descendants<PIC.ShapeProperties>().First();
                var extent = run.Descendants<DW.Extent>().First();
                var oldRatio = new AspectRatio(extent.Cx, extent.Cy);
                AspectRatio newRatio = null;

                var ip = dynPart.AddImagePart(ImagePartType.Png); //Lägg till en ny bild i paketet
                using (var ms = new MemoryStream())
                {
                    stream.Position = 0;
                    stream.CopyTo(ms);
                    ms.Position = 0;
                    ip.FeedData(ms);
                }

                //Uppdatera bildens höjd...
                ImageHelper.GetEMUsForImage(stream, out w, out h);
                newRatio = new AspectRatio(w, h); //Objekt som håller aspect-ratio efter konstruktion. Ändra x så ändras y enligt tidigare proportioner.
                if (w == 0 || h == 0) return;
                altText = OpenXmlGenericHelper.SetIfLogoMax(altText, oldRatio, newRatio);

                blip.Embed = dynPart.GetIdOfPart(ip); //Peka om blippen till den nya bilden

                if (altText.EndsWith("_maxwidth"))
                {
                    newRatio.Width = extent.Cx;

                    //Måtten måste ändras i båda extenterna enligt dokumentstandarden.
                    extent.Cy = newRatio.Height;
                    shapeprops.Descendants().OfType<A.Extents>().First().Cy = newRatio.Height;
                }
                else if (altText.EndsWith("_maxheight"))
                {
                    newRatio.Height = extent.Cy;

                    extent.Cx = newRatio.Width;
                    shapeprops.Descendants().OfType<A.Extents>().First().Cx = newRatio.Width;
                }
                else
                {
                    extent.Cx = w;
                    shapeprops.Descendants().OfType<A.Extents>().First().Cx = w;

                    extent.Cy = h;
                    shapeprops.Descendants().OfType<A.Extents>().First().Cy = h;
                }
            }
        }

        public void SwitchImage(OpenXmlPart part, string altText, Stream stream)
        {
            var runs = GetAllRuns(part);
            foreach (var run in runs)
            {
                var blips = run.Descendants<A.Blip>();
                if (blips.Any())
                {
                    SwitchImage(part, run, blips.FirstOrDefault(), altText, stream);
                }
            }
        }

        /// <summary>
        /// Create list of Parts ID needed to new footer
        /// </summary>
        /// <param name="srcPart"></param>
        /// <param name="destPart"></param>
        /// <returns></returns>
        private List<KeyValuePair<Type, Stream>> CopyParts(OpenXmlPart srcPart, OpenXmlPart destPart)
        {
            var listToAdd = new List<KeyValuePair<Type, Stream>>();
            foreach (var idPart in srcPart.Parts)
            {
                var part = srcPart.GetPartById(idPart.RelationshipId);
                if (part is ImagePart)
                {
                    destPart.AddNewPart<ImagePart>(part.ContentType, idPart.RelationshipId);
                    listToAdd.Add(new KeyValuePair<Type, Stream>(typeof(ImagePart), part.GetStream()));
                }
                else if (part is DiagramStylePart)
                {
                    destPart.AddNewPart<DiagramStylePart>(idPart.RelationshipId);
                    listToAdd.Add(new KeyValuePair<Type, Stream>(typeof(DiagramStylePart), part.GetStream()));
                }
                else if (part is DiagramColorsPart)
                {
                    destPart.AddNewPart<DiagramColorsPart>(idPart.RelationshipId);
                    listToAdd.Add(new KeyValuePair<Type, Stream>(typeof(DiagramColorsPart),
                        part.GetStream()));
                }
                else if (part is DiagramDataPart)
                {
                    destPart.AddNewPart<DiagramDataPart>(idPart.RelationshipId);
                    listToAdd.Add(new KeyValuePair<Type, Stream>(typeof(DiagramDataPart), part.GetStream()));
                }
                else if (part is DiagramPersistLayoutPart)
                {
                    destPart.AddNewPart<DiagramPersistLayoutPart>(idPart.RelationshipId);
                    listToAdd.Add(new KeyValuePair<Type, Stream>(typeof(DiagramPersistLayoutPart),
                        part.GetStream()));
                }
                else if (part is StyleDefinitionsPart)
                {
                    destPart.AddNewPart<StyleDefinitionsPart>(idPart.RelationshipId);
                    listToAdd.Add(new KeyValuePair<Type, Stream>(typeof(StyleDefinitionsPart),
                        part.GetStream()));
                }
                else if (part is StylesWithEffectsPart)
                {
                    destPart.AddNewPart<StylesWithEffectsPart>(idPart.RelationshipId);
                    listToAdd.Add(new KeyValuePair<Type, Stream>(typeof(StylesWithEffectsPart),
                        part.GetStream()));
                }
                else if (part is ThemePart)
                {
                    destPart.AddNewPart<ThemePart>(idPart.RelationshipId);
                    listToAdd.Add(new KeyValuePair<Type, Stream>(typeof(ThemePart),
                        part.GetStream()));
                }
                else if (part is PivotTablePart)
                {
                    destPart.AddNewPart<PivotTablePart>(idPart.RelationshipId);
                    listToAdd.Add(new KeyValuePair<Type, Stream>(typeof(PivotTablePart),
                        part.GetStream()));
                }
                else if (part is TableDefinitionPart)
                {
                    destPart.AddNewPart<TableDefinitionPart>(idPart.RelationshipId);
                    listToAdd.Add(new KeyValuePair<Type, Stream>(typeof(TableDefinitionPart),
                        part.GetStream()));
                }
                else if (part is TableStylesPart)
                {
                    destPart.AddNewPart<TableStylesPart>(idPart.RelationshipId);
                    listToAdd.Add(new KeyValuePair<Type, Stream>(typeof(TableStylesPart),
                        part.GetStream()));
                }
            }

            return listToAdd;
        }

        private void PrependHeaderInternal(HeaderPart srcHeaderPart, HeaderPart destHeaderPart, MainDocumentPart destMainPart)
        {
            if (srcHeaderPart != null)
            {
                // Copy content of header to new header
                destHeaderPart.FeedData(srcHeaderPart.GetStream());

                if (destHeaderPart.Header != null)
                {
                    // Keep formatting
                    KeepParagraphStyle(destHeaderPart.Header);
                }

                // Get all ids of every 'Parts'
                var listToAdd = CopyParts(srcHeaderPart, destHeaderPart);
                // Foreach Part, copy stream to new header
                var i = 0;
                foreach (var idPart in destHeaderPart.Parts)
                {
                    var part = destHeaderPart.GetPartById(idPart.RelationshipId);
                    if (part.GetType() == listToAdd[i].Key)
                    {
                        part.FeedData(listToAdd[i].Value);
                    }
                    i++;
                }
            }
            else
            {
                destMainPart.DeleteParts(destMainPart.HeaderParts);
                var sectToRemovePrs = destMainPart.Document.Body.Descendants<SectionProperties>();
                foreach (var sectPr in sectToRemovePrs)
                {
                    // Delete reference of old header
                    sectPr.RemoveAllChildren<HeaderReference>();
                }
            }
        }

        public void PrependHeader(string documentPath, WordHeaderSetting settings = null)
        {
            settings = settings ?? new WordHeaderSetting();

            // Open docx where we need to add header
            using (var destDoc = WordprocessingDocument.Open(documentPath, true))
            {
                var destMainPart = destDoc.MainDocumentPart;
                AddEvenAndOddHeaders(WordDoc.MainDocumentPart, destMainPart, settings);

                // Delete exist header
                destMainPart.DeleteParts(destMainPart.HeaderParts);

                //Copy styles
                CopyStyles(WordDoc.MainDocumentPart, destMainPart);

                var destSections = destMainPart.Document.Body.Descendants<SectionProperties>();
                foreach (var sectPr in destSections)
                {
                    // Remove old header reference
                    sectPr.RemoveAllChildren<HeaderReference>();
                }

                // Get header part
                foreach (var srcHeaderPart in WordDoc.MainDocumentPart.HeaderParts)
                {
                    // Create new header
                    var destHeaderPart = destMainPart.AddNewPart<HeaderPart>();

                    PrependHeaderInternal(srcHeaderPart, destHeaderPart, destMainPart);

                    // Get all sections, and past header to each section (Page)
                    PrependHeaderToSection(srcHeaderPart, destHeaderPart, destMainPart, destSections, settings);
                }
            }
        }

        private void PrependHeaderToSection(HeaderPart srcPart, HeaderPart destPart, MainDocumentPart destMainPart, IEnumerable<SectionProperties> destSections, WordHeaderSetting settings)
        {
            if (srcPart == null) return;

            // Get id of new header
            var rId = destMainPart.GetIdOfPart(destPart);

            // Get first sections, and past header to each section (Page)
            var sectPr = destSections.FirstOrDefault();
            if (sectPr == null) return;

            // Get header section
            var srcSectPr = WordDoc.MainDocumentPart.Document.Body.Descendants<SectionProperties>().FirstOrDefault();
            if (srcSectPr == null) return;

            AddTitlePage(srcSectPr, sectPr, settings);

            string rIdSource = WordDoc.MainDocumentPart.GetIdOfPart(srcPart);
            var hrefs = srcSectPr.Descendants<HeaderReference>().Where(x => x.Id == rIdSource).ToList();

            // Add new header reference
            foreach (var href in hrefs)
            {
                sectPr.PrependChild(new HeaderReference { Id = rId, Type = href.Type });
            }
        }

        /// <summary>
        /// Keep formatting
        /// </summary>
        /// <param name="destElm"></param>
        private void KeepParagraphStyle(OpenXmlPartRootElement destElm)
        {
            var paragraphs = destElm.Descendants<Paragraph>();
            foreach (var paragraph in paragraphs)
            {
                if (paragraph.ParagraphProperties == null)
                {
                    continue;
                }

                if (paragraph.ParagraphProperties.SpacingBetweenLines == null)
                {
                    paragraph.ParagraphProperties.SpacingBetweenLines = new SpacingBetweenLines
                    {
                        After = "0"
                    };
                }

                if (paragraph.ParagraphProperties.ParagraphStyleId == null)
                {
                    paragraph.ParagraphProperties.ParagraphStyleId = new ParagraphStyleId
                    {
                        Val = "No Spacing"
                    };
                }
            }
        }

        private void PrependFooterInternal(FooterPart srcFooterPart, FooterPart destFooterPart, MainDocumentPart destMainPart)
        {
            if (srcFooterPart != null)
            {
                // Copy content of footer
                destFooterPart.FeedData(srcFooterPart.GetStream());
                // Keep formatting
                if (destFooterPart.Footer != null)
                {
                    KeepParagraphStyle(destFooterPart.Footer);
                }

                // Create list of Parts ID needed to new footer
                var listToAdd = CopyParts(srcFooterPart, destFooterPart);

                // Foreach ID, copy stream to new footer
                var i = 0;
                foreach (var idPart in destFooterPart.Parts)
                {
                    var part = destFooterPart.GetPartById(idPart.RelationshipId);
                    if (part.GetType() == listToAdd[i].Key)
                    {
                        part.FeedData(listToAdd[i].Value);
                    }
                    i++;
                }
            }
            else
            {
                destMainPart.DeleteParts(destMainPart.FooterParts);
                var sectToRemovePrs = destMainPart.Document.Body.Descendants<SectionProperties>();
                foreach (var sectPr in sectToRemovePrs)
                {
                    // Delete reference of footer
                    sectPr.RemoveAllChildren<FooterReference>();
                }
            }
        }

        /// <summary>
        /// Used to copy styles from footer and header files if they do not exist in the destination.
        /// </summary>
        /// <param name="srcMainPart"></param>
        /// <param name="destMainPart"></param>
        private void CopyStyles(MainDocumentPart srcMainPart, MainDocumentPart destMainPart)
        {
            var sourceStyles = srcMainPart.StyleDefinitionsPart.RootElement;
            var styles = sourceStyles.Descendants<Style>().ToList();
            foreach (var style in styles)
            {
                if (style != null)
                {
                    var destinationStyles = destMainPart.StyleDefinitionsPart.RootElement;
                    var exists = destinationStyles.Descendants<Style>().Any(x => x.StyleId.Value == style.StyleId.Value);
                    if (!exists)
                    {
                        destinationStyles.AppendChild(style.CloneNode(true));
                    }
                }
            }
        }

        public void PrependFooter(string documentPath, WordFooterSetting settings = null)
        {
            settings = settings ?? new WordFooterSetting();

            // Open docx where append footer
            using (var destDoc = WordprocessingDocument.Open(documentPath, true))
            {
                var destMainPart = destDoc.MainDocumentPart;
                AddEvenAndOddHeaders(WordDoc.MainDocumentPart, destMainPart, settings);

                // Remove exist footer
                destMainPart.DeleteParts(destMainPart.FooterParts);

                //Copy styles
                CopyStyles(WordDoc.MainDocumentPart, destMainPart);

                var destSections = destMainPart.Document.Body.Descendants<SectionProperties>();
                foreach (var sectPr in destSections)
                {
                    // Remove old footer reference
                    sectPr.RemoveAllChildren<FooterReference>();
                }

                // Get header part
                foreach (var srcFooterPart in WordDoc.MainDocumentPart.FooterParts)
                {
                    // Create new header
                    var destFooterPart = destMainPart.AddNewPart<FooterPart>();

                    PrependFooterInternal(srcFooterPart, destFooterPart, destMainPart);

                    // Get all sections, and past header to each section (Page)
                    PrependFooterToSection(srcFooterPart, destFooterPart, destMainPart, destSections, settings);
                }
            }
        }

        /// <summary>
        /// Add EvenAndOddHeaders from part
        /// </summary>
        /// <param name="srcMainPart"></param>
        /// <param name="destMainPart"></param>
        /// <param name="settings"></param>
        private void AddEvenAndOddHeaders(MainDocumentPart srcMainPart, MainDocumentPart destMainPart, WordInsertPartSetting settings)
        {
            //Settings part exists
            if (settings.AddEvenAndOddHeaders && srcMainPart.DocumentSettingsPart != null && destMainPart.DocumentSettingsPart != null)
            {
                //Adding element if not already existing.
                if (srcMainPart.DocumentSettingsPart.Settings.Descendants<EvenAndOddHeaders>().Any() && !destMainPart.DocumentSettingsPart.Settings.Descendants<EvenAndOddHeaders>().Any())
                {
                    destMainPart.DocumentSettingsPart.Settings.PrependChild(new EvenAndOddHeaders());
                }
            }
        }

        /// <summary>
        /// Add First Different Page Header/Footer from part.
        /// </summary>
        /// <param name="srcSectPr"></param>
        /// <param name="sectPr"></param>
        /// <param name="settings"></param>
        private void AddTitlePage(SectionProperties srcSectPr, SectionProperties sectPr, WordInsertPartSetting settings)
        {
            if (settings.AddTitlePage && srcSectPr.Descendants<TitlePage>().Any() && !sectPr.Descendants<TitlePage>().Any())
            {
                sectPr.PrependChild(new TitlePage());
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="srcPart">from the footer file</param>
        /// <param name="destPart">the generated file</param>
        /// <param name="destMainPart"></param>
        private void PrependFooterToSection(FooterPart srcPart, FooterPart destPart, MainDocumentPart destMainPart, IEnumerable<SectionProperties> destSections, WordFooterSetting settings)
        {
            if (srcPart == null) return;

            // Get id of new footer
            var rId = destMainPart.GetIdOfPart(destPart);

            // Get all sections, and past footer to each section (Page)
            var sectPr = destSections.FirstOrDefault();
            if (sectPr == null) return;

            //Footer file sections
            var srcSectPr = WordDoc.MainDocumentPart.Document.Body.Descendants<SectionProperties>().FirstOrDefault();
            if (srcSectPr == null) return;

            AddTitlePage(srcSectPr, sectPr, settings);

            string rIdSource = WordDoc.MainDocumentPart.GetIdOfPart(srcPart);
            var hrefs = srcSectPr.Descendants<FooterReference>().Where(x => x.Id == rIdSource).ToList();

            // Add new footer reference
            foreach (var href in hrefs)
            {
                sectPr.PrependChild(new FooterReference { Id = rId, Type = href.Type });
            }
        }

        public static async Task InsertPartsAsync(DocBuilderSettings settings)
        {
            string tempHeaderPath = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + ".brandarise.tmp";
            string tempFooterPath = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + ".brandarise.tmp";

            try
            {
                if (!settings.KeepTemplateHeader && settings.HeaderStream == null)
                {
                    using (var openXmlHelper = new OpenXmlWordHelper(settings.TemplateFilePath))
                    {
                        openXmlHelper.RemoveHeaders();
                        openXmlHelper.Save();
                    }
                }
                else if (settings.HeaderStream != null) //Merge header from another document
                {
                    await OpenXmlWordHelper.CopyToAsync(settings.HeaderStream, tempHeaderPath);
                    using (var openXmlHelper = new OpenXmlWordHelper(tempHeaderPath, true))
                    {
                        openXmlHelper.RemoveEmptyParagraph();
                        openXmlHelper.PrependHeader(settings.TemplateFilePath, settings.HeaderSettings);
                        openXmlHelper.Save();
                    }
                }

                if (!settings.KeepTemplateFooter && settings.FooterStream == null)
                {
                    using (var openXmlHelper = new OpenXmlWordHelper(settings.TemplateFilePath))
                    {
                        openXmlHelper.RemoveFooters();
                        openXmlHelper.Save();
                    }
                }
                else if (settings.FooterStream != null)
                {
                    await OpenXmlWordHelper.CopyToAsync(settings.FooterStream, tempFooterPath);
                    using (var openXmlHelper = new OpenXmlWordHelper(tempFooterPath, true))
                    {
                        openXmlHelper.RemoveEmptyParagraph();
                        openXmlHelper.PrependFooter(settings.TemplateFilePath, settings.FooterSettings);
                        openXmlHelper.Save();
                    }
                }
            }
            finally
            {
                if (File.Exists(tempHeaderPath))
                    File.Delete(tempHeaderPath);

                if (File.Exists(tempFooterPath))
                    File.Delete(tempFooterPath);
            }
        }

        #region IDisposable Support

        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing && WordDoc != null)
                {
                    WordDoc.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion IDisposable Support
    }
}