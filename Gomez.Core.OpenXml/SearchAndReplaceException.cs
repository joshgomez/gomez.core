﻿using System;
using System.Runtime.Serialization;

namespace Gomez.Core.OpenXml
{
    [Serializable]
    public class SearchAndReplaceException : Exception
    {
        public SearchAndReplaceException()
        {
        }

        public SearchAndReplaceException(string message) : base(message)
        {
        }

        public SearchAndReplaceException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected SearchAndReplaceException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}