﻿namespace Gomez.Core.OpenXml
{
    public class WordInsertPartSetting
    {
        /// <summary>
        /// Decide if the part should add Different first page header and footer setting.
        /// </summary>
        public bool AddTitlePage { get; set; }

        public bool AddEvenAndOddHeaders { get; set; }
    }

    public class WordHeaderSetting : WordInsertPartSetting
    {
    }

    public class WordFooterSetting : WordInsertPartSetting
    {
    }
}