﻿using PhoneNumbers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Gomez.Core.Phone
{
    public class NumberParser
    {
        private static readonly Dictionary<string, int> numbers = new Dictionary<string, int>
        {
            ["Zero"] = 0,
            ["One"] = 1,
            ["Two"] = 2,
            ["Three"] = 3,
            ["Four"] = 4,
            ["Five"] = 5,
            ["Six"] = 6,
            ["Seven"] = 7,
            ["Eight"] = 8,
            ["Nine"] = 9,
        };

        public enum AlphaNumber
        {
            A = 2, B = 2, C = 2, D = 3, E = 3, F = 3, G = 4, H = 4, I = 4, J = 5, K = 5, L = 5,
            M = 6, N = 6, O = 6, P = 7, Q = 7, R = 7, S = 8, T = 8, U = 8, V = 9, W = 9, X = 9, Y = 9, Z = 9
        }

        private static string ConvertFromMixedToStandardNumbers(string input)
        {
            return numbers.Aggregate(input,
                (current, number) => current.Replace(number.Key.ToLower(), number.Value.ToString()));
        }

        private static string AlphaToNumbers(string number)
        {
            number = ConvertFromMixedToStandardNumbers(number);
            var normalizedNumber = new StringBuilder();
            foreach (char character in number)
            {
                normalizedNumber.Append(ParseAlphaNumber(character));
            }

            return normalizedNumber.ToString();
        }

        private static char ParseAlphaNumber(char input)
        {
            if (input == '-' || char.IsDigit(input) || input == '+')
            {
                return input;
            }

            if (char.IsLetter(input))
            {
                var num = (AlphaNumber)(Enum.Parse(typeof(AlphaNumber), (char.IsLower(input) ? char.ToUpperInvariant(input) : input).ToString()));
                return ((int)num).ToString()[0];
            }

            return input;
        }

        private string _twoLetterISORegionName;
        private PhoneNumberUtil _instance;
        private PhoneNumber _phoneNumber;

        public NumberParser(string number, CultureInfo c, bool ignoreInternationalCheck = false)
        {
            var ri = new RegionInfo(c.Name);
            Init(number, ri.TwoLetterISORegionName, ignoreInternationalCheck);
        }

        public NumberParser(string number, string twoLetterISORegionName, bool ignoreInternationalCheck)
        {
            Init(number, twoLetterISORegionName, ignoreInternationalCheck);
        }

        public string OutOfCountryCallingNumberToString()
        {
            return _instance.FormatOutOfCountryCallingNumber(_phoneNumber, _twoLetterISORegionName);
        }

        public override string ToString()
        {
            return _instance.Format(_phoneNumber, PhoneNumberFormat.INTERNATIONAL);
        }

        private void Init(string number, string twoLetterISORegionName, bool ignoreInternationalCheck)
        {
            number = AlphaToNumbers(number);

            _twoLetterISORegionName = twoLetterISORegionName.ToUpper();
            _instance = PhoneNumberUtil.GetInstance();
            if (!ignoreInternationalCheck && (number.StartsWith("+") || number.StartsWith("00")))
            {
                number = number.StartsWith("00") ? number.Replace("00", "+") : number;
                _phoneNumber = _instance.Parse(number, null);
                return;
            }

            _phoneNumber = _instance.Parse(number, _twoLetterISORegionName);
        }
    }
}