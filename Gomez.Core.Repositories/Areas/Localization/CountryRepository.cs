﻿using Gomez.Core.Models.Entity;
using Gomez.Core.ViewModels.Areas.Localization.Countries;
using Microsoft.Extensions.Localization;
using System.Linq;
using System.Threading.Tasks;

namespace Gomez.Core.Repositories.Areas.Localization
{
    public class CountryRepository : RepositoryBase<CountryViewModel>
    {
        public CountryRepository(IStringLocalizerFactory localizerFactory) : base(localizerFactory)
        {
            Init();
            Query = Utilities.CultureInfoUtility.CountryList()
                .Select(x => new CountryViewModel() { Name = x.Value, TwoLetterISORegionName = x.Key })
                .AsQueryable();
        }

        public Task<IndexViewModel> IndexAsync(IndexViewModel vm)
        {
            var query = MultiSearch(vm.Search, Query)
                .OrderBy(x => x.Name);

            vm.Countries = query.GetPaged(vm.Page, vm.Rows);
            return Task.FromResult(vm);
        }

        public Task<GetViewModel> GetAsync(GetViewModel vm)
        {
            var query = Query.Where(x => x.TwoLetterISORegionName == vm.Id);
            vm.Country = query.FirstOrDefault();
            return Task.FromResult(vm);
        }

        protected override IQueryable<CountryViewModel> SearchQuery(IQueryable<CountryViewModel> query, string term)
        {
            return query.Where(x =>
                x.Name.ToLower().StartsWith(term) ||
                x.Name.ToLower().Contains(term) ||
                x.Name.ToLower().EndsWith(term)
            );
        }
    }
}