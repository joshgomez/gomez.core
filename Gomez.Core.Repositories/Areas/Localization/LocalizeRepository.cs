﻿using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Gomez.Core.Repositories.Areas.Localization
{
    public class LocalizeRepository : RepositoryBase<LocalizedString>
    {
        private readonly Dictionary<string, IStringLocalizer> _localizers;

        public LocalizeRepository(IStringLocalizerFactory localizerFactory) : base(localizerFactory)
        {
            _localizers = new Dictionary<string, IStringLocalizer>();
            Init();
        }

        public void SetResource(Type type)
        {
            if (!_localizers.ContainsKey(type.Name))
            {
                _localizers.TryAdd(type.Name, _localizerFactory.Create(type));
            }
        }

        public IStringLocalizer GetResource(string name)
        {
            if (_localizers.ContainsKey(name))
            {
                return _localizers[name];
            }

            return null;
        }

        public IEnumerable<LocalizedString> GetAsync(string name, string culture)
        {
            return GetResource(name)?.GetAllStrings()
                .Where(x => x.Name.EndsWith($".{culture}"))
                .Select(x => new LocalizedString(x.Name.Remove(x.Name.LastIndexOf($".{culture}")), x.Value)) ?? Enumerable.Empty<LocalizedString>();
        }

        public IEnumerable<LocalizedString> IndexAsync(string name, string search)
        {
            var query = GetResource(name)?.GetAllStrings().AsQueryable();
            if (query != null)
            {
                query = MultiSearch(search, query);
                return query.AsEnumerable();
            }

            return Enumerable.Empty<LocalizedString>();
        }

        protected override IQueryable<LocalizedString> SearchQuery(IQueryable<LocalizedString> query, string term)
        {
            return query.Where(x =>
                x.Name.ToLower().StartsWith(term) ||
                x.Name.ToLower().Contains(term) ||
                x.Name.ToLower().EndsWith(term)
            );
        }
    }
}