﻿using AutoMapper;
using Gomez.Core.Localization;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Gomez.Core.Repositories
{
    public abstract class SingletonRepositoryBase
    {
        protected class ModelMapper
        {
            public MapperConfiguration Cfg { get; set; }
            public Mapper Mapper { get; set; }
        }

        protected readonly static Dictionary<string, ModelMapper> _mappers = new Dictionary<string, ModelMapper>();
    }

    public abstract class RepositoryBase<T> : SingletonRepositoryBase where T : class
    {
        protected readonly IStringLocalizerFactory _localizerFactory;

        protected IStringLocalizer SharedLocalizer { get; private set; }
        protected IStringLocalizer Localizer { get; private set; }

        protected IQueryable<T> Query { get; set; }

        protected RepositoryBase(IStringLocalizerFactory localizerFactory)
        {
            _localizerFactory = localizerFactory;
        }

        protected void Init()
        {
            SharedLocalizer = _localizerFactory.Create(typeof(SharedResource));
            Localizer = _localizerFactory.Create(GetType());
        }

        protected void SetMapper(string key, MapperConfiguration cfg)
        {
            key = typeof(T).FullName + key;
            lock (_mappers)
            {
                if (!_mappers.ContainsKey(key))
                {
                    _mappers.TryAdd(key, new ModelMapper());
                    _mappers[key].Cfg = cfg;
                    _mappers[key].Mapper = new Mapper(_mappers[key].Cfg);
                }
            }
        }

        protected Mapper GetMapper(string key)
        {
            key = typeof(T).FullName + key;
            if (_mappers.ContainsKey(key))
            {
                return _mappers[key].Mapper;
            }

            return null;
        }

        protected MapperConfiguration GetMapperCfg(string key)
        {
            key = typeof(T).FullName + key;
            if (_mappers.ContainsKey(key))
            {
                return _mappers[key].Cfg;
            }

            return null;
        }

        public void SetMultiSearch(string q)
        {
            Query = MultiSearch(q);
        }

        public IQueryable<T> MultiSearch(string q, IQueryable<T> query = null)
        {
            query ??= Query;
            return MultiSearch(q, query, SearchQuery);
        }

        private IQueryable<TItem> MultiSearch<TItem>(string q, IQueryable<TItem> query, Func<IQueryable<TItem>, string, IQueryable<TItem>> action)
        {
            if (!string.IsNullOrEmpty(q))
            {
                var searchTerms = q.ToLower().Trim().Split(new char[] { ' ', '+' });
                if (searchTerms.Length == 1)
                {
                    query = action.Invoke(query, searchTerms[0]);
                }
                else
                {
                    foreach (var term in searchTerms)
                    {
                        query = action.Invoke(query, searchTerms[0]);
                    }
                }
            }

            return query;
        }

        protected virtual IQueryable<T> SearchQuery(IQueryable<T> query, string term)
        {
            return query;
        }
    }
}