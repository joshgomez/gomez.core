﻿namespace Gomez.Core.Storage.Ftp
{
    public class FtpOption
    {
        public string HostName { get; set; }
        public int Port { get; set; } = 21;
        public string UserName { get; set; }
        public string Password { get; set; }
        public string RootDir { get; set; }
    }
}