﻿using FluentFTP;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;

namespace Gomez.Core.Storage.Ftp
{
    public class FtpStorageProvider : IStorageProvider
    {
        public StorageProvider Provider { get; private set; } = StorageProvider.Ftp;
        private readonly string _rootDir;
        private FtpClient _ftp;

        public FtpStorageProvider(string hostname, string userName, string password, string rootDir, int port = 21)
        {
            _rootDir = rootDir;
            _ftp = new FtpClient(hostname, port, userName, password);
        }

        public FtpStorageProvider(FtpOption option) : this(option.HostName, option.UserName, option.Password, option.RootDir, option.Port)
        {
        }

        private async Task TryConnect()
        {
            if (!_ftp.IsConnected)
                await _ftp.ConnectAsync();
        }

        public async Task DeleteBlobAsync(string containerName, string blobName)
        {
            await TryConnect();
            var path = Path.Combine(_rootDir, containerName, blobName);
            if (!await _ftp.FileExistsAsync(path))
            {
                throw new StorageException(StorageErrorCode.InvalidName.ToStorageError(), null);
            }

            try { await _ftp.DeleteFileAsync(path); }
            catch (Exception ex)
            {
                throw ex.ToStorageException();
            }
        }

        public async Task DeleteContainerAsync(string containerName)
        {
            await TryConnect();
            try
            {
                var path = Path.Combine(_rootDir, containerName);
                await _ftp.DeleteDirectoryAsync(path, FtpListOption.AllFiles);
            }
            catch (Exception ex)
            {
                throw ex.ToStorageException();
            }
        }

        public async Task CopyBlobAsync(string sourceContainerName, string sourceBlobName, string destinationContainerName, string destinationBlobName = null)
        {
            using (var stream = await GetBlobStreamAsync(sourceContainerName, sourceBlobName))
            {
                await SaveBlobStreamAsync(destinationContainerName, destinationBlobName, stream);
            }
        }

        public async Task MoveBlobAsync(string sourceContainerName, string sourceBlobName, string destinationContainerName, string destinationBlobName = null)
        {
            await TryConnect();
            try
            {
                var srcPath = Path.Combine(_rootDir, sourceContainerName, sourceBlobName);
                var destPath = Path.Combine(_rootDir, destinationContainerName, destinationBlobName);
                if (!await _ftp.MoveFileAsync(srcPath, destPath, FtpRemoteExists.Skip))
                {
                    throw new StorageException(StorageErrorCode.GenericException, "File exists.");
                }
            }
            catch (Exception ex)
            {
                throw ex.ToStorageException();
            }
        }

        public async Task<bool> BlobExistsAsync(string containerName, string blobName)
        {
            await TryConnect();
            var path = Path.Combine(_rootDir, containerName, blobName);
            if (await _ftp.FileExistsAsync(path))
            {
                return true;
            }

            return false;
        }

        public async Task<bool> ContainerExistsAsync(string containerName)
        {
            await TryConnect();
            var path = Path.Combine(_rootDir, containerName);
            if (await _ftp.DirectoryExistsAsync(path))
            {
                return true;
            }

            return false;
        }

        public async Task<BlobDescriptor> GetBlobDescriptorAsync(string containerName, string blobName)
        {
            await TryConnect();
            var path = Path.Combine(_rootDir, containerName, blobName);
            return await GetBlobDescriptorByPathAsync(containerName, path);
        }

        private async Task<BlobDescriptor> GetBlobDescriptorByPathAsync(string containerName, string path)
        {
            var info = await _ftp.GetObjectInfoAsync(path);
            if (info != null)
            {
                string mimeType = MimeMapping.MimeUtility.GetMimeMapping(Path.GetExtension(info.Name));
                var md5 = await _ftp.GetMD5Async(path);

                var blobInfo = new BlobDescriptor
                {
                    Container = containerName,
                    ContentMD5 = md5,
                    ContentType = mimeType,
                    ETag = "",
                    LastModified = info.Modified,
                    Length = info.Size,
                    Name = info.Name,
                    Security = BlobSecurity.Private,
                    Url = info.FullName
                };

                blobInfo.Metadata ??= new Dictionary<string, string>();
                blobInfo.Metadata[StorageMetaData.LastWriteTimeUtc] = info.Modified.ToString(CultureInfo.InvariantCulture);
                blobInfo.Metadata[StorageMetaData.CreationTimeUtc] = info.Created.ToString(CultureInfo.InvariantCulture);

                return blobInfo;
            }

            return null;
        }

        public async Task<Stream> GetBlobStreamAsync(string containerName, string blobName)
        {
            await TryConnect();
            var path = Path.Combine(_rootDir, containerName, blobName);

            var ms = new MemoryStream();
            await _ftp.DownloadAsync(ms, path);
            ms.Position = 0;
            return ms;
        }

        public string GetBlobSasUrl(string containerName, string blobName, DateTimeOffset expiry,
            bool isDownload = false, string fileName = null, string contentType = null, BlobUrlAccess access = BlobUrlAccess.Read)
        {
            return Path.Combine(_rootDir, containerName, blobName);
        }

        public string GetBlobUrl(string containerName, string blobName)
        {
            return Path.Combine(_rootDir, containerName, blobName);
        }

        public async Task<IList<BlobDescriptor>> ListBlobsAsync(string containerName)
        {
            await TryConnect();
            var path = Path.Combine(_rootDir, containerName);

            var infoCollection = new List<BlobDescriptor>();
            foreach (FtpListItem item in await _ftp.GetListingAsync(path))
            {
                if (item.Type != FtpFileSystemObjectType.File)
                {
                    continue;
                }

                infoCollection.Add(await GetBlobDescriptorByPathAsync(containerName, item.FullName));
            }

            return infoCollection;
        }

        public async Task SaveBlobStreamAsync(string containerName, string blobName, Stream source, BlobProperties properties = null, bool closeStream = true, FileInfo info = null)
        {
            await TryConnect();
            var path = Path.Combine(_rootDir, containerName, blobName);
            try
            {
                await _ftp.UploadAsync(source, path, FtpRemoteExists.Overwrite, true);
            }
            catch (Exception ex)
            {
                throw ex.ToStorageException();
            }
            finally
            {
                if (source != null && closeStream)
                {
                    source.Dispose();
                }
            }
        }

        public async Task UpdateBlobPropertiesAsync(string containerName, string blobName, BlobProperties properties, FileInfo info = null)
        {
            await TryConnect();
            var path = Path.Combine(_rootDir, containerName, blobName);
            await _ftp.SetModifiedTimeAsync(path, properties.LastWriteTimeUtc);
        }

        #region IDisposable Support

        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing && _ftp != null)
                {
                    _ftp.Dispose();
                    _ftp = null;
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion IDisposable Support
    }
}