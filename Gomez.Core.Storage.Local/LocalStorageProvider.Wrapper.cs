﻿using Gomez.Core.Utilities;
using System;
using System.Collections.Generic;
using System.IO;

namespace Gomez.Core.Storage.Local
{
    public partial class LocalStorageProvider
    {
        private static readonly ILockManager _fileLockManager = new LockManager();

        private bool BlobExists(string containerName, string blobName)
        {
            var path = Path.Combine(_basePath, containerName, blobName);
            return _fileLockManager.GetLock<bool>(path, () =>
            {
                if (File.Exists(path))
                {
                    return true;
                }

                return false;
            });
        }

        private bool ContainerExists(string containerName)
        {
            var path = Path.Combine(_basePath, containerName);
            return _fileLockManager.GetLock<bool>(path, () =>
            {
                if (Directory.Exists(path))
                {
                    return true;
                }

                return false;
            });
        }

        private void DeleteBlob(string containerName, string blobName)
        {
            var path = Path.Combine(_basePath, containerName, blobName);
            _fileLockManager.GetLock(path, () =>
            {
                if (!File.Exists(path))
                {
                    throw new StorageException(StorageErrorCode.InvalidName.ToStorageError(), null);
                }

                try { File.Delete(path); }
                catch (Exception ex)
                {
                    throw ex.ToStorageException();
                }
            });
        }

        private void DeleteContainer(string containerName)
        {
            var path = Path.Combine(_basePath, containerName);
            _fileLockManager.GetLock(path, () =>
            {
                Directory.Delete(path, true);
            });
        }

        public void CopyBlob(string sourceContainerName, string sourceBlobName, string destinationContainerName,
            string destinationBlobName = null)
        {
            if (string.IsNullOrEmpty(sourceContainerName))
            {
                throw new StorageException(StorageErrorCode.InvalidName, $"Invalid {nameof(sourceContainerName)}");
            }
            if (string.IsNullOrEmpty(sourceBlobName))
            {
                throw new StorageException(StorageErrorCode.InvalidName, $"Invalid {nameof(sourceBlobName)}");
            }
            if (string.IsNullOrEmpty(destinationContainerName))
            {
                throw new StorageException(StorageErrorCode.InvalidName, $"Invalid {nameof(destinationContainerName)}");
            }
            if (destinationBlobName == string.Empty)
            {
                throw new StorageException(StorageErrorCode.InvalidName, $"Invalid {nameof(destinationBlobName)}");
            }

            var sourcePath = Path.Combine(_basePath, sourceContainerName, sourceBlobName);
            var destDir = Path.Combine(_basePath, destinationContainerName);
            var destPath = Path.Combine(destDir, destinationBlobName ?? sourceBlobName);

            _fileLockManager.GetLock(sourcePath, () =>
            {
                _fileLockManager.GetLock(destPath, () =>
                {
                    Directory.CreateDirectory(destDir);
                    File.Copy(sourcePath, destPath, true);
                });
            });
        }

        private BlobDescriptor GetBlobDescriptor(string containerName, string blobName)
        {
            var path = Path.Combine(_basePath, containerName, blobName);
            return _fileLockManager.GetLock<BlobDescriptor>(path, () =>
            {
                try
                {
                    var info = new FileInfo(path);
                    var blobInfo = new BlobDescriptor
                    {
                        Container = containerName,
                        ContentMD5 = info.ToMd5Hex(),
                        ContentType = MimeMapping.MimeUtility.GetMimeMapping(info.Extension),
                        ETag = "",
                        LastModified = info.LastWriteTimeUtc,
                        Length = info.Length,
                        Name = info.Name,
                        Security = BlobSecurity.Private,
                        Url = info.FullName
                    };

                    blobInfo = StorageFunctions.SetMetaData(blobInfo, info);
                    return blobInfo;
                }
                catch (Exception ex)
                {
                    throw new StorageException(1001.ToStorageError(), ex);
                }
            });
        }

        private Stream GetBlobStream(string containerName, string blobName)
        {
            var path = Path.Combine(_basePath, containerName, blobName);
            return _fileLockManager.GetLock<Stream>(path, () =>
            {
                try
                {
                    return File.OpenRead(path);
                }
                catch (Exception ex)
                {
                    throw ex.ToStorageException();
                }
            });
        }

        private IList<BlobDescriptor> ListBlobs(string containerName)
        {
            var dir = Path.Combine(_basePath, containerName);
            return _fileLockManager.GetLock<IList<BlobDescriptor>>(dir, () =>
            {
                var localFilesInfo = new List<BlobDescriptor>();

                try
                {
                    var dirInfo = new DirectoryInfo(dir);
                    var fileInfo = dirInfo.GetFiles();

                    foreach (var f in fileInfo)
                    {
                        _fileLockManager.GetLock(f.FullName, () =>
                        {
                            var blobInfo = new BlobDescriptor
                            {
                                ContentMD5 = f.ToMd5Hex(),
                                ETag = "",
                                ContentType = MimeMapping.MimeUtility.GetMimeMapping(f.Extension),
                                Container = containerName,
                                LastModified = f.LastWriteTime,
                                Length = f.Length,
                                Name = f.Name,
                                Url = f.FullName,
                                Security = BlobSecurity.Private,
                            };

                            blobInfo = StorageFunctions.SetMetaData(blobInfo, f);
                            localFilesInfo.Add(blobInfo);
                        });
                    }

                    return localFilesInfo;
                }
                catch (Exception ex)
                {
                    throw new StorageException(1001.ToStorageError(), ex);
                }
            });
        }

        public void UpdateBlobProperties(string containerName, string blobName, BlobProperties properties, FileInfo info = null)
        {
            var dir = Path.Combine(_basePath, containerName);
            string filePath = Path.Combine(dir, blobName);
            _fileLockManager.GetLock(filePath, () =>
            {
                try
                {
                    SetFileInfo(filePath, info);
                }
                catch (Exception ex)
                {
                    throw ex.ToStorageException();
                }
            });
        }
    }
}