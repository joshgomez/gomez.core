﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Gomez.Core.Storage.Local
{
    public partial class LocalStorageProvider : IStorageProvider
    {
        public StorageProvider Provider { get; private set; } = StorageProvider.Local;
        private readonly string _basePath;

        public LocalStorageProvider(string basePath)
        {
            _basePath = basePath;
        }

        public Task DeleteBlobAsync(string containerName, string blobName)
        {
            return Task.Factory.StartNew(() => DeleteBlob(containerName, blobName));
        }

        public Task DeleteContainerAsync(string containerName)
        {
            return Task.Run(() => DeleteContainer(containerName));
        }

        public Task CopyBlobAsync(string sourceContainerName, string sourceBlobName, string destinationContainerName, string destinationBlobName = null)
        {
            return Task.Run(() => CopyBlob(sourceContainerName, sourceBlobName, destinationContainerName, destinationBlobName));
        }

        public async Task MoveBlobAsync(string sourceContainerName, string sourceBlobName, string destinationContainerName, string destinationBlobName = null)
        {
            await CopyBlobAsync(sourceContainerName, sourceBlobName, destinationContainerName, destinationBlobName);
            await DeleteBlobAsync(sourceContainerName, sourceBlobName);
        }

        public Task<bool> BlobExistsAsync(string containerName, string blobName)
        {
            return Task.Run(() => BlobExists(containerName, blobName));
        }

        public Task<bool> ContainerExistsAsync(string containerName)
        {
            return Task.Run(() => ContainerExists(containerName));
        }

        public Task<BlobDescriptor> GetBlobDescriptorAsync(string containerName, string blobName)
        {
            return Task.Run(() => GetBlobDescriptor(containerName, blobName));
        }

        public Task<Stream> GetBlobStreamAsync(string containerName, string blobName)
        {
            return Task.Run(() => GetBlobStream(containerName, blobName));
        }

        public string GetBlobSasUrl(string containerName, string blobName, DateTimeOffset expiry,
            bool isDownload = false, string fileName = null, string contentType = null, BlobUrlAccess access = BlobUrlAccess.Read)
        {
            return Path.Combine(_basePath, containerName, blobName);
        }

        public string GetBlobUrl(string containerName, string blobName)
        {
            return Path.Combine(_basePath, containerName, blobName);
        }

        public Task<IList<BlobDescriptor>> ListBlobsAsync(string containerName)
        {
            return Task.Run(() => ListBlobs(containerName));
        }

        private void SetFileInfo(string filePath, FileInfo info)
        {
            if (info == null)
            {
                return;
            }

            File.SetCreationTimeUtc(filePath, info.CreationTime);
            File.SetCreationTimeUtc(filePath, info.CreationTimeUtc);

            File.SetLastWriteTime(filePath, info.LastWriteTime);
            File.SetLastWriteTimeUtc(filePath, info.LastWriteTimeUtc);
        }

        public async Task SaveBlobStreamAsync(string containerName, string blobName, Stream source, BlobProperties properties = null, bool closeStream = true, FileInfo info = null)
        {
            var dir = Path.Combine(_basePath, containerName);

            try
            {
                string filePath = Path.Combine(dir, blobName);
                Directory.CreateDirectory(dir);
                using (var file = File.Create(filePath))
                {
                    await source.CopyToAsync(file);
                }

                SetFileInfo(filePath, info);
            }
            catch (Exception ex)
            {
                throw ex.ToStorageException();
            }
            finally
            {
                if (source != null && closeStream)
                {
                    source.Dispose();
                }
            }
        }

        public Task UpdateBlobPropertiesAsync(string containerName, string blobName, BlobProperties properties, FileInfo info = null)
        {
            return Task.Run(() => UpdateBlobProperties(containerName, blobName, properties, info));
        }

        #region IDisposable Support

        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    //Nothing to dispose
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion IDisposable Support
    }
}