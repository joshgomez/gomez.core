﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Gomez.Core.Storage
{
    public class BlobBaseProperties
    {
        public string ContentType { get; set; }

        public string ContentMD5 { get; set; }

        public string ContentDisposition { get; set; }
        public BlobSecurity Security { get; set; }
        public IDictionary<string, string> Metadata { get; set; }

        public virtual DateTime CreationTimeUtc
        {
            get
            {
                if (Metadata?.ContainsKey(StorageMetaData.CreationTimeUtc) == true)
                    return Convert.ToDateTime(Metadata[StorageMetaData.CreationTimeUtc], CultureInfo.InvariantCulture);

                return DateTime.MinValue;
            }
        }

        public virtual DateTime LastWriteTimeUtc
        {
            get
            {
                if (Metadata?.ContainsKey(StorageMetaData.LastWriteTimeUtc) == true)
                    return Convert.ToDateTime(Metadata[StorageMetaData.LastWriteTimeUtc], CultureInfo.InvariantCulture);

                return DateTime.MinValue;
            }
        }
    }
}