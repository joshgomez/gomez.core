﻿using System;
using System.Collections.Generic;

namespace Gomez.Core.Storage
{
    public class BlobDescriptor : BlobBaseProperties
    {
        public BlobDescriptor()
        {
            Metadata = new Dictionary<string, string>();
        }

        public string ETag { get; set; }

        public long Length { get; set; }

        public DateTimeOffset? LastModified { get; set; }

        public override DateTime CreationTimeUtc
        {
            get
            {
                if (LastModified.HasValue && base.CreationTimeUtc == DateTime.MinValue)
                {
                    return LastModified.Value.UtcDateTime;
                }

                return base.CreationTimeUtc;
            }
        }

        public override DateTime LastWriteTimeUtc
        {
            get
            {
                if (LastModified.HasValue && base.LastWriteTimeUtc == DateTime.MinValue)
                {
                    return LastModified.Value.UtcDateTime;
                }

                return base.LastWriteTimeUtc;
            }
        }

        public string Name { get; set; }

        public string Container { get; set; }

        public string Url { get; set; }
    }
}