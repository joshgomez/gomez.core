﻿namespace Gomez.Core.Storage
{
    public class BlobProperties : BlobBaseProperties
    {
        public static readonly BlobProperties Empty = new BlobProperties
        {
            Security = BlobSecurity.Private,
        };

        public BlobProperties WithContentDispositionFilename(string filename)
        {
            ContentDisposition = $"attachment; filename=\"{filename}\"";
            return this;
        }
    }
}