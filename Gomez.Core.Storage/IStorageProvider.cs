﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Gomez.Core.Storage
{
    public interface IStorageProvider : IDisposable
    {
        StorageProvider Provider { get; }

        Task SaveBlobStreamAsync(string containerName, string blobName, Stream source, BlobProperties properties = null, bool closeStream = true, FileInfo info = null);

        Task<Stream> GetBlobStreamAsync(string containerName, string blobName);

        Task<BlobDescriptor> GetBlobDescriptorAsync(string containerName, string blobName);

        Task<IList<BlobDescriptor>> ListBlobsAsync(string containerName);

        Task DeleteBlobAsync(string containerName, string blobName);

        Task DeleteContainerAsync(string containerName);

        Task<bool> ContainerExistsAsync(string containerName);

        Task<bool> BlobExistsAsync(string containerName, string blobName);

        Task CopyBlobAsync(string sourceContainerName, string sourceBlobName, string destinationContainerName, string destinationBlobName = null);

        Task MoveBlobAsync(string sourceContainerName, string sourceBlobName, string destinationContainerName, string destinationBlobName = null);

        Task UpdateBlobPropertiesAsync(string containerName, string blobName, BlobProperties properties, FileInfo info = null);

        string GetBlobUrl(string containerName, string blobName);

        string GetBlobSasUrl
        (
            string containerName, string blobName, DateTimeOffset expiry, bool isDownload = false,
            string fileName = null, string contentType = null, BlobUrlAccess access = BlobUrlAccess.Read
        );
    }
}