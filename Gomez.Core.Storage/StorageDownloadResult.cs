﻿using System.Net;

namespace Gomez.Core.Storage
{
    public class StorageDownloadResult
    {
        public StorageDownloadResult()
        {
            Status = HttpStatusCode.InternalServerError;
        }

        public HttpStatusCode Status { get; set; }
        public StorageFileResponse Response { get; set; }
    }
}