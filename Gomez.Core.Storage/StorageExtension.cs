﻿using System.IO;
using System.Threading.Tasks;

namespace Gomez.Core.Storage
{
    public static class StorageExtension
    {
        public static async Task<StorageFileResponse> GetFileResponse(this IStorageProvider provider, string container, string filename, long streamPosition = 0)
        {
            if (!await provider.BlobExistsAsync(container, filename))
            {
                return null;
            }

            var model = new StorageFileResponse();
            using (var stream = await provider.GetBlobStreamAsync(container, filename))
            {
                await stream.CopyToAsync(model.MemoryStream);
            }

            string ext = System.IO.Path.GetExtension(filename);
            model.MemoryStream.Position = streamPosition;
            model.FileName = System.IO.Path.GetFileName(filename);
            model.MimeType = MimeMapping.MimeUtility.GetMimeMapping($"empty{ext}");
            return model;
        }

        public static async Task SaveBlobStreamAsync(this IStorageProvider provider, string containerName, string blobName, string localFilePath, BlobProperties properties = null, bool closeStream = true)
        {
            if (!File.Exists(localFilePath))
            {
                return;
            }

            var info = new FileInfo(localFilePath);
            await provider.SaveBlobStreamAsync(containerName, blobName, info.OpenRead(), properties, closeStream, info);
        }

        public static async Task SaveBlobStreamAsync(this IStorageProvider provider, string containerName, string blobName, string localFilePath, FileInfo info, BlobProperties properties = null, bool closeStream = true)
        {
            if (!File.Exists(localFilePath))
            {
                return;
            }

            await provider.SaveBlobStreamAsync(containerName, blobName, info.OpenRead(), properties, closeStream, info);
        }

        public static async Task SaveBlobStreamToLocalAsync(this IStorageProvider provider, string containerName, string blobName, string localFilePath, BlobProperties properties = null)
        {
            if (string.IsNullOrEmpty(localFilePath))
            {
                return;
            }

            var dir = Path.GetDirectoryName(localFilePath);
            Directory.CreateDirectory(dir);

            using (var stream = await provider.GetBlobStreamAsync(containerName, blobName))
            using (var fileStream = File.Create(localFilePath))
            {
                stream.Seek(0, SeekOrigin.Begin);
                await stream.CopyToAsync(fileStream);
            }

            if (properties != null)
            {
                File.SetCreationTimeUtc(localFilePath, properties.CreationTimeUtc);
                File.SetLastWriteTimeUtc(localFilePath, properties.LastWriteTimeUtc);
            }
        }
    }
}