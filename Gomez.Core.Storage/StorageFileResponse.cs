﻿using System;
using System.IO;

namespace Gomez.Core.Storage
{
    public class StorageFileResponse : IDisposable
    {
        public StorageFileResponse()
        {
            MemoryStream = new MemoryStream();
        }

        public MemoryStream MemoryStream { get; set; }
        public string MimeType { get; set; }
        public string FileName { get; set; }

        public long ContentLength
        {
            get { return MemoryStream?.Length ?? 0; }
        }

        #region IDisposable Support

        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing && MemoryStream != null)
                {
                    MemoryStream.Dispose();
                    MemoryStream = null;
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion IDisposable Support
    }
}