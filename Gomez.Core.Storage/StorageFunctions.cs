﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace Gomez.Core.Storage
{
    public static class StorageFunctions
    {
        public static BlobProperties SetMetaData(BlobProperties properties, FileInfo info)
        {
            if (info == null)
            {
                return properties;
            }

            properties.Metadata = properties.Metadata ?? new Dictionary<string, string>();
            properties.Metadata[StorageMetaData.LastWriteTimeUtc] = info.LastWriteTimeUtc.ToString(CultureInfo.InvariantCulture);
            properties.Metadata[StorageMetaData.CreationTimeUtc] = info.CreationTimeUtc.ToString(CultureInfo.InvariantCulture);

            return properties;
        }

        public static BlobDescriptor SetMetaData(BlobDescriptor properties, FileInfo info)
        {
            if (info == null)
            {
                return properties;
            }

            properties.Metadata = properties.Metadata ?? new Dictionary<string, string>();
            properties.Metadata[StorageMetaData.LastWriteTimeUtc] = info.LastWriteTimeUtc.ToString(CultureInfo.InvariantCulture);
            properties.Metadata[StorageMetaData.CreationTimeUtc] = info.CreationTimeUtc.ToString(CultureInfo.InvariantCulture);

            return properties;
        }
    }
}