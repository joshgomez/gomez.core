﻿namespace Gomez.Core.Storage
{
    public static class StorageMetaData
    {
        public const string LastWriteTimeUtc = "LastWriteTimeUtc";
        public const string CreationTimeUtc = "CreationTimeUtc";
    }
}