using Microsoft.DotNet.PlatformAbstractions;
using System;
using System.IO;
using System.Linq;

namespace Gomez.Core.UnitTest
{
    public abstract class UnitTestBase
    {
        protected virtual string TestDataFolder
        {
            get
            {
                return GetTestDataFolderInternal();
            }
        }

        private string GetTestDataFolderInternal()
        {
            return Path.Combine(GetProjectPathInternal(), "TestData");
        }

        protected virtual string GetTestDataFolder(string folder)
        {
            var dir = Path.Combine(TestDataFolder, folder);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            return dir;
        }

        protected virtual string GetFilePath(string folder, string filename)
        {
            return Path.Combine(GetTestDataFolder(folder), Path.GetFileName(filename));
        }

        private string GetProjectPathInternal()
        {
            string startupPath = ApplicationEnvironment.ApplicationBasePath;
            var pathItems = startupPath.Split(Path.DirectorySeparatorChar);
            var pos = pathItems.Reverse().ToList().FindIndex(x => string.Equals("bin", x));
            string projectPath = string.Join(Path.DirectorySeparatorChar.ToString(), pathItems.Take(pathItems.Length - pos - 1));
            return projectPath;
        }
    }
}