using SpreadsheetLight;
using System.IO;
using Xunit;

namespace Gomez.Core.UnitTest
{
    public class UnitTestExcel : UnitTestBase
    {
        private const string FOLDER = "Excel";

        private string GetFilePath(string filename)
        {
            return GetFilePath(FOLDER, filename);
        }

        [Fact(Skip = "Requires gdi+")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "xUnit1004:Test methods should not be skipped", Justification = "<Pending>")]
        public void TestExcel()
        {
            using var slDoc = new SLDocument();
            string filePath = GetFilePath("excel.xlsx");
            slDoc.SetCellValue(1, 1, "Hello World");
            slDoc.SetCellValue(1, 2, 1000);
            Assert.Equal(1000, slDoc.GetCellValueAsInt32(1, 2));
            slDoc.SaveAs(filePath);
            Assert.True(File.Exists(filePath));
        }
    }
}