using Gomez.Core.Utilities.Game.Models.Cards;
using Gomez.Core.Utilities.Game.Models.Slots;
using System.Linq;
using Xunit;
using Roulette = Gomez.Core.Utilities.Game.Models.Roulette;

namespace Gomez.Core.UnitTest
{
    public class UnitTestGame : UnitTestBase
    {
        private const string FOLDER = "Games";

        protected string GetFilePath(string filename)
        {
            return GetFilePath(FOLDER, filename);
        }

        [Fact]
        public void BlackJackSave_Equal_True()
        {
            IBlackJack blackjack = new BlackJack(100);
            blackjack.Hit();
            var saveFile = blackjack.Save();

            var existingGame = BlackJack.Load(saveFile);
            var saveFile2 = existingGame.Save();

            Assert.Equal(existingGame.State, blackjack.State);
            Assert.Equal(saveFile2.Deck.Count, saveFile.Deck.Count);
        }

        [Fact]
        public void BlackJackCardOrder_Equal_True()
        {
            IBlackJack blackjack = new BlackJack(100);
            var saveFile = blackjack.Save();

            var existingGame = BlackJack.Load(saveFile);
            existingGame.Hit();
            var saveFile2 = existingGame.Save();

            Assert.Equal(saveFile2.PlayersCards[0].Order, saveFile.PlayersCards[0].Order);
            Assert.Equal(saveFile2.PlayersCards[1].Order, saveFile.PlayersCards[1].Order);
        }

        [Fact]
        public void SlotMachine_CountSlots_3()
        {
            ISlotMachine slotMachine = new SlotMachine(1);
            Assert.True(slotMachine.Slots.Count() == 3);
            slotMachine.NewGame();
            Assert.True(slotMachine.Slots.Count() == 3);
        }

        [Fact]
        public void RouletteSlots_Count_37()
        {
            var test = Roulette.RouletteTable.GetSlots();
            Assert.Equal(37, test.Count());
        }
    }
}