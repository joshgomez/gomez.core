using Gomez.Core.MachineLearning;
using System.IO;
using System.IO.Compression;
using System.Net;
using Xunit;

namespace Gomez.Core.UnitTest
{
    public class UnitTestMachineLearning : UnitTestBase
    {
        private const string FOLDER = "MachineLearning";

        private string GetFilePath(string filename)
        {
            return GetFilePath(FOLDER, filename);
        }

        private string GetFolder()
        {
            return base.GetTestDataFolder(FOLDER);
        }

        [Fact]
        public void DetectSpam_Contains_TrueFalse()
        {
            var smsSpamFilePath = GetFilePath("SMSSpamCollection");
            var downloadPath = GetFilePath("spam.zip");
            var trainedPath = GetFilePath("trained.zip");

            if (!File.Exists(smsSpamFilePath))
            {
                using (var client = new WebClient())
                {
                    //The code below will download a dataset from a third-party, UCI (link), and may be governed by separate third-party terms.
                    //By proceeding, you agree to those separate terms.
                    client.DownloadFile("https://archive.ics.uci.edu/ml/machine-learning-databases/00228/smsspamcollection.zip", downloadPath);
                }

                ZipFile.ExtractToDirectory(downloadPath, GetFolder());
            }

            using var spamDetection = new SpamDetection(trainedPath);
            spamDetection.LoadTrainedModel();
            if (!File.Exists(trainedPath))
            {
                spamDetection.SetData(smsSpamFilePath);
                spamDetection.Train(10);
            }

            spamDetection.SetEngine();

            Assert.False(spamDetection.Predict("That's a great idea. It should work."));
            Assert.True(spamDetection.Predict("free medicine winner! congratulations"));
            Assert.False(spamDetection.Predict("Yes we should meet over the weekend!"));
            Assert.True(spamDetection.Predict("you win pills and free entry vouchers"));
        }
    }
}