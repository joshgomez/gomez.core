using Gomez.Core.Utilities.Game;
using Xunit;

namespace Gomez.Core.UnitTest
{
    public class UnitTestNameGen : UnitTestBase
    {
        private const string FOLDER = "Names";

        private string GetFilePath(string filename)
        {
            return GetFilePath(FOLDER, filename);
        }

        protected string GetFolder()
        {
            return base.GetTestDataFolder(FOLDER);
        }

        [Fact]
        public void Name_NotNull_True()
        {
            using var gen = new RandomName();
            gen.LoadFemaleNames(GetFilePath("indian_female_names.json"));
            gen.LoadMaleNames(GetFilePath("indian_male_names.json"));
            gen.LoadLastNames(GetFilePath("indian_last_names.json"));

            var name = gen.Generate(Utilities.Game.Models.Sex.Female);
            Assert.NotNull(name);
        }
    }
}