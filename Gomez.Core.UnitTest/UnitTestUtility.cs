using Gomez.Core.Utilities.AsyncBridge;
using System.Threading.Tasks;
using Xunit;

namespace Gomez.Core.UnitTest
{
    public class UnitTestUtility : UnitTestBase
    {
        private const string FOLDER = "Utilities";

        protected string GetFilePath(string filename)
        {
            return GetFilePath(FOLDER, filename);
        }

        [Fact]
        public void TestValidationUrls()
        {
            Assert.True(Utilities.UriUtility.ValidateUrlFromList("http://google.com", "google.com"));
            Assert.False(Utilities.UriUtility.ValidateUrlFromList("http://google.com", "google2.com"));
            Assert.True(Utilities.UriUtility.ValidateUrlFromList("http://google.com:80", "google.com"));
            Assert.False(Utilities.UriUtility.ValidateUrlFromList("http://google.com:81", "google.com:80"));

            Assert.True(Utilities.UriUtility.ValidateUrlFromList("http://google.com?search=this", "google.com"));
            Assert.True(Utilities.UriUtility.ValidateUrlFromList("http://google.com:80?search=this", "google.com"));
        }

        [Fact]
        public void TestNamesFromEmails()
        {
            Assert.Equal("Gomez", Utilities.StringUtility.GetNamesFromEmail("gomez@gomez.com"));
            Assert.Equal("Josh", Utilities.StringUtility.GetNamesFromEmail("josh90@gomez.com"));
            Assert.Equal("Josh Gomez", Utilities.StringUtility.GetNamesFromEmail("josh.gomez@gomez.com"));
        }

        [Fact]
        public void TestAsyncBridge()
        {
            async Task<string> AsyncString()
            {
                await Task.Delay(1000);
                return "TestAsync";
            }

            string string1 = "";
            string string2 = "";

            using (var A = AsyncHelper.Wait)
            {
                A.Run(AsyncString(), res => string1 = res);
                A.Run(AsyncString(), res => string2 = res);
            }

            Assert.Equal("TestAsync", string1);
            Assert.Equal("TestAsync", string2);
        }
    }
}