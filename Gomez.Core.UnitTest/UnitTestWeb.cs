using Gomez.Core.Web.Helpers;
using System;
using Xunit;

namespace Gomez.Core.UnitTest
{
    public class UnitTestWeb : UnitTestBase
    {
        private const string FOLDER = "Web";

        protected string GetFilePath(string filename)
        {
            return GetFilePath(FOLDER, filename);
        }

        [Fact]
        public void Guid_TryParse_EqualStr()
        {
            string guidStr = "08d76b69-dceb-0654-ec3a-9ae5ff193df5";
            Assert.True(Guid.TryParse(guidStr, out Guid result));
            Assert.Equal(new Guid(guidStr).ToString(), result.ToString());
        }

        [Fact]
        public void SanitizeHtml_Contains_SafeText()
        {
            var sanitizer = new SanitizeHelper();
            Assert.Contains("<b>", sanitizer.ToSafeHtml("[b]test[/b]"));
            string colorHtml = sanitizer.ToSafeHtml("[color=red]test[/color]");
            Assert.Contains("<span", colorHtml);

            string text = sanitizer.ToSafeText("<b>Test</b>");
            Assert.Contains("Test", text);

            string urlHtml = sanitizer.ToSafeHtml("[url=www.google.com]google[/url]");
            Assert.Contains("[url", urlHtml);
        }
    }
}