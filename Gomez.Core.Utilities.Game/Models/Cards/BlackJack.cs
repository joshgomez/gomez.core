﻿using System.Collections.Generic;
using System.Linq;

namespace Gomez.Core.Utilities.Game.Models.Cards
{
    public class BlackJack : IBlackJack
    {
        private const int STARTING_HAND = 2;
        private const int BLACKJACK_SCORE = 21;

        public enum StateType
        {
            Undetermined,
            Draw,
            Winning,
            Losing,
            BlackJack,
        }

        public StateType State { get; private set; }
        public decimal Stake { get; private set; }

        private readonly Deck _deck;
        private Hand _playerHand;
        private Hand _dealerHand;

        public int PlayerScore => _playerHand.Score;
        public int DealerScore => _dealerHand.Score;
        public bool CanStay => _dealerHand.Score <= 17;

        public BlackJack(decimal stake)
        {
            _deck = new Deck();
            NewGame(stake);
        }

        private BlackJack(Deck deck)
        {
            _deck = deck;
        }

        public SaveData Save()
        {
            return new SaveData()
            {
                Deck = _deck.Cards.OrderByDescending(x => x.Order).ToList(),
                PlayersCards = _playerHand.Cards.OrderByDescending(x => x.Order).ToList(),
                DealersCards = _dealerHand.Cards.OrderByDescending(x => x.Order).ToList(),
                Stake = Stake
            };
        }

        public static IBlackJack Load(SaveData data)
        {
            var game = new BlackJack(new Deck() { Cards = data.Deck })
            {
                Stake = data.Stake,
                _playerHand = new Hand(data.PlayersCards),
                _dealerHand = new Hand(data.DealersCards)
            };

            game.SetState();
            return game;
        }

        public void NewGame(decimal stake)
        {
            _deck.ShuffleNew();

            Stake = stake;
            _playerHand = new Hand(STARTING_HAND, _deck);
            _dealerHand = new Hand(1, _deck);
            SetState();
        }

        public void Hit()
        {
            if (State != StateType.Undetermined)
            {
                return;
            }

            var card = _deck.DrawCard(_playerHand);
            _playerHand.AddValue(card);
            SetState();
        }

        public void Stay()
        {
            if (State != StateType.Undetermined)
            {
                return;
            }

            if (CanStay)
            {
                var card = _deck.DrawCard(_dealerHand);
                _dealerHand.AddValue(card);
            }

            SetState();
        }

        public IEnumerable<string> DealersCards() => _dealerHand.ShowCards();

        public IEnumerable<string> PlayersCards() => _playerHand.ShowCards();

        private bool IsLosingScore(int score) => _playerHand.Score == score && _dealerHand.Score == score;

        public decimal Profit
        {
            get
            {
                return State switch
                {
                    StateType.BlackJack => Stake * 2.5m,
                    StateType.Draw => Stake,
                    StateType.Winning => Stake * 2.0m,
                    _ => -Stake,
                };
            }
        }

        private void SetState()
        {
            int[] losingScores = new int[] { 17, 18, 19 };
            //losing the stake
            if (_playerHand.Score > BLACKJACK_SCORE || losingScores.Any(x => IsLosingScore(x)))
            {
                State = StateType.Losing;
                return;
            }

            //Get the stake x2
            if (_dealerHand.Score > BLACKJACK_SCORE)
            {
                State = StateType.Winning;
                return;
            }

            //Get back the stake
            if (_dealerHand.Score == _playerHand.Score)
            {
                State = StateType.Draw;
                return;
            }

            //Get 2.5x the stake
            if (_playerHand.IsBlackJack)
            {
                State = StateType.BlackJack;
                return;
            }

            //Get 2x the stake
            if (_playerHand.Score > _dealerHand.Score && _dealerHand.Score >= losingScores[0])
            {
                State = StateType.Winning;
                return;
            }

            State = StateType.Undetermined;
        }

        protected class Hand : Cards.Hand
        {
            public Hand(IEnumerable<Card> cards) : base(cards)
            {
                AddValues(cards);
            }

            public Hand(int startingHand, Deck deck) : base(startingHand, deck)
            {
                AddValues(Cards);
            }

            public bool IsBlackJack => Score == BLACKJACK_SCORE;

            public void AddValues(IEnumerable<Card> cards)
            {
                Score = 0;
                foreach (var card in cards)
                {
                    AddValue(card);
                }
            }

            public void AddValue(Card drawn)
            {
                if (drawn.Value == Card.CardValueType.Ace)
                {
                    if (Score <= 10)
                    {
                        Score += 11;
                        return;
                    }

                    Score += 1;
                    return;
                }

                if (drawn.Value == Card.CardValueType.Jack
                    || drawn.Value == Card.CardValueType.Queen
                    || drawn.Value == Card.CardValueType.King)
                {
                    Score += 10;
                    return;
                }

                Score += (int)drawn.Value;
            }
        }
    }
}