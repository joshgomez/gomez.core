﻿using System;

namespace Gomez.Core.Utilities.Game.Models.Cards
{
    public class Card
    {
        public Card()
        {
            Id = Guid.NewGuid();
        }

        public Card(CardValueType value, CardSuitType suit) : this()
        {
            Value = value;
            Suit = suit;
        }

        public Card(string value, string suit) : this()
        {
            if (Enum.TryParse(value, out CardValueType cardValue))
            {
                Value = cardValue;
            }

            if (Enum.TryParse(suit, out CardSuitType cardSuit))
            {
                Suit = cardSuit;
            }
        }

        public Card(int value, int suit) : this(value.ToString(), suit.ToString())
        {
        }

        public Guid Id { get; set; }
        public int Order { get; set; }
        public CardValueType Value { get; set; }
        public CardSuitType Suit { get; set; }

        public enum CardValueType
        {
            Ace = 1,
            Two = 2,
            Three = 3,
            Four = 4,
            Five = 5,
            Six = 6,
            Seven = 7,
            Eight = 8,
            Nine = 9,
            Ten = 10,
            Jack = 11,
            Queen = 12,
            King = 13
        }

        public enum CardSuitType
        {
            Hearts = 1,
            Spades = 2,
            Clubs = 3,
            Diamonds = 4
        }
    }
}