﻿using System.Collections.Generic;
using System.Linq;

namespace Gomez.Core.Utilities.Game.Models.Cards
{
    public class Deck
    {
        public List<Card> Cards { get; set; }

        public Deck()
        {
            Cards = new List<Card>();
        }

        public Card DrawCard(Hand hand)
        {
            Card drawn = Cards[Cards.Count - 1];
            Cards.Remove(drawn);
            hand.Cards.Push(drawn);
            return drawn;
        }

        public void ShuffleNew()
        {
            if (Cards.Any())
                Cards.Clear();

            for (int i = 1; i < 5; i++)
            {
                for (int j = 1; j < 14; j++)
                {
                    Cards.Add(new Card(j, i));
                }
            }

            Cards = Cards.OrderBy(x => x.Id).ToList();
            int order = 0;
            Cards.ForEach(x =>
            {
                order++;
                x.Order = order;
            });
        }
    }
}