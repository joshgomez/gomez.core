﻿using System.Collections.Generic;
using System.Linq;

namespace Gomez.Core.Utilities.Game.Models.Cards
{
    public abstract class Hand
    {
        public int Score { get; protected set; }
        public Stack<Card> Cards { get; set; }

        protected Hand(IEnumerable<Card> cards)
        {
            Cards = new Stack<Card>(cards);
        }

        protected Hand(int startingHand, Deck deck)
        {
            if (deck == null) throw new DeckException("No decks available to draw from!");
            else if (deck.Cards.Count == 0) throw new DeckException("No more cards to draw!");
            else
            {
                Cards = new Stack<Card>();
                for (int i = 0; i < startingHand; i++)
                {
                    deck.DrawCard(this);
                }
            }
        }

        public IEnumerable<string> ShowCards() => Cards
            .OrderByDescending(x => x.Order)
            .Select(x => $"{x.Value}_{x.Suit}");
    }
}