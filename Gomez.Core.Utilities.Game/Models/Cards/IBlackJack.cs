﻿using System.Collections.Generic;

namespace Gomez.Core.Utilities.Game.Models.Cards
{
    public interface IBlackJack
    {
        int PlayerScore { get; }
        int DealerScore { get; }

        decimal Profit { get; }
        decimal Stake { get; }

        bool CanStay { get; }
        BlackJack.StateType State { get; }

        void Hit();

        void NewGame(decimal stake);

        void Stay();

        SaveData Save();

        IEnumerable<string> DealersCards();

        IEnumerable<string> PlayersCards();
    }
}