﻿using System.Collections.Generic;

namespace Gomez.Core.Utilities.Game.Models.Cards
{
    public class SaveData
    {
        public decimal Stake { get; set; }
        public List<Card> Deck { get; set; }
        public List<Card> PlayersCards { get; set; }
        public List<Card> DealersCards { get; set; }
    }
}