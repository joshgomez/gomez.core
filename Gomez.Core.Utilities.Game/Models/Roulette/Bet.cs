﻿namespace Gomez.Core.Utilities.Game.Models.Roulette
{
    public class Bet : IBet
    {
        protected Bet()
        {
        }

        public static Bet SetNumber(int number)
        {
            return new Bet() { Number = number };
        }

        public static Bet SetOneToEighteen()
        {
            return new Bet() { OneToEighteen = true };
        }

        public static Bet SetNineteenToThirtySix()
        {
            return new Bet() { NineteenToThirtySix = true };
        }

        public EvenOrOdd? EvenOrOdd { get; set; }
        public SlotColor? Color { get; set; }
        public int Number { get; private set; }
        public bool OneToEighteen { get; private set; }
        public bool NineteenToThirtySix { get; private set; }
    }
}