﻿namespace Gomez.Core.Utilities.Game.Models.Roulette
{
    public interface IBet
    {
        SlotColor? Color { get; set; }
        EvenOrOdd? EvenOrOdd { get; set; }
        bool NineteenToThirtySix { get; }
        int Number { get; }
        bool OneToEighteen { get; }
    }
}