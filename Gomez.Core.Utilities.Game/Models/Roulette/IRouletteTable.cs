﻿namespace Gomez.Core.Utilities.Game.Models.Roulette
{
    public interface IRouletteTable
    {
        int Number { get; }
        bool IsBlack { get; }
        bool IsEven { get; }
        bool IsNineteenToThirtySix { get; }
        bool IsNumber { get; }
        bool IsOdd { get; }
        bool IsOneToEighteen { get; }
        bool IsRed { get; }
        decimal Profit { get; }
        decimal Stake { get; }
    }
}