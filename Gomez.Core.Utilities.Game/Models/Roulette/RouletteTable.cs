﻿using System.Collections.Generic;
using System.Linq;
using U = Gomez.Core.Utilities;

namespace Gomez.Core.Utilities.Game.Models.Roulette
{
    public class RouletteTable : IRouletteTable
    {
        private readonly static int[] _redNumbers = new int[] { 1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36 };
        private readonly static int[] _blackNumbers = new int[] { 2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 24, 26, 28, 29, 31, 33, 35 };
        private readonly Bet _playerBet;

        public static IEnumerable<Slot> GetSlots()
        {
            yield return new Slot(0, Slot.ColorType.Green);
            for (int i = 1; i <= 36; i++)
            {
                if (_redNumbers.Contains(i))
                {
                    yield return new Slot(i, Slot.ColorType.Red);
                }
                else
                {
                    yield return new Slot(i, Slot.ColorType.Black);
                }
            }
        }

        public int Number { get; }
        public decimal Stake { get; private set; }

        public RouletteTable(Bet playerBet, decimal stake)
        {
            Stake = stake;
            _playerBet = playerBet;
            Number = U.NumberUtility.RandomBetween(0, 36);
        }

        public bool IsRed => _redNumbers.Contains(Number) && _playerBet.Color == SlotColor.Red;
        public bool IsBlack => _blackNumbers.Contains(Number) && _playerBet.Color == SlotColor.Black;
        public bool IsEven => Number % 2 == 0 && _playerBet.EvenOrOdd == EvenOrOdd.Even;
        public bool IsOdd => Number % 2 != 0 && _playerBet.EvenOrOdd == EvenOrOdd.Odd;
        public bool IsOneToEighteen => Number <= 18 && _playerBet.OneToEighteen;
        public bool IsNineteenToThirtySix => Number >= 19 && _playerBet.NineteenToThirtySix;
        public bool IsNumber => Number == _playerBet.Number;

        public decimal Profit
        {
            get
            {
                if (Number == 0)
                {
                    return -Stake;
                }

                if (IsNumber) return Stake * 36m;
                if (_playerBet.Color != null && (!IsRed && !IsBlack))
                {
                    return -Stake;
                }

                if (_playerBet.EvenOrOdd != null && (!IsEven && !IsOdd))
                {
                    return -Stake;
                }

                int multiplier = 0;
                if (IsEven || IsOdd)
                {
                    multiplier += 1;
                }

                if (IsRed || IsBlack)
                {
                    multiplier += 1;
                }

                if (IsOneToEighteen || IsNineteenToThirtySix)
                {
                    multiplier += 1;
                }

                if (multiplier > 0)
                {
                    return Stake * multiplier;
                }

                return -Stake;
            }
        }
    }
}