﻿namespace Gomez.Core.Utilities.Game.Models.Roulette
{
    public class Slot
    {
        public enum ColorType
        {
            Green,
            Red,
            Black,
        }

        public Slot()
        {
        }

        public Slot(int number, ColorType color)
        {
            Number = number;
            Color = color;
        }

        public int Number { get; set; }
        public ColorType Color { get; set; }
    }
}