﻿namespace Gomez.Core.Utilities.Game.Models
{
    public enum Sex
    {
        Male,
        Female
    }
}