﻿using System.Collections.Generic;

namespace Gomez.Core.Utilities.Game.Models.Slots
{
    public interface ISlotMachine
    {
        decimal Credits { get; }
        decimal Profit { get; }
        IEnumerable<SlotItem> Slots { get; }

        void NewGame();
    }
}