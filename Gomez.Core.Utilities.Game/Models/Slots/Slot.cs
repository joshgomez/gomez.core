﻿using System;
using U = Gomez.Core.Utilities;

namespace Gomez.Core.Utilities.Game.Models.Slots
{
    internal class Slot
    {
        public SlotItem Item { get; }

        public Slot(SlotItem[] slotItems)
        {
            if (slotItems == null)
            {
                throw new ArgumentNullException(nameof(slotItems));
            }

            if (slotItems.Length != 32)
            {
                throw new ArgumentOutOfRangeException(nameof(slotItems));
            }

            int remainder = U.NumberUtility.RandomBetween(10000, 20000) % 32;
            Item = slotItems[remainder];
        }
    }
}