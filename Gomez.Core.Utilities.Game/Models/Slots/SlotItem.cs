﻿namespace Gomez.Core.Utilities.Game.Models.Slots
{
    public enum SlotItem
    {
        Grape,
        Apple,
        WildCherry,
        Bell,
        Bar,
        LuckySeven
    }
}