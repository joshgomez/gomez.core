﻿using System.Collections.Generic;
using System.Linq;

namespace Gomez.Core.Utilities.Game.Models.Slots
{
    public class SlotMachine : ISlotMachine
    {
        private readonly SlotItem[] _slotItems = new SlotItem[32];
        private readonly List<Slot> _slots = new List<Slot>();

        public decimal Credits { get; private set; }

        /// <summary>
        /// Default machine with 3 slots
        /// </summary>
        public SlotMachine(decimal credits) : this(credits, 3)
        {
        }

        public SlotMachine(decimal credits, int slots)
        {
            Credits = credits;
            InitSlots();
            NewGame(slots);
        }

        private void NewGame(int slots)
        {
            _slots.Clear();
            for (int i = 0; i < slots; i++)
            {
                _slots.Add(new Slot(_slotItems));
            }
        }

        public void NewGame()
        {
            NewGame(3);
        }

        public IEnumerable<SlotItem> Slots => _slots.Select(x => x.Item);

        public decimal Profit
        {
            get
            {
                decimal GetSameOfKind()
                {
                    if (_slots.All(x => x.Item == SlotItem.Grape)) return 1 * Credits;
                    if (_slots.All(x => x.Item == SlotItem.Apple)) return 2 * Credits;
                    if (_slots.All(x => x.Item == SlotItem.WildCherry)) return 3 * Credits;
                    if (_slots.All(x => x.Item == SlotItem.Bell)) return 7 * Credits;
                    if (_slots.All(x => x.Item == SlotItem.Bar)) return 13 * Credits;
                    if (_slots.All(x => x.Item == SlotItem.LuckySeven)) return 32 * Credits;
                    return -Credits;
                }

                return GetSameOfKind();
            }
        }

        /// <summary>
        /// Referenced later for the random modulus lookups using the remainder!
        /// here are the odds
        /// </summary>
        private void InitSlots()
        {
            for (int i = 0; i < 8; i++)
                _slotItems[i] = SlotItem.Grape;
            for (int i = 8; i < 15; i++)
                _slotItems[i] = SlotItem.Apple;
            for (int i = 15; i < 21; i++)
                _slotItems[i] = SlotItem.WildCherry;
            for (int i = 21; i < 26; i++)
                _slotItems[i] = SlotItem.Bell;
            for (int i = 26; i < 30; i++)
                _slotItems[i] = SlotItem.Bar;
            for (int i = 30; i < 32; i++)
                _slotItems[i] = SlotItem.LuckySeven;
        }
    }
}