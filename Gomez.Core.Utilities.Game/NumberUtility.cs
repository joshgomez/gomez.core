﻿using System;

namespace Gomez.Core.Utilities.Game
{
    public static class NumberUtility
    {
        private static double NextLevelInternal(int level, int initialXp)
        {
            double factor = level * Math.Log10(level) + 1d;
            return factor * initialXp;
        }

        public static double Nextlevel(int level, int initialXp = 100)
        {
            return Utilities.NumberUtility.RoundToNearestHundred(NextLevelInternal(level, initialXp));
        }

        public static double ScalingMaxDiff(double a, double b, double maxDiff = 30)
        {
            double diff = a - b;
            if (diff > maxDiff)
            {
                return a - diff + maxDiff;
            }

            return a;
        }
    }
}