﻿using Gomez.Core.Utilities.Game.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using U = Gomez.Core.Utilities;

namespace Gomez.Core.Utilities.Game
{
    /// <summary>
    /// RandomName class, used to generate a random name.
    /// </summary>
    public class RandomName : IDisposable
    {
        public string DataSet { get; set; } = "EnglishNames";
        private readonly Dictionary<string, string> _initials;
        public string Initials => _initials[DataSet];

        private readonly Dictionary<string, Stream[]> _maleCollection;
        private readonly Dictionary<string, Stream[]> _femaleCollection;
        private readonly Dictionary<string, Stream[]> _lastNameCollection;

        private readonly Dictionary<string, int> _countMales;
        private readonly Dictionary<string, int> _countFemales;
        private readonly Dictionary<string, int> _countLastnames;

        public IEnumerable<string> MaleCollection => Names(_maleCollection[DataSet]);
        public IEnumerable<string> FemaleCollection => Names(_femaleCollection[DataSet]);
        public IEnumerable<string> LastNameCollection => Names(_lastNameCollection[DataSet]);

        public int CountMales => _countMales[DataSet];
        public int CountFemales => _countFemales[DataSet];
        public int CountLastNames => _countLastnames[DataSet];

        /// <summary>
        /// Initialises a new instance of the RandomName class.
        /// </summary>
        public RandomName()
        {
            _initials = new Dictionary<string, string>
            {
                [DataSet] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            };

            _maleCollection = new Dictionary<string, Stream[]>();
            _femaleCollection = new Dictionary<string, Stream[]>();
            _lastNameCollection = new Dictionary<string, Stream[]>();

            _countMales = new Dictionary<string, int>();
            _countFemales = new Dictionary<string, int>();
            _countLastnames = new Dictionary<string, int>();
        }

        private IEnumerable<string> Names(Stream[] collection)
        {
            var names = Enumerable.Empty<string>();
            for (int i = 0; i < collection.Length; i++)
            {
                collection[i].Position = 0;
                names = names.Union(StreamNamesFromFile(collection[i])).Distinct();
            }

            return names;
        }

        private int CountNames(Stream[] collection)
        {
            return Names(collection).Count();
        }

        public void LoadMaleNames(params string[] filePaths)
        {
            _maleCollection[DataSet] = new Stream[filePaths.Length];
            for (int i = 0; i < filePaths.Length; i++)
            {
                _maleCollection[DataSet][i] = StreamFromFile(filePaths[i]);
            }

            _countMales[DataSet] = CountNames(_maleCollection[DataSet]);
        }

        public void LoadFemaleNames(params string[] filePaths)
        {
            _femaleCollection[DataSet] = new Stream[filePaths.Length];
            for (int i = 0; i < filePaths.Length; i++)
            {
                _femaleCollection[DataSet][i] = StreamFromFile(filePaths[i]);
            }

            _countFemales[DataSet] = CountNames(_femaleCollection[DataSet]);
        }

        public void LoadLastNames(params string[] filePaths)
        {
            _lastNameCollection[DataSet] = new Stream[filePaths.Length];
            for (int i = 0; i < filePaths.Length; i++)
            {
                _lastNameCollection[DataSet][i] = StreamFromFile(filePaths[i]);
            }

            _countLastnames[DataSet] = CountNames(_femaleCollection[DataSet]);
        }

        private Stream StreamFromFile(string filePath)
        {
            return new FileStream(filePath, FileMode.Open, FileAccess.Read);
        }

        private IEnumerable<string> StreamNamesFromFile(Stream stream)
        {
            StreamReader sr = new StreamReader(stream);
            JsonTextReader reader = new JsonTextReader(sr);
            if (reader.Read() && reader.TokenType == JsonToken.StartArray)
            {
                // Load each object from the stream and do something with it
                while (reader.Read())
                {
                    if (reader.TokenType != JsonToken.String)
                    {
                        continue;
                    }

                    yield return reader.ReadAsString();
                }
            }
        }

        private string RandomMale()
        {
            return MaleCollection.Skip(U.NumberUtility.RandomBetween(0, CountMales - 1)).First();
        }

        private string RandomFemale()
        {
            return FemaleCollection.Skip(U.NumberUtility.RandomBetween(0, CountFemales - 1)).First();
        }

        private string RandomLastName()
        {
            if (CountLastNames == 0)
            {
                return string.Empty;
            }

            return LastNameCollection.Skip(U.NumberUtility.RandomBetween(0, CountLastNames - 1)).First();
        }

        /// <summary>
        /// Returns a new random name
        /// </summary>
        /// <param name="sex">The sex of the person to be named. true for male, false for female</param>
        /// <param name="middle">How many middle names do generate</param>
        /// <param name="isInital">Should the middle names be initials or not?</param>
        /// <returns>The random name as a string</returns>
        public string Generate(Sex sex, int middle = 0, bool isInital = false)
        {
            string first = sex == Sex.Male ? RandomMale() : RandomFemale(); // determines if we should select a name from male or female, and randomly picks
            string last = RandomLastName(); // gets the last name

            List<string> middles = new List<string>();

            for (int i = 0; i < middle; i++)
            {
                if (isInital)
                {
                    middles.Add(Initials[U.NumberUtility.RandomBetween(0, Initials.Length - 1)].ToString() + "."); // randomly selects an uppercase letter to use as the inital and appends a dot
                }
                else
                {
                    middles.Add(sex == Sex.Male ? RandomMale() : RandomFemale()); // randomly selects a name that fits with the sex of the person
                }
            }

            StringBuilder b = new StringBuilder();
            b.Append(first + " "); // put a space after our names
            foreach (string m in middles)
            {
                b.Append(m + " ");
            }

            if (!string.IsNullOrEmpty(last))
                b.Append(last);

            return b.ToString().Trim();
        }

        /// <summary>
        /// Generates a list of random names
        /// </summary>
        /// <param name="number">The number of names to be generated</param>
        /// <param name="maxMiddleNames">The maximum number of middle names</param>
        /// <param name="sex">The sex of the names, if null sex is randomised</param>
        /// <param name="initials">Should the middle names have initials, if null this will be randomised</param>
        /// <returns>List of strings of names</returns>
        public List<string> RandomNames(int number, int maxMiddleNames, Sex? sex = null, bool? initials = null)
        {
            List<string> names = new List<string>();

            for (int i = 0; i < number; i++)
            {
                if (sex != null && initials != null)
                {
                    names.Add(Generate((Sex)sex, U.NumberUtility.RandomBetween(0, maxMiddleNames), (bool)initials));
                }
                else if (sex != null)
                {
                    bool init = U.NumberUtility.RandomBetween(0, 1) != 0;
                    names.Add(Generate((Sex)sex, U.NumberUtility.RandomBetween(0, maxMiddleNames), init));
                }
                else if (initials != null)
                {
                    Sex s = (Sex)U.NumberUtility.RandomBetween(0, 1);
                    names.Add(Generate(s, U.NumberUtility.RandomBetween(0, maxMiddleNames), (bool)initials));
                }
                else
                {
                    Sex s = (Sex)U.NumberUtility.RandomBetween(0, 1);
                    bool init = U.NumberUtility.RandomBetween(0, 1) != 0;
                    names.Add(Generate(s, U.NumberUtility.RandomBetween(0, maxMiddleNames), init));
                }
            }

            return names;
        }

        #region IDisposable Support

        private bool disposedValue = false; // To detect redundant calls

        private void DisposeCollection(Dictionary<string, Stream[]> collection)
        {
            foreach (var streams in collection)
            {
                if (streams.Value == null)
                {
                    continue;
                }

                for (int i = 0; i < streams.Value.Length; i++)
                {
                    if (streams.Value[i] != null && streams.Value[i].CanRead)
                    {
                        streams.Value[i].Dispose();
                        streams.Value[i] = null;
                    }
                }
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    DisposeCollection(_femaleCollection);
                    DisposeCollection(_maleCollection);
                    DisposeCollection(_lastNameCollection);
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion IDisposable Support
    }
}