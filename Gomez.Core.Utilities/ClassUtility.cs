﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Gomez.Core.Utilities
{
    public static class ClassUtility
    {
        public static T GetPropertyValue<T>(object sourceInstance, string targetPropertyName)
        {
            if (sourceInstance == null || string.IsNullOrWhiteSpace(targetPropertyName))
            {
                return default;
            }

            Type returnType = typeof(T);
            Type sourceType = sourceInstance.GetType();

            PropertyInfo propertyInfo = sourceType.GetProperty(targetPropertyName, returnType);
            if (propertyInfo == null)
            {
                return default;
            }

            return (T)propertyInfo.GetValue(sourceInstance, null);
        }

        public static void SetPropertyValue<T>(object sourceInstance, string targetPropertyName, T value)
        {
            if (sourceInstance == null || string.IsNullOrWhiteSpace(targetPropertyName))
            {
                return;
            }

            Type returnType = typeof(T);
            Type sourceType = sourceInstance.GetType();

            PropertyInfo propertyInfo = sourceType.GetProperty(targetPropertyName, returnType);
            if (propertyInfo == null)
            {
                return;
            }

            propertyInfo.SetValue(sourceInstance, value, null);
        }

        private static IEnumerable<FieldInfo> GetConstantsInternal<T>(Type type)
        {
            FieldInfo[] fieldInfos = type.GetFields(BindingFlags.Public |
                 BindingFlags.Static | BindingFlags.FlattenHierarchy);

            return fieldInfos.Where(fi => fi.IsLiteral && !fi.IsInitOnly && fi.FieldType == typeof(T));
        }

        public static Dictionary<string, T> GetConstants<T>(Type type)
        {
            return GetConstants<T>(type, null);
        }

        public static Dictionary<string, T> GetConstants<T>(Type type, Func<FieldInfo, bool> filter)
        {
            var query = GetConstantsInternal<T>(type);
            if (filter != null)
            {
                query = query.Where(filter);
            }

            return query
                .ToDictionary(x => x.Name, x => (T)x.GetRawConstantValue());
        }
    }
}