﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Gomez.Core.Utilities
{
    public static class CultureInfoUtility
    {
        public static void Try(string cultureName, out CultureInfo ci)
        {
            try
            {
                ci = new CultureInfo(cultureName);
                return;
            }
            catch (CultureNotFoundException)
            {
                //Not found
            }

            ci = null;
        }

        public static IDictionary<string, string> CountryList()
        {
            return CountryListInternal().ToDictionary(x => x.Key, x => x.Value);
        }

        private static IEnumerable<KeyValuePair<string, string>> CountryListInternal()
        {
            var keys = new HashSet<string>();
            foreach (CultureInfo ci in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
            {
                RegionInfo ri = new RegionInfo(ci.Name);
                if (!keys.Contains(ri.TwoLetterISORegionName))
                {
                    keys.Add(ri.TwoLetterISORegionName.ToUpper());
                    yield return new KeyValuePair<string, string>(ri.TwoLetterISORegionName, ri.EnglishName);
                }
            }
        }
    }
}