﻿using System;

namespace Gomez.Core.Utilities
{
    public static class DateTimeUtility
    {
        public class DateTimeRange
        {
            public DateTime Start { get; set; }
            public DateTime End { get; set; }
        }

        public static DateTime ByTimeZoneUtc(DateTime date, TimeZoneInfo info)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(date, info);
        }

        public static DateTime Trim(DateTime date, long roundTicks)
        {
            return new DateTime(date.Ticks - date.Ticks % roundTicks);
        }

        public static int GetQuarter(DateTime date)
        {
            if (date.Month >= 1 && date.Month <= 3)
                return 1;
            else if (date.Month >= 4 && date.Month <= 7)
                return 2;
            else if (date.Month >= 8 && date.Month <= 10)
                return 3;
            else
                return 4;
        }

        public static DateTime GetLocalTime(DateTime datetime)
        {
            var timezone = TimeZoneInfo.Local;
            return TimeZoneInfo.ConvertTimeFromUtc(datetime, timezone);
        }

        public static DateTime FirstDayOfMonth(DateTime datetime)
        {
            return datetime.AddDays(1 - datetime.Day);
        }

        /// <summary>
        /// Convert a date time object to Unix time representation.
        /// </summary>
        /// <returns>Returns a numerical representation (Unix time) of the DateTime object.</returns>
        public static long ConvertToUnixTime(DateTime datetime)
        {
            DateTime sTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return (long)(datetime - sTime).TotalSeconds;
        }

        public static DateTime ToDateTimeFromUnixSeconds(long seconds)
        {
            DateTime sTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return sTime.AddSeconds(Convert.ToDouble(seconds));
        }

        public static DateTimeRange GetWeekRange(DateTime datetime)
        {
            datetime = datetime.Date;

            var dtmReturn = new DateTimeRange
            {
                Start = datetime.AddDays(-(int)datetime.DayOfWeek)
            };
            dtmReturn.End = dtmReturn.Start.AddDays(7).AddSeconds(-1);

            return dtmReturn;
        }

        public static DateTimeRange GetMonthRange(DateTime datetime)
        {
            datetime = datetime.Date;

            var dtmReturn = new DateTimeRange
            {
                Start = datetime.AddDays(1 - datetime.Day)
            };
            dtmReturn.End = dtmReturn.Start.AddMonths(1).AddSeconds(-1);

            return dtmReturn;
        }

        public static DateTimeRange GetQuarterRange(DateTime datetime)
        {
            datetime = datetime.Date;

            var intQuarter = (int)Math.Ceiling(datetime.Month / 3M);

            var intLastMonthOfQuarter = intQuarter * 3;
            var intFirstMonthOfQuarter = intLastMonthOfQuarter - 2;
            var intLastDayOfQuarter = DateTime.DaysInMonth(datetime.Year, intLastMonthOfQuarter);

            var dtmReturn = new DateTimeRange
            {
                Start = new DateTime(datetime.Year, intFirstMonthOfQuarter, 1),
                End = new DateTime(datetime.Year, intLastMonthOfQuarter, intLastDayOfQuarter)
            };

            return dtmReturn;
        }

        public static bool IsYearMonthLower(DateTime current, DateTime compareWith)
        {
            if ((current.Year == compareWith.Year && current.Month < compareWith.Month) || current.Year < compareWith.Year)
            {
                return true;
            }

            return false;
        }

        public static DateTime Truncate(DateTime dateTime, TimeSpan timeSpan)
        {
            if (timeSpan == TimeSpan.Zero) return dateTime; // Or could throw an ArgumentException
            return dateTime.AddTicks(-(dateTime.Ticks % timeSpan.Ticks));
        }

        public static int GetAge(DateTime birthdate)
        {
            var today = DateTime.UtcNow.Date;
            var age = today.Year - birthdate.Year;

            // Go back to the year the person was born in case of a leap year
            if (birthdate > today.AddYears(-age))
            {
                age--;
            }

            return age;
        }
    }
}