﻿using System;

namespace Gomez.Core.Utilities.Extensions
{
    public static class ArrayExtension
    {
        public static T ValueOrNull<T>(this Array arr, int index)
        {
            if (arr == null)
            {
                return default;
            }

            if (index < arr.Length)
            {
                return (T)(arr.GetValue(index));
            }

            return default;
        }

        public static string ValueOrNull(this string[] arr, int index)
        {
            return ValueOrNull<string>(arr, index);
        }

        public static bool IndexExists(this Array arr, int index)
        {
            if (arr == null)
            {
                return false;
            }

            if (index < arr.Length)
            {
                return true;
            }

            return false;
        }
    }
}