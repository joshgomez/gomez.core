﻿using System;

namespace Gomez.Core.Utilities.Extensions
{
    public static class DateTimeExtension
    {
        public static int ToAge(this DateTime birthdate)
        {
            return DateTimeUtility.GetAge(birthdate);
        }
    }
}