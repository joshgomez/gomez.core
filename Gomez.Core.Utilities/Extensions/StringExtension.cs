﻿namespace Gomez.Core.Utilities.Extensions
{
    public static class StringExtension
    {
        public static bool ExactMatch(this string source, string match)
        {
            return StringUtility.ExactMatch(source, match);
        }
    }
}