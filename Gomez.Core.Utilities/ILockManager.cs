﻿using System;

namespace Gomez.Core.Utilities
{
    public interface ILockManager
    {
        void GetLock(string key, Action action);

        TResult GetLock<TResult>(string key, Func<TResult> action);
    }
}