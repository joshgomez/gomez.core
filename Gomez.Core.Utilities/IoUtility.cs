﻿using System;
using System.IO;
using System.Text;

namespace Gomez.Core.Utilities
{
    public static class IoUtility
    {
        private const string NUMBER_PATTERN = " ({0})";
        private const int BYTES_TO_READ = sizeof(long);

        public static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        public static string ToMd5Hex(this FileInfo file)
        {
            using var fileStream = file.OpenRead();
            return StreamToMd5Hex(fileStream);
        }

        public static string StreamToMd5Hex(Stream stream)
        {
            using var fileMd5 = System.Security.Cryptography.MD5.Create();
            byte[] fileHash = fileMd5.ComputeHash(stream);
            return ByteUtility.ToHex(fileHash, true);
        }

        public static void Empty(DirectoryInfo directory)
        {
            foreach (FileInfo file in directory.GetFiles())
            {
                if (!IsFileLocked(file))
                {
                    file.Delete();
                }
            }

            foreach (DirectoryInfo subDirectory in directory.GetDirectories()) subDirectory.Delete(true);
        }

        public static string RenameTo(DirectoryInfo di, string name)
        {
            if (di == null)
            {
                throw new ArgumentNullException(nameof(di), "Directory info to rename cannot be null.");
            }

            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException("New name cannot be null or blank.", nameof(name));
            }

            string newPath = Path.Combine(di.Parent.FullName, name);
            di.MoveTo(newPath);

            return newPath;
        }

        public static DirectoryInfo CopyTo(DirectoryInfo sourceDir, string destinationPath, bool overwrite = false)
        {
            var sourcePath = sourceDir.FullName;

            var destination = new DirectoryInfo(destinationPath);

            destination.Create();

            foreach (var sourceSubDirPath in Directory.EnumerateDirectories(sourcePath, "*", SearchOption.AllDirectories))
                Directory.CreateDirectory(sourceSubDirPath.Replace(sourcePath, destinationPath));

            foreach (var file in Directory.EnumerateFiles(sourcePath, "*", SearchOption.AllDirectories))
                File.Copy(file, file.Replace(sourcePath, destinationPath), overwrite);

            return destination;
        }

        public static Stream ToStream(StringBuilder builder)
        {
            return ToStream(builder.ToString());
        }

        public static Stream ToStream(string s)
        {
            return ToStream(s, Encoding.UTF8);
        }

        public static Stream ToStream(string s, Encoding encoding)
        {
            return new MemoryStream(encoding.GetBytes(s ?? ""));
        }

        public static byte[] ToByteArray(Stream input)
        {
            return ToByteArrayInternal(input);
        }

        private static byte[] ToByteArrayInternal(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using MemoryStream ms = new MemoryStream();
            int read;
            while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                ms.Write(buffer, 0, read);
            }
            return ms.ToArray();
        }

        public static string ToBase64(MemoryStream memstream)
        {
            using (memstream)
            {
                return Convert.ToBase64String(memstream.ToArray());
            }
        }

        public static string NextAvailableFilename(string path)
        {
            // Short-cut if already available
            if (!File.Exists(path))
                return path;

            // If path has extension then insert the number pattern just before the extension and return next filename
            if (Path.HasExtension(path))
                return GetNextFilename(path.Insert(path.LastIndexOf(Path.GetExtension(path)), NUMBER_PATTERN));

            // Otherwise just append the pattern to the path and return next filename
            return GetNextFilename(path + NUMBER_PATTERN);
        }

        private static string GetNextFilename(string pattern)
        {
            string tmp = string.Format(pattern, 1);
            if (tmp == pattern)
                throw new ArgumentException("The pattern must include an index place-holder", nameof(pattern));

            if (!File.Exists(tmp))
                return tmp; // short-circuit if no matches

            int min = 1, max = 2; // min is inclusive, max is exclusive/untested

            while (File.Exists(string.Format(pattern, max)))
            {
                min = max;
                max *= 2;
            }

            while (max != min + 1)
            {
                int pivot = (max + min) / 2;
                if (File.Exists(string.Format(pattern, pivot)))
                    min = pivot;
                else
                    max = pivot;
            }

            return string.Format(pattern, max);
        }

        public static bool FilesAreEqualByByte(this FileInfo file, FileInfo compareFile)
        {
            if (file.Length != compareFile.Length)
                return false;

            int iterations = (int)Math.Ceiling((double)file.Length / BYTES_TO_READ);

            using (FileStream fs1 = file.OpenRead())
            using (FileStream fs2 = compareFile.OpenRead())
            {
                byte[] one = new byte[BYTES_TO_READ];
                byte[] two = new byte[BYTES_TO_READ];

                for (int i = 0; i < iterations; i++)
                {
                    fs1.Read(one, 0, BYTES_TO_READ);
                    fs2.Read(two, 0, BYTES_TO_READ);

                    if (BitConverter.ToInt64(one, 0) != BitConverter.ToInt64(two, 0))
                        return false;
                }
            }

            return true;
        }

        public static bool AreEqualByHash(FileInfo file, FileInfo compareFile)
        {
            using (var fileMd5 = System.Security.Cryptography.MD5.Create())
            using (var compareFileMd5 = System.Security.Cryptography.MD5.Create())
            {
                using var fileStream = file.OpenRead();
                using var compareFileStream = compareFile.OpenRead();
                byte[] fileHash = fileMd5.ComputeHash(fileStream);
                byte[] compareFileHash = compareFileMd5.ComputeHash(compareFileStream);

                if (fileHash.Length != compareFileHash.Length)
                    return false;

                for (int i = 0; i < fileHash.Length; i++)
                {
                    if (fileHash[i] != compareFileHash[i])
                        return false;
                }
            }

            return true;
        }
    }
}