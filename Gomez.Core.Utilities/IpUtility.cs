﻿using System;
using System.Net;

namespace Lexicon.Utilities
{
    public static class IpUtility
    {
        private static IPEndPoint ParseIPEndPoint(string text)
        {
            if (Uri.TryCreate(text, UriKind.Absolute, out Uri uri))
                return new IPEndPoint(IPAddress.Parse(uri.Host), uri.Port < 0 ? 0 : uri.Port);
            if (Uri.TryCreate(string.Concat("tcp://", text), UriKind.Absolute, out uri))
                return new IPEndPoint(IPAddress.Parse(uri.Host), uri.Port < 0 ? 0 : uri.Port);
            if (Uri.TryCreate(string.Concat("tcp://", string.Concat("[", text, "]")), UriKind.Absolute, out uri))
                return new IPEndPoint(IPAddress.Parse(uri.Host), uri.Port < 0 ? 0 : uri.Port);
            throw new FormatException("Failed to parse text to IPEndPoint");
        }

        public static IPAddress UserIp(string ipStr, bool anonymization)
        {
            if (string.IsNullOrEmpty(ipStr))
            {
                return null;
            }

            var ip = ParseIPEndPoint(ipStr).Address;
            if (anonymization)
            {
                var bytes = ip.GetAddressBytes();
                if (bytes.Length == 4)
                {
                    bytes[3] = 0;
                    return new IPAddress(bytes);
                }

                if (bytes.Length == 16)
                {
                    for (int i = 15; i >= 6; i--)
                    {
                        bytes[i] = 0;
                    }

                    return new IPAddress(bytes);
                }
            }

            return ip;
        }
    }
}