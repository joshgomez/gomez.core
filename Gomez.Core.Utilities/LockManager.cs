﻿using System;
using System.Collections.Concurrent;

namespace Gomez.Core.Utilities
{
    public class LockManager : ILockManager
    {
        private class LockObj
        {
            public int count = 0;
        }

        private readonly ConcurrentDictionary<string, LockObj> _locks =
                new ConcurrentDictionary<string, LockObj>();

        private LockObj GetLock(string key)
        {
            if (_locks.TryGetValue(key.ToLower(), out LockObj o))
            {
                o.count++;
                return o;
            }
            else
            {
                o = new LockObj();
                _locks.TryAdd(key.ToLower(), o);
                o.count++;
                return o;
            }
        }

        public void GetLock(string key, Action action)
        {
            lock (GetLock(key))
            {
                action();
                Unlock(key);
            }
        }

        public TResult GetLock<TResult>(string key, Func<TResult> action)
        {
            lock (GetLock(key))
            {
                var result = action();
                Unlock(key);
                return result;
            }
        }

        private void Unlock(string key)
        {
            if (_locks.TryGetValue(key.ToLower(), out LockObj o))
            {
                o.count--;
                if (o.count == 0)
                    _ = _locks.TryRemove(key.ToLower(), out _);
            }
        }
    }
}