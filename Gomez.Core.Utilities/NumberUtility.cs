﻿using System;

namespace Gomez.Core.Utilities
{
    public static class NumberUtility
    {
        public static Random Random { get; private set; } = new Random();

        public static int RandomBetween(int min = 1, int max = 100)
        {
            if(min == max)
            {
                return min;
            }

            if (min > max)
            {
                throw new ArgumentOutOfRangeException(nameof(min));
            }

            return Random.Next(min, max + 1);
        }

        public static long RandomBetween(long min = 0, long max = 100)
        {
            byte[] buf = new byte[8];
            Random.NextBytes(buf);
            long longRand = BitConverter.ToInt64(buf, 0);
            return (Math.Abs(longRand % (max - min)) + min);
        }

        public static double RandomBetween(double min = 1d, double max = 100d)
        {
            if (min == max)
            {
                return min;
            }

            if (min > max)
            {
                throw new ArgumentOutOfRangeException(nameof(min));
            }

            return Random.NextDouble() * (max - min) + min;
        }

        public static int CountDecimalPlaces(double value)
        {
            return CountDecimalPlaces(Convert.ToDecimal(value));
        }

        public static int CountDecimalPlaces(decimal value)
        {
            var text = value.ToString(System.Globalization.CultureInfo.InvariantCulture).TrimEnd('0');
            var decpoint = text.IndexOf('.');

            if (decpoint < 0)
                return 0;

            return text.Length - decpoint - 1;
        }

        public static double RoundToNearestHundred(double value)
        {
            return RoundToNearest(value, 100);
        }

        public static decimal RoundToNearestHundred(decimal value)
        {
            return RoundToNearest(value, 100);
        }

        public static double RoundToNearestTen(double value)
        {
            return RoundToNearest(value, 10);
        }

        public static decimal RoundToNearestTen(decimal value)
        {
            return RoundToNearest(value, 10);
        }

        public static decimal RoundToNearest(decimal value, int number)
        {
            return Math.Round(value / number, 0) * number;
        }

        public static double RoundToNearest(double value, int number)
        {
            return Math.Round(value / number, 0) * number;
        }

        public static double Factor(double a, double b) => a / b;

        public static double Percent(double a, double b) => Factor(a, b) * 100d;
    }
}