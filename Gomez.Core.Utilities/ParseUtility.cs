﻿using System;
using System.Globalization;

namespace Gomez.Core.Utilities
{
    public static class ParseUtility
    {
        public static object ConvertNumberOrDefault(object value)
        {
            if (value == null)
            {
                return null;
            }

            object obj;
            string strValue = value?.ToString();
            bool success;
            if (!string.IsNullOrEmpty(strValue) && (strValue.Contains(',') || strValue.Contains('.')))
            {
                obj = ConvertDecimal(value, out success);
            }
            else
            {
                obj = ConvertInt(value, out success);
            }

            if (success)
            {
                return obj;
            }

            return value;
        }

        public static int ConvertInt(object value)
        {
            return ConvertInt(value, out _);
        }

        public static int ConvertInt(object value, out bool success)
        {
            success = false;
            int? actualValue = null;
            string strValue = value?.ToString();

            if (!string.IsNullOrEmpty(strValue))
            {
                if (strValue.Contains(',') || strValue.Contains('.'))
                {
                    var decimalNumber = ConvertDecimal(strValue, out bool decimalSuccess);
                    if (decimalSuccess)
                    {
                        success = true;
                        return Convert.ToInt32(Math.Round(decimalNumber));
                    }
                }

                string attemptedValue = StringUtility.ToNumberString(strValue);
                success = int.TryParse(attemptedValue, NumberStyles.Any, CultureInfo.CurrentCulture, out int tryedValue);
                if (success)
                {
                    actualValue = tryedValue;
                }
            }

            return actualValue ?? 0;
        }

        public static long ConvertLong(object value)
        {
            return ConvertLong(value, out _);
        }

        public static long ConvertLong(object value, out bool success)
        {
            success = false;
            long? actualValue = null;
            string strValue = value?.ToString();

            if (!string.IsNullOrEmpty(strValue))
            {
                if (strValue.Contains(',') || strValue.Contains('.'))
                {
                    var decimalNumber = ConvertDecimal(strValue, out bool decimalSuccess);
                    if (decimalSuccess)
                    {
                        success = true;
                        return Convert.ToInt64(Math.Round(decimalNumber));
                    }
                }

                string attemptedValue = StringUtility.ToNumberString(strValue);
                success = long.TryParse(attemptedValue, NumberStyles.Any, CultureInfo.CurrentCulture, out long tryedValue);
                if (success)
                {
                    actualValue = tryedValue;
                }
            }

            return actualValue ?? 0;
        }

        public static decimal ConvertDecimal(object value)
        {
            return ConvertDecimal(value, out _);
        }

        public static decimal ConvertDecimal(object value, out bool success)
        {
            success = false;
            decimal? actualValue = null;
            string strValue = value?.ToString();

            if (!string.IsNullOrEmpty(strValue))
            {
                string attemptedValue = StringUtility.ToNumberString(strValue);
                success = decimal.TryParse(attemptedValue, NumberStyles.Any, CultureInfo.CurrentCulture.NumberFormat, out decimal tryedValue);
                if (success)
                {
                    actualValue = tryedValue;
                }
            }

            return actualValue ?? 0;
        }

        public static double ConvertDouble(object value)
        {
            return ConvertDouble(value, out _);
        }

        public static double ConvertDouble(object value, out bool success)
        {
            success = false;
            double? actualValue = null;
            string strValue = value?.ToString();

            if (!string.IsNullOrEmpty(strValue))
            {
                string attemptedValue = StringUtility.ToNumberString(strValue);
                success = double.TryParse(attemptedValue, NumberStyles.Any, CultureInfo.CurrentCulture, out double tryedValue);
                if (success)
                {
                    actualValue = tryedValue;
                }
            }

            return actualValue ?? 0;
        }

        public static float ConvertFloat(object value)
        {
            return ConvertFloat(value, out _);
        }

        public static float ConvertFloat(object value, out bool success)
        {
            success = false;
            float? actualValue = null;
            string strValue = value?.ToString();

            if (!string.IsNullOrEmpty(strValue))
            {
                string attemptedValue = StringUtility.ToNumberString(strValue);
                success = float.TryParse(attemptedValue, NumberStyles.Any, CultureInfo.CurrentCulture, out float tryedValue);
                if (success)
                {
                    actualValue = tryedValue;
                }
            }

            return actualValue ?? 0;
        }

        public static T ConvertTo<T>(object value, out bool success) where T : struct
        {
            return (T)ConvertTo(typeof(T), value, out success);
        }

        public static object ConvertTo(Type valueType, object value, out bool success)
        {
            if (value == null)
            {
                success = true;
                return TypeUtility.GetDefault(valueType);
            }

            var underlyingType = Nullable.GetUnderlyingType(valueType);
            if (underlyingType != null)
            {
                valueType = underlyingType;
            }

            success = false;
            return (valueType) switch
            {
                Type type when type == typeof(int) => ConvertInt(value, out success),
                Type type when type == typeof(long) => ConvertLong(value, out success),
                Type type when type == typeof(decimal) => ConvertDecimal(value, out success),
                Type type when type == typeof(double) => ConvertDouble(value, out success),
                Type type when type == typeof(float) => ConvertFloat(value, out success),
                _ => TypeUtility.GetDefault(valueType),
            };
        }
    }
}