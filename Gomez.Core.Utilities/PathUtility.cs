﻿using System;
using System.IO;
using System.Linq;

namespace Gomez.Core.Utilities
{
    public static class PathUtility
    {
        public static string SanitizeContainerPath(string containerName, string blobName, out string blobPath)
        {
            containerName = containerName.ToLower();
            blobName = blobName?.ToLower();

            var pathArr = PathUtility.SplitPath(containerName);
            string[] blobArr = null;
            if (!string.IsNullOrEmpty(blobName))
            {
                blobArr = PathUtility.SplitPath(blobName);
            }

            blobPath = string.Join("/", pathArr.Skip(1).ToArray());
            if (blobArr != null)
            {
                blobPath += "/" + string.Join("/", blobArr);
            }

            blobPath = blobPath.TrimStart(new[] { Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar });
            return pathArr.Take(1).FirstOrDefault();
        }

        public static string SanitizeContainerPath(string containerName)
        {
            containerName = containerName.ToLower();
            var pathArr = PathUtility.SplitPath(containerName);
            return pathArr.Take(1).FirstOrDefault();
        }

        public static string[] SplitPath(string path)
        {
            return path.Split(new[] { Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar }
                                , StringSplitOptions.RemoveEmptyEntries);
        }
    }
}