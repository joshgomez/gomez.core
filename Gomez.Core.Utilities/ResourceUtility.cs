using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace Gomez.Core.Utilities
{
    public static class ResourceUtility
    {
        public static async Task<string> GetAsStringAsync(string resourceName)
        {
            return await GetAsStringAsync(resourceName, Assembly.GetCallingAssembly());
        }

        public static async Task<byte[]> GetAsBytesAsync(string resourceName)
        {
            return await GetAsBytesAsync(resourceName, Assembly.GetCallingAssembly());
        }

        public static Stream GetAsStrean(string resourceName)
        {
            return GetAsStream(resourceName, Assembly.GetCallingAssembly());
        }

        public static async Task<string> GetAsStringAsync(string resourceName, Type type)
        {
            return await GetAsStringAsync(resourceName, type.Assembly);
        }

        public static async Task<byte[]> GetAsBytesAsync(string resourceName, Type type)
        {
            return await GetAsBytesAsync(resourceName, type.Assembly);
        }

        public static Stream GetAsStrean(string resourceName, Type type)
        {
            return GetAsStream(resourceName, type.Assembly);
        }

        public static async Task<string> GetAsStringAsync(string resourceName, Assembly assembly)
        {
            using (Stream resourceStream = GetAsStream(resourceName, assembly))
            {
                if (resourceStream == null)
                    return null;

                using (StreamReader reader = new StreamReader(resourceStream))
                {
                    return await reader.ReadToEndAsync();
                }
            }
        }

        public static async Task<byte[]> GetAsBytesAsync(string resourceName, Assembly assembly)
        {
            using (Stream resourceStream = GetAsStream(resourceName, assembly))
            {
                byte[] content = new byte[resourceStream.Length];
                await resourceStream.ReadAsync(content, 0, content.Length);
                return content;
            }
        }

        public static Stream GetAsStream(string resourceName, Assembly assembly)
        {
            resourceName = FormatResourceName(assembly, resourceName);
            return assembly.GetManifestResourceStream(resourceName);
        }

        private static string FormatResourceName(Assembly assembly, string resourceName)
        {
            return assembly.GetName().Name + "." + resourceName.Replace(" ", "_").Replace("\\", ".").Replace("/", ".");
        }
    }
}