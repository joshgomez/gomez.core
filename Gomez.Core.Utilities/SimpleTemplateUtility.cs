﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Gomez.Core.Utilities
{
    public static class SimpleTemplateUtility
    {
        public static async Task<string> LoadAsync(string fileName, Dictionary<string, string> replacements = null, string basePath = null)
        {
            basePath ??= Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            string filePath = Path.Combine(basePath, Path.GetFileName(fileName));
            if (!File.Exists(filePath))
            {
                return null;
            }

            string content = await File.ReadAllTextAsync(filePath);
            if (replacements != null)
            {
                foreach (var item in replacements)
                {
                    content = content.Replace("{{" + item.Key + "}}", item.Value);
                }
            }

            return content;
        }
    }
}