﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Gomez.Core.Utilities
{
    public static class StringUtility
    {
        public static bool ExactMatch(string input, string match)
        {
            return Regex.IsMatch(input, string.Format(@"\b{0}\b", Regex.Escape(match)));
        }

        public static string TrimEnd(this string source, string value)
        {
            if (!source.EndsWith(value))
                return source;

            return source.Remove(source.LastIndexOf(value));
        }

        public static string TrimStart(string source, string value)
        {
            if (!source.StartsWith(value))
                return source;

            return source.Remove(source.IndexOf(value), value.Length);
        }

        public static string AddToEnd(string s, string suffix)
        {
            return s + suffix;
        }

        public static string AddToStart(string s, string prefix)
        {
            return prefix + s;
        }

        public static string FirstCharToUpper(string input)
        {
            if (string.IsNullOrEmpty(input))
                return string.Empty;

            return input.First().ToString().ToUpper() + input.Substring(1);
        }

        public static string Replace(string str, string[] separators, string newVal)
        {
            string[] temp;

            temp = str.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            return string.Join(newVal, temp);
        }

        public static string ToMd5String(string input)
        {
            // step 1, calculate MD5 hash from input
            using var md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();
        }

        public static string Base64ToHex(string value)
        {
            byte[] bytes = Convert.FromBase64String(value);
            return BitConverter.ToString(bytes).Replace("-", "");
        }

        public static string FromBase64(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return string.Empty;
            }

            return Encoding.UTF8.GetString(Convert.FromBase64String(input));
        }

        public static string ToNumberString(string attempedValue)
        {
            attempedValue = attempedValue.Replace(" ", "");

            string decimalSeparator = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            switch (decimalSeparator)
            {
                case ".":
                    attempedValue = attempedValue.Replace(",", ".");
                    break;

                case ",":
                    attempedValue = attempedValue.Replace(".", ",");
                    break;
            }

            return attempedValue;
        }

        public static string GetBetween(string source, string start, string end)
        {
            int iStart, iEnd;
            if (source.Contains(start) && source.Contains(end))
            {
                iStart = source.IndexOf(start, 0) + start.Length;
                iEnd = source.IndexOf(end, iStart);
                return source.Substring(iStart, iEnd - iStart);
            }
            else
            {
                return "";
            }
        }

        public static string RemoveSyntaxes(string input, string startTag = "{{", string endTag = "}}", int startSearchIndex = 0)
        {
            if (startSearchIndex > input.Length - 1)
            {
                return input;
            }

            var startIndex = input.IndexOf(startTag, startSearchIndex);
            var endIndex = input.IndexOf(endTag, startSearchIndex);

            if (startIndex == -1 || endIndex == -1)
            {
                return input;
            }

            //Invalid start tag?
            var nextStartIndex = input.IndexOf(startTag, startIndex + 1);
            if (nextStartIndex > -1 && nextStartIndex < endIndex)
            {
                //Try on next tag instead?
                return RemoveSyntaxes(input, startTag, endTag, nextStartIndex);
            }

            //End tag begin before start tag.
            if (endIndex < startIndex)
            {
                //Skip the end tag, begin search a new tag.
                return RemoveSyntaxes(input, startTag, endTag, endIndex + 2);
            }

            input = input.Remove(startIndex, endIndex - startIndex + 2);

            //Try search again, if no tags found it will return the input.
            return RemoveSyntaxes(input, startTag, endTag);
        }

        public static string GetNamesFromEmail(string email)
        {
            string[] parts = email.Split('@');
            if (parts.Length > 1)
            {
                var names = parts[0].Split('.');
                for (int i = 0; i < names.Length; i++)
                {
                    var name = UppercaseFirst(names[i]);
                    names[i] = Regex.Replace(name, "[^a-zA-Z]", "");
                }

                return string.Join(' ', names);
            }

            return string.Empty;
        }

        public static string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            char[] a = s.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }
    }
}