﻿using System;
using System.Linq;

namespace Gomez.Core.Utilities
{
    public static class UriUtility
    {
        public static bool ValidateUrlFromList(string returnUrl, params string[] whitelist)
        {
            var authority = new Uri(returnUrl).GetComponents(UriComponents.SchemeAndServer, UriFormat.Unescaped).ToLower().TrimEnd('/');
            return whitelist.Any(x => authority.EndsWith(x));
        }
    }
}