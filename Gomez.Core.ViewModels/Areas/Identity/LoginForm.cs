﻿using HybridModelBinding;
using System.ComponentModel.DataAnnotations;

namespace Gomez.Core.ViewModels.Areas.Identity
{
    public class LoginForm
    {
        [Display(Name = "Email")]
        [HybridBindProperty(new[] { Source.Body, Source.Form })]
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Display(Name = "Password")]
        [HybridBindProperty(new[] { Source.Body, Source.Form })]
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [HybridBindProperty(new[] { Source.Body, Source.Form })]
        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

        [HybridBindProperty(new[] { Source.QueryString, Source.Body, Source.Form, })]
        public string ReturnUrl { get; set; }
    }
}