﻿using HybridModelBinding;
using System.ComponentModel.DataAnnotations;

namespace Gomez.Core.ViewModels.Areas.Identity
{
    public class QueryRefreshTokenViewModel
    {
        [HybridBindProperty(new[] { Source.Body, Source.Form })]
        [Required]
        public string Token { get; set; }

        [HybridBindProperty(new[] { Source.Body, Source.Form })]
        [Required]
        public string RefreshToken { get; set; }
    }
}