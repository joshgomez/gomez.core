﻿using System.ComponentModel.DataAnnotations;

namespace Gomez.Core.ViewModels.Areas.Identity
{
    public class RegisterForm
    {
        [Display(Name = "Email")]
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Confirm Password")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Your password and the confirmed password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}