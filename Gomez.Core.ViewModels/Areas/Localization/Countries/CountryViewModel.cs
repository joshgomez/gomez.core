﻿using System.ComponentModel.DataAnnotations;

namespace Gomez.Core.ViewModels.Areas.Localization.Countries
{
    public class CountryViewModel
    {
        [Display(Name = "Two-letter code")]
        public string TwoLetterISORegionName { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }
    }
}