﻿using Gomez.Core.ViewModels.Shared;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Gomez.Core.ViewModels.Areas.Localization.Countries
{
    public class QueryGetViewModel : QueryBaseModel<QueryGetViewModel, GetViewModel>
    {
        public QueryGetViewModel()
        {
        }

        [FromRoute]
        [MaxLength(2)]
        public string Id { get; set; }
    }

    public class GetViewModel : QueryGetViewModel
    {
        public GetViewModel()
        {
        }

        public GetViewModel(QueryGetViewModel query) : base()
        {
            Update(query);
        }

        public CountryViewModel Country { get; set; }
    }
}