﻿using Gomez.Core.Models.Entity;
using Gomez.Core.ViewModels.Shared;

namespace Gomez.Core.ViewModels.Areas.Localization.Countries
{
    public class QueryIndexViewModel : PagedQueryBaseModel<QueryIndexViewModel, IndexViewModel>
    {
    }

    public class IndexViewModel : QueryIndexViewModel
    {
        public IndexViewModel(QueryIndexViewModel query) : base()
        {
            Update(query);
        }

        public PagedResult<CountryViewModel> Countries { get; set; }
    }
}