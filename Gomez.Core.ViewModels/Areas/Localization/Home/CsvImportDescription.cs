﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace Gomez.Core.ViewModels.Areas.Localization.Home
{
    public class CsvImportDescription
    {
        public string Information { get; set; }
        public ICollection<IFormFile> File { get; set; }
    }
}