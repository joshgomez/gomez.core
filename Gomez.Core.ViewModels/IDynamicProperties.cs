﻿using System.Collections.Generic;

namespace Gomez.Core.ViewModels
{
    public interface IDynamicProperties
    {
        Dictionary<string, object> DynamicProperties { get; set; }

        T GetPropertyByType<T>();
    }
}