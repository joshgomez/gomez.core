﻿using Microsoft.AspNetCore.Html;

namespace Gomez.Core.ViewModels.Shared
{
    public class FooterViewModel
    {
        public FooterViewModel()
        {
        }

        public HtmlString Scripts { get; set; }
    }
}