﻿namespace Gomez.Core.ViewModels.Shared
{
    public interface IPagedQueryBaseModel
    {
        int Page { get; set; }
        int Rows { get; set; }
    }
}