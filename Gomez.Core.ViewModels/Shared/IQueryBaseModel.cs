﻿namespace Gomez.Core.ViewModels.Shared
{
    public interface IQueryBaseModel
    {
        string ReturnUrl { get; set; }
        string Search { get; set; }
    }
}