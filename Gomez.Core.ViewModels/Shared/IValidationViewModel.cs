﻿using System.Collections.Generic;

namespace Gomez.Core.ViewModels.Shared
{
    public interface IValidationViewModel<out T>
    {
        string ErrorMessage { get; }
        IEnumerable<string> ErrorMessages { get; }
        bool IsValid { get; }
        T Model { get; }
        string Status { get; set; }

        void AddError(string message);
    }
}