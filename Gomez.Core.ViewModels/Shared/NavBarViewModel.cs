﻿namespace Gomez.Core.ViewModels.Shared
{
    public class NavBarViewModel
    {
        public NavBarViewModel()
        {
        }

        public bool DisplayTitle { get; set; } = true;
    }
}