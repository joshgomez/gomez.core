﻿namespace Gomez.Core.ViewModels.Shared
{
    public class PagedQueryBaseModel<TSrc, TDest> : QueryBaseModel<TSrc, TDest>, IPagedQueryBaseModel
    {
        public int Page { get; set; } = 1;
        public int Rows { get; set; } = 30;
    }
}