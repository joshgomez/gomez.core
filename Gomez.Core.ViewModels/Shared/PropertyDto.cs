﻿using Gomez.Core.Models.Entity;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Gomez.Core.ViewModels.Shared
{
    public class PropertyDto<T> : IPropertyBaseModel, IPropertyValueBaseModel
    {
        public PropertyDto() : base()
        {
        }

        public PropertyDto(Type type, string key, object value = null) : this()
        {
            Key = key;
            TypeNum = PropertyBaseModel.GetPropertyTypeNum(type);
            SetValue(value);
        }

        public PropertyDto(string key, object value = null) : this()
        {
            Key = key;
            TypeNum = PropertyBaseModel.GetPropertyTypeNum(value.GetType());
            SetValue(value);
        }

        [Required]
        public virtual string Key { get; set; }

        [Required]
        public int TypeNum { get; set; }

        public byte[] RowVersion { get; set; }

        public T RelationId { get; set; }

        public int? ValueInt32 { get; set; }
        public long? ValueInt64 { get; set; }
        public float? ValueFloat { get; set; }
        public double? ValueDouble { get; set; }
        public decimal? ValueDecimal { get; set; }
        public string ValueString { get; set; }
        public bool? ValueBool { get; set; }
        public DateTime? ValueDateTime { get; set; }

        protected void SetToNull()
        {
            this.SetValuesToNull();
        }

        protected object ReturnValue()
        {
            return this.ReturnValue(TypeNum);
        }

        protected void SetValue(object data)
        {
            this.SetValue(TypeNum, data);
        }

        [Newtonsoft.Json.JsonIgnore]
        [System.Xml.Serialization.XmlIgnore]
        [IgnoreDataMember]
        [NotMapped]
        public object Value
        {
            get
            {
                return ReturnValue();
            }
            set
            {
                if (value == null)
                {
                    SetToNull();
                }

                SetValue(value);
            }
        }
    }
}