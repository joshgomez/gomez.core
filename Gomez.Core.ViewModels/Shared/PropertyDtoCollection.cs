﻿using Gomez.Core.Models.Entity;
using System;
using System.Collections.Generic;

namespace Gomez.Core.ViewModels.Shared
{
    public class PropertyDtoCollection<TIdentity> : IPropertyCollection
    {
        public PropertyDtoCollection()
        {
            Properties = new List<PropertyDto<TIdentity>>();
        }

        public ICollection<PropertyDto<TIdentity>> Properties { get; set; }

        public bool DeleteProperty(string key)
        {
            return PropertyBaseModel.DeleteProperty(Properties, key);
        }

        public byte[] GetRowVersion(string key)
        {
            return PropertyBaseModel.GetRowVersion(Properties, key);
        }

        public T GetProperty<T>(string key)
        {
            return GetProperty<T>(key, out _);
        }

        public T GetProperty<T>(string key, out byte[] rowVersion)
        {
            var value = GetProperty(typeof(T), key, out rowVersion);
            if (value == null)
            {
                return default;
            }

            return (T)value;
        }

        public object GetProperty(int typeNum, string key)
        {
            return GetProperty(typeNum, key, out _);
        }

        public object GetProperty(int typeNum, string key, out byte[] rowVersion)
        {
            var type = PropertyBaseModel.GetPropertyType(typeNum);
            return GetProperty(type, key, out rowVersion);
        }

        public object GetProperty(Type type, string key)
        {
            return GetProperty(type, key, out _);
        }

        public object GetProperty(Type type, string key, out byte[] rowVersion)
        {
            return PropertyBaseModel.GetProperty(Properties, type, key, out rowVersion);
        }

        public void SetProperty<T>(string key, T value)
        {
            SetProperty(typeof(T), key, value);
        }

        public void SetProperty(int typeNum, string key, object value)
        {
            var type = PropertyBaseModel.GetPropertyType(typeNum);
            SetProperty(type, key, value);
        }

        public bool SetProperty(Type type, string key, object value)
        {
            if (!PropertyBaseModel.UpdateProperty(Properties, type, key, value))
            {
                Properties.Add(new PropertyDto<TIdentity>(type, key, value));
            }

            return true;
        }

        public bool PropertyExists(string key)
        {
            return PropertyBaseModel.PropertyExists(Properties, key);
        }
    }
}