﻿using AutoMapper;
using HybridModelBinding;

namespace Gomez.Core.ViewModels.Shared
{
    public class QueryBaseModel<TSrc, TDest> : IQueryBaseModel
    {
        [HybridBindProperty(new[] { Source.Body, Source.Form, Source.QueryString })]
        public string ReturnUrl { get; set; }

        public string Search { get; set; }

        protected MapperConfiguration _mapperConfig;
        protected Mapper _mapper;

        public QueryBaseModel()
        {
            _mapperConfig = new MapperConfiguration(cfg => cfg.CreateMap<TSrc, TDest>());
        }

        protected void Update(TSrc src)
        {
            if (src != null)
            {
                var mapper = new Mapper(_mapperConfig);
                mapper.Map(src, this);
            }
        }
    }
}