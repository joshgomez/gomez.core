﻿using Gomez.Core.Models.Entity;
using System;

namespace Gomez.Core.ViewModels.Shared
{
    public class SimplePropertyDto : IPropertyBaseModel
    {
        public SimplePropertyDto()
        {
        }

        public static SimplePropertyDto Create<T>(string key, T value)
        {
            return new SimplePropertyDto()
            {
                Key = key,
                TypeNum = PropertyBaseModel.GetPropertyTypeNum(typeof(T)),
                Value = value
            };
        }

        public static SimplePropertyDto Create(Type type, string key, object value)
        {
            return new SimplePropertyDto()
            {
                Key = key,
                TypeNum = PropertyBaseModel.GetPropertyTypeNum(type),
                Value = value
            };
        }

        public string Key { get; set; }
        public int TypeNum { get; set; }
        public object Value { get; set; }
        public byte[] RowVersion { get; set; }
    }
}