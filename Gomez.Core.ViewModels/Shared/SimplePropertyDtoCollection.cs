﻿using Gomez.Core.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Gomez.Core.ViewModels.Shared
{
    public class SimplePropertyDtoCollection : SimplePropertyDtoCollection<SimplePropertyDto>
    {
        public SimplePropertyDtoCollection()
        {
            Properties = new List<SimplePropertyDto>();
        }

        public SimplePropertyDtoCollection(IEnumerable<SimplePropertyDto> properties)
        {
            Properties = properties;
        }
    }

    public class SimplePropertyDtoCollection<TProp> : ISimplePropertyCollection where TProp : IPropertyBaseModel
    {
        public SimplePropertyDtoCollection()
        {
            Properties = new List<TProp>();
        }

        public SimplePropertyDtoCollection(IEnumerable<TProp> properties)
        {
            Properties = properties;
        }

        public IEnumerable<TProp> Properties { get; set; }

        public byte[] GetRowVersion(string key)
        {
            return Properties.Where(x => x.Key == key)
                .Select(x => x.RowVersion).FirstOrDefault();
        }

        public bool PropertyExists(string key)
        {
            return Properties.Any(x => x.Key == key);
        }

        public T GetProperty<T>(string key)
        {
            return GetProperty<T>(key, out _);
        }

        public T GetProperty<T>(string key, out byte[] rowVersion)
        {
            var value = GetProperty(typeof(T), key, out rowVersion);
            if (value == null)
            {
                return default;
            }

            return (T)value;
        }

        public object GetProperty(Type type, string key)
        {
            return GetProperty(type, key, out _);
        }

        public object GetProperty(Type type, string key, out byte[] rowVersion)
        {
            int typeNum = PropertyBaseModel.GetPropertyTypeNum(type);
            return GetProperty(typeNum, key, out rowVersion);
        }

        public object GetProperty(int typeNum, string key)
        {
            return GetProperty(typeNum, key, out _);
        }

        public object GetProperty(int typeNum, string key, out byte[] rowVersion)
        {
            var data = GetPropertyData(typeNum, key);
            if (data == null)
            {
                rowVersion = null;
                return null;
            }

            rowVersion = data.RowVersion;
            return data.Value;
        }

        sealed private class PropertyData
        {
            public object Value { get; set; }
            public byte[] RowVersion { get; set; }
        }

        private PropertyData GetPropertyData(int typeNum, string key)
        {
            return Properties.Where(x => x.TypeNum == typeNum && x.Key == key)
                .Select(x => new PropertyData { Value = x.Value, RowVersion = x.RowVersion })
                .FirstOrDefault();
        }
    }
}