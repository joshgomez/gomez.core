﻿using System.Collections.Generic;
using System.Linq;

namespace Gomez.Core.ViewModels.Shared
{
    public class ValidationViewModel<T> : IValidationViewModel<T>
    {
        public bool ConcurrencyConflict { get; set; }

        private readonly List<string> _errors = new List<string>();
        public T Model { get; private set; }

        public ValidationViewModel()
        {
        }

        public ValidationViewModel(T model)
        {
            Model = model;
        }

        public ValidationViewModel(T model, params string[] errors)
        {
            Model = model;
            _errors.AddRange(errors);
        }

        public void AddError(string message)
        {
            _errors.Add(message);
        }

        public bool IsValid
        {
            get
            {
                return !_errors.Any() && !ConcurrencyConflict;
            }
        }

        public IEnumerable<string> ErrorMessages
        {
            get
            {
                return _errors;
            }
        }

        public string ErrorMessage
        {
            get
            {
                return _errors.FirstOrDefault();
            }
        }

        private string _statusMessage;

        public string Status
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_statusMessage))
                {
                    return _errors.FirstOrDefault();
                }

                return _statusMessage;
            }
            set { _statusMessage = value; }
        }
    }
}