﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;

namespace Gomez.Core.Web.Attributes.ActionFilters
{
    public class ValidateMimeMultipartContentFilterAttribute : ActionFilterAttribute
    {
        private readonly ILogger _logger;

        public ValidateMimeMultipartContentFilterAttribute(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger("ctor ValidateMimeMultipartContentFilterAttribute");
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            string contentType = context.HttpContext.Request.ContentType;
            if (!IsMultipartContentType(contentType))
            {
                int statusCode = 415;
                context.Result = new StatusCodeResult(statusCode);
                _logger.LogWarning($"Invalid ContentType: {contentType}, Status: {statusCode}");
                return;
            }

            base.OnActionExecuting(context);
        }

        private static bool IsMultipartContentType(string contentType)
        {
            return !string.IsNullOrEmpty(contentType) && contentType.IndexOf("multipart/", StringComparison.OrdinalIgnoreCase) >= 0;
        }
    }
}