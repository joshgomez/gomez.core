﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using System;

namespace Gomez.Core.Web.BinderProviders
{
    public class EnumFlagModelBinderProvider<TEnum> : IModelBinderProvider where TEnum : struct
    {
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (context.Metadata.ModelType == typeof(TEnum) || context.Metadata.ModelType == typeof(TEnum?))
            {
                return new BinderTypeModelBinder(typeof(Binders.EnumFlagModelBinder<TEnum>));
            }

            return null;
        }
    }
}