﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using System;

namespace Gomez.Core.Web.BinderProviders
{
    public class NumberModelBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (context.Metadata.ModelType == typeof(decimal))
            {
                return new BinderTypeModelBinder(typeof(Binders.NumberModelBinder<decimal>));
            }

            if (context.Metadata.ModelType == typeof(decimal?))
            {
                return new BinderTypeModelBinder(typeof(Binders.NumberModelBinder<decimal?>));
            }

            if (context.Metadata.ModelType == typeof(double))
            {
                return new BinderTypeModelBinder(typeof(Binders.NumberModelBinder<double>));
            }

            if (context.Metadata.ModelType == typeof(double?))
            {
                return new BinderTypeModelBinder(typeof(Binders.NumberModelBinder<double?>));
            }

            if (context.Metadata.ModelType == typeof(float))
            {
                return new BinderTypeModelBinder(typeof(Binders.NumberModelBinder<float>));
            }

            if (context.Metadata.ModelType == typeof(float?))
            {
                return new BinderTypeModelBinder(typeof(Binders.NumberModelBinder<float?>));
            }

            if (context.Metadata.ModelType == typeof(int))
            {
                return new BinderTypeModelBinder(typeof(Binders.NumberModelBinder<int>));
            }

            if (context.Metadata.ModelType == typeof(int?))
            {
                return new BinderTypeModelBinder(typeof(Binders.NumberModelBinder<int?>));
            }

            if (context.Metadata.ModelType == typeof(long))
            {
                return new BinderTypeModelBinder(typeof(Binders.NumberModelBinder<long>));
            }

            if (context.Metadata.ModelType == typeof(long?))
            {
                return new BinderTypeModelBinder(typeof(Binders.NumberModelBinder<long?>));
            }

            return null;
        }
    }
}