﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Threading.Tasks;

namespace Gomez.Core.Web.Binders
{
    public class EnumFlagModelBinder<TEnum> : IModelBinder where TEnum : struct
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (bindingContext == null)
            {
                throw new ArgumentNullException(nameof(bindingContext));
            }

            var modelName = bindingContext.ModelName;

            // Try to fetch the value of the argument by name
            var valueProviderResult =
                bindingContext.ValueProvider.GetValue(modelName);

            if (valueProviderResult == ValueProviderResult.None)
            {
                return Task.CompletedTask;
            }

            bindingContext.ModelState.SetModelValue(modelName,
                valueProviderResult);

            var value = valueProviderResult.FirstValue;

            // Check if the argument value is null or empty
            if (string.IsNullOrEmpty(value))
            {
                return Task.CompletedTask;
            }

            if (!Enum.TryParse<TEnum>(string.Join(",", value), out TEnum tryedValue))
            {
                // Non-decimal arguments result in model state errors
                bindingContext.ModelState.TryAddModelError(
                                        modelName, "Must be a decimal number.");
                return Task.CompletedTask;
            }

            bindingContext.Result = ModelBindingResult.Success(tryedValue);
            return Task.CompletedTask;
        }
    }
}