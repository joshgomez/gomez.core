﻿using System;
using System.Security.Claims;

namespace Gomez.Core.Web.Extensions
{
    public static class ClaimsPrincipalExtension
    {
        public static T GetUserId<T>(this ClaimsPrincipal principal)
        {
            if (principal == null)
                throw new ArgumentNullException(nameof(principal));

            var nameIdentifier = principal.FindFirstValue(ClaimTypes.NameIdentifier);
            if (typeof(T) == typeof(Guid))
            {
                if (Guid.TryParse(nameIdentifier, out Guid result))
                {
                    return (T)((object)result);
                }

                return default;
            }

            return nameIdentifier != null ? (T)Convert.ChangeType(nameIdentifier, typeof(T)) : default;
        }

        public static string GetUserName(this ClaimsPrincipal principal)
        {
            if (principal == null)
                throw new ArgumentNullException(nameof(principal));

            return principal.FindFirstValue(ClaimTypes.Name);
        }

        public static string GetUserEmail(this ClaimsPrincipal principal)
        {
            if (principal == null)
                throw new ArgumentNullException(nameof(principal));

            return principal.FindFirstValue(ClaimTypes.Email);
        }
    }
}