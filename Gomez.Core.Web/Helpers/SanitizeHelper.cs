﻿using Ganss.XSS;
using Narochno.BBCode;
using System;
using System.Linq;

namespace Gomez.Core.Web.Helpers
{
    public class SanitizeHelper
    {
        private readonly HtmlSanitizer _sanitizer;
        private readonly BBCodeParser _parser;

        public int MinLength { get; set; }
        public int MaxLength { get; set; }
        public bool StripLength { get; set; }

        /// <summary>
        ///
        /// </summary>
        /// <param name="bbWhitelist">Comma separator</param>
        public SanitizeHelper(string bbWhitelist = "color,b,i,u")
        {
            var empty = Enumerable.Empty<string>();
            _sanitizer = new HtmlSanitizer(empty, empty, empty, empty, empty)
            {
                KeepChildNodes = true
            };

            var arrbbWhitelist = bbWhitelist.Split(',');
            var bbTags = BBCodeParser.DefaultTags
                .Where(x => arrbbWhitelist.Contains(x.Name)).ToList();
            _parser = new BBCodeParser(bbTags);
        }

        /// <summary>
        /// Read ToSafeText for better description.
        /// </summary>
        /// <param name="text"></param>
        /// <returns>return safe html content from bbcode</returns>
        public string ToSafeHtml(string text)
        {
            text = ToSafeText(text);
            return _parser.ToHtml(text);
        }

        /// <summary>
        /// Strip html tags and keeps the content, throw ArgumentException
        /// containing MinLength or MaxLength. Throw only MaxLength if StripLength is false.
        /// </summary>
        /// <param name="text"></param>
        /// <returns>safe text without html tags.</returns>
        public string ToSafeText(string text)
        {
            if (MinLength > 0 && text.Length < MinLength)
            {
                throw new ArgumentException($"{nameof(MinLength)}: {text.Length}<{MinLength}",
                    nameof(text));
            }

            if (MaxLength > 0 && text.Length > MaxLength)
            {
                if (!StripLength)
                {
                    throw new ArgumentException($"{nameof(MaxLength)}: {text.Length}/{MaxLength}",
                        nameof(text));
                }

                text = text.Substring(0, MaxLength);
            }

            return _sanitizer.Sanitize(text);
        }
    }
}