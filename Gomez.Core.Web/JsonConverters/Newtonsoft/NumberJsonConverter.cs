﻿using Gomez.Core.Utilities;
using Newtonsoft.Json;
using System;
using System.Diagnostics.CodeAnalysis;

namespace Gomez.Core.Web.JsonConverters.Newtonsoft
{
    public class NumberJsonConverter<T> : JsonConverter<T>
    {
        public override T ReadJson(JsonReader reader, Type objectType, [AllowNull] T existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.String)
            {
                string value = (string)reader.Value;
                if (string.IsNullOrEmpty(value))
                {
                    return default;
                }

                var tryedValue = (T)ParseUtility.ConvertTo(typeof(T), value, out bool success);
                if (success)
                {
                    return tryedValue;
                }
            }
            else if (reader.TokenType == JsonToken.Float || reader.TokenType == JsonToken.Integer)
            {
                return (T)Convert.ChangeType(reader.Value, typeof(T));
            }

            throw new JsonSerializationException($"Unexpected token type:  {reader.TokenType.ToString()}");
        }

        public override void WriteJson(JsonWriter writer, [AllowNull] T value, JsonSerializer serializer)
        {
            switch (value)
            {
                case int i:
                    writer.WriteValue(i);
                    break;

                case double d:
                    writer.WriteValue(d);
                    break;

                case decimal m:
                    writer.WriteValue(m);
                    break;

                case float f:
                    writer.WriteValue(f);
                    break;
            }
        }
    }
}