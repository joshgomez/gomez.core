﻿using Gomez.Core.Utilities;
using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Gomez.Core.Web.JsonConverters
{
    public class NumberJsonConverter<T> : JsonConverter<T>
    {
        public NumberJsonConverter()
        {
        }

        public override bool CanConvert(Type typeToConvert)
        {
            return typeof(T).IsAssignableFrom(typeToConvert);
        }

        public override T Read(ref Utf8JsonReader reader,
            Type typeToConvert,
            JsonSerializerOptions options)
        {
            string value = reader.GetString();
            if (string.IsNullOrEmpty(value)) return default;

            var tryedValue = (T)ParseUtility.ConvertTo(typeof(T), value, out bool success);
            if (success)
            {
                return tryedValue;
            }

            throw new JsonException("Invalid value.");
        }

        public override void Write(Utf8JsonWriter writer,
            T value,
            JsonSerializerOptions options)
        {
            switch (value)
            {
                case int i:
                    writer.WriteNumberValue(i);
                    break;

                case double d:
                    writer.WriteNumberValue(d);
                    break;

                case decimal m:
                    writer.WriteNumberValue(m);
                    break;

                case float f:
                    writer.WriteNumberValue(f);
                    break;
            }
        }
    }
}