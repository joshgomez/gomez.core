﻿using System;
using System.Globalization;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Gomez.Core.Web.JsonConverters
{
    /// <summary>
    /// Unix datetime converter.
    /// </summary>
    public class UnixDateTimeJsonConverter : JsonConverter<DateTime>
    {
        private static readonly DateTime UnixStartTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

        public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            string value = reader.GetString();
            if (string.IsNullOrEmpty(value)) return UnixStartTime;

            
            if (double.TryParse(value,NumberStyles.Any,CultureInfo.InvariantCulture,out double result))
            {
                return UnixStartTime.AddSeconds(result);
            }

            throw new JsonException("Invalid value.");
        }

        public override void Write(Utf8JsonWriter writer, DateTime value, JsonSerializerOptions options)
        {
            long totalSeconds = (long)(value - UnixStartTime).TotalSeconds;
            writer.WriteNumberValue(totalSeconds);
        }
    }
}
