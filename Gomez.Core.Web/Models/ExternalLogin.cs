﻿using Microsoft.AspNetCore.Identity;

namespace Gomez.Core.Web.Models
{
    public class ExternalLogin
    {
        public ExternalLoginInfo Info { get; set; }
        public string ReturnUrl { get; set; }
    }
}