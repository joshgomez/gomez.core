﻿using Gomez.Core.Web.Sockets.ConnectionDataModels;
using System;
using System.Collections.Generic;

namespace Gomez.Core.Web.Sockets
{
    public class ConnectionData<T, TModel> : IConnectionData<T, TModel, ConnectionGroup>
    {
        public ConnectionData()
        {
            Connections = new HashSet<ConnectionGroup>();
            ExpireAt = DateTime.UtcNow.AddMinutes(5);
        }

        public T UserId { get; set; }
        public TModel Data { get; set; }
        public HashSet<ConnectionGroup> Connections { get; set; }
        public DateTime ExpireAt { get; set; }
    }
}