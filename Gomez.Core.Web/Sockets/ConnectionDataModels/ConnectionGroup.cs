﻿using System;

namespace Gomez.Core.Web.Sockets.ConnectionDataModels
{
    public struct ConnectionGroup : IConnectionGroup, IEquatable<ConnectionGroup>
    {
        public ConnectionGroup(string connectionId, string groupId)
        {
            ConnectionId = connectionId;
            GroupId = groupId;
        }

        public string ConnectionId { get; set; }
        public string GroupId { get; set; }

        public override bool Equals(object obj)
        {
            return obj is ConnectionGroup group && Equals(group);
        }

        public bool Equals(ConnectionGroup other)
        {
            return ConnectionId == other.ConnectionId && GroupId == other.GroupId;
        }

        public override int GetHashCode()
        {
            return ConnectionId.GetHashCode() + GroupId.GetHashCode();
        }

        public static bool operator ==(ConnectionGroup left, ConnectionGroup right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(ConnectionGroup left, ConnectionGroup right)
        {
            return !(left == right);
        }
    }
}
