﻿using Gomez.Core.Web.Sockets.ConnectionDataModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Gomez.Core.Web.Sockets
{
    public class CountableConnectionMapping<T, TModel> : QueryableConnectionMapping<T, TModel>, ICountableConnectionMapping<T, TModel>
    {
        public int Count(bool? online)
        {
            var currentTime = DateTime.UtcNow;
            if (online == true)
            {
                return _store.Count(x => currentTime <= x.Value.ExpireAt);
            }

            if (online == false)
            {
                return _store.Count(x => currentTime > x.Value.ExpireAt);
            }

            return _store.Count;
        }

        public int Count(bool? online, string groupId)
        {
            var currentTime = DateTime.UtcNow;
            if (online == true)
            {
                return _store.Count(x => currentTime <= x.Value.ExpireAt && x.Value.Connections.Any(z => z.GroupId == groupId));
            }

            if (online == false)
            {
                return _store.Count(x => currentTime > x.Value.ExpireAt && x.Value.Connections.Any(z => z.GroupId == groupId));
            }

            return _store.Count(x => x.Value.Connections.Any(z => z.GroupId == groupId));
        }
    }

    public class QueryableConnectionMapping<T, TModel> : ConnectionMapping<T, TModel>, IQueryableConnectionMapping<T, TModel>
    {
        public IQueryable<ConnectionData<T, TModel>> Query => _store.Select(x => x.Value).AsQueryable();
    }

    public class ConnectionMapping<T, TModel> : IConnectionMapping<T, TModel>
    {
        protected readonly Dictionary<T, ConnectionData<T, TModel>> _store;

        public ConnectionMapping()
        {
            _store = new Dictionary<T, ConnectionData<T, TModel>>();
        }

        public void Add(T userId, string connectionId, TModel data, params string[] groupIds)
        {
            lock (_store)
            {
                if (!_store.TryGetValue(userId, out ConnectionData<T, TModel> store))
                {
                    store = new ConnectionData<T, TModel>
                    {
                        UserId = userId,
                        Data = data
                    };

                    _store.Add(userId, store);
                }
                else
                {
                    ExtendExpireDate(ref store);
                }

                lock (store)
                {
                    if(groupIds != null && groupIds.Length > 0)
                    {
                        foreach (var groupId in groupIds)
                        {
                            store.Connections.Add(new ConnectionGroup(connectionId, groupId));
                        }
                    }
                    else
                    {
                        store.Connections.Add(new ConnectionGroup(connectionId,null));
                    }
                }
            }
        }

        private void ExtendExpireDate(ref ConnectionData<T, TModel> store)
        {
            lock (store)
            {
                var currentTime = DateTime.UtcNow;
                store.ExpireAt = currentTime.AddMinutes(5);
            }
        }

        public IEnumerable<IConnectionGroup> GetConnections(T userId)
        {
            if (_store.TryGetValue(userId, out ConnectionData<T, TModel> store))
            {
                ExtendExpireDate(ref store);
                foreach (var connection in store.Connections)
                {
                    yield return connection;
                }
            };
        }

        public TModel GetData(T userId)
        {
            if (_store.TryGetValue(userId, out ConnectionData<T, TModel> store))
            {
                ExtendExpireDate(ref store);
                return store.Data;
            }

            return default;
        }

        public DateTime? GetExpireDate(T userId)
        {
            if (_store.TryGetValue(userId, out ConnectionData<T, TModel> store))
            {
                ExtendExpireDate(ref store);
                return store.ExpireAt;
            }

            return null;
        }

        public IEnumerable<T> GetExpireUsers()
        {
            var currentTime = DateTime.UtcNow;
            return _store.Values.Where(x => currentTime > x.ExpireAt)
                .Select(x => x.UserId);
        }

        public IEnumerable<T> GetExpireUsers(string groupId)
        {
            var currentTime = DateTime.UtcNow;
            return _store.Values.Where(x => currentTime > x.ExpireAt && x.Connections.Any(z => z.GroupId == groupId))
                .Select(x => x.UserId);
        }

        public IEnumerable<T> GetActiveUsers()
        {
            var currentTime = DateTime.UtcNow;
            return _store.Values.Where(x => currentTime <= x.ExpireAt)
                .Select(x => x.UserId);
        }

        public IEnumerable<T> GetActiveUsers(string groupId)
        {
            var currentTime = DateTime.UtcNow;
            return _store.Values.Where(x => currentTime <= x.ExpireAt && x.Connections.Any(z => z.GroupId == groupId))
                .Select(x => x.UserId);
        }

        public void Remove(T userId)
        {
            Remove(userId, null);
        }

        public void Remove(T userId, string connectionId)
        {
            lock (_store)
            {
                if (!_store.TryGetValue(userId, out ConnectionData<T, TModel> store))
                {
                    return;
                }

                lock (store)
                {
                    if (string.IsNullOrEmpty(connectionId))
                    {
                        _store.Remove(userId);
                        return;
                    }

                    store.Connections.RemoveWhere(x => x.ConnectionId == connectionId);
                    if (store.Connections.Count == 0)
                    {
                        _store.Remove(userId);
                    }
                }
            }
        }
    }
}