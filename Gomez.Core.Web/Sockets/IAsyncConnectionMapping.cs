﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gomez.Core.Web.Sockets
{
    public interface IAsyncConnectionMapping<T, TModel>
    {
        /// <summary>
        /// All users in the hub
        /// </summary>
        /// <param name="online"></param>
        /// <returns></returns>
        Task<int> CountAsync(bool? online);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="online"></param>
        /// <param name="groupId">Can also be null</param>
        /// <returns></returns>
        Task<int> CountAsync(bool? online, string groupId);

        /// <summary>
        /// Add a connection id to a store mapped to a user with additional data
        /// Add you limit the maximum connection id which one user can have
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="connectionId"></param>
        /// <param name="data">optional data</param>
        /// <param name="groupIds">Can be empty if no groups is mapped to the user</param>
        /// <returns></returns>
        Task AddAsync(T userId, string connectionId, TModel data, params string[] groupIds);

        IAsyncEnumerable<IConnectionGroup> GetConnectionsAsync(T userId);

        /// <summary>
        /// Get additional data and extend the expire date.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<TModel> GetDataAsync(T userId);

        /// <summary>
        /// Get a new expire date which is extended.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<DateTime?> GetExpireDateAsync(T userId);

        /// <summary>
        /// All expired users in the entire hub
        /// </summary>
        /// <returns></returns>
        IAsyncEnumerable<T> GetExpireUsersAsync();
        IAsyncEnumerable<T> GetExpireUsersAsync(string groupId);
        /// <summary>
        /// All active users in the entire hub
        /// </summary>
        /// <returns></returns>
        IAsyncEnumerable<T> GetActiveUsersAsync();
        IAsyncEnumerable<T> GetActiveUsersAsync(string groupId);

        Task RemoveAsync(T userId);

        Task RemoveAsync(T userId, string connectionId);
    }
}