﻿using System;
using System.Collections.Generic;

namespace Gomez.Core.Web.Sockets
{
    public interface IConnectionData<T, TModel, TConnection> where TConnection : IConnectionGroup
    {
        T UserId { get; set; }
        HashSet<TConnection> Connections { get; set; }
        TModel Data { get; set; }
        DateTime ExpireAt { get; set; }
    }
}