﻿using Newtonsoft.Json;

namespace Gomez.Core.Web.Sockets
{
    public interface IConnectionGroup
    {
        [JsonProperty("connectionId")]
        string ConnectionId { get; set; }

        [JsonProperty("groupId")]
        string GroupId { get; set; }
    }
}
