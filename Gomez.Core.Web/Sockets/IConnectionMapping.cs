﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Gomez.Core.Web.Sockets
{
    public interface ICountableConnectionMapping<T, TModel> : IQueryableConnectionMapping<T, TModel>
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="online">If null count both offline and online</param>
        /// <returns></returns>
        int Count(bool? online);
        int Count(bool? online, string groupId);
    }

    public interface IQueryableConnectionMapping<T, TModel> : IConnectionMapping<T, TModel>
    {
        IQueryable<ConnectionData<T, TModel>> Query { get; }
    }

    public interface IConnectionMapping<T, TModel>
    {
        /// <summary>
        /// Add a connection id to a store mapped to a user with additional data
        /// Add you limit the maximum connection id which one user can have
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="connectionId"></param>
        /// <param name="data"></param>
        /// <param name="groupIds">Can be empty if no groups is mapped to the user</param>
        void Add(T userId, string connectionId, TModel data, params string[] groupIds);

        IEnumerable<IConnectionGroup> GetConnections(T userId);

        /// <summary>
        /// Get additional data and extend the expire date.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        TModel GetData(T userId);

        /// <summary>
        /// Get a new expire date which is extended.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        DateTime? GetExpireDate(T userId);

        IEnumerable<T> GetExpireUsers();
        IEnumerable<T> GetExpireUsers(string groupId);
        IEnumerable<T> GetActiveUsers();
        IEnumerable<T> GetActiveUsers(string groupId);

        void Remove(T userId);

        void Remove(T userId, string connectionId);
    }
}