﻿using Microsoft.AspNetCore.Http;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Gomez.Core.Web.Sockets
{
    /// <summary>
    /// The middleware is registered as a scoped or transient service in the app's service container.
    /// </summary>
    public class WebSocketsBearerTokenMiddleware : IMiddleware
    {
        private readonly string[] _hubSegments;

        public WebSocketsBearerTokenMiddleware()
        {
            _hubSegments = new string[] { "/hubs" };
        }

        public WebSocketsBearerTokenMiddleware(string[] hubSegments)
        {
            _hubSegments = hubSegments;
        }

        public Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            var request = context.Request;

            // web sockets cannot pass headers so we must take the access token from query param and
            // add it to the header before authentication middleware runs
            if (StartsWithSegments(request) && request.Query.TryGetValue("access_token", out var accessToken))
            {
                request.Headers.Add("Authorization", $"Bearer {accessToken}");
            }

            return next(context);
        }

        private bool StartsWithSegments(HttpRequest request)
        {
            return _hubSegments
                .Any(segment => request.Path.StartsWithSegments(segment, StringComparison.OrdinalIgnoreCase));
        }
    }
}
