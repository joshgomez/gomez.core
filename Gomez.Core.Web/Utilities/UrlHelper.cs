﻿using System;
using System.Globalization;
using System.Web;

namespace Gomez.Core.Web.Utilities
{
    public static class UrlHelper
    {
        public static string BuildRedirectTokenUrl(string token, string refreshToken, DateTime expires, string returnUrl, bool isQuery = false)
        {
            var redirectUrl = new UriBuilder(returnUrl);
            var expireStr = expires.ToString("u");
            if (isQuery)
            {
                var query = HttpUtility.ParseQueryString(redirectUrl.Query);
                query["token"] = token;
                query["refreshToken"] = refreshToken;
                query["expires"] = expireStr;
                redirectUrl.Query = query.ToString();
                return redirectUrl.ToString();
            }

            redirectUrl.Fragment = $"token={Uri.EscapeDataString(token)}&refreshToken={Uri.EscapeDataString(refreshToken)}&expires={Uri.EscapeDataString(expireStr)}";
            return redirectUrl.ToString();
        }
    }
}