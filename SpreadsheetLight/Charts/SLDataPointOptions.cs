﻿using System.Collections.Generic;
using C = DocumentFormat.OpenXml.Drawing.Charts;
using SLA = SpreadsheetLight.Drawing;

namespace SpreadsheetLight.Charts
{
    /// <summary>
    /// Encapsulates properties and methods for setting data point options for charts.
    /// </summary>
    public class SLDataPointOptions
    {
        internal SLA.SLShapeProperties ShapeProperties { get; set; }

        /// <summary>
        /// Fill properties.
        /// </summary>
        public SLA.SLFill Fill { get { return ShapeProperties.Fill; } }

        /// <summary>
        /// Border/Line properties.
        /// </summary>
        public SLA.SLLinePropertiesType Line { get { return ShapeProperties.Outline; } }

        /// <summary>
        /// Shadow properties.
        /// </summary>
        public SLA.SLShadowEffect Shadow { get { return ShapeProperties.EffectList.Shadow; } }

        /// <summary>
        /// Glow properties.
        /// </summary>
        public SLA.SLGlow Glow { get { return ShapeProperties.EffectList.Glow; } }

        /// <summary>
        /// Soft edge properties.
        /// </summary>
        public SLA.SLSoftEdge SoftEdge { get { return ShapeProperties.EffectList.SoftEdge; } }

        /// <summary>
        /// 3D format properties.
        /// </summary>
        public SLA.SLFormat3D Format3D { get { return ShapeProperties.Format3D; } }

        // internally, the default is actually true in Open XML, but when null it's false.
        // The Open XML docs state it's supposed to be true when the tag is missing. I don't know...
        /// <summary>
        /// Invert colors if negative. If null, the effective default is used (false). This is for bar charts, column charts and bubble charts.
        /// </summary>
        public bool? InvertIfNegative { get; set; }

        /// <summary>
        /// Marker properties. This is for line charts, radar charts and scatter charts.
        /// </summary>
        public SLMarker Marker { get; set; }

        // "default" is 25%, range of 0% to 400%
        // but we're not enforcing the range
        internal uint? iExplosion;

        /// <summary>
        /// The explosion distance from the center of the pie in percentage. It is suggested to keep the range between 0% and 400%.
        /// </summary>
        public uint Explosion
        {
            get { return iExplosion ?? 0; }
            set { iExplosion = value; }
        }

        internal bool? bBubble3D;

        internal bool Bubble3D
        {
            get { return bBubble3D ?? true; }
            set { bBubble3D = value; }
        }

        // pictureoptions?

        internal SLDataPointOptions(List<System.Drawing.Color> ThemeColors)
        {
            ShapeProperties = new SLA.SLShapeProperties(ThemeColors);
            InvertIfNegative = null;
            Marker = new SLMarker(ThemeColors);
            iExplosion = null;
            bBubble3D = null;
        }

        internal C.DataPoint ToDataPoint(int index, bool IsStylish = false)
        {
            C.DataPoint pt = new C.DataPoint
            {
                Index = new C.Index() { Val = (uint)index }
            };

            if (Marker.HasMarker) pt.Marker = Marker.ToMarker(IsStylish);

            if (bBubble3D != null) pt.Bubble3D = new C.Bubble3D() { Val = bBubble3D.Value };

            if (iExplosion != null) pt.Explosion = new C.Explosion() { Val = iExplosion.Value };

            if (ShapeProperties.HasShapeProperties) pt.ChartShapeProperties = ShapeProperties.ToChartShapeProperties(IsStylish);

            return pt;
        }

        internal SLDataPointOptions Clone()
        {
            SLDataPointOptions dpo = new SLDataPointOptions(ShapeProperties.listThemeColors)
            {
                ShapeProperties = ShapeProperties.Clone(),
                InvertIfNegative = InvertIfNegative,
                Marker = Marker.Clone(),
                iExplosion = iExplosion,
                bBubble3D = bBubble3D
            };

            return dpo;
        }
    }
}