﻿using System.Collections.Generic;
using A = DocumentFormat.OpenXml.Drawing;
using C = DocumentFormat.OpenXml.Drawing.Charts;
using SLA = SpreadsheetLight.Drawing;

namespace SpreadsheetLight.Charts
{
    /// <summary>
    /// Encapsulates properties and methods for setting the data table of charts.
    /// </summary>
    public class SLDataTable
    {
        internal SLA.SLShapeProperties ShapeProperties;

        /// <summary>
        /// Specifies if horizontal table borders are shown.
        /// </summary>
        public bool ShowHorizontalBorder { get; set; }

        /// <summary>
        /// Specifies if vertical table borders are shown.
        /// </summary>
        public bool ShowVerticalBorder { get; set; }

        /// <summary>
        /// Specifies if table outline borders are shown.
        /// </summary>
        public bool ShowOutlineBorder { get; set; }

        /// <summary>
        /// Specifies if legend keys are shown.
        /// </summary>
        public bool ShowLegendKeys { get; set; }

        /// <summary>
        /// Fill properties.
        /// </summary>
        public SLA.SLFill Fill { get { return ShapeProperties.Fill; } }

        /// <summary>
        /// Border properties.
        /// </summary>
        public SLA.SLLinePropertiesType Border { get { return ShapeProperties.Outline; } }

        /// <summary>
        /// Shadow properties.
        /// </summary>
        public SLA.SLShadowEffect Shadow { get { return ShapeProperties.EffectList.Shadow; } }

        /// <summary>
        /// Glow properties.
        /// </summary>
        public SLA.SLGlow Glow { get { return ShapeProperties.EffectList.Glow; } }

        /// <summary>
        /// Soft edge properties.
        /// </summary>
        public SLA.SLSoftEdge SoftEdge { get { return ShapeProperties.EffectList.SoftEdge; } }

        /// <summary>
        /// 3D format properties.
        /// </summary>
        public SLA.SLFormat3D Format3D { get { return ShapeProperties.Format3D; } }

        internal SLFont Font { get; set; }

        internal SLDataTable(List<System.Drawing.Color> ThemeColors, bool IsStylish = false)
        {
            ShowHorizontalBorder = true;
            ShowVerticalBorder = true;
            ShowOutlineBorder = true;
            ShowLegendKeys = true;
            ShapeProperties = new SLA.SLShapeProperties(ThemeColors);

            if (IsStylish)
            {
                ShapeProperties.Fill.SetNoFill();
                ShapeProperties.Outline.Width = 0.75m;
                ShapeProperties.Outline.CapType = A.LineCapValues.Flat;
                ShapeProperties.Outline.CompoundLineType = A.CompoundLineValues.Single;
                ShapeProperties.Outline.Alignment = A.PenAlignmentValues.Center;
                ShapeProperties.Outline.SetSolidLine(A.SchemeColorValues.Text1, 0.85m, 0);
                ShapeProperties.Outline.JoinType = SLA.SLLineJoinValues.Round;
            }

            Font = null;
        }

        /// <summary>
        /// Clear all styling shape properties. Use this if you want to start styling from a clean slate.
        /// </summary>
        public void ClearShapeProperties()
        {
            ShapeProperties = new SLA.SLShapeProperties(ShapeProperties.listThemeColors);
        }

        /// <summary>
        /// Set font settings for the contents of the data table.
        /// </summary>
        /// <param name="Font">The SLFont containing the font settings.</param>
        public void SetFont(SLFont Font)
        {
            this.Font = Font.Clone();
        }

        internal C.DataTable ToDataTable(bool IsStylish = false)
        {
            C.DataTable dt = new C.DataTable();

            if (ShowHorizontalBorder) dt.ShowHorizontalBorder = new C.ShowHorizontalBorder() { Val = true };
            if (ShowVerticalBorder) dt.ShowVerticalBorder = new C.ShowVerticalBorder() { Val = true };
            if (ShowOutlineBorder) dt.ShowOutlineBorder = new C.ShowOutlineBorder() { Val = true };
            if (ShowLegendKeys) dt.ShowKeys = new C.ShowKeys() { Val = true };

            if (ShapeProperties.HasShapeProperties) dt.ChartShapeProperties = ShapeProperties.ToChartShapeProperties(IsStylish);

            if (Font != null)
            {
                dt.TextProperties = new C.TextProperties
                {
                    BodyProperties = new A.BodyProperties(),
                    ListStyle = new A.ListStyle()
                };

                dt.TextProperties.Append(Font.ToParagraph());
            }
            else if (IsStylish)
            {
                dt.TextProperties = new C.TextProperties
                {
                    BodyProperties = new A.BodyProperties()
                    {
                        Rotation = 0,
                        UseParagraphSpacing = true,
                        VerticalOverflow = A.TextVerticalOverflowValues.Ellipsis,
                        Vertical = A.TextVerticalValues.Horizontal,
                        Wrap = A.TextWrappingValues.Square,
                        Anchor = A.TextAnchoringTypeValues.Center,
                        AnchorCenter = true
                    },
                    ListStyle = new A.ListStyle()
                };

                A.Paragraph para = new A.Paragraph
                {
                    ParagraphProperties = new A.ParagraphProperties()
                };

                A.DefaultRunProperties defrunprops = new A.DefaultRunProperties
                {
                    FontSize = 900,
                    Bold = false,
                    Italic = false,
                    Underline = A.TextUnderlineValues.None,
                    Strike = A.TextStrikeValues.NoStrike,
                    Kerning = 1200,
                    Baseline = 0
                };

                A.SchemeColor schclr = new A.SchemeColor() { Val = A.SchemeColorValues.Text1 };
                schclr.Append(new A.LuminanceModulation() { Val = 65000 });
                schclr.Append(new A.LuminanceOffset() { Val = 35000 });
                defrunprops.Append(new A.SolidFill()
                {
                    SchemeColor = schclr
                });

                defrunprops.Append(new A.LatinFont() { Typeface = "+mn-lt" });
                defrunprops.Append(new A.EastAsianFont() { Typeface = "+mn-ea" });
                defrunprops.Append(new A.ComplexScriptFont() { Typeface = "+mn-cs" });

                para.ParagraphProperties.Append(defrunprops);
                para.Append(new A.EndParagraphRunProperties() { Language = System.Globalization.CultureInfo.CurrentCulture.Name });

                dt.TextProperties.Append(para);
            }

            return dt;
        }

        internal SLDataTable Clone()
        {
            SLDataTable dt = new SLDataTable(ShapeProperties.listThemeColors)
            {
                ShapeProperties = ShapeProperties.Clone(),
                ShowHorizontalBorder = ShowHorizontalBorder,
                ShowVerticalBorder = ShowVerticalBorder,
                ShowOutlineBorder = ShowOutlineBorder,
                ShowLegendKeys = ShowLegendKeys
            };
            if (Font != null) dt.Font = Font.Clone();

            return dt;
        }
    }
}