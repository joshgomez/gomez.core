﻿using System.Collections.Generic;
using System.Linq;
using A = DocumentFormat.OpenXml.Drawing;
using C = DocumentFormat.OpenXml.Drawing.Charts;

namespace SpreadsheetLight.Charts
{
    /// <summary>
    /// Encapsulates properties and methods for setting group data label options for charts.
    /// </summary>
    public class SLGroupDataLabelOptions : EGDLblShared
    {
        /// <summary>
        /// Specifies if leader lines are shown. This is for pie charts (I think...).
        /// </summary>
        public bool ShowLeaderLines { get; set; }

        // TODO Leaderlines (pie charts)

        internal SLGroupDataLabelOptions(List<System.Drawing.Color> ThemeColors) : base(ThemeColors)
        {
            ShowLeaderLines = false;
        }

        internal C.DataLabels ToDataLabels(Dictionary<int, SLDataLabelOptions> Options, bool ToDelete)
        {
            C.DataLabels lbls = new C.DataLabels();

            if (Options.Count > 0)
            {
                List<int> indexlist = Options.Keys.ToList<int>();
                indexlist.Sort();
                int index;
                for (int i = 0; i < indexlist.Count; ++i)
                {
                    index = indexlist[i];
                    lbls.Append(Options[index].ToDataLabel(index));
                }
            }

            if (ToDelete)
            {
                lbls.Append(new C.Delete() { Val = true });
            }
            else
            {
                if (HasNumberingFormat)
                {
                    lbls.Append(new C.NumberingFormat() { FormatCode = FormatCode, SourceLinked = SourceLinked });
                }

                if (ShapeProperties.HasShapeProperties) lbls.Append(ShapeProperties.ToChartShapeProperties());

                if (Rotation != null || Vertical != null || Anchor != null || AnchorCenter != null)
                {
                    C.TextProperties txtprops = new C.TextProperties
                    {
                        BodyProperties = new A.BodyProperties()
                    };
                    if (Rotation != null) txtprops.BodyProperties.Rotation = (int)(Rotation.Value * SLConstants.DegreeToAngleRepresentation);
                    if (Vertical != null) txtprops.BodyProperties.Vertical = Vertical.Value;
                    if (Anchor != null) txtprops.BodyProperties.Anchor = Anchor.Value;
                    if (AnchorCenter != null) txtprops.BodyProperties.AnchorCenter = AnchorCenter.Value;

                    txtprops.ListStyle = new A.ListStyle();

                    A.Paragraph para = new A.Paragraph
                    {
                        ParagraphProperties = new A.ParagraphProperties()
                    };
                    para.ParagraphProperties.Append(new A.DefaultRunProperties());
                    txtprops.Append(para);

                    lbls.Append(txtprops);
                }

                if (vLabelPosition != null) lbls.Append(new C.DataLabelPosition() { Val = vLabelPosition.Value });

                lbls.Append(new C.ShowLegendKey() { Val = ShowLegendKey });
                lbls.Append(new C.ShowValue() { Val = ShowValue });
                lbls.Append(new C.ShowCategoryName() { Val = ShowCategoryName });
                lbls.Append(new C.ShowSeriesName() { Val = ShowSeriesName });
                lbls.Append(new C.ShowPercent() { Val = ShowPercentage });
                lbls.Append(new C.ShowBubbleSize() { Val = ShowBubbleSize });

                if (Separator != null && Separator.Length > 0) lbls.Append(new C.Separator() { Text = Separator });

                if (ShowLeaderLines) lbls.Append(new C.ShowLeaderLines() { Val = ShowLeaderLines });
            }

            return lbls;
        }

        internal SLGroupDataLabelOptions Clone()
        {
            SLGroupDataLabelOptions gdlo = new SLGroupDataLabelOptions(ShapeProperties.listThemeColors)
            {
                Rotation = Rotation,
                Vertical = Vertical,
                Anchor = Anchor,
                AnchorCenter = AnchorCenter,
                HasNumberingFormat = HasNumberingFormat,
                sFormatCode = sFormatCode,
                bSourceLinked = bSourceLinked,
                vLabelPosition = vLabelPosition,
                ShapeProperties = ShapeProperties.Clone(),
                ShowLegendKey = ShowLegendKey,
                ShowValue = ShowValue,
                ShowCategoryName = ShowCategoryName,
                ShowSeriesName = ShowSeriesName,
                ShowPercentage = ShowPercentage,
                ShowBubbleSize = ShowBubbleSize,
                Separator = Separator,
                ShowLeaderLines = ShowLeaderLines
            };

            return gdlo;
        }
    }
}