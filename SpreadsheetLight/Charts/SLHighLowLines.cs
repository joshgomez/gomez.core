﻿using System.Collections.Generic;
using A = DocumentFormat.OpenXml.Drawing;
using C = DocumentFormat.OpenXml.Drawing.Charts;
using SLA = SpreadsheetLight.Drawing;

namespace SpreadsheetLight.Charts
{
    /// <summary>
    /// Encapsulates properties and methods for high-low lines.
    /// This simulates the DocumentFormat.OpenXml.Drawing.Charts.HighLowLines class.
    /// </summary>
    public class SLHighLowLines
    {
        internal SLA.SLShapeProperties ShapeProperties { get; set; }

        /// <summary>
        /// Line properties.
        /// </summary>
        public SLA.SLLinePropertiesType Line { get { return ShapeProperties.Outline; } }

        /// <summary>
        /// Shadow properties.
        /// </summary>
        public SLA.SLShadowEffect Shadow { get { return ShapeProperties.EffectList.Shadow; } }

        /// <summary>
        /// Glow properties.
        /// </summary>
        public SLA.SLGlow Glow { get { return ShapeProperties.EffectList.Glow; } }

        /// <summary>
        /// Soft edge properties.
        /// </summary>
        public SLA.SLSoftEdge SoftEdge { get { return ShapeProperties.EffectList.SoftEdge; } }

        internal SLHighLowLines(List<System.Drawing.Color> ThemeColors, bool IsStylish = false)
        {
            ShapeProperties = new SLA.SLShapeProperties(ThemeColors);
            if (IsStylish)
            {
                ShapeProperties.Outline.Width = 0.75m;
                ShapeProperties.Outline.CapType = A.LineCapValues.Flat;
                ShapeProperties.Outline.CompoundLineType = A.CompoundLineValues.Single;
                ShapeProperties.Outline.Alignment = A.PenAlignmentValues.Center;
                ShapeProperties.Outline.SetSolidLine(A.SchemeColorValues.Text1, 0.25m, 0);
                ShapeProperties.Outline.JoinType = SLA.SLLineJoinValues.Round;
            }
        }

        /// <summary>
        /// Clear all styling shape properties. Use this if you want to start styling from a clean slate.
        /// </summary>
        public void ClearShapeProperties()
        {
            ShapeProperties = new SLA.SLShapeProperties(ShapeProperties.listThemeColors);
        }

        internal C.HighLowLines ToHighLowLines(bool IsStylish = false)
        {
            C.HighLowLines hll = new C.HighLowLines();

            if (ShapeProperties.HasShapeProperties) hll.ChartShapeProperties = ShapeProperties.ToChartShapeProperties(IsStylish);

            return hll;
        }

        internal SLHighLowLines Clone()
        {
            SLHighLowLines hll = new SLHighLowLines(ShapeProperties.listThemeColors)
            {
                ShapeProperties = ShapeProperties.Clone()
            };

            return hll;
        }
    }
}