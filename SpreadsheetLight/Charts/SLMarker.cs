﻿using System.Collections.Generic;
using C = DocumentFormat.OpenXml.Drawing.Charts;
using SLA = SpreadsheetLight.Drawing;

namespace SpreadsheetLight.Charts
{
    /// <summary>
    /// Encapsulates properties and methods for setting data markers in charts.
    /// This simulates the DocumentFormat.OpenXml.Drawing.Charts.Marker class.
    /// </summary>
    public class SLMarker
    {
        internal bool HasMarker
        {
            get
            {
                return vSymbol != null || bySize != null || ShapeProperties.HasShapeProperties;
            }
        }

        internal C.MarkerStyleValues? vSymbol;

        /// <summary>
        /// Marker symbol.
        /// </summary>
        public C.MarkerStyleValues Symbol
        {
            get { return vSymbol ?? C.MarkerStyleValues.Auto; }
            set
            {
                vSymbol = value;
            }
        }

        internal byte? bySize;

        /// <summary>
        /// Range is 2 to 72 inclusive. Default is 5 in Open XML but Excel uses 7.
        /// </summary>
        public byte Size
        {
            get { return bySize ?? 5; }
            set
            {
                bySize = value;
                if (bySize != null)
                {
                    if (bySize < 2) bySize = 2;
                    if (bySize > 72) bySize = 72;
                }
            }
        }

        internal SLA.SLShapeProperties ShapeProperties { get; set; }

        /// <summary>
        /// Fill properties.
        /// </summary>
        public SLA.SLFill Fill { get { return ShapeProperties.Fill; } }

        /// <summary>
        /// Line properties.
        /// </summary>
        public SLA.SLLinePropertiesType Line { get { return ShapeProperties.Outline; } }

        internal SLMarker(List<System.Drawing.Color> ThemeColors)
        {
            vSymbol = null;
            bySize = null;
            ShapeProperties = new SLA.SLShapeProperties(ThemeColors);
        }

        internal C.Marker ToMarker(bool IsStylish = false)
        {
            C.Marker m = new C.Marker();
            if (vSymbol != null) m.Symbol = new C.Symbol() { Val = vSymbol.Value };
            if (bySize != null) m.Size = new C.Size() { Val = bySize.Value };

            if (ShapeProperties.HasShapeProperties)
            {
                m.ChartShapeProperties = ShapeProperties.ToChartShapeProperties(IsStylish);
            }

            return m;
        }

        internal SLMarker Clone()
        {
            SLMarker m = new SLMarker(ShapeProperties.listThemeColors)
            {
                Symbol = Symbol,
                bySize = bySize,
                ShapeProperties = ShapeProperties.Clone()
            };

            return m;
        }
    }
}