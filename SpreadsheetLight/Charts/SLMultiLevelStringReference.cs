﻿using System.Collections.Generic;
using C = DocumentFormat.OpenXml.Drawing.Charts;

namespace SpreadsheetLight.Charts
{
    internal class SLMultiLevelStringReference
    {
        internal string Formula { get; set; }
        internal SLMultiLevelStringCache MultiLevelStringCache { get; set; }

        internal SLMultiLevelStringReference()
        {
            Formula = string.Empty;
            MultiLevelStringCache = new SLMultiLevelStringCache();
        }

        internal C.MultiLevelStringReference ToMultiLevelStringReference()
        {
            C.MultiLevelStringReference mlsr = new C.MultiLevelStringReference
            {
                Formula = new C.Formula(Formula),
                MultiLevelStringCache = MultiLevelStringCache.ToMultiLevelStringCache()
            };

            return mlsr;
        }

        internal SLMultiLevelStringReference Clone()
        {
            SLMultiLevelStringReference mlsr = new SLMultiLevelStringReference
            {
                Formula = Formula,
                MultiLevelStringCache = MultiLevelStringCache.Clone()
            };

            return mlsr;
        }
    }

    internal class SLMultiLevelStringCache
    {
        internal uint PointCount { get; set; }

        internal List<SLLevel> Levels { get; set; }

        internal SLMultiLevelStringCache()
        {
            PointCount = 0;
            Levels = new List<SLLevel>();
        }

        internal C.MultiLevelStringCache ToMultiLevelStringCache()
        {
            C.MultiLevelStringCache mlsc = new C.MultiLevelStringCache
            {
                PointCount = new C.PointCount() { Val = PointCount }
            };

            C.Level lvl;
            int i, j;
            for (i = 0; i < Levels.Count; ++i)
            {
                lvl = new C.Level();
                for (j = 0; j < Levels[i].Points.Count; ++j)
                {
                    lvl.Append(Levels[i].Points[j].ToStringPoint());
                }
                mlsc.Append(lvl);
            }

            return mlsc;
        }

        internal SLMultiLevelStringCache Clone()
        {
            SLMultiLevelStringCache mlsc = new SLMultiLevelStringCache
            {
                PointCount = PointCount
            };
            for (int i = 0; i < Levels.Count; ++i)
            {
                mlsc.Levels.Add(Levels[i].Clone());
            }

            return mlsc;
        }
    }

    internal class SLLevel
    {
        internal List<SLStringPoint> Points { get; set; }

        internal SLLevel()
        {
            Points = new List<SLStringPoint>();
        }

        internal SLLevel Clone()
        {
            SLLevel lvl = new SLLevel();
            for (int i = 0; i < Points.Count; ++i)
            {
                lvl.Points.Add(Points[i].Clone());
            }

            return lvl;
        }
    }
}