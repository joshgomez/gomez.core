﻿using C = DocumentFormat.OpenXml.Drawing.Charts;

namespace SpreadsheetLight.Charts
{
    /// <summary>
    /// For BubbleSize, Minus, Plus, Values, YValues
    /// </summary>
    internal class SLNumberDataSourceType
    {
        private bool bUseNumberReference;

        internal bool UseNumberReference
        {
            get { return bUseNumberReference; }
            set
            {
                bUseNumberReference = value;
                if (value)
                {
                    bUseNumberReference = true;
                    bUseNumberLiteral = false;
                }
            }
        }

        internal SLNumberReference NumberReference { get; set; }

        private bool bUseNumberLiteral;

        internal bool UseNumberLiteral
        {
            get { return bUseNumberLiteral; }
            set
            {
                bUseNumberLiteral = value;
                if (value)
                {
                    bUseNumberReference = false;
                    bUseNumberLiteral = true;
                }
            }
        }

        internal SLNumberLiteral NumberLiteral { get; set; }

        internal SLNumberDataSourceType()
        {
            UseNumberReference = true;

            NumberReference = new SLNumberReference();
            NumberLiteral = new SLNumberLiteral();
        }

        internal C.BubbleSize ToBubbleSize()
        {
            C.BubbleSize bs = new C.BubbleSize();
            if (UseNumberReference) bs.NumberReference = NumberReference.ToNumberReference();
            if (UseNumberLiteral) bs.NumberLiteral = NumberLiteral.ToNumberLiteral();

            return bs;
        }

        internal C.Minus ToMinus()
        {
            C.Minus minus = new C.Minus();
            if (UseNumberReference) minus.NumberReference = NumberReference.ToNumberReference();
            if (UseNumberLiteral) minus.NumberLiteral = NumberLiteral.ToNumberLiteral();

            return minus;
        }

        internal C.Plus ToPlus()
        {
            C.Plus plus = new C.Plus();
            if (UseNumberReference) plus.NumberReference = NumberReference.ToNumberReference();
            if (UseNumberLiteral) plus.NumberLiteral = NumberLiteral.ToNumberLiteral();

            return plus;
        }

        internal C.Values ToValues()
        {
            C.Values v = new C.Values();
            if (UseNumberReference) v.NumberReference = NumberReference.ToNumberReference();
            if (UseNumberLiteral) v.NumberLiteral = NumberLiteral.ToNumberLiteral();

            return v;
        }

        internal C.YValues ToYValues()
        {
            C.YValues yv = new C.YValues();
            if (UseNumberReference) yv.NumberReference = NumberReference.ToNumberReference();
            if (UseNumberLiteral) yv.NumberLiteral = NumberLiteral.ToNumberLiteral();

            return yv;
        }

        internal SLNumberDataSourceType Clone()
        {
            SLNumberDataSourceType ndst = new SLNumberDataSourceType
            {
                bUseNumberReference = bUseNumberReference,
                NumberReference = NumberReference.Clone(),
                bUseNumberLiteral = bUseNumberLiteral,
                NumberLiteral = NumberLiteral.Clone()
            };

            return ndst;
        }
    }
}