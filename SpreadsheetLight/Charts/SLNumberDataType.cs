﻿using System.Collections.Generic;
using C = DocumentFormat.OpenXml.Drawing.Charts;

namespace SpreadsheetLight.Charts
{
    internal class SLNumberingCache : SLNumberDataType
    {
        internal SLNumberingCache()
        {
        }

        internal SLNumberingCache Clone()
        {
            SLNumberingCache nc = new SLNumberingCache
            {
                FormatCode = FormatCode,
                PointCount = PointCount
            };
            for (int i = 0; i < Points.Count; ++i)
            {
                nc.Points.Add(Points[i].Clone());
            }

            return nc;
        }
    }

    internal class SLNumberLiteral : SLNumberDataType
    {
        internal SLNumberLiteral()
        {
        }

        internal SLNumberLiteral Clone()
        {
            SLNumberLiteral nl = new SLNumberLiteral
            {
                FormatCode = FormatCode,
                PointCount = PointCount
            };
            for (int i = 0; i < Points.Count; ++i)
            {
                nl.Points.Add(Points[i].Clone());
            }

            return nl;
        }
    }

    /// <summary>
    /// For NumberingCache and NumberLiteral
    /// </summary>
    internal abstract class SLNumberDataType
    {
        internal string FormatCode { get; set; }
        internal uint PointCount { get; set; }
        internal List<SLNumericPoint> Points { get; set; }

        internal SLNumberDataType()
        {
            FormatCode = string.Empty;
            PointCount = 0;
            Points = new List<SLNumericPoint>();
        }

        internal C.NumberingCache ToNumberingCache()
        {
            C.NumberingCache nc = new C.NumberingCache
            {
                FormatCode = new C.FormatCode(FormatCode),
                PointCount = new C.PointCount() { Val = PointCount }
            };
            for (int i = 0; i < Points.Count; ++i)
            {
                nc.Append(Points[i].ToNumericPoint());
            }

            return nc;
        }

        internal C.NumberLiteral ToNumberLiteral()
        {
            C.NumberLiteral nl = new C.NumberLiteral
            {
                FormatCode = new C.FormatCode(FormatCode),
                PointCount = new C.PointCount() { Val = PointCount }
            };
            for (int i = 0; i < Points.Count; ++i)
            {
                nl.Append(Points[i].ToNumericPoint());
            }

            return nl;
        }
    }
}