﻿using C = DocumentFormat.OpenXml.Drawing.Charts;

namespace SpreadsheetLight.Charts
{
    internal class SLNumberReference
    {
        internal string WorksheetName { get; set; }
        internal int StartRowIndex { get; set; }
        internal int StartColumnIndex { get; set; }
        internal int EndRowIndex { get; set; }
        internal int EndColumnIndex { get; set; }

        internal string Formula { get; set; }
        internal SLNumberingCache NumberingCache { get; set; }

        internal SLNumberReference()
        {
            WorksheetName = string.Empty;
            StartRowIndex = 1;
            StartColumnIndex = 1;
            EndRowIndex = 1;
            EndColumnIndex = 1;

            Formula = string.Empty;
            NumberingCache = new SLNumberingCache();
        }

        internal C.NumberReference ToNumberReference()
        {
            C.NumberReference nr = new C.NumberReference
            {
                Formula = new C.Formula(Formula),
                NumberingCache = NumberingCache.ToNumberingCache()
            };

            return nr;
        }

        internal void RefreshFormula()
        {
            Formula = SLChartTool.GetChartReferenceFormula(WorksheetName, StartRowIndex, StartColumnIndex, EndRowIndex, EndColumnIndex);
        }

        internal SLNumberReference Clone()
        {
            SLNumberReference nr = new SLNumberReference
            {
                WorksheetName = WorksheetName,
                StartRowIndex = StartRowIndex,
                StartColumnIndex = StartColumnIndex,
                EndRowIndex = EndRowIndex,
                EndColumnIndex = EndColumnIndex,
                Formula = Formula,
                NumberingCache = NumberingCache.Clone()
            };

            return nr;
        }
    }
}