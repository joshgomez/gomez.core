﻿using C = DocumentFormat.OpenXml.Drawing.Charts;

namespace SpreadsheetLight.Charts
{
    internal class SLNumericPoint
    {
        internal string NumericValue { get; set; }
        internal uint Index { get; set; }
        internal string FormatCode { get; set; }

        internal SLNumericPoint()
        {
            NumericValue = string.Empty;
            Index = 0;
            FormatCode = string.Empty;
        }

        internal C.NumericPoint ToNumericPoint()
        {
            C.NumericPoint np = new C.NumericPoint
            {
                Index = Index
            };
            if (FormatCode.Length > 0) np.FormatCode = FormatCode;
            np.NumericValue = new C.NumericValue(NumericValue);

            return np;
        }

        internal SLNumericPoint Clone()
        {
            SLNumericPoint np = new SLNumericPoint
            {
                NumericValue = NumericValue,
                Index = Index,
                FormatCode = FormatCode
            };

            return np;
        }
    }
}