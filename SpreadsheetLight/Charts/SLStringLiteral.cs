﻿using System.Collections.Generic;
using C = DocumentFormat.OpenXml.Drawing.Charts;

namespace SpreadsheetLight.Charts
{
    internal class SLStringLiteral
    {
        internal uint PointCount { get; set; }
        internal List<SLStringPoint> Points { get; set; }

        internal SLStringLiteral()
        {
            PointCount = 0;
            Points = new List<SLStringPoint>();
        }

        internal C.StringLiteral ToStringLiteral()
        {
            C.StringLiteral sl = new C.StringLiteral
            {
                PointCount = new C.PointCount() { Val = PointCount }
            };
            for (int i = 0; i < Points.Count; ++i)
            {
                sl.Append(Points[i].ToStringPoint());
            }

            return sl;
        }

        internal SLStringLiteral Clone()
        {
            SLStringLiteral sl = new SLStringLiteral
            {
                PointCount = PointCount
            };
            for (int i = 0; i < Points.Count; ++i)
            {
                sl.Points.Add(Points[i].Clone());
            }

            return sl;
        }
    }
}