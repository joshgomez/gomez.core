﻿using C = DocumentFormat.OpenXml.Drawing.Charts;

namespace SpreadsheetLight.Charts
{
    internal class SLStringPoint
    {
        internal string NumericValue { get; set; }
        internal uint Index { get; set; }

        internal SLStringPoint()
        {
            NumericValue = string.Empty;
            Index = 0;
        }

        internal C.StringPoint ToStringPoint()
        {
            C.StringPoint sp = new C.StringPoint
            {
                Index = Index,
                NumericValue = new C.NumericValue(NumericValue)
            };

            return sp;
        }

        internal SLStringPoint Clone()
        {
            SLStringPoint sp = new SLStringPoint
            {
                NumericValue = NumericValue,
                Index = Index
            };

            return sp;
        }
    }
}