﻿using System.Collections.Generic;
using C = DocumentFormat.OpenXml.Drawing.Charts;

namespace SpreadsheetLight.Charts
{
    internal class SLStringReference
    {
        internal string WorksheetName { get; set; }
        internal int StartRowIndex { get; set; }
        internal int StartColumnIndex { get; set; }
        internal int EndRowIndex { get; set; }
        internal int EndColumnIndex { get; set; }

        internal string Formula { get; set; }

        // this is StringCache
        internal uint PointCount { get; set; }

        /// <summary>
        /// This takes the place of StringCache
        /// </summary>
        internal List<SLStringPoint> Points { get; set; }

        internal SLStringReference()
        {
            WorksheetName = string.Empty;
            StartRowIndex = 1;
            StartColumnIndex = 1;
            EndRowIndex = 1;
            EndColumnIndex = 1;

            Formula = string.Empty;
            PointCount = 0;
            Points = new List<SLStringPoint>();
        }

        internal C.StringReference ToStringReference()
        {
            C.StringReference sr = new C.StringReference
            {
                Formula = new C.Formula(Formula),
                StringCache = new C.StringCache
                {
                    PointCount = new C.PointCount() { Val = PointCount }
                }
            };
            for (int i = 0; i < Points.Count; ++i)
            {
                sr.StringCache.Append(Points[i].ToStringPoint());
            }

            return sr;
        }

        internal void RefreshFormula()
        {
            Formula = SLChartTool.GetChartReferenceFormula(WorksheetName, StartRowIndex, StartColumnIndex, EndRowIndex, EndColumnIndex);
        }

        internal SLStringReference Clone()
        {
            SLStringReference sr = new SLStringReference
            {
                WorksheetName = WorksheetName,
                StartRowIndex = StartRowIndex,
                StartColumnIndex = StartColumnIndex,
                EndRowIndex = EndRowIndex,
                EndColumnIndex = EndColumnIndex,
                Formula = Formula,
                PointCount = PointCount
            };
            for (int i = 0; i < Points.Count; ++i)
            {
                sr.Points.Add(Points[i].Clone());
            }

            return sr;
        }
    }
}