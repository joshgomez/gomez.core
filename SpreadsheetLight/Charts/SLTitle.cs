﻿using System.Collections.Generic;
using A = DocumentFormat.OpenXml.Drawing;
using C = DocumentFormat.OpenXml.Drawing.Charts;
using SLA = SpreadsheetLight.Drawing;

namespace SpreadsheetLight.Charts
{
    /// <summary>
    /// Encapsulates properties and methods for setting titles for charts.
    /// This simulates the DocumentFormat.OpenXml.Drawing.Charts.Title class.
    /// </summary>
    public class SLTitle : SLChartAlignment
    {
        internal SLRstType rst { get; set; }

        /// <summary>
        /// Title text. This returns the plain text version if rich text is applied.
        /// </summary>
        public string Text
        {
            get { return rst.ToPlainString(); }
            set
            {
                rst = new SLRstType(SLConstants.OfficeThemeMajorLatinFont, SLConstants.OfficeThemeMinorLatinFont, new List<System.Drawing.Color>(), new List<System.Drawing.Color>());
                rst.SetText(value);
            }
        }

        /// <summary>
        /// Specifies if the title overlaps.
        /// </summary>
        public bool Overlay { get; set; }

        internal SLA.SLShapeProperties ShapeProperties;

        /// <summary>
        /// Fill properties.
        /// </summary>
        public SLA.SLFill Fill { get { return ShapeProperties.Fill; } }

        /// <summary>
        /// Border properties.
        /// </summary>
        public SLA.SLLinePropertiesType Border { get { return ShapeProperties.Outline; } }

        /// <summary>
        /// Shadow properties.
        /// </summary>
        public SLA.SLShadowEffect Shadow { get { return ShapeProperties.EffectList.Shadow; } }

        /// <summary>
        /// Glow properties.
        /// </summary>
        public SLA.SLGlow Glow { get { return ShapeProperties.EffectList.Glow; } }

        /// <summary>
        /// Soft edge properties.
        /// </summary>
        public SLA.SLSoftEdge SoftEdge { get { return ShapeProperties.EffectList.SoftEdge; } }

        /// <summary>
        /// 3D format properties.
        /// </summary>
        public SLA.SLFormat3D Format3D { get { return ShapeProperties.Format3D; } }

        internal SLTitle(List<System.Drawing.Color> ThemeColors, bool IsStylish = false)
        {
            // just put in the theme colors, even though it's probably not needed.
            // Memory optimisations? Take it out.
            rst = new SLRstType(SLConstants.OfficeThemeMajorLatinFont, SLConstants.OfficeThemeMinorLatinFont, ThemeColors, new List<System.Drawing.Color>());
            Overlay = false;
            ShapeProperties = new SLA.SLShapeProperties(ThemeColors);

            if (IsStylish)
            {
                ShapeProperties.Fill.SetNoFill();
                ShapeProperties.Outline.SetNoLine();
            }

            RemoveTextAlignment();
        }

        /// <summary>
        /// Clear all styling shape properties. Use this if you want to start styling from a clean slate.
        /// </summary>
        public void ClearShapeProperties()
        {
            ShapeProperties = new SLA.SLShapeProperties(ShapeProperties.listThemeColors);
        }

        /// <summary>
        /// Set the title text.
        /// </summary>
        /// <param name="Text">The title text.</param>
        public void SetTitle(string Text)
        {
            this.Text = Text;
        }

        /// <summary>
        /// Set the title with a rich text string.
        /// </summary>
        /// <param name="RichText">The rich text.</param>
        public void SetTitle(SLRstType RichText)
        {
            rst = RichText.Clone();
        }

        internal C.Title ToTitle(bool IsStylish = false)
        {
            C.Title t = new C.Title();

            bool bHasText = rst.ToPlainString().Length > 0;
            if (bHasText || Rotation != null || Vertical != null || Anchor != null || AnchorCenter != null)
            {
                t.ChartText = new C.ChartText
                {
                    RichText = new C.RichText
                    {
                        BodyProperties = new A.BodyProperties()
                    }
                };

                if (Rotation != null || Vertical != null || Anchor != null || AnchorCenter != null)
                {
                    if (Rotation != null) t.ChartText.RichText.BodyProperties.Rotation = (int)(Rotation.Value * SLConstants.DegreeToAngleRepresentation);
                    if (Vertical != null) t.ChartText.RichText.BodyProperties.Vertical = Vertical.Value;
                    if (Anchor != null) t.ChartText.RichText.BodyProperties.Anchor = Anchor.Value;
                    if (AnchorCenter != null) t.ChartText.RichText.BodyProperties.AnchorCenter = AnchorCenter.Value;
                }

                t.ChartText.RichText.ListStyle = new A.ListStyle();

                if (bHasText) t.ChartText.RichText.Append(rst.ToParagraph());
            }

            t.Layout = new C.Layout();
            t.Overlay = new C.Overlay() { Val = Overlay };
            if (ShapeProperties.HasShapeProperties) t.ChartShapeProperties = ShapeProperties.ToChartShapeProperties(IsStylish);

            return t;
        }

        internal SLTitle Clone()
        {
            SLTitle t = new SLTitle(ShapeProperties.listThemeColors)
            {
                Rotation = Rotation,
                Vertical = Vertical,
                Anchor = Anchor,
                AnchorCenter = AnchorCenter,
                rst = rst.Clone(),
                Overlay = Overlay,
                ShapeProperties = ShapeProperties.Clone()
            };

            return t;
        }
    }
}