﻿using System.Collections.Generic;
using A = DocumentFormat.OpenXml.Drawing;
using C = DocumentFormat.OpenXml.Drawing.Charts;
using SLA = SpreadsheetLight.Drawing;

namespace SpreadsheetLight.Charts
{
    /// <summary>
    /// Encapsulates properties and methods for up bars.
    /// This simulates the DocumentFormat.OpenXml.Drawing.Charts.UpBars class.
    /// </summary>
    public class SLUpBars
    {
        internal SLA.SLShapeProperties ShapeProperties { get; set; }

        /// <summary>
        /// Fill properties.
        /// </summary>
        public SLA.SLFill Fill { get { return ShapeProperties.Fill; } }

        /// <summary>
        /// Border properties.
        /// </summary>
        public SLA.SLLinePropertiesType Border { get { return ShapeProperties.Outline; } }

        /// <summary>
        /// Shadow properties.
        /// </summary>
        public SLA.SLShadowEffect Shadow { get { return ShapeProperties.EffectList.Shadow; } }

        /// <summary>
        /// Glow properties.
        /// </summary>
        public SLA.SLGlow Glow { get { return ShapeProperties.EffectList.Glow; } }

        /// <summary>
        /// Soft edge properties.
        /// </summary>
        public SLA.SLSoftEdge SoftEdge { get { return ShapeProperties.EffectList.SoftEdge; } }

        /// <summary>
        /// 3D format properties.
        /// </summary>
        public SLA.SLFormat3D Format3D { get { return ShapeProperties.Format3D; } }

        internal SLUpBars(List<System.Drawing.Color> ThemeColors, bool IsStylish = false)
        {
            ShapeProperties = new SLA.SLShapeProperties(ThemeColors);
            if (IsStylish)
            {
                ShapeProperties.Fill.SetSolidFill(A.SchemeColorValues.Light1, 0, 0);
                ShapeProperties.Outline.Width = 0.75m;
                ShapeProperties.Outline.SetSolidLine(A.SchemeColorValues.Text1, 0.85m, 0);
            }
        }

        /// <summary>
        /// Clear all styling shape properties. Use this if you want to start styling from a clean slate.
        /// </summary>
        public void ClearShapeProperties()
        {
            ShapeProperties = new SLA.SLShapeProperties(ShapeProperties.listThemeColors);
        }

        internal C.UpBars ToUpBars(bool IsStylish = false)
        {
            C.UpBars ub = new C.UpBars();

            if (ShapeProperties.HasShapeProperties) ub.ChartShapeProperties = ShapeProperties.ToChartShapeProperties(IsStylish);

            return ub;
        }

        internal SLUpBars Clone()
        {
            SLUpBars ub = new SLUpBars(ShapeProperties.listThemeColors)
            {
                ShapeProperties = ShapeProperties.Clone()
            };

            return ub;
        }
    }
}