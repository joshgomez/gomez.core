﻿using A = DocumentFormat.OpenXml.Drawing;

namespace SpreadsheetLight.Drawing
{
    internal class SLExtents
    {
        internal long Cx { get; set; }
        internal long Cy { get; set; }

        internal SLExtents()
        {
            Cx = 0;
            Cy = 0;
        }

        internal A.Extents ToExtents()
        {
            A.Extents ext = new A.Extents
            {
                Cx = Cx,
                Cy = Cy
            };

            return ext;
        }

        internal SLExtents Clone()
        {
            SLExtents ext = new SLExtents
            {
                Cx = Cx,
                Cy = Cy
            };

            return ext;
        }
    }
}