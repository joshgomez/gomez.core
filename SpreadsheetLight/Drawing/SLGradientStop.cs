﻿using System;
using System.Collections.Generic;
using A = DocumentFormat.OpenXml.Drawing;

namespace SpreadsheetLight.Drawing
{
    internal class SLGradientStop
    {
        internal SLColorTransform Color { get; set; }
        private decimal decPosition;

        /// <summary>
        /// The position in percentage ranging from 0% to 100%. Accurate to 1/1000 of a percent.
        /// </summary>
        internal decimal Position
        {
            get { return decPosition; }
            set
            {
                decPosition = value;
                if (decPosition < 0m) decPosition = 0m;
                if (decPosition > 100m) decPosition = 100m;
            }
        }

        internal SLGradientStop(List<System.Drawing.Color> ThemeColors)
        {
            Color = new SLColorTransform(ThemeColors);
            Position = 0m;
        }

        internal SLGradientStop(List<System.Drawing.Color> ThemeColors, string HexColor, decimal Position)
        {
            Color = new SLColorTransform(ThemeColors);
            this.Position = Position;

            System.Drawing.Color clr = new System.Drawing.Color();
            try
            {
                clr = System.Drawing.Color.FromArgb(int.Parse(HexColor, System.Globalization.NumberStyles.HexNumber));
            }
            catch
            {
                clr = System.Drawing.Color.White;
            }
            Color.SetColor(clr, 0);
        }

        internal A.GradientStop ToGradientStop()
        {
            A.GradientStop gs = new A.GradientStop();
            if (Color.IsRgbColorModelHex) gs.RgbColorModelHex = Color.ToRgbColorModelHex();
            else gs.SchemeColor = Color.ToSchemeColor();

            gs.Position = Convert.ToInt32(Position * 1000m);

            return gs;
        }

        internal SLGradientStop Clone()
        {
            SLGradientStop gs = new SLGradientStop(Color.listThemeColors)
            {
                Color = Color.Clone(),
                decPosition = decPosition
            };

            return gs;
        }
    }
}