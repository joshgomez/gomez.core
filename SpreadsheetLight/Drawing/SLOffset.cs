﻿using A = DocumentFormat.OpenXml.Drawing;

namespace SpreadsheetLight.Drawing
{
    internal class SLOffset
    {
        internal long X { get; set; }
        internal long Y { get; set; }

        internal SLOffset()
        {
            X = 0;
            Y = 0;
        }

        internal A.Offset ToOffset()
        {
            A.Offset off = new A.Offset
            {
                X = X,
                Y = Y
            };

            return off;
        }

        internal SLOffset Clone()
        {
            SLOffset off = new SLOffset
            {
                X = X,
                Y = Y
            };

            return off;
        }
    }
}