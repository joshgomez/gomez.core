﻿using A = DocumentFormat.OpenXml.Drawing;

namespace SpreadsheetLight.Drawing
{
    internal class SLTransform2D
    {
        internal bool HasOffset;
        internal SLOffset Offset { get; set; }
        internal bool HasExtents;
        internal SLExtents Extents { get; set; }

        internal int? Rotation { get; set; }
        internal bool? HorizontalFlip { get; set; }
        internal bool? VerticalFlip { get; set; }

        internal SLTransform2D()
        {
            HasOffset = false;
            Offset = new SLOffset();
            HasExtents = false;
            Extents = new SLExtents();

            Rotation = null;
            HorizontalFlip = null;
            VerticalFlip = null;
        }

        internal A.Transform2D ToTransform2D()
        {
            A.Transform2D trans = new A.Transform2D();
            if (HasOffset) trans.Offset = Offset.ToOffset();
            if (HasExtents) trans.Extents = Extents.ToExtents();

            if (Rotation != null) trans.Rotation = Rotation.Value;
            if (HorizontalFlip != null) trans.HorizontalFlip = HorizontalFlip.Value;
            if (VerticalFlip != null) trans.VerticalFlip = VerticalFlip.Value;

            return trans;
        }

        internal SLTransform2D Clone()
        {
            SLTransform2D trans = new SLTransform2D
            {
                HasOffset = HasOffset,
                Offset = Offset.Clone(),
                HasExtents = HasExtents,
                Extents = Extents.Clone(),

                Rotation = Rotation,
                HorizontalFlip = HorizontalFlip,
                VerticalFlip = VerticalFlip
            };

            return trans;
        }
    }
}