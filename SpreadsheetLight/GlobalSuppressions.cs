﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Minor Code Smell", "S3220:Method calls should not resolve ambiguously to overloads with \"params\"", Justification = "<Pending>", Scope = "member", Target = "~M:SpreadsheetLight.SLDocument.WriteSelectedWorksheet")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Minor Code Smell", "S3220:Method calls should not resolve ambiguously to overloads with \"params\"", Justification = "<Pending>", Scope = "member", Target = "~M:SpreadsheetLight.SLDocument.WriteWorkbook")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Minor Code Smell", "S3220:Method calls should not resolve ambiguously to overloads with \"params\"", Justification = "<Pending>", Scope = "member", Target = "~M:SpreadsheetLight.SLDocument.WriteTabSelectedWorksheet")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Minor Code Smell", "S3220:Method calls should not resolve ambiguously to overloads with \"params\"", Justification = "<Pending>", Scope = "member", Target = "~M:SpreadsheetLight.Charts.SLDataLabelOptions.ToDataLabel(System.Int32)~DocumentFormat.OpenXml.Drawing.Charts.DataLabel")]