﻿using DocumentFormat.OpenXml.Spreadsheet;
using X14 = DocumentFormat.OpenXml.Office2010.Excel;

namespace SpreadsheetLight
{
    internal class SLConditionalFormatValueObject
    {
        internal ConditionalFormatValueObjectValues Type { get; set; }
        internal string Val { get; set; }
        internal bool GreaterThanOrEqual { get; set; }

        internal SLConditionalFormatValueObject()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            Type = ConditionalFormatValueObjectValues.Percentile;
            Val = string.Empty;
            GreaterThanOrEqual = true;
        }

        internal void FromConditionalFormatValueObject(ConditionalFormatValueObject cfvo)
        {
            SetAllNull();

            Type = cfvo.Type.Value;
            if (cfvo.Val != null) Val = cfvo.Val.Value;
            if (cfvo.GreaterThanOrEqual != null) GreaterThanOrEqual = cfvo.GreaterThanOrEqual.Value;
        }

        internal ConditionalFormatValueObject ToConditionalFormatValueObject()
        {
            ConditionalFormatValueObject cfvo = new ConditionalFormatValueObject
            {
                Type = Type
            };

            if (Val.Length > 0)
            {
                if (Val.StartsWith("=")) cfvo.Val = Val.Substring(1);
                else cfvo.Val = Val;
            }

            if (!GreaterThanOrEqual) cfvo.GreaterThanOrEqual = false;

            return cfvo;
        }

        internal SLConditionalFormattingValueObject2010 ToSLConditionalFormattingValueObject2010()
        {
            SLConditionalFormattingValueObject2010 cfvo2010 = new SLConditionalFormattingValueObject2010
            {
                Formula = Val
            };

            switch (Type)
            {
                case ConditionalFormatValueObjectValues.Formula:
                    cfvo2010.Type = X14.ConditionalFormattingValueObjectTypeValues.Formula;
                    break;

                case ConditionalFormatValueObjectValues.Max:
                    cfvo2010.Type = X14.ConditionalFormattingValueObjectTypeValues.Max;
                    break;

                case ConditionalFormatValueObjectValues.Min:
                    cfvo2010.Type = X14.ConditionalFormattingValueObjectTypeValues.Min;
                    break;

                case ConditionalFormatValueObjectValues.Number:
                    cfvo2010.Type = X14.ConditionalFormattingValueObjectTypeValues.Numeric;
                    break;

                case ConditionalFormatValueObjectValues.Percent:
                    cfvo2010.Type = X14.ConditionalFormattingValueObjectTypeValues.Percent;
                    break;

                case ConditionalFormatValueObjectValues.Percentile:
                    cfvo2010.Type = X14.ConditionalFormattingValueObjectTypeValues.Percentile;
                    break;
            }

            cfvo2010.GreaterThanOrEqual = GreaterThanOrEqual;

            return cfvo2010;
        }

        internal SLConditionalFormatValueObject Clone()
        {
            SLConditionalFormatValueObject cfvo = new SLConditionalFormatValueObject
            {
                Type = Type,
                Val = Val,
                GreaterThanOrEqual = GreaterThanOrEqual
            };

            return cfvo;
        }
    }
}