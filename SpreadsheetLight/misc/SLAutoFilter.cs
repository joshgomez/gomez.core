﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Collections.Generic;

namespace SpreadsheetLight
{
    internal class SLAutoFilter
    {
        internal List<SLFilterColumn> FilterColumns { get; set; }
        internal bool HasSortState;
        internal SLSortState SortState { get; set; }

        internal int StartRowIndex { get; set; }
        internal int StartColumnIndex { get; set; }
        internal int EndRowIndex { get; set; }
        internal int EndColumnIndex { get; set; }

        internal SLAutoFilter()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            FilterColumns = new List<SLFilterColumn>();
            SortState = new SLSortState();
            HasSortState = false;
            StartRowIndex = 1;
            StartColumnIndex = 1;
            EndRowIndex = 1;
            EndColumnIndex = 1;
        }

        internal void FromAutoFilter(AutoFilter af)
        {
            SetAllNull();

            int iStartRowIndex = 1;
            int iStartColumnIndex = 1;
            int iEndRowIndex = 1;
            int iEndColumnIndex = 1;
            string sRef = af.Reference.Value;
            if (sRef.IndexOf(":") > 0)
            {
                if (SLTool.FormatCellReferenceRangeToRowColumnIndex(sRef, out iStartRowIndex, out iStartColumnIndex, out iEndRowIndex, out iEndColumnIndex))
                {
                    StartRowIndex = iStartRowIndex;
                    StartColumnIndex = iStartColumnIndex;
                    EndRowIndex = iEndRowIndex;
                    EndColumnIndex = iEndColumnIndex;
                }
            }
            else
            {
                if (SLTool.FormatCellReferenceToRowColumnIndex(sRef, out iStartRowIndex, out iStartColumnIndex))
                {
                    StartRowIndex = iStartRowIndex;
                    StartColumnIndex = iStartColumnIndex;
                    EndRowIndex = iStartRowIndex;
                    EndColumnIndex = iStartColumnIndex;
                }
            }

            if (af.HasChildren)
            {
                SLFilterColumn fc;
                using (OpenXmlReader oxr = OpenXmlReader.Create(af))
                {
                    while (oxr.Read())
                    {
                        if (oxr.ElementType == typeof(FilterColumn))
                        {
                            fc = new SLFilterColumn();
                            fc.FromFilterColumn((FilterColumn)oxr.LoadCurrentElement());
                            FilterColumns.Add(fc);
                        }
                        else if (oxr.ElementType == typeof(SortState))
                        {
                            SortState = new SLSortState();
                            SortState.FromSortState((SortState)oxr.LoadCurrentElement());
                            HasSortState = true;
                        }
                    }
                }
            }
        }

        internal AutoFilter ToAutoFilter()
        {
            AutoFilter af = new AutoFilter();

            if (StartRowIndex == EndRowIndex && StartColumnIndex == EndColumnIndex)
            {
                af.Reference = SLTool.ToCellReference(StartRowIndex, StartColumnIndex);
            }
            else
            {
                af.Reference = string.Format("{0}:{1}",
                    SLTool.ToCellReference(StartRowIndex, StartColumnIndex),
                    SLTool.ToCellReference(EndRowIndex, EndColumnIndex));
            }

            foreach (SLFilterColumn fc in FilterColumns)
            {
                af.Append(fc.ToFilterColumn());
            }

            if (HasSortState) af.Append(SortState.ToSortState());

            return af;
        }

        internal SLAutoFilter Clone()
        {
            SLAutoFilter af = new SLAutoFilter
            {
                FilterColumns = new List<SLFilterColumn>()
            };
            for (int i = 0; i < FilterColumns.Count; ++i)
            {
                af.FilterColumns.Add(FilterColumns[i].Clone());
            }

            af.HasSortState = HasSortState;
            af.SortState = SortState.Clone();

            af.StartRowIndex = StartRowIndex;
            af.StartColumnIndex = StartColumnIndex;
            af.EndRowIndex = EndRowIndex;
            af.EndColumnIndex = EndColumnIndex;

            return af;
        }
    }
}