﻿using DocumentFormat.OpenXml.Spreadsheet;

namespace SpreadsheetLight
{
    internal class SLSortCondition
    {
        internal bool? Descending { get; set; }

        internal bool HasSortBy;
        private SortByValues vSortBy;

        internal SortByValues SortBy
        {
            get { return vSortBy; }
            set
            {
                vSortBy = value;
                HasSortBy = vSortBy != SortByValues.Value ? true : false;
            }
        }

        internal int StartRowIndex { get; set; }
        internal int StartColumnIndex { get; set; }
        internal int EndRowIndex { get; set; }
        internal int EndColumnIndex { get; set; }

        internal string CustomList { get; set; }
        internal uint? FormatId { get; set; }

        internal bool HasIconSet;
        private IconSetValues vIconSet;

        internal IconSetValues IconSet
        {
            get { return vIconSet; }
            set
            {
                vIconSet = value;
                HasIconSet = vIconSet != IconSetValues.ThreeArrows ? true : false;
            }
        }

        internal uint? IconId { get; set; }

        internal SLSortCondition()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            Descending = null;
            vSortBy = SortByValues.Value;
            HasSortBy = false;

            StartRowIndex = 1;
            StartColumnIndex = 1;
            EndRowIndex = 1;
            EndColumnIndex = 1;

            CustomList = null;
            FormatId = null;

            vIconSet = IconSetValues.ThreeArrows;
            HasIconSet = false;

            IconId = null;
        }

        internal void FromSortCondition(SortCondition sc)
        {
            SetAllNull();

            if (sc.Descending != null && sc.Descending.Value) Descending = sc.Descending.Value;
            if (sc.SortBy != null) SortBy = sc.SortBy.Value;

            int iStartRowIndex = 1;
            int iStartColumnIndex = 1;
            int iEndRowIndex = 1;
            int iEndColumnIndex = 1;
            string sRef = sc.Reference.Value;
            if (sRef.IndexOf(":") > 0)
            {
                if (SLTool.FormatCellReferenceRangeToRowColumnIndex(sRef, out iStartRowIndex, out iStartColumnIndex, out iEndRowIndex, out iEndColumnIndex))
                {
                    StartRowIndex = iStartRowIndex;
                    StartColumnIndex = iStartColumnIndex;
                    EndRowIndex = iEndRowIndex;
                    EndColumnIndex = iEndColumnIndex;
                }
            }
            else
            {
                if (SLTool.FormatCellReferenceToRowColumnIndex(sRef, out iStartRowIndex, out iStartColumnIndex))
                {
                    StartRowIndex = iStartRowIndex;
                    StartColumnIndex = iStartColumnIndex;
                    EndRowIndex = iStartRowIndex;
                    EndColumnIndex = iStartColumnIndex;
                }
            }

            if (sc.CustomList != null) CustomList = sc.CustomList.Value;
            if (sc.FormatId != null) FormatId = sc.FormatId.Value;
            if (sc.IconSet != null) IconSet = sc.IconSet.Value;
            if (sc.IconId != null) IconId = sc.IconId.Value;
        }

        internal SortCondition ToSortCondition()
        {
            SortCondition sc = new SortCondition();
            if (Descending != null) sc.Descending = Descending.Value;
            if (HasSortBy) sc.SortBy = SortBy;

            if (StartRowIndex == EndRowIndex && StartColumnIndex == EndColumnIndex)
            {
                sc.Reference = SLTool.ToCellReference(StartRowIndex, StartColumnIndex);
            }
            else
            {
                sc.Reference = string.Format("{0}:{1}",
                    SLTool.ToCellReference(StartRowIndex, StartColumnIndex),
                    SLTool.ToCellReference(EndRowIndex, EndColumnIndex));
            }

            if (CustomList != null) sc.CustomList = CustomList;
            if (FormatId != null) sc.FormatId = FormatId;
            if (HasIconSet) sc.IconSet = IconSet;
            if (IconId != null) sc.IconId = IconId.Value;

            return sc;
        }

        internal SLSortCondition Clone()
        {
            SLSortCondition sc = new SLSortCondition
            {
                Descending = Descending,
                HasSortBy = HasSortBy,
                vSortBy = vSortBy,
                StartRowIndex = StartRowIndex,
                StartColumnIndex = StartColumnIndex,
                EndRowIndex = EndRowIndex,
                EndColumnIndex = EndColumnIndex,
                CustomList = CustomList,
                FormatId = FormatId,
                HasIconSet = HasIconSet,
                vIconSet = vIconSet,
                IconId = IconId
            };

            return sc;
        }
    }
}