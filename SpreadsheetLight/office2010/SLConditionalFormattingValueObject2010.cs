﻿using Excel = DocumentFormat.OpenXml.Office.Excel;
using X14 = DocumentFormat.OpenXml.Office2010.Excel;

namespace SpreadsheetLight
{
    internal class SLConditionalFormattingValueObject2010
    {
        //http://msdn.microsoft.com/en-us/library/documentformat.openxml.office2010.excel.conditionalformattingvalueobject.aspx

        internal string Formula { get; set; }
        internal X14.ConditionalFormattingValueObjectTypeValues Type { get; set; }
        internal bool GreaterThanOrEqual { get; set; }

        internal SLConditionalFormattingValueObject2010()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            Formula = string.Empty;
            Type = X14.ConditionalFormattingValueObjectTypeValues.Percentile;
            GreaterThanOrEqual = true;
        }

        internal void FromConditionalFormattingValueObject(X14.ConditionalFormattingValueObject cfvo)
        {
            SetAllNull();

            if (cfvo.Formula != null) Formula = cfvo.Formula.Text;
            Type = cfvo.Type.Value;
            if (cfvo.GreaterThanOrEqual != null) GreaterThanOrEqual = cfvo.GreaterThanOrEqual.Value;
        }

        internal X14.ConditionalFormattingValueObject ToConditionalFormattingValueObject()
        {
            X14.ConditionalFormattingValueObject cfvo = new X14.ConditionalFormattingValueObject();

            if (Formula.Length > 0)
            {
                if (Formula.StartsWith("="))
                {
                    cfvo.Formula = new Excel.Formula(Formula.Substring(1));
                }
                else
                {
                    cfvo.Formula = new Excel.Formula(Formula);
                }
            }
            cfvo.Type = Type;
            if (!GreaterThanOrEqual) cfvo.GreaterThanOrEqual = GreaterThanOrEqual;

            return cfvo;
        }

        internal SLConditionalFormattingValueObject2010 Clone()
        {
            SLConditionalFormattingValueObject2010 cfvo = new SLConditionalFormattingValueObject2010
            {
                Formula = Formula,
                Type = Type,
                GreaterThanOrEqual = GreaterThanOrEqual
            };

            return cfvo;
        }
    }
}