﻿using DocumentFormat.OpenXml.Spreadsheet;

namespace SpreadsheetLight
{
    internal class SLAutoSortScope
    {
        internal SLPivotArea PivotArea { get; set; }

        internal SLAutoSortScope()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            PivotArea = new SLPivotArea();
        }

        // ahahahahah... I did *not* just come up with this variable name... :)
        internal void FromAutoSortScope(AutoSortScope ass)
        {
            SetAllNull();

            if (ass.PivotArea != null) PivotArea.FromPivotArea(ass.PivotArea);
        }

        internal AutoSortScope ToAutoSortScope()
        {
            AutoSortScope ass = new AutoSortScope
            {
                PivotArea = PivotArea.ToPivotArea()
            };

            return ass;
        }

        internal SLAutoSortScope Clone()
        {
            SLAutoSortScope ass = new SLAutoSortScope
            {
                PivotArea = PivotArea.Clone()
            };

            return ass;
        }
    }
}