﻿using DocumentFormat.OpenXml.Spreadsheet;

namespace SpreadsheetLight
{
    internal class SLCalculatedItem
    {
        internal SLPivotArea PivotArea { get; set; }

        internal uint? Field { get; set; }
        internal string Formula { get; set; }

        internal SLCalculatedItem()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            PivotArea = new SLPivotArea();
            Field = null;
            Formula = "";
        }

        internal void FromCalculatedItem(CalculatedItem ci)
        {
            SetAllNull();

            if (ci.Field != null) Field = ci.Field.Value;
            if (ci.Formula != null) Formula = ci.Formula.Value;

            if (ci.PivotArea != null) PivotArea.FromPivotArea(ci.PivotArea);
        }

        internal CalculatedItem ToCalculatedItem()
        {
            CalculatedItem ci = new CalculatedItem();
            if (Field != null) ci.Field = Field.Value;
            if (Formula != null && Formula.Length > 0) ci.Formula = Formula;

            ci.PivotArea = PivotArea.ToPivotArea();

            return ci;
        }

        internal SLCalculatedItem Clone()
        {
            SLCalculatedItem ci = new SLCalculatedItem
            {
                Field = Field,
                Formula = Formula,
                PivotArea = PivotArea.Clone()
            };

            return ci;
        }
    }
}