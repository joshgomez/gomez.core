﻿using DocumentFormat.OpenXml.Spreadsheet;

namespace SpreadsheetLight
{
    internal class SLChartFormat
    {
        internal SLPivotArea PivotArea { get; set; }
        internal uint Chart { get; set; }
        internal uint Format { get; set; }
        internal bool Series { get; set; }

        internal SLChartFormat()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            PivotArea = new SLPivotArea();
            Chart = 0;
            Format = 0;
            Series = false;
        }

        internal void FromChartFormat(ChartFormat cf)
        {
            SetAllNull();

            if (cf.PivotArea != null) PivotArea.FromPivotArea(cf.PivotArea);

            if (cf.Chart != null) Chart = cf.Chart.Value;
            if (cf.Format != null) Format = cf.Format.Value;
            if (cf.Series != null) Series = cf.Series.Value;
        }

        internal ChartFormat ToChartFormat()
        {
            ChartFormat cf = new ChartFormat
            {
                PivotArea = PivotArea.ToPivotArea(),

                Chart = Chart,
                Format = Format
            };
            if (Series != false) cf.Series = Series;

            return cf;
        }

        internal SLChartFormat Clone()
        {
            SLChartFormat cf = new SLChartFormat
            {
                PivotArea = PivotArea.Clone(),
                Chart = Chart,
                Format = Format,
                Series = Series
            };

            return cf;
        }
    }
}