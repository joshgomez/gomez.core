﻿using DocumentFormat.OpenXml.Spreadsheet;

namespace SpreadsheetLight
{
    internal class SLDataField
    {
        internal string Name { get; set; }
        internal uint Field { get; set; }
        internal DataConsolidateFunctionValues Subtotal { get; set; }
        internal ShowDataAsValues ShowDataAs { get; set; }
        internal int BaseField { get; set; }
        internal uint BaseItem { get; set; }
        internal uint? NumberFormatId { get; set; }

        internal SLDataField()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            Name = "";
            Field = 1;
            Subtotal = DataConsolidateFunctionValues.Sum;
            ShowDataAs = ShowDataAsValues.Normal;
            BaseField = -1;

            // why the weird default value? It's 2^20 + 2^8 for what it's worth...
            BaseItem = 1048832;

            NumberFormatId = null;
        }

        internal void FromDataField(DataField df)
        {
            SetAllNull();

            if (df.Name != null) Name = df.Name.Value;
            if (df.Field != null) Field = df.Field.Value;
            if (df.Subtotal != null) Subtotal = df.Subtotal.Value;
            if (df.ShowDataAs != null) ShowDataAs = df.ShowDataAs.Value;
            if (df.BaseField != null) BaseField = df.BaseField.Value;
            if (df.BaseItem != null) BaseItem = df.BaseItem.Value;
            if (df.NumberFormatId != null) NumberFormatId = df.NumberFormatId.Value;
        }

        internal DataField ToDataField()
        {
            DataField df = new DataField();
            if (Name != null && Name.Length > 0) df.Name = Name;
            df.Field = Field;
            if (Subtotal != DataConsolidateFunctionValues.Sum) df.Subtotal = Subtotal;
            if (ShowDataAs != ShowDataAsValues.Normal) df.ShowDataAs = ShowDataAs;
            if (BaseField != -1) df.BaseField = BaseField;
            if (BaseItem != 1048832) df.BaseItem = BaseItem;
            if (NumberFormatId != null) df.NumberFormatId = NumberFormatId.Value;

            return df;
        }

        internal SLDataField Clone()
        {
            SLDataField df = new SLDataField
            {
                Name = Name,
                Field = Field,
                Subtotal = Subtotal,
                ShowDataAs = ShowDataAs,
                BaseField = BaseField,
                BaseItem = BaseItem,
                NumberFormatId = NumberFormatId
            };

            return df;
        }
    }
}