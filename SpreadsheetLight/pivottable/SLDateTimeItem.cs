﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;

namespace SpreadsheetLight
{
    internal class SLDateTimeItem
    {
        internal List<int> MemberPropertyIndexes { get; set; }

        internal DateTime Val { get; set; }
        internal bool? Unused { get; set; }
        internal bool? Calculated { get; set; }
        internal string Caption { get; set; }
        internal uint? PropertyCount { get; set; }

        internal SLDateTimeItem()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            MemberPropertyIndexes = new List<int>();

            Val = new DateTime();
            Unused = null;
            Calculated = null;
            Caption = "";
            PropertyCount = null;
        }

        internal void FromDateTimeItem(DateTimeItem dti)
        {
            SetAllNull();

            if (dti.Val != null) Val = dti.Val.Value;
            if (dti.Unused != null) Unused = dti.Unused.Value;
            if (dti.Calculated != null) Calculated = dti.Calculated.Value;
            if (dti.Caption != null) Caption = dti.Caption.Value;
            if (dti.PropertyCount != null) PropertyCount = dti.PropertyCount.Value;

            MemberPropertyIndex mpi;
            using (OpenXmlReader oxr = OpenXmlReader.Create(dti))
            {
                while (oxr.Read())
                {
                    if (oxr.ElementType == typeof(MemberPropertyIndex))
                    {
                        // 0 is the default value.
                        mpi = (MemberPropertyIndex)oxr.LoadCurrentElement();
                        if (mpi.Val != null) MemberPropertyIndexes.Add(mpi.Val.Value);
                        else MemberPropertyIndexes.Add(0);
                    }
                }
            }
        }

        internal DateTimeItem ToDateTimeItem()
        {
            DateTimeItem dti = new DateTimeItem
            {
                Val = Val
            };
            if (Unused != null) dti.Unused = Unused.Value;
            if (Calculated != null) dti.Calculated = Calculated.Value;
            if (Caption != null && Caption.Length > 0) dti.Caption = Caption;
            if (PropertyCount != null) dti.PropertyCount = PropertyCount.Value;

            foreach (int i in MemberPropertyIndexes)
            {
                if (i != 0) dti.Append(new MemberPropertyIndex() { Val = i });
                else dti.Append(new MemberPropertyIndex());
            }

            return dti;
        }

        internal SLDateTimeItem Clone()
        {
            SLDateTimeItem dti = new SLDateTimeItem
            {
                Val = Val,
                Unused = Unused,
                Calculated = Calculated,
                Caption = Caption,
                PropertyCount = PropertyCount,

                MemberPropertyIndexes = new List<int>()
            };
            foreach (int i in MemberPropertyIndexes)
            {
                dti.MemberPropertyIndexes.Add(i);
            }

            return dti;
        }
    }
}