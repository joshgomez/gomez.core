﻿using DocumentFormat.OpenXml.Spreadsheet;

namespace SpreadsheetLight
{
    internal class SLDimension
    {
        internal bool Measure { get; set; }
        internal string Name { get; set; }
        internal string UniqueName { get; set; }
        internal string Caption { get; set; }

        internal SLDimension()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            Measure = false;
            Name = "";
            UniqueName = "";
            Caption = "";
        }

        internal void FromDimension(Dimension d)
        {
            SetAllNull();

            if (d.Measure != null) Measure = d.Measure.Value;
            if (d.Name != null) Name = d.Name.Value;
            if (d.UniqueName != null) UniqueName = d.UniqueName.Value;
            if (d.Caption != null) Caption = d.Caption.Value;
        }

        internal Dimension ToDimension()
        {
            Dimension d = new Dimension();
            if (Measure != false) d.Measure = Measure;
            d.Name = Name;
            d.UniqueName = UniqueName;
            d.Caption = Caption;

            return d;
        }

        internal SLDimension Clone()
        {
            SLDimension d = new SLDimension
            {
                Measure = Measure,
                Name = Name,
                UniqueName = UniqueName,
                Caption = Caption
            };

            return d;
        }
    }
}