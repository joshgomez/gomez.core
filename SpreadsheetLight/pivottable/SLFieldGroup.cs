﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Collections.Generic;

namespace SpreadsheetLight
{
    internal class SLFieldGroup
    {
        internal bool HasRangeProperties;
        internal SLRangeProperties RangeProperties { get; set; }

        internal List<uint> DiscreteProperties { get; set; }

        internal bool HasGroupItems;
        internal SLGroupItems GroupItems { get; set; }

        internal uint? ParentId { get; set; }
        internal uint? Base { get; set; }

        internal SLFieldGroup()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            HasRangeProperties = false;
            RangeProperties = new SLRangeProperties();

            DiscreteProperties = new List<uint>();

            HasGroupItems = false;
            GroupItems = new SLGroupItems();

            ParentId = null;
            Base = null;
        }

        internal void FromFieldGroup(FieldGroup fg)
        {
            SetAllNull();

            if (fg.ParentId != null) ParentId = fg.ParentId.Value;
            if (fg.Base != null) Base = fg.Base.Value;

            using (OpenXmlReader oxr = OpenXmlReader.Create(fg))
            {
                while (oxr.Read())
                {
                    if (oxr.ElementType == typeof(RangeProperties))
                    {
                        RangeProperties.FromRangeProperties((RangeProperties)oxr.LoadCurrentElement());
                        HasRangeProperties = true;
                    }
                    else if (oxr.ElementType == typeof(DiscreteProperties))
                    {
                        DiscreteProperties dp = (DiscreteProperties)oxr.LoadCurrentElement();
                        FieldItem fi;
                        using (OpenXmlReader oxrDiscrete = OpenXmlReader.Create(dp))
                        {
                            while (oxrDiscrete.Read())
                            {
                                if (oxrDiscrete.ElementType == typeof(FieldItem))
                                {
                                    fi = (FieldItem)oxrDiscrete.LoadCurrentElement();
                                    DiscreteProperties.Add(fi.Val);
                                }
                            }
                        }
                    }
                    else if (oxr.ElementType == typeof(GroupItems))
                    {
                        GroupItems.FromGroupItems((GroupItems)oxr.LoadCurrentElement());
                        HasGroupItems = true;
                    }
                }
            }
        }

        internal FieldGroup ToFieldGroup()
        {
            FieldGroup fg = new FieldGroup();
            if (ParentId != null) fg.ParentId = ParentId.Value;
            if (Base != null) fg.Base = Base.Value;

            if (HasRangeProperties)
            {
                fg.Append(RangeProperties.ToRangeProperties());
            }

            if (DiscreteProperties.Count > 0)
            {
                DiscreteProperties dp = new DiscreteProperties
                {
                    Count = (uint)DiscreteProperties.Count
                };
                foreach (uint i in DiscreteProperties)
                {
                    dp.Append(new FieldItem() { Val = i });
                }

                fg.Append(dp);
            }

            if (HasGroupItems)
            {
                fg.Append(GroupItems.ToGroupItems());
            }

            return fg;
        }

        internal SLFieldGroup Clone()
        {
            SLFieldGroup fg = new SLFieldGroup
            {
                ParentId = ParentId,
                Base = Base,

                HasRangeProperties = HasRangeProperties,
                RangeProperties = RangeProperties.Clone(),

                DiscreteProperties = new List<uint>()
            };
            foreach (uint i in DiscreteProperties)
            {
                fg.DiscreteProperties.Add(i);
            }

            fg.HasGroupItems = HasGroupItems;
            fg.GroupItems = GroupItems.Clone();

            return fg;
        }
    }
}