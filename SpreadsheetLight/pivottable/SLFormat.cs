﻿using DocumentFormat.OpenXml.Spreadsheet;

namespace SpreadsheetLight
{
    internal class SLFormat
    {
        internal SLPivotArea PivotArea { get; set; }
        internal FormatActionValues Action { get; set; }
        internal uint? FormatId { get; set; }

        internal SLFormat()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            PivotArea = new SLPivotArea();
            Action = FormatActionValues.Formatting;
            FormatId = null;
        }

        internal void FromFormat(Format f)
        {
            SetAllNull();

            if (f.PivotArea != null) PivotArea.FromPivotArea(f.PivotArea);

            if (f.Action != null) Action = f.Action.Value;
            if (f.FormatId != null) FormatId = f.FormatId.Value;
        }

        internal Format ToFormat()
        {
            Format f = new Format
            {
                PivotArea = PivotArea.ToPivotArea()
            };

            if (Action != FormatActionValues.Formatting) f.Action = Action;
            if (FormatId != null) f.FormatId = FormatId.Value;

            return f;
        }

        internal SLFormat Clone()
        {
            SLFormat f = new SLFormat
            {
                PivotArea = PivotArea.Clone(),

                Action = Action,
                FormatId = FormatId
            };

            return f;
        }
    }
}