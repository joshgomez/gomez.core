﻿using DocumentFormat.OpenXml.Spreadsheet;
using System.Collections.Generic;

namespace SpreadsheetLight
{
    internal class SLGroup
    {
        internal List<SLGroupMember> GroupMembers { get; set; }

        internal string Name { get; set; }
        internal string UniqueName { get; set; }
        internal string Caption { get; set; }
        internal string UniqueParent { get; set; }
        internal int? Id { get; set; }

        internal SLGroup()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            GroupMembers = new List<SLGroupMember>();

            Name = "";
            UniqueName = "";
            Caption = "";
            UniqueParent = "";
            Id = null;
        }

        internal void FromGroup(Group g)
        {
            SetAllNull();

            if (g.Name != null) Name = g.Name.Value;
            if (g.UniqueName != null) UniqueName = g.UniqueName.Value;
            if (g.Caption != null) Caption = g.Caption.Value;
            if (g.UniqueParent != null) UniqueParent = g.UniqueParent.Value;
            if (g.Id != null) Id = g.Id.Value;
        }

        internal Group ToGroup()
        {
            Group g = new Group
            {
                Name = Name,
                UniqueName = UniqueName,
                Caption = Caption
            };
            if (UniqueParent != null && UniqueParent.Length > 0) g.UniqueParent = UniqueParent;
            if (Id != null) g.Id = Id.Value;

            if (GroupMembers.Count > 0)
            {
                g.GroupMembers = new GroupMembers() { Count = (uint)GroupMembers.Count };
                foreach (SLGroupMember gm in GroupMembers)
                {
                    g.GroupMembers.Append(gm.ToGroupMember());
                }
            }

            return g;
        }

        internal SLGroup Clone()
        {
            SLGroup g = new SLGroup
            {
                Name = Name,
                UniqueName = UniqueName,
                Caption = Caption,
                UniqueParent = UniqueParent,
                Id = Id,

                GroupMembers = new List<SLGroupMember>()
            };
            foreach (SLGroupMember gm in GroupMembers)
            {
                g.GroupMembers.Add(gm.Clone());
            }

            return g;
        }
    }
}