﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Collections.Generic;

namespace SpreadsheetLight
{
    internal class SLGroupLevel
    {
        internal List<SLGroup> Groups { get; set; }

        internal string UniqueName { get; set; }
        internal string Caption { get; set; }
        internal bool User { get; set; }
        internal bool CustomRollUp { get; set; }

        internal SLGroupLevel()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            Groups = new List<SLGroup>();

            UniqueName = "";
            Caption = "";
            User = false;
            CustomRollUp = false;
        }

        internal void FromGroupLevel(GroupLevel gl)
        {
            SetAllNull();

            if (gl.UniqueName != null) UniqueName = gl.UniqueName.Value;
            if (gl.Caption != null) Caption = gl.Caption.Value;
            if (gl.User != null) User = gl.User.Value;
            if (gl.CustomRollUp != null) CustomRollUp = gl.CustomRollUp.Value;

            if (gl.Groups != null)
            {
                SLGroup g;
                using (OpenXmlReader oxr = OpenXmlReader.Create(gl.Groups))
                {
                    while (oxr.Read())
                    {
                        if (oxr.ElementType == typeof(Group))
                        {
                            g = new SLGroup();
                            g.FromGroup((Group)oxr.LoadCurrentElement());
                            Groups.Add(g);
                        }
                    }
                }
            }
        }

        internal GroupLevel ToGroupLevel()
        {
            GroupLevel gl = new GroupLevel
            {
                UniqueName = UniqueName,
                Caption = Caption
            };
            if (User != false) gl.User = User;
            if (CustomRollUp != false) gl.CustomRollUp = CustomRollUp;

            if (Groups.Count > 0)
            {
                gl.Groups = new Groups() { Count = (uint)Groups.Count };
                foreach (SLGroup g in Groups)
                {
                    gl.Groups.Append(g.ToGroup());
                }
            }

            return gl;
        }

        internal SLGroupLevel Clone()
        {
            SLGroupLevel gl = new SLGroupLevel
            {
                UniqueName = UniqueName,
                Caption = Caption,
                User = User,
                CustomRollUp = CustomRollUp,

                Groups = new List<SLGroup>()
            };
            foreach (SLGroup g in Groups)
            {
                gl.Groups.Add(g.Clone());
            }

            return gl;
        }
    }
}