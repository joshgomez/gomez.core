﻿using DocumentFormat.OpenXml.Spreadsheet;

namespace SpreadsheetLight
{
    internal class SLGroupMember
    {
        internal string UniqueName { get; set; }
        internal bool Group { get; set; }

        internal SLGroupMember()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            UniqueName = "";
            Group = false;
        }

        internal void FromGroupMember(GroupMember gm)
        {
            SetAllNull();

            if (gm.UniqueName != null) UniqueName = gm.UniqueName.Value;
            if (gm.Group != null) Group = gm.Group.Value;
        }

        internal GroupMember ToGroupMember()
        {
            GroupMember gm = new GroupMember
            {
                UniqueName = UniqueName
            };
            if (Group != false) gm.Group = Group;

            return gm;
        }

        internal SLGroupMember Clone()
        {
            SLGroupMember gm = new SLGroupMember
            {
                UniqueName = UniqueName,
                Group = Group
            };

            return gm;
        }
    }
}