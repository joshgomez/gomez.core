﻿using DocumentFormat.OpenXml.Spreadsheet;

namespace SpreadsheetLight
{
    internal class SLMeasureDimensionMap
    {
        internal uint? MeasureGroup { get; set; }
        internal uint? Dimension { get; set; }

        internal SLMeasureDimensionMap()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            MeasureGroup = null;
            Dimension = null;
        }

        internal void FromMeasureDimensionMap(MeasureDimensionMap mdm)
        {
            SetAllNull();

            if (mdm.MeasureGroup != null) MeasureGroup = mdm.MeasureGroup.Value;
            if (mdm.Dimension != null) Dimension = mdm.Dimension.Value;
        }

        internal MeasureDimensionMap ToMeasureDimensionMap()
        {
            MeasureDimensionMap mdm = new MeasureDimensionMap();
            if (MeasureGroup != null) mdm.MeasureGroup = MeasureGroup.Value;
            if (Dimension != null) mdm.Dimension = Dimension.Value;

            return mdm;
        }

        internal SLMeasureDimensionMap Clone()
        {
            SLMeasureDimensionMap mdm = new SLMeasureDimensionMap
            {
                MeasureGroup = MeasureGroup,
                Dimension = Dimension
            };

            return mdm;
        }
    }
}