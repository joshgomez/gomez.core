﻿using DocumentFormat.OpenXml.Spreadsheet;

namespace SpreadsheetLight
{
    internal class SLMeasureGroup
    {
        internal string Name { get; set; }
        internal string Caption { get; set; }

        internal SLMeasureGroup()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            Name = "";
            Caption = "";
        }

        internal void FromMeasureGroup(MeasureGroup mg)
        {
            SetAllNull();

            if (mg.Name != null) Name = mg.Name.Value;
            if (mg.Caption != null) Caption = mg.Caption.Value;
        }

        internal MeasureGroup ToMeasureGroup()
        {
            MeasureGroup mg = new MeasureGroup
            {
                Name = Name,
                Caption = Caption
            };

            return mg;
        }

        internal SLMeasureGroup Clone()
        {
            SLMeasureGroup mg = new SLMeasureGroup
            {
                Name = Name,
                Caption = Caption
            };

            return mg;
        }
    }
}