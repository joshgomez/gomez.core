﻿using DocumentFormat.OpenXml.Spreadsheet;

namespace SpreadsheetLight
{
    internal class SLPageField
    {
        internal int Field { get; set; }
        internal uint? Item { get; set; }
        internal int Hierarchy { get; set; }
        internal string Name { get; set; }
        internal string Caption { get; set; }

        internal SLPageField()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            Field = 0;
            Item = null;
            Hierarchy = 0;
            Name = "";
            Caption = "";
        }

        internal void FromPageField(PageField pf)
        {
            SetAllNull();

            if (pf.Field != null) Field = pf.Field.Value;
            if (pf.Item != null) Item = pf.Item.Value;
            if (pf.Hierarchy != null) Hierarchy = pf.Hierarchy.Value;
            if (pf.Name != null) Name = pf.Name.Value;
            if (pf.Caption != null) Caption = pf.Caption.Value;
        }

        internal PageField ToPageField()
        {
            PageField pf = new PageField
            {
                Field = Field
            };
            if (Item != null) pf.Item = Item.Value;
            pf.Hierarchy = Hierarchy;
            if (Name != null && Name.Length > 0) pf.Name = Name;
            if (Caption != null && Caption.Length > 0) pf.Caption = Caption;

            return pf;
        }

        internal SLPageField Clone()
        {
            SLPageField pf = new SLPageField
            {
                Field = Field,
                Item = Item,
                Hierarchy = Hierarchy,
                Name = Name,
                Caption = Caption
            };

            return pf;
        }
    }
}