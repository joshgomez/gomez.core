﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Collections.Generic;

namespace SpreadsheetLight
{
    internal class SLPivotArea
    {
        private List<SLPivotAreaReference> PivotAreaReferences { get; set; }

        internal int? Field { get; set; }
        internal PivotAreaValues Type { get; set; }
        internal bool DataOnly { get; set; }
        internal bool LabelOnly { get; set; }
        internal bool GrandRow { get; set; }
        internal bool GrandColumn { get; set; }
        internal bool CacheIndex { get; set; }
        internal bool Outline { get; set; }
        internal string Offset { get; set; }
        internal bool CollapsedLevelsAreSubtotals { get; set; }
        internal PivotTableAxisValues? Axis { get; set; }
        internal uint? FieldPosition { get; set; }

        internal SLPivotArea()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            PivotAreaReferences = new List<SLPivotAreaReference>();

            Field = null;
            Type = PivotAreaValues.Normal;
            DataOnly = true;
            LabelOnly = false;
            GrandRow = false;
            GrandColumn = false;
            CacheIndex = false;
            Outline = true;
            Offset = "";
            CollapsedLevelsAreSubtotals = false;
            Axis = null;
            FieldPosition = null;
        }

        internal void FromPivotArea(PivotArea pa)
        {
            SetAllNull();

            if (pa.Field != null) Field = pa.Field.Value;
            if (pa.Type != null) Type = pa.Type.Value;
            if (pa.DataOnly != null) DataOnly = pa.DataOnly.Value;
            if (pa.LabelOnly != null) LabelOnly = pa.LabelOnly.Value;
            if (pa.GrandRow != null) GrandRow = pa.GrandRow.Value;
            if (pa.GrandColumn != null) GrandColumn = pa.GrandColumn.Value;
            if (pa.CacheIndex != null) CacheIndex = pa.CacheIndex.Value;
            if (pa.Outline != null) Outline = pa.Outline.Value;
            if (pa.Offset != null) Offset = pa.Offset.Value;
            if (pa.CollapsedLevelsAreSubtotals != null) CollapsedLevelsAreSubtotals = pa.CollapsedLevelsAreSubtotals.Value;
            if (pa.Axis != null) Axis = pa.Axis.Value;
            if (pa.FieldPosition != null) FieldPosition = pa.FieldPosition.Value;

            SLPivotAreaReference par;
            using (OpenXmlReader oxr = OpenXmlReader.Create(pa))
            {
                while (oxr.Read())
                {
                    if (oxr.ElementType == typeof(PivotAreaReference))
                    {
                        par = new SLPivotAreaReference();
                        par.FromPivotAreaReference((PivotAreaReference)oxr.LoadCurrentElement());
                        PivotAreaReferences.Add(par);
                    }
                }
            }
        }

        internal PivotArea ToPivotArea()
        {
            PivotArea pa = new PivotArea();
            if (Field != null) pa.Field = Field.Value;
            if (Type != PivotAreaValues.Normal) pa.Type = Type;
            if (DataOnly != true) pa.DataOnly = DataOnly;
            if (LabelOnly != false) pa.LabelOnly = LabelOnly;
            if (GrandRow != false) pa.GrandRow = GrandRow;
            if (GrandColumn != false) pa.GrandColumn = GrandColumn;
            if (CacheIndex != false) pa.CacheIndex = CacheIndex;
            if (Outline != true) pa.Outline = Outline;
            if (Offset != null && Offset.Length > 0) pa.Offset = Offset;
            if (CollapsedLevelsAreSubtotals != false) pa.CollapsedLevelsAreSubtotals = CollapsedLevelsAreSubtotals;
            if (Axis != null) pa.Axis = Axis.Value;
            if (FieldPosition != null) pa.FieldPosition = FieldPosition.Value;

            if (PivotAreaReferences.Count > 0)
            {
                pa.PivotAreaReferences = new PivotAreaReferences();
                foreach (SLPivotAreaReference par in PivotAreaReferences)
                {
                    pa.PivotAreaReferences.Append(par.ToPivotAreaReference());
                }
            }

            return pa;
        }

        internal SLPivotArea Clone()
        {
            SLPivotArea pa = new SLPivotArea
            {
                Field = Field,
                Type = Type,
                DataOnly = DataOnly,
                LabelOnly = LabelOnly,
                GrandRow = GrandRow,
                GrandColumn = GrandColumn,
                CacheIndex = CacheIndex,
                Outline = Outline,
                Offset = Offset,
                CollapsedLevelsAreSubtotals = CollapsedLevelsAreSubtotals,
                Axis = Axis,
                FieldPosition = FieldPosition,

                PivotAreaReferences = new List<SLPivotAreaReference>()
            };
            foreach (SLPivotAreaReference par in PivotAreaReferences)
            {
                pa.PivotAreaReferences.Add(par.Clone());
            }

            return pa;
        }
    }
}