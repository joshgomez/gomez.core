﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Collections.Generic;

namespace SpreadsheetLight
{
    internal class SLPivotAreaReference
    {
        internal List<uint> FieldItems { get; set; }

        internal uint? Field { get; set; }

        //internal uint Count { get; set; }
        internal bool Selected { get; set; }

        internal bool ByPosition { get; set; }
        internal bool Relative { get; set; }
        internal bool DefaultSubtotal { get; set; }
        internal bool SumSubtotal { get; set; }
        internal bool CountASubtotal { get; set; }
        internal bool AverageSubtotal { get; set; }
        internal bool MaxSubtotal { get; set; }
        internal bool MinSubtotal { get; set; }
        internal bool ApplyProductInSubtotal { get; set; }
        internal bool CountSubtotal { get; set; }
        internal bool ApplyStandardDeviationInSubtotal { get; set; }
        internal bool ApplyStandardDeviationPInSubtotal { get; set; }
        internal bool ApplyVarianceInSubtotal { get; set; }
        internal bool ApplyVariancePInSubtotal { get; set; }

        internal SLPivotAreaReference()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            FieldItems = new List<uint>();
            Field = null;
            //this.Count = 0;
            Selected = true;
            ByPosition = false;
            Relative = false;
            DefaultSubtotal = false;
            SumSubtotal = false;
            CountASubtotal = false;
            AverageSubtotal = false;
            MaxSubtotal = false;
            MinSubtotal = false;
            ApplyProductInSubtotal = false;
            CountSubtotal = false;
            ApplyStandardDeviationInSubtotal = false;
            ApplyStandardDeviationPInSubtotal = false;
            ApplyVarianceInSubtotal = false;
            ApplyVariancePInSubtotal = false;
        }

        internal void FromPivotAreaReference(PivotAreaReference par)
        {
            SetAllNull();

            if (par.Field != null) Field = par.Field.Value;
            if (par.Selected != null) Selected = par.Selected.Value;
            if (par.ByPosition != null) ByPosition = par.ByPosition.Value;
            if (par.Relative != null) Relative = par.Relative.Value;
            if (par.DefaultSubtotal != null) DefaultSubtotal = par.DefaultSubtotal.Value;
            if (par.SumSubtotal != null) SumSubtotal = par.SumSubtotal.Value;
            if (par.CountASubtotal != null) CountASubtotal = par.CountASubtotal.Value;
            if (par.AverageSubtotal != null) AverageSubtotal = par.AverageSubtotal.Value;
            if (par.MaxSubtotal != null) MaxSubtotal = par.MaxSubtotal.Value;
            if (par.MinSubtotal != null) MinSubtotal = par.MinSubtotal.Value;
            if (par.ApplyProductInSubtotal != null) ApplyProductInSubtotal = par.ApplyProductInSubtotal.Value;
            if (par.CountSubtotal != null) CountSubtotal = par.CountSubtotal.Value;
            if (par.ApplyStandardDeviationInSubtotal != null) ApplyStandardDeviationInSubtotal = par.ApplyStandardDeviationInSubtotal.Value;
            if (par.ApplyStandardDeviationPInSubtotal != null) ApplyStandardDeviationPInSubtotal = par.ApplyStandardDeviationPInSubtotal.Value;
            if (par.ApplyVarianceInSubtotal != null) ApplyVarianceInSubtotal = par.ApplyVarianceInSubtotal.Value;
            if (par.ApplyVariancePInSubtotal != null) ApplyVariancePInSubtotal = par.ApplyVariancePInSubtotal.Value;

            FieldItem fi;
            using (OpenXmlReader oxr = OpenXmlReader.Create(par))
            {
                while (oxr.Read())
                {
                    if (oxr.ElementType == typeof(FieldItem))
                    {
                        fi = (FieldItem)oxr.LoadCurrentElement();
                        // the Val property is required
                        FieldItems.Add(fi.Val.Value);
                    }
                }
            }
        }

        internal PivotAreaReference ToPivotAreaReference()
        {
            PivotAreaReference par = new PivotAreaReference();
            if (Field != null) par.Field = Field.Value;
            par.Count = (uint)FieldItems.Count;
            if (Selected != true) par.Selected = Selected;
            if (ByPosition != false) par.ByPosition = ByPosition;
            if (Relative != false) par.Relative = Relative;
            if (DefaultSubtotal != false) par.DefaultSubtotal = DefaultSubtotal;
            if (SumSubtotal != false) par.SumSubtotal = SumSubtotal;
            if (CountASubtotal != false) par.CountASubtotal = CountASubtotal;
            if (AverageSubtotal != false) par.AverageSubtotal = AverageSubtotal;
            if (MaxSubtotal != false) par.MaxSubtotal = MaxSubtotal;
            if (MinSubtotal != false) par.MinSubtotal = MinSubtotal;
            if (ApplyProductInSubtotal != false) par.ApplyProductInSubtotal = ApplyProductInSubtotal;
            if (CountSubtotal != false) par.CountSubtotal = CountSubtotal;
            if (ApplyStandardDeviationInSubtotal != false) par.ApplyStandardDeviationInSubtotal = ApplyStandardDeviationInSubtotal;
            if (ApplyStandardDeviationPInSubtotal != false) par.ApplyStandardDeviationPInSubtotal = ApplyStandardDeviationPInSubtotal;
            if (ApplyVarianceInSubtotal != false) par.ApplyVarianceInSubtotal = ApplyVarianceInSubtotal;
            if (ApplyVariancePInSubtotal != false) par.ApplyVariancePInSubtotal = ApplyVariancePInSubtotal;

            foreach (uint i in FieldItems)
            {
                par.Append(new FieldItem() { Val = i });
            }

            return par;
        }

        internal SLPivotAreaReference Clone()
        {
            SLPivotAreaReference par = new SLPivotAreaReference
            {
                Field = Field,
                Selected = Selected,
                ByPosition = ByPosition,
                Relative = Relative,
                DefaultSubtotal = DefaultSubtotal,
                SumSubtotal = SumSubtotal,
                CountASubtotal = CountASubtotal,
                AverageSubtotal = AverageSubtotal,
                MaxSubtotal = MaxSubtotal,
                MinSubtotal = MinSubtotal,
                ApplyProductInSubtotal = ApplyProductInSubtotal,
                CountSubtotal = CountSubtotal,
                ApplyStandardDeviationInSubtotal = ApplyStandardDeviationInSubtotal,
                ApplyStandardDeviationPInSubtotal = ApplyStandardDeviationPInSubtotal,
                ApplyVarianceInSubtotal = ApplyVarianceInSubtotal,
                ApplyVariancePInSubtotal = ApplyVariancePInSubtotal,

                FieldItems = new List<uint>()
            };
            foreach (uint i in FieldItems)
            {
                par.FieldItems.Add(i);
            }

            return par;
        }
    }
}