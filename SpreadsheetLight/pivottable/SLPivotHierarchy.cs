﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Collections.Generic;

namespace SpreadsheetLight
{
    internal class SLPivotHierarchy
    {
        internal List<SLMemberProperty> MemberProperties { get; set; }
        internal List<SLMembers> Members { get; set; }

        internal bool Outline { get; set; }
        internal bool MultipleItemSelectionAllowed { get; set; }
        internal bool SubtotalTop { get; set; }
        internal bool ShowInFieldList { get; set; }
        internal bool DragToRow { get; set; }
        internal bool DragToColumn { get; set; }
        internal bool DragToPage { get; set; }
        internal bool DragToData { get; set; }
        internal bool DragOff { get; set; }
        internal bool IncludeNewItemsInFilter { get; set; }
        internal string Caption { get; set; }

        internal SLPivotHierarchy()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            MemberProperties = new List<SLMemberProperty>();
            Members = new List<SLMembers>();

            Outline = false;
            MultipleItemSelectionAllowed = false;
            SubtotalTop = false;
            ShowInFieldList = true;
            DragToRow = true;
            DragToColumn = true;
            DragToPage = true;
            DragToData = false;
            DragOff = true;
            IncludeNewItemsInFilter = false;
            Caption = "";
        }

        internal void FromPivotHierarchy(PivotHierarchy ph)
        {
            SetAllNull();

            if (ph.Outline != null) Outline = ph.Outline.Value;
            if (ph.MultipleItemSelectionAllowed != null) Outline = ph.MultipleItemSelectionAllowed.Value;
            if (ph.SubtotalTop != null) SubtotalTop = ph.SubtotalTop.Value;
            if (ph.ShowInFieldList != null) ShowInFieldList = ph.ShowInFieldList.Value;
            if (ph.DragToRow != null) DragToRow = ph.DragToRow.Value;
            if (ph.DragToColumn != null) DragToColumn = ph.DragToColumn.Value;
            if (ph.DragToPage != null) DragToPage = ph.DragToPage.Value;
            if (ph.DragToData != null) DragToData = ph.DragToData.Value;
            if (ph.DragOff != null) DragOff = ph.DragOff.Value;
            if (ph.IncludeNewItemsInFilter != null) IncludeNewItemsInFilter = ph.IncludeNewItemsInFilter.Value;
            if (ph.Caption != null) Caption = ph.Caption.Value;

            SLMemberProperty mp;
            SLMembers mems;
            using (OpenXmlReader oxr = OpenXmlReader.Create(ph))
            {
                while (oxr.Read())
                {
                    if (oxr.ElementType == typeof(MemberProperty))
                    {
                        mp = new SLMemberProperty();
                        mp.FromMemberProperty((MemberProperty)oxr.LoadCurrentElement());
                        MemberProperties.Add(mp);
                    }
                    else if (oxr.ElementType == typeof(Members))
                    {
                        mems = new SLMembers();
                        mems.FromMembers((Members)oxr.LoadCurrentElement());
                        Members.Add(mems);
                    }
                }
            }
        }

        internal PivotHierarchy ToPivotHierarchy()
        {
            PivotHierarchy ph = new PivotHierarchy();

            if (Outline != false) ph.Outline = Outline;
            if (MultipleItemSelectionAllowed != false) ph.MultipleItemSelectionAllowed = MultipleItemSelectionAllowed;
            if (SubtotalTop != false) ph.SubtotalTop = SubtotalTop;
            if (ShowInFieldList != true) ph.ShowInFieldList = ShowInFieldList;
            if (DragToRow != true) ph.DragToRow = DragToRow;
            if (DragToColumn != true) ph.DragToColumn = DragToColumn;
            if (DragToPage != true) ph.DragToPage = DragToPage;
            if (DragToData != false) ph.DragToData = DragToData;
            if (DragOff != true) ph.DragOff = DragOff;
            if (IncludeNewItemsInFilter != false) ph.IncludeNewItemsInFilter = IncludeNewItemsInFilter;
            if (Caption != null && Caption.Length > 0) ph.Caption = Caption;

            if (MemberProperties.Count > 0)
            {
                ph.MemberProperties = new MemberProperties() { Count = (uint)MemberProperties.Count };
                foreach (SLMemberProperty mp in MemberProperties)
                {
                    ph.MemberProperties.Append(mp.ToMemberProperty());
                }
            }

            foreach (SLMembers mems in Members)
            {
                ph.Append(mems.ToMembers());
            }

            return ph;
        }

        internal SLPivotHierarchy Clone()
        {
            SLPivotHierarchy ph = new SLPivotHierarchy
            {
                Outline = Outline,
                MultipleItemSelectionAllowed = MultipleItemSelectionAllowed,
                SubtotalTop = SubtotalTop,
                ShowInFieldList = ShowInFieldList,
                DragToRow = DragToRow,
                DragToColumn = DragToColumn,
                DragToPage = DragToPage,
                DragToData = DragToData,
                DragOff = DragOff,
                IncludeNewItemsInFilter = IncludeNewItemsInFilter,
                Caption = Caption,

                MemberProperties = new List<SLMemberProperty>()
            };
            foreach (SLMemberProperty mp in MemberProperties)
            {
                ph.MemberProperties.Add(mp.Clone());
            }

            ph.Members = new List<SLMembers>();
            foreach (SLMembers mems in Members)
            {
                ph.Members.Add(mems.Clone());
            }

            return ph;
        }
    }
}