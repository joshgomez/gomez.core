﻿using DocumentFormat.OpenXml.Spreadsheet;

namespace SpreadsheetLight
{
    internal class SLQuery
    {
        internal bool HasTuples;
        internal SLTuplesType Tuples { get; set; }

        internal string Mdx { get; set; }

        internal SLQuery()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            HasTuples = false;
            Tuples = new SLTuplesType();

            Mdx = "";
        }

        internal void FromQuery(Query q)
        {
            SetAllNull();

            if (q.Mdx != null) Mdx = q.Mdx.Value;

            if (q.Tuples != null)
            {
                Tuples.FromTuples(q.Tuples);
                HasTuples = true;
            }
        }

        internal Query ToQuery()
        {
            Query q = new Query
            {
                Mdx = Mdx
            };

            if (HasTuples) q.Tuples = Tuples.ToTuples();

            return q;
        }

        internal SLQuery Clone()
        {
            SLQuery q = new SLQuery
            {
                Mdx = Mdx,
                HasTuples = HasTuples,
                Tuples = Tuples.Clone()
            };

            return q;
        }
    }
}