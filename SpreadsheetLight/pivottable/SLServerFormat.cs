﻿using DocumentFormat.OpenXml.Spreadsheet;

namespace SpreadsheetLight
{
    internal class SLServerFormat
    {
        internal string Culture { get; set; }
        internal string Format { get; set; }

        internal SLServerFormat()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            Culture = "";
            Format = "";
        }

        internal void FromServerFormat(ServerFormat sf)
        {
            SetAllNull();

            if (sf.Culture != null) Culture = sf.Culture.Value;
            if (sf.Format != null) Format = sf.Format.Value;
        }

        internal ServerFormat ToServerFormat()
        {
            ServerFormat sf = new ServerFormat();
            if (Culture != null && Culture.Length > 0) sf.Culture = Culture;
            if (Format != null && Format.Length > 0) sf.Format = Format;

            return sf;
        }

        internal SLServerFormat Clone()
        {
            SLServerFormat sf = new SLServerFormat
            {
                Culture = Culture,
                Format = Format
            };

            return sf;
        }
    }
}