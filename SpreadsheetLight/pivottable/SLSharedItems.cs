﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;

namespace SpreadsheetLight
{
    internal enum SLSharedGroupItemsType
    {
        Missing = 0,
        Number,
        Boolean,
        Error,
        String,
        DateTime
    }

    internal struct SLSharedGroupItemsTypeIndexPair
    {
        internal SLSharedGroupItemsType Type;

        // this is the 0-based index into whichever List<> depending on Type.
        internal int Index;

        internal SLSharedGroupItemsTypeIndexPair(SLSharedGroupItemsType Type, int Index)
        {
            this.Type = Type;
            this.Index = Index;
        }
    }

    internal class SLSharedItems
    {
        internal List<SLSharedGroupItemsTypeIndexPair> Items { get; set; }

        internal List<SLMissingItem> MissingItems { get; set; }
        internal List<SLNumberItem> NumberItems { get; set; }
        internal List<SLBooleanItem> BooleanItems { get; set; }
        internal List<SLErrorItem> ErrorItems { get; set; }
        internal List<SLStringItem> StringItems { get; set; }
        internal List<SLDateTimeItem> DateTimeItems { get; set; }

        internal bool ContainsSemiMixedTypes { get; set; }
        internal bool ContainsNonDate { get; set; }
        internal bool ContainsDate { get; set; }
        internal bool ContainsString { get; set; }
        internal bool ContainsBlank { get; set; }
        internal bool ContainsMixedTypes { get; set; }
        internal bool ContainsNumber { get; set; }
        internal bool ContainsInteger { get; set; }
        internal double? MinValue { get; set; }
        internal double? MaxValue { get; set; }
        internal DateTime? MinDate { get; set; }
        internal DateTime? MaxDate { get; set; }

        //No need? internal uint? Count { get; set; }
        internal bool LongText { get; set; }

        internal SLSharedItems()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            Items = new List<SLSharedGroupItemsTypeIndexPair>();

            MissingItems = new List<SLMissingItem>();
            NumberItems = new List<SLNumberItem>();
            BooleanItems = new List<SLBooleanItem>();
            ErrorItems = new List<SLErrorItem>();
            StringItems = new List<SLStringItem>();
            DateTimeItems = new List<SLDateTimeItem>();

            ContainsSemiMixedTypes = true;
            ContainsNonDate = true;
            ContainsDate = false;
            ContainsString = true;
            ContainsBlank = false;
            ContainsMixedTypes = false;
            ContainsNumber = false;
            ContainsInteger = false;
            MinValue = null;
            MaxValue = null;
            MinDate = null;
            MaxDate = null;
            //this.Count = null;
            LongText = false;
        }

        internal void FromSharedItems(SharedItems sis)
        {
            SetAllNull();

            if (sis.ContainsSemiMixedTypes != null) ContainsSemiMixedTypes = sis.ContainsSemiMixedTypes.Value;
            if (sis.ContainsNonDate != null) ContainsNonDate = sis.ContainsNonDate.Value;
            if (sis.ContainsDate != null) ContainsDate = sis.ContainsDate.Value;
            if (sis.ContainsString != null) ContainsString = sis.ContainsString.Value;
            if (sis.ContainsBlank != null) ContainsBlank = sis.ContainsBlank.Value;
            if (sis.ContainsMixedTypes != null) ContainsMixedTypes = sis.ContainsMixedTypes.Value;
            if (sis.ContainsNumber != null) ContainsNumber = sis.ContainsNumber.Value;
            if (sis.ContainsInteger != null) ContainsInteger = sis.ContainsInteger.Value;
            if (sis.MinValue != null) MinValue = sis.MinValue.Value;
            if (sis.MaxValue != null) MaxValue = sis.MaxValue.Value;
            if (sis.MinDate != null) MinDate = sis.MinDate.Value;
            if (sis.MaxDate != null) MaxDate = sis.MaxDate.Value;
            //count
            if (sis.LongText != null) LongText = sis.LongText.Value;

            SLMissingItem mi;
            SLNumberItem ni;
            SLBooleanItem bi;
            SLErrorItem ei;
            SLStringItem si;
            SLDateTimeItem dti;
            using (OpenXmlReader oxr = OpenXmlReader.Create(sis))
            {
                while (oxr.Read())
                {
                    // make sure to add to Items first, because of the Count thing.
                    if (oxr.ElementType == typeof(MissingItem))
                    {
                        mi = new SLMissingItem();
                        mi.FromMissingItem((MissingItem)oxr.LoadCurrentElement());
                        Items.Add(new SLSharedGroupItemsTypeIndexPair(SLSharedGroupItemsType.Missing, MissingItems.Count));
                        MissingItems.Add(mi);
                    }
                    else if (oxr.ElementType == typeof(NumberItem))
                    {
                        ni = new SLNumberItem();
                        ni.FromNumberItem((NumberItem)oxr.LoadCurrentElement());
                        Items.Add(new SLSharedGroupItemsTypeIndexPair(SLSharedGroupItemsType.Number, NumberItems.Count));
                        NumberItems.Add(ni);
                    }
                    else if (oxr.ElementType == typeof(BooleanItem))
                    {
                        bi = new SLBooleanItem();
                        bi.FromBooleanItem((BooleanItem)oxr.LoadCurrentElement());
                        Items.Add(new SLSharedGroupItemsTypeIndexPair(SLSharedGroupItemsType.Boolean, BooleanItems.Count));
                        BooleanItems.Add(bi);
                    }
                    else if (oxr.ElementType == typeof(ErrorItem))
                    {
                        ei = new SLErrorItem();
                        ei.FromErrorItem((ErrorItem)oxr.LoadCurrentElement());
                        Items.Add(new SLSharedGroupItemsTypeIndexPair(SLSharedGroupItemsType.Error, ErrorItems.Count));
                        ErrorItems.Add(ei);
                    }
                    else if (oxr.ElementType == typeof(StringItem))
                    {
                        si = new SLStringItem();
                        si.FromStringItem((StringItem)oxr.LoadCurrentElement());
                        Items.Add(new SLSharedGroupItemsTypeIndexPair(SLSharedGroupItemsType.String, StringItems.Count));
                        StringItems.Add(si);
                    }
                    else if (oxr.ElementType == typeof(DateTimeItem))
                    {
                        dti = new SLDateTimeItem();
                        dti.FromDateTimeItem((DateTimeItem)oxr.LoadCurrentElement());
                        Items.Add(new SLSharedGroupItemsTypeIndexPair(SLSharedGroupItemsType.DateTime, DateTimeItems.Count));
                        DateTimeItems.Add(dti);
                    }
                }
            }
        }

        internal SharedItems ToSharedItems()
        {
            SharedItems sis = new SharedItems();
            if (ContainsSemiMixedTypes != true) sis.ContainsSemiMixedTypes = ContainsSemiMixedTypes;
            if (ContainsNonDate != true) sis.ContainsNonDate = ContainsNonDate;
            if (ContainsDate != false) sis.ContainsDate = ContainsDate;
            if (ContainsString != true) sis.ContainsString = ContainsString;
            if (ContainsBlank != false) sis.ContainsBlank = ContainsBlank;
            if (ContainsMixedTypes != false) sis.ContainsMixedTypes = ContainsMixedTypes;
            if (ContainsNumber != false) sis.ContainsNumber = ContainsNumber;
            if (ContainsInteger != false) sis.ContainsInteger = ContainsInteger;
            if (MinValue != null) sis.MinValue = MinValue.Value;
            if (MaxValue != null) sis.MaxValue = MaxValue.Value;
            if (MinDate != null) sis.MinDate = new DateTimeValue(MinDate.Value);
            if (MaxDate != null) sis.MaxDate = new DateTimeValue(MaxDate.Value);

            // is it the sum of all the various items?
            if (Items.Count > 0) sis.Count = (uint)Items.Count;

            if (LongText != false) sis.LongText = LongText;

            foreach (SLSharedGroupItemsTypeIndexPair pair in Items)
            {
                switch (pair.Type)
                {
                    case SLSharedGroupItemsType.Missing:
                        sis.Append(MissingItems[pair.Index].ToMissingItem());
                        break;

                    case SLSharedGroupItemsType.Number:
                        sis.Append(NumberItems[pair.Index].ToNumberItem());
                        break;

                    case SLSharedGroupItemsType.Boolean:
                        sis.Append(BooleanItems[pair.Index].ToBooleanItem());
                        break;

                    case SLSharedGroupItemsType.Error:
                        sis.Append(ErrorItems[pair.Index].ToErrorItem());
                        break;

                    case SLSharedGroupItemsType.String:
                        sis.Append(StringItems[pair.Index].ToStringItem());
                        break;

                    case SLSharedGroupItemsType.DateTime:
                        sis.Append(DateTimeItems[pair.Index].ToDateTimeItem());
                        break;
                }
            }

            return sis;
        }

        internal SLSharedItems Clone()
        {
            SLSharedItems sis = new SLSharedItems
            {
                ContainsSemiMixedTypes = ContainsSemiMixedTypes,
                ContainsNonDate = ContainsNonDate,
                ContainsDate = ContainsDate,
                ContainsString = ContainsString,
                ContainsBlank = ContainsBlank,
                ContainsMixedTypes = ContainsMixedTypes,
                ContainsNumber = ContainsNumber,
                ContainsInteger = ContainsInteger,
                MinValue = MinValue,
                MaxValue = MaxValue,
                MinDate = MinDate,
                MaxDate = MaxDate,
                //count
                LongText = LongText,

                Items = new List<SLSharedGroupItemsTypeIndexPair>()
            };
            foreach (SLSharedGroupItemsTypeIndexPair pair in Items)
            {
                sis.Items.Add(new SLSharedGroupItemsTypeIndexPair(pair.Type, pair.Index));
            }

            sis.MissingItems = new List<SLMissingItem>();
            foreach (SLMissingItem mi in MissingItems)
            {
                sis.MissingItems.Add(mi.Clone());
            }

            sis.NumberItems = new List<SLNumberItem>();
            foreach (SLNumberItem ni in NumberItems)
            {
                sis.NumberItems.Add(ni.Clone());
            }

            sis.BooleanItems = new List<SLBooleanItem>();
            foreach (SLBooleanItem bi in BooleanItems)
            {
                sis.BooleanItems.Add(bi.Clone());
            }

            sis.ErrorItems = new List<SLErrorItem>();
            foreach (SLErrorItem ei in ErrorItems)
            {
                sis.ErrorItems.Add(ei.Clone());
            }

            sis.StringItems = new List<SLStringItem>();
            foreach (SLStringItem si in StringItems)
            {
                sis.StringItems.Add(si.Clone());
            }

            sis.DateTimeItems = new List<SLDateTimeItem>();
            foreach (SLDateTimeItem dti in DateTimeItems)
            {
                sis.DateTimeItems.Add(dti.Clone());
            }

            return sis;
        }
    }
}