﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Collections.Generic;

namespace SpreadsheetLight
{
    internal class SLStringItem
    {
        internal List<SLTuplesType> Tuples { get; set; }
        internal List<int> MemberPropertyIndexes { get; set; }

        internal string Val { get; set; }
        internal bool? Unused { get; set; }
        internal bool? Calculated { get; set; }
        internal string Caption { get; set; }
        internal uint? PropertyCount { get; set; }
        internal uint? FormatIndex { get; set; }
        internal string BackgroundColor { get; set; }
        internal string ForegroundColor { get; set; }
        internal bool Italic { get; set; }
        internal bool Underline { get; set; }
        internal bool Strikethrough { get; set; }
        internal bool Bold { get; set; }

        internal SLStringItem()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            Tuples = new List<SLTuplesType>();
            MemberPropertyIndexes = new List<int>();

            Val = "";
            Unused = null;
            Calculated = null;
            Caption = "";
            PropertyCount = null;
            FormatIndex = null;
            BackgroundColor = "";
            ForegroundColor = "";
            Italic = false;
            Underline = false;
            Strikethrough = false;
            Bold = false;
        }

        internal void FromStringItem(StringItem si)
        {
            SetAllNull();

            if (si.Val != null) Val = si.Val.Value;
            if (si.Unused != null) Unused = si.Unused.Value;
            if (si.Calculated != null) Calculated = si.Calculated.Value;
            if (si.Caption != null) Caption = si.Caption.Value;
            if (si.PropertyCount != null) PropertyCount = si.PropertyCount.Value;
            if (si.FormatIndex != null) FormatIndex = si.FormatIndex.Value;
            if (si.BackgroundColor != null) BackgroundColor = si.BackgroundColor.Value;
            if (si.ForegroundColor != null) ForegroundColor = si.ForegroundColor.Value;
            if (si.Italic != null) Italic = si.Italic.Value;
            if (si.Underline != null) Underline = si.Underline.Value;
            if (si.Strikethrough != null) Strikethrough = si.Strikethrough.Value;
            if (si.Bold != null) Bold = si.Bold.Value;

            SLTuplesType tt;
            MemberPropertyIndex mpi;
            using (OpenXmlReader oxr = OpenXmlReader.Create(si))
            {
                while (oxr.Read())
                {
                    if (oxr.ElementType == typeof(Tuples))
                    {
                        tt = new SLTuplesType();
                        tt.FromTuples((Tuples)oxr.LoadCurrentElement());
                        Tuples.Add(tt);
                    }
                    else if (oxr.ElementType == typeof(MemberPropertyIndex))
                    {
                        // 0 is the default value.
                        mpi = (MemberPropertyIndex)oxr.LoadCurrentElement();
                        if (mpi.Val != null) MemberPropertyIndexes.Add(mpi.Val.Value);
                        else MemberPropertyIndexes.Add(0);
                    }
                }
            }
        }

        internal StringItem ToStringItem()
        {
            StringItem si = new StringItem
            {
                Val = Val
            };
            if (Unused != null) si.Unused = Unused.Value;
            if (Calculated != null) si.Calculated = Calculated.Value;
            if (Caption != null && Caption.Length > 0) si.Caption = Caption;
            if (PropertyCount != null) si.PropertyCount = PropertyCount.Value;
            if (FormatIndex != null) si.FormatIndex = FormatIndex.Value;
            if (BackgroundColor != null && BackgroundColor.Length > 0) si.BackgroundColor = new HexBinaryValue(BackgroundColor);
            if (ForegroundColor != null && ForegroundColor.Length > 0) si.ForegroundColor = new HexBinaryValue(ForegroundColor);
            if (Italic != false) si.Italic = Italic;
            if (Underline != false) si.Underline = Underline;
            if (Strikethrough != false) si.Strikethrough = Strikethrough;
            if (Bold != false) si.Bold = Bold;

            foreach (SLTuplesType tt in Tuples)
            {
                si.Append(tt.ToTuples());
            }

            foreach (int i in MemberPropertyIndexes)
            {
                if (i != 0) si.Append(new MemberPropertyIndex() { Val = i });
                else si.Append(new MemberPropertyIndex());
            }

            return si;
        }

        internal SLStringItem Clone()
        {
            SLStringItem si = new SLStringItem
            {
                Val = Val,
                Unused = Unused,
                Calculated = Calculated,
                Caption = Caption,
                PropertyCount = PropertyCount,
                FormatIndex = FormatIndex,
                BackgroundColor = BackgroundColor,
                ForegroundColor = ForegroundColor,
                Italic = Italic,
                Underline = Underline,
                Strikethrough = Strikethrough,
                Bold = Bold,

                Tuples = new List<SLTuplesType>()
            };
            foreach (SLTuplesType tt in Tuples)
            {
                si.Tuples.Add(tt.Clone());
            }

            si.MemberPropertyIndexes = new List<int>();
            foreach (int i in MemberPropertyIndexes)
            {
                si.MemberPropertyIndexes.Add(i);
            }

            return si;
        }
    }
}