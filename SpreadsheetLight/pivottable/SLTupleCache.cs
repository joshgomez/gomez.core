﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Collections.Generic;

namespace SpreadsheetLight
{
    internal class SLTupleCache
    {
        internal bool HasEntries;
        internal SLEntries Entries { get; set; }
        internal List<SLTupleSet> Sets { get; set; }
        internal List<SLQuery> QueryCache { get; set; }
        internal List<SLServerFormat> ServerFormats { get; set; }

        internal SLTupleCache()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            HasEntries = false;
            Entries = new SLEntries();
            Sets = new List<SLTupleSet>();
            QueryCache = new List<SLQuery>();
            ServerFormats = new List<SLServerFormat>();
        }

        internal void FromTupleCache(TupleCache tc)
        {
            SetAllNull();

            // I decided to do this one by one instead of just running through all the child
            // elements. Mainly because this seems safer... so complicated! It's just a pivot table
            // for goodness sakes...

            if (tc.Entries != null)
            {
                Entries.FromEntries(tc.Entries);
                HasEntries = true;
            }

            if (tc.Sets != null)
            {
                SLTupleSet ts;
                using (OpenXmlReader oxr = OpenXmlReader.Create(tc.Sets))
                {
                    while (oxr.Read())
                    {
                        if (oxr.ElementType == typeof(TupleSet))
                        {
                            ts = new SLTupleSet();
                            ts.FromTupleSet((TupleSet)oxr.LoadCurrentElement());
                            Sets.Add(ts);
                        }
                    }
                }
            }

            if (tc.QueryCache != null)
            {
                SLQuery q;
                using (OpenXmlReader oxr = OpenXmlReader.Create(tc.QueryCache))
                {
                    while (oxr.Read())
                    {
                        if (oxr.ElementType == typeof(Query))
                        {
                            q = new SLQuery();
                            q.FromQuery((Query)oxr.LoadCurrentElement());
                            QueryCache.Add(q);
                        }
                    }
                }
            }

            if (tc.ServerFormats != null)
            {
                SLServerFormat sf;
                using (OpenXmlReader oxr = OpenXmlReader.Create(tc.ServerFormats))
                {
                    while (oxr.Read())
                    {
                        if (oxr.ElementType == typeof(ServerFormat))
                        {
                            sf = new SLServerFormat();
                            sf.FromServerFormat((ServerFormat)oxr.LoadCurrentElement());
                            ServerFormats.Add(sf);
                        }
                    }
                }
            }
        }

        internal TupleCache ToTupleCache()
        {
            TupleCache tc = new TupleCache();
            if (HasEntries) tc.Entries = Entries.ToEntries();

            if (Sets.Count > 0)
            {
                tc.Sets = new Sets() { Count = (uint)Sets.Count };
                foreach (SLTupleSet ts in Sets)
                {
                    tc.Sets.Append(ts.ToTupleSet());
                }
            }

            if (QueryCache.Count > 0)
            {
                tc.QueryCache = new QueryCache() { Count = (uint)QueryCache.Count };
                foreach (SLQuery q in QueryCache)
                {
                    tc.QueryCache.Append(q.ToQuery());
                }
            }

            if (ServerFormats.Count > 0)
            {
                tc.ServerFormats = new ServerFormats() { Count = (uint)ServerFormats.Count };
                foreach (SLServerFormat sf in ServerFormats)
                {
                    tc.ServerFormats.Append(sf.ToServerFormat());
                }
            }

            return tc;
        }

        internal SLTupleCache Clone()
        {
            SLTupleCache tc = new SLTupleCache
            {
                HasEntries = HasEntries,
                Entries = Entries.Clone(),

                Sets = new List<SLTupleSet>()
            };
            foreach (SLTupleSet ts in Sets)
            {
                tc.Sets.Add(ts.Clone());
            }

            tc.QueryCache = new List<SLQuery>();
            foreach (SLQuery q in QueryCache)
            {
                tc.QueryCache.Add(q.Clone());
            }

            tc.ServerFormats = new List<SLServerFormat>();
            foreach (SLServerFormat sf in ServerFormats)
            {
                tc.ServerFormats.Add(sf.Clone());
            }

            return tc;
        }
    }
}