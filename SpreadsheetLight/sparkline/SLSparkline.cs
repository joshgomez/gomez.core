﻿using Excel = DocumentFormat.OpenXml.Office.Excel;
using X14 = DocumentFormat.OpenXml.Office2010.Excel;

namespace SpreadsheetLight
{
    internal class SLSparkline
    {
        internal string WorksheetName;
        internal int StartRowIndex;
        internal int StartColumnIndex;
        internal int EndRowIndex;
        internal int EndColumnIndex;
        internal int LocationRowIndex;
        internal int LocationColumnIndex;

        internal SLSparkline()
        {
            WorksheetName = string.Empty;
            StartRowIndex = 1;
            StartColumnIndex = 1;
            EndRowIndex = 1;
            EndColumnIndex = 1;
            LocationRowIndex = 1;
            LocationColumnIndex = 1;
        }

        internal X14.Sparkline ToSparkline()
        {
            X14.Sparkline spk = new X14.Sparkline();

            if (StartRowIndex == EndRowIndex && StartColumnIndex == EndColumnIndex)
            {
                spk.Formula = new Excel.Formula
                {
                    Text = SLTool.ToCellReference(WorksheetName, StartRowIndex, StartColumnIndex)
                };
            }
            else
            {
                spk.Formula = new Excel.Formula
                {
                    Text = SLTool.ToCellRange(WorksheetName, StartRowIndex, StartColumnIndex, EndRowIndex, EndColumnIndex)
                };
            }

            spk.ReferenceSequence = new Excel.ReferenceSequence
            {
                Text = SLTool.ToCellReference(LocationRowIndex, LocationColumnIndex)
            };

            return spk;
        }

        internal SLSparkline Clone()
        {
            SLSparkline spk = new SLSparkline
            {
                WorksheetName = WorksheetName,
                StartRowIndex = StartRowIndex,
                StartColumnIndex = StartColumnIndex,
                EndRowIndex = EndRowIndex,
                EndColumnIndex = EndColumnIndex,
                LocationRowIndex = LocationRowIndex,
                LocationColumnIndex = LocationColumnIndex
            };

            return spk;
        }
    }
}