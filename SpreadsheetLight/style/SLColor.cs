﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Text;
using X14 = DocumentFormat.OpenXml.Office2010.Excel;

namespace SpreadsheetLight
{
    /// <summary>
    /// Encapsulates properties and methods for setting a color. This includes using theme colors. This simulates the DocumentFormat.OpenXml.Spreadsheet.Color class.
    /// </summary>
    public class SLColor
    {
        internal List<System.Drawing.Color> listThemeColors;
        internal List<System.Drawing.Color> listIndexedColors;

        private System.Drawing.Color clrDisplay = new System.Drawing.Color();

        /// <summary>
        /// The color value.
        /// </summary>
        public System.Drawing.Color Color
        {
            get { return clrDisplay; }
            set
            {
                SetAllNull();
                clrDisplay = value;
                Rgb = clrDisplay.ToArgb().ToString("X8");
            }
        }

        internal bool? Auto { get; set; }
        internal uint? Indexed { get; set; }
        internal string Rgb { get; set; }
        internal uint? Theme { get; set; }

        internal double? fTint;

        internal double? Tint
        {
            get { return fTint; }
            set
            {
                fTint = value;
                if (fTint != null)
                {
                    if (fTint.Value < -1.0) fTint = -1.0;
                    if (fTint.Value > 1.0) fTint = 1.0;
                }
            }
        }

        internal SLColor(List<System.Drawing.Color> ThemeColors, List<System.Drawing.Color> IndexedColors)
        {
            int i;
            listThemeColors = new List<System.Drawing.Color>();
            for (i = 0; i < ThemeColors.Count; ++i)
            {
                listThemeColors.Add(ThemeColors[i]);
            }

            listIndexedColors = new List<System.Drawing.Color>();
            for (i = 0; i < IndexedColors.Count; ++i)
            {
                listIndexedColors.Add(IndexedColors[i]);
            }

            SetAllNull();
        }

        private void SetAllNull()
        {
            clrDisplay = new System.Drawing.Color();
            Auto = null;
            Indexed = null;
            Rgb = null;
            Theme = null;
            Tint = null;
        }

        /// <summary>
        /// Whether the color value is empty.
        /// </summary>
        /// <returns>True if the color value is empty. False otherwise.</returns>
        public bool IsEmpty()
        {
            return Auto == null && Indexed == null && Rgb == null && Theme == null;
        }

        /// <summary>
        /// Set a color using a theme color.
        /// </summary>
        /// <param name="ThemeColorIndex">The theme color to be used.</param>
        public void SetThemeColor(SLThemeColorIndexValues ThemeColorIndex)
        {
            int index = (int)ThemeColorIndex;
            if (index >= 0 && index < listThemeColors.Count)
            {
                clrDisplay = listThemeColors[index];
            }
            Auto = null;
            Indexed = null;
            Rgb = null;
            Theme = (uint)ThemeColorIndex;
            Tint = null;
        }

        /// <summary>
        /// Set a color using a theme color, modifying the theme color with a tint value.
        /// </summary>
        /// <param name="ThemeColorIndex">The theme color to be used.</param>
        /// <param name="Tint">The tint applied to the theme color, ranging from -1.0 to 1.0. Negative tints darken the theme color and positive tints lighten the theme color.</param>
        public void SetThemeColor(SLThemeColorIndexValues ThemeColorIndex, double Tint)
        {
            System.Drawing.Color clrRgb = new System.Drawing.Color();
            int index = (int)ThemeColorIndex;
            if (index >= 0 && index < listThemeColors.Count)
            {
                clrRgb = listThemeColors[index];
            }
            Auto = null;
            Indexed = null;
            Rgb = null;
            Theme = (uint)ThemeColorIndex;
            this.Tint = Tint;
            clrDisplay = SLTool.ToColor(clrRgb, Tint);
        }

        internal BackgroundColor ToBackgroundColor()
        {
            BackgroundColor clr = new BackgroundColor();
            if (Auto != null) clr.Auto = Auto.Value;
            if (Indexed != null) clr.Indexed = Indexed.Value;
            if (Rgb != null) clr.Rgb = new HexBinaryValue(Rgb);
            if (Theme != null) clr.Theme = Theme.Value;
            if (Tint != null && Tint.Value != 0.0) clr.Tint = Tint.Value;

            return clr;
        }

        internal ForegroundColor ToForegroundColor()
        {
            ForegroundColor clr = new ForegroundColor();
            if (Auto != null) clr.Auto = Auto.Value;
            if (Indexed != null) clr.Indexed = Indexed.Value;
            if (Rgb != null) clr.Rgb = new HexBinaryValue(Rgb);
            if (Theme != null) clr.Theme = Theme.Value;
            if (Tint != null && Tint.Value != 0.0) clr.Tint = Tint.Value;

            return clr;
        }

        internal TabColor ToTabColor()
        {
            TabColor clr = new TabColor();
            if (Auto != null) clr.Auto = Auto.Value;
            if (Indexed != null) clr.Indexed = Indexed.Value;
            if (Rgb != null) clr.Rgb = new HexBinaryValue(Rgb);
            if (Theme != null) clr.Theme = Theme.Value;
            if (Tint != null && Tint.Value != 0.0) clr.Tint = Tint.Value;

            return clr;
        }

        internal Color ToSpreadsheetColor()
        {
            Color clr = new Color();
            if (Auto != null) clr.Auto = Auto.Value;
            if (Indexed != null) clr.Indexed = Indexed.Value;
            if (Rgb != null) clr.Rgb = new HexBinaryValue(Rgb);
            if (Theme != null) clr.Theme = Theme.Value;
            if (Tint != null && Tint.Value != 0.0) clr.Tint = Tint.Value;

            return clr;
        }

        internal X14.AxisColor ToAxisColor()
        {
            X14.AxisColor clr = new X14.AxisColor();
            if (Auto != null) clr.Auto = Auto.Value;
            if (Indexed != null) clr.Indexed = Indexed.Value;
            if (Rgb != null) clr.Rgb = new HexBinaryValue(Rgb);
            if (Theme != null) clr.Theme = Theme.Value;
            if (Tint != null && Tint.Value != 0.0) clr.Tint = Tint.Value;

            return clr;
        }

        internal X14.BarAxisColor ToBarAxisColor()
        {
            X14.BarAxisColor clr = new X14.BarAxisColor();
            if (Auto != null) clr.Auto = Auto.Value;
            if (Indexed != null) clr.Indexed = Indexed.Value;
            if (Rgb != null) clr.Rgb = new HexBinaryValue(Rgb);
            if (Theme != null) clr.Theme = Theme.Value;
            if (Tint != null && Tint.Value != 0.0) clr.Tint = Tint.Value;

            return clr;
        }

        internal X14.BorderColor ToBorderColor()
        {
            X14.BorderColor clr = new X14.BorderColor();
            if (Auto != null) clr.Auto = Auto.Value;
            if (Indexed != null) clr.Indexed = Indexed.Value;
            if (Rgb != null) clr.Rgb = new HexBinaryValue(Rgb);
            if (Theme != null) clr.Theme = Theme.Value;
            if (Tint != null && Tint.Value != 0.0) clr.Tint = Tint.Value;

            return clr;
        }

        internal X14.Color ToExcel2010Color()
        {
            X14.Color clr = new X14.Color();
            if (Auto != null) clr.Auto = Auto.Value;
            if (Indexed != null) clr.Indexed = Indexed.Value;
            if (Rgb != null) clr.Rgb = new HexBinaryValue(Rgb);
            if (Theme != null) clr.Theme = Theme.Value;
            if (Tint != null && Tint.Value != 0.0) clr.Tint = Tint.Value;

            return clr;
        }

        internal X14.FillColor ToFillColor()
        {
            X14.FillColor clr = new X14.FillColor();
            if (Auto != null) clr.Auto = Auto.Value;
            if (Indexed != null) clr.Indexed = Indexed.Value;
            if (Rgb != null) clr.Rgb = new HexBinaryValue(Rgb);
            if (Theme != null) clr.Theme = Theme.Value;
            if (Tint != null && Tint.Value != 0.0) clr.Tint = Tint.Value;

            return clr;
        }

        internal X14.FirstMarkerColor ToFirstMarkerColor()
        {
            X14.FirstMarkerColor clr = new X14.FirstMarkerColor();
            if (Auto != null) clr.Auto = Auto.Value;
            if (Indexed != null) clr.Indexed = Indexed.Value;
            if (Rgb != null) clr.Rgb = new HexBinaryValue(Rgb);
            if (Theme != null) clr.Theme = Theme.Value;
            if (Tint != null && Tint.Value != 0.0) clr.Tint = Tint.Value;

            return clr;
        }

        internal X14.HighMarkerColor ToHighMarkerColor()
        {
            X14.HighMarkerColor clr = new X14.HighMarkerColor();
            if (Auto != null) clr.Auto = Auto.Value;
            if (Indexed != null) clr.Indexed = Indexed.Value;
            if (Rgb != null) clr.Rgb = new HexBinaryValue(Rgb);
            if (Theme != null) clr.Theme = Theme.Value;
            if (Tint != null && Tint.Value != 0.0) clr.Tint = Tint.Value;

            return clr;
        }

        internal X14.LastMarkerColor ToLastMarkerColor()
        {
            X14.LastMarkerColor clr = new X14.LastMarkerColor();
            if (Auto != null) clr.Auto = Auto.Value;
            if (Indexed != null) clr.Indexed = Indexed.Value;
            if (Rgb != null) clr.Rgb = new HexBinaryValue(Rgb);
            if (Theme != null) clr.Theme = Theme.Value;
            if (Tint != null && Tint.Value != 0.0) clr.Tint = Tint.Value;

            return clr;
        }

        internal X14.LowMarkerColor ToLowMarkerColor()
        {
            X14.LowMarkerColor clr = new X14.LowMarkerColor();
            if (Auto != null) clr.Auto = Auto.Value;
            if (Indexed != null) clr.Indexed = Indexed.Value;
            if (Rgb != null) clr.Rgb = new HexBinaryValue(Rgb);
            if (Theme != null) clr.Theme = Theme.Value;
            if (Tint != null && Tint.Value != 0.0) clr.Tint = Tint.Value;

            return clr;
        }

        internal X14.MarkersColor ToMarkersColor()
        {
            X14.MarkersColor clr = new X14.MarkersColor();
            if (Auto != null) clr.Auto = Auto.Value;
            if (Indexed != null) clr.Indexed = Indexed.Value;
            if (Rgb != null) clr.Rgb = new HexBinaryValue(Rgb);
            if (Theme != null) clr.Theme = Theme.Value;
            if (Tint != null && Tint.Value != 0.0) clr.Tint = Tint.Value;

            return clr;
        }

        internal X14.NegativeBorderColor ToNegativeBorderColor()
        {
            X14.NegativeBorderColor clr = new X14.NegativeBorderColor();
            if (Auto != null) clr.Auto = Auto.Value;
            if (Indexed != null) clr.Indexed = Indexed.Value;
            if (Rgb != null) clr.Rgb = new HexBinaryValue(Rgb);
            if (Theme != null) clr.Theme = Theme.Value;
            if (Tint != null && Tint.Value != 0.0) clr.Tint = Tint.Value;

            return clr;
        }

        internal X14.NegativeColor ToNegativeColor()
        {
            X14.NegativeColor clr = new X14.NegativeColor();
            if (Auto != null) clr.Auto = Auto.Value;
            if (Indexed != null) clr.Indexed = Indexed.Value;
            if (Rgb != null) clr.Rgb = new HexBinaryValue(Rgb);
            if (Theme != null) clr.Theme = Theme.Value;
            if (Tint != null && Tint.Value != 0.0) clr.Tint = Tint.Value;

            return clr;
        }

        internal X14.NegativeFillColor ToNegativeFillColor()
        {
            X14.NegativeFillColor clr = new X14.NegativeFillColor();
            if (Auto != null) clr.Auto = Auto.Value;
            if (Indexed != null) clr.Indexed = Indexed.Value;
            if (Rgb != null) clr.Rgb = new HexBinaryValue(Rgb);
            if (Theme != null) clr.Theme = Theme.Value;
            if (Tint != null && Tint.Value != 0.0) clr.Tint = Tint.Value;

            return clr;
        }

        internal X14.SeriesColor ToSeriesColor()
        {
            X14.SeriesColor clr = new X14.SeriesColor();
            if (Auto != null) clr.Auto = Auto.Value;
            if (Indexed != null) clr.Indexed = Indexed.Value;
            if (Rgb != null) clr.Rgb = new HexBinaryValue(Rgb);
            if (Theme != null) clr.Theme = Theme.Value;
            if (Tint != null && Tint.Value != 0.0) clr.Tint = Tint.Value;

            return clr;
        }

        internal void FromBackgroundColor(BackgroundColor clr)
        {
            SetAllNull();
            if (clr.Auto != null) Auto = clr.Auto.Value;
            if (clr.Indexed != null) Indexed = clr.Indexed.Value;
            if (clr.Rgb != null) Rgb = clr.Rgb.Value;
            if (clr.Theme != null) Theme = clr.Theme.Value;
            if (clr.Tint != null) Tint = clr.Tint.Value;
            SetDisplayColor();
        }

        internal void FromForegroundColor(ForegroundColor clr)
        {
            SetAllNull();
            if (clr.Auto != null) Auto = clr.Auto.Value;
            if (clr.Indexed != null) Indexed = clr.Indexed.Value;
            if (clr.Rgb != null) Rgb = clr.Rgb.Value;
            if (clr.Theme != null) Theme = clr.Theme.Value;
            if (clr.Tint != null) Tint = clr.Tint.Value;
            SetDisplayColor();
        }

        internal void FromTabColor(TabColor clr)
        {
            SetAllNull();
            if (clr.Auto != null) Auto = clr.Auto.Value;
            if (clr.Indexed != null) Indexed = clr.Indexed.Value;
            if (clr.Rgb != null) Rgb = clr.Rgb.Value;
            if (clr.Theme != null) Theme = clr.Theme.Value;
            if (clr.Tint != null) Tint = clr.Tint.Value;
            SetDisplayColor();
        }

        internal void FromSpreadsheetColor(Color clr)
        {
            SetAllNull();
            if (clr.Auto != null) Auto = clr.Auto.Value;
            if (clr.Indexed != null) Indexed = clr.Indexed.Value;
            if (clr.Rgb != null) Rgb = clr.Rgb.Value;
            if (clr.Theme != null) Theme = clr.Theme.Value;
            if (clr.Tint != null) Tint = clr.Tint.Value;
            SetDisplayColor();
        }

        internal void FromAxisColor(X14.AxisColor clr)
        {
            SetAllNull();
            if (clr.Auto != null) Auto = clr.Auto.Value;
            if (clr.Indexed != null) Indexed = clr.Indexed.Value;
            if (clr.Rgb != null) Rgb = clr.Rgb.Value;
            if (clr.Theme != null) Theme = clr.Theme.Value;
            if (clr.Tint != null) Tint = clr.Tint.Value;
            SetDisplayColor();
        }

        internal void FromBarAxisColor(X14.BarAxisColor clr)
        {
            SetAllNull();
            if (clr.Auto != null) Auto = clr.Auto.Value;
            if (clr.Indexed != null) Indexed = clr.Indexed.Value;
            if (clr.Rgb != null) Rgb = clr.Rgb.Value;
            if (clr.Theme != null) Theme = clr.Theme.Value;
            if (clr.Tint != null) Tint = clr.Tint.Value;
            SetDisplayColor();
        }

        internal void FromBorderColor(X14.BorderColor clr)
        {
            SetAllNull();
            if (clr.Auto != null) Auto = clr.Auto.Value;
            if (clr.Indexed != null) Indexed = clr.Indexed.Value;
            if (clr.Rgb != null) Rgb = clr.Rgb.Value;
            if (clr.Theme != null) Theme = clr.Theme.Value;
            if (clr.Tint != null) Tint = clr.Tint.Value;
            SetDisplayColor();
        }

        internal void FromExcel2010Color(X14.Color clr)
        {
            SetAllNull();
            if (clr.Auto != null) Auto = clr.Auto.Value;
            if (clr.Indexed != null) Indexed = clr.Indexed.Value;
            if (clr.Rgb != null) Rgb = clr.Rgb.Value;
            if (clr.Theme != null) Theme = clr.Theme.Value;
            if (clr.Tint != null) Tint = clr.Tint.Value;
            SetDisplayColor();
        }

        internal void FromFillColor(X14.FillColor clr)
        {
            SetAllNull();
            if (clr.Auto != null) Auto = clr.Auto.Value;
            if (clr.Indexed != null) Indexed = clr.Indexed.Value;
            if (clr.Rgb != null) Rgb = clr.Rgb.Value;
            if (clr.Theme != null) Theme = clr.Theme.Value;
            if (clr.Tint != null) Tint = clr.Tint.Value;
            SetDisplayColor();
        }

        internal void FromFirstMarkerColor(X14.FirstMarkerColor clr)
        {
            SetAllNull();
            if (clr.Auto != null) Auto = clr.Auto.Value;
            if (clr.Indexed != null) Indexed = clr.Indexed.Value;
            if (clr.Rgb != null) Rgb = clr.Rgb.Value;
            if (clr.Theme != null) Theme = clr.Theme.Value;
            if (clr.Tint != null) Tint = clr.Tint.Value;
            SetDisplayColor();
        }

        internal void FromHighMarkerColor(X14.HighMarkerColor clr)
        {
            SetAllNull();
            if (clr.Auto != null) Auto = clr.Auto.Value;
            if (clr.Indexed != null) Indexed = clr.Indexed.Value;
            if (clr.Rgb != null) Rgb = clr.Rgb.Value;
            if (clr.Theme != null) Theme = clr.Theme.Value;
            if (clr.Tint != null) Tint = clr.Tint.Value;
            SetDisplayColor();
        }

        internal void FromLastMarkerColor(X14.LastMarkerColor clr)
        {
            SetAllNull();
            if (clr.Auto != null) Auto = clr.Auto.Value;
            if (clr.Indexed != null) Indexed = clr.Indexed.Value;
            if (clr.Rgb != null) Rgb = clr.Rgb.Value;
            if (clr.Theme != null) Theme = clr.Theme.Value;
            if (clr.Tint != null) Tint = clr.Tint.Value;
            SetDisplayColor();
        }

        internal void FromLowMarkerColor(X14.LowMarkerColor clr)
        {
            SetAllNull();
            if (clr.Auto != null) Auto = clr.Auto.Value;
            if (clr.Indexed != null) Indexed = clr.Indexed.Value;
            if (clr.Rgb != null) Rgb = clr.Rgb.Value;
            if (clr.Theme != null) Theme = clr.Theme.Value;
            if (clr.Tint != null) Tint = clr.Tint.Value;
            SetDisplayColor();
        }

        internal void FromMarkersColor(X14.MarkersColor clr)
        {
            SetAllNull();
            if (clr.Auto != null) Auto = clr.Auto.Value;
            if (clr.Indexed != null) Indexed = clr.Indexed.Value;
            if (clr.Rgb != null) Rgb = clr.Rgb.Value;
            if (clr.Theme != null) Theme = clr.Theme.Value;
            if (clr.Tint != null) Tint = clr.Tint.Value;
            SetDisplayColor();
        }

        internal void FromNegativeBorderColor(X14.NegativeBorderColor clr)
        {
            SetAllNull();
            if (clr.Auto != null) Auto = clr.Auto.Value;
            if (clr.Indexed != null) Indexed = clr.Indexed.Value;
            if (clr.Rgb != null) Rgb = clr.Rgb.Value;
            if (clr.Theme != null) Theme = clr.Theme.Value;
            if (clr.Tint != null) Tint = clr.Tint.Value;
            SetDisplayColor();
        }

        internal void FromNegativeColor(X14.NegativeColor clr)
        {
            SetAllNull();
            if (clr.Auto != null) Auto = clr.Auto.Value;
            if (clr.Indexed != null) Indexed = clr.Indexed.Value;
            if (clr.Rgb != null) Rgb = clr.Rgb.Value;
            if (clr.Theme != null) Theme = clr.Theme.Value;
            if (clr.Tint != null) Tint = clr.Tint.Value;
            SetDisplayColor();
        }

        internal void FromNegativeFillColor(X14.NegativeFillColor clr)
        {
            SetAllNull();
            if (clr.Auto != null) Auto = clr.Auto.Value;
            if (clr.Indexed != null) Indexed = clr.Indexed.Value;
            if (clr.Rgb != null) Rgb = clr.Rgb.Value;
            if (clr.Theme != null) Theme = clr.Theme.Value;
            if (clr.Tint != null) Tint = clr.Tint.Value;
            SetDisplayColor();
        }

        internal void FromSeriesColor(X14.SeriesColor clr)
        {
            SetAllNull();
            if (clr.Auto != null) Auto = clr.Auto.Value;
            if (clr.Indexed != null) Indexed = clr.Indexed.Value;
            if (clr.Rgb != null) Rgb = clr.Rgb.Value;
            if (clr.Theme != null) Theme = clr.Theme.Value;
            if (clr.Tint != null) Tint = clr.Tint.Value;
            SetDisplayColor();
        }

        private void SetDisplayColor()
        {
            clrDisplay = System.Drawing.Color.FromArgb(255, 0, 0, 0);

            int index = 0;
            if (Theme != null)
            {
                index = (int)Theme.Value;
                if (index >= 0 && index < listThemeColors.Count)
                {
                    clrDisplay = System.Drawing.Color.FromArgb(255, listThemeColors[index]);
                    if (Tint != null)
                    {
                        clrDisplay = SLTool.ToColor(clrDisplay, Tint.Value);
                    }
                }
            }
            else if (Rgb != null)
            {
                clrDisplay = SLTool.ToColor(Rgb);
            }
            else if (Indexed != null)
            {
                index = (int)Indexed.Value;
                if (index >= 0 && index < listIndexedColors.Count)
                {
                    clrDisplay = System.Drawing.Color.FromArgb(255, listIndexedColors[index]);
                }
            }
        }

        internal void FromHash(string Hash)
        {
            SetAllNull();

            string[] sa = Hash.Split(new string[] { SLConstants.XmlColorAttributeSeparator }, StringSplitOptions.None);
            if (sa.Length >= 5)
            {
                if (!sa[0].Equals("null")) Auto = bool.Parse(sa[0]);

                if (!sa[1].Equals("null")) Indexed = uint.Parse(sa[1]);

                if (!sa[2].Equals("null")) Rgb = sa[2];

                if (!sa[3].Equals("null")) Theme = uint.Parse(sa[3]);

                if (!sa[4].Equals("null")) Tint = double.Parse(sa[4]);
            }

            SetDisplayColor();
        }

        internal string ToHash()
        {
            StringBuilder sb = new StringBuilder();

            if (Auto != null) sb.AppendFormat("{0}{1}", Auto.Value, SLConstants.XmlColorAttributeSeparator);
            else sb.AppendFormat("null{0}", SLConstants.XmlColorAttributeSeparator);

            if (Indexed != null) sb.AppendFormat("{0}{1}", Indexed.Value, SLConstants.XmlColorAttributeSeparator);
            else sb.AppendFormat("null{0}", SLConstants.XmlColorAttributeSeparator);

            if (Rgb != null) sb.AppendFormat("{0}{1}", Rgb, SLConstants.XmlColorAttributeSeparator);
            else sb.AppendFormat("null{0}", SLConstants.XmlColorAttributeSeparator);

            if (Theme != null) sb.AppendFormat("{0}{1}", Theme.Value, SLConstants.XmlColorAttributeSeparator);
            else sb.AppendFormat("null{0}", SLConstants.XmlColorAttributeSeparator);

            if (Tint != null) sb.AppendFormat("{0}{1}", Tint.Value, SLConstants.XmlColorAttributeSeparator);
            else sb.AppendFormat("null{0}", SLConstants.XmlColorAttributeSeparator);

            return sb.ToString();
        }

        internal SLColor Clone()
        {
            SLColor clr = new SLColor(listThemeColors, listIndexedColors)
            {
                clrDisplay = clrDisplay,
                Auto = Auto,
                Indexed = Indexed,
                Rgb = Rgb,
                Theme = Theme,
                Tint = Tint
            };

            return clr;
        }
    }
}