﻿using DocumentFormat.OpenXml.Spreadsheet;

namespace SpreadsheetLight
{
    internal class SLNumberingFormat
    {
        private uint iNumberFormatId;

        internal uint NumberFormatId
        {
            get { return iNumberFormatId; }
            set { iNumberFormatId = value; }
        }

        private string sFormatCode;

        internal string FormatCode
        {
            get { return sFormatCode; }
            set { sFormatCode = value; }
        }

        internal SLNumberingFormat()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            iNumberFormatId = 0;
            sFormatCode = string.Empty;
        }

        internal void FromNumberingFormat(NumberingFormat nf)
        {
            SetAllNull();

            if (nf.NumberFormatId != null)
            {
                NumberFormatId = nf.NumberFormatId.Value;
            }
            else
            {
                NumberFormatId = 0;
            }

            if (nf.FormatCode != null)
            {
                FormatCode = nf.FormatCode.Value;
            }
            else
            {
                FormatCode = string.Empty;
            }
        }

        internal NumberingFormat ToNumberingFormat()
        {
            NumberingFormat nf = new NumberingFormat
            {
                NumberFormatId = NumberFormatId,
                FormatCode = FormatCode
            };

            return nf;
        }

        internal void FromHash(string Hash)
        {
            FormatCode = Hash;
        }

        internal string ToHash()
        {
            return FormatCode;
        }

        internal SLNumberingFormat Clone()
        {
            SLNumberingFormat nf = new SLNumberingFormat
            {
                iNumberFormatId = iNumberFormatId,
                sFormatCode = sFormatCode
            };

            return nf;
        }
    }
}