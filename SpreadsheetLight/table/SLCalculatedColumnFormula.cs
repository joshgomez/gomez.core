﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;

namespace SpreadsheetLight
{
    internal class SLCalculatedColumnFormula
    {
        internal bool Array { get; set; }
        internal string Text { get; set; }

        internal SLCalculatedColumnFormula()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            Array = false;
            Text = string.Empty;
        }

        internal void FromCalculatedColumnFormula(CalculatedColumnFormula ccf)
        {
            SetAllNull();

            if (ccf.Array != null && ccf.Array.Value) Array = true;
            Text = ccf.Text;
        }

        internal CalculatedColumnFormula ToCalculatedColumnFormula()
        {
            CalculatedColumnFormula ccf = new CalculatedColumnFormula();
            if (Array) ccf.Array = Array;

            if (SLTool.ToPreserveSpace(Text))
            {
                ccf.Space = SpaceProcessingModeValues.Preserve;
            }
            ccf.Text = Text;

            return ccf;
        }

        internal SLCalculatedColumnFormula Clone()
        {
            SLCalculatedColumnFormula ccf = new SLCalculatedColumnFormula
            {
                Array = Array,
                Text = Text
            };

            return ccf;
        }
    }
}