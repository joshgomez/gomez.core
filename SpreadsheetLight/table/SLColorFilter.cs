﻿using DocumentFormat.OpenXml.Spreadsheet;

namespace SpreadsheetLight
{
    internal class SLColorFilter
    {
        internal uint? FormatId { get; set; }
        internal bool? CellColor { get; set; }

        internal SLColorFilter()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            FormatId = null;
            CellColor = null;
        }

        internal void FromColorFilter(ColorFilter cf)
        {
            SetAllNull();

            if (cf.FormatId != null) FormatId = cf.FormatId.Value;
            if (cf.CellColor != null && !cf.CellColor.Value) CellColor = cf.CellColor.Value;
        }

        internal ColorFilter ToColorFilter()
        {
            ColorFilter cf = new ColorFilter();
            if (FormatId != null) cf.FormatId = FormatId.Value;
            if (CellColor != null && !CellColor.Value) cf.CellColor = CellColor.Value;

            return cf;
        }

        internal SLColorFilter Clone()
        {
            SLColorFilter cf = new SLColorFilter
            {
                FormatId = FormatId,
                CellColor = CellColor
            };

            return cf;
        }
    }
}