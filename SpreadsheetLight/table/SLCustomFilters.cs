﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;

namespace SpreadsheetLight
{
    internal class SLCustomFilters
    {
        internal bool OneCustomFilter;

        internal bool HasFirstOperator;
        private FilterOperatorValues vFirstOperator;

        internal FilterOperatorValues FirstOperator
        {
            get { return vFirstOperator; }
            set
            {
                vFirstOperator = value;
                HasFirstOperator = vFirstOperator != FilterOperatorValues.Equal ? true : false;
            }
        }

        internal string FirstVal { get; set; }

        internal bool HasSecondOperator;
        private FilterOperatorValues vSecondOperator;

        internal FilterOperatorValues SecondOperator
        {
            get { return vSecondOperator; }
            set
            {
                vSecondOperator = value;
                HasSecondOperator = vSecondOperator != FilterOperatorValues.Equal ? true : false;
            }
        }

        internal string SecondVal { get; set; }

        internal bool? And { get; set; }

        internal SLCustomFilters()
        {
            CustomFilter cf = new CustomFilter
            {
                Operator = FilterOperatorValues.Equal,
                Val = ""
            };
        }

        private void SetAllNull()
        {
            OneCustomFilter = true;
            vFirstOperator = FilterOperatorValues.Equal;
            HasFirstOperator = false;
            FirstVal = string.Empty;
            vSecondOperator = FilterOperatorValues.Equal;
            HasSecondOperator = false;
            SecondVal = string.Empty;
            And = null;
        }

        internal void FromCustomFilters(CustomFilters cfs)
        {
            SetAllNull();

            if (cfs.And != null && cfs.And.Value) And = cfs.And.Value;

            int i = 0;
            CustomFilter cf;
            using (OpenXmlReader oxr = OpenXmlReader.Create(cfs))
            {
                while (oxr.Read())
                {
                    if (oxr.ElementType == typeof(CustomFilter))
                    {
                        ++i;
                        cf = (CustomFilter)oxr.LoadCurrentElement();
                        if (i == 1)
                        {
                            OneCustomFilter = true;
                            if (cf.Operator != null) FirstOperator = cf.Operator.Value;
                            if (cf.Val != null) FirstVal = cf.Val.Value;
                        }
                        else if (i == 2)
                        {
                            OneCustomFilter = false;
                            if (cf.Operator != null) SecondOperator = cf.Operator.Value;
                            if (cf.Val != null) SecondVal = cf.Val.Value;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
        }

        internal CustomFilters ToCustomFilters()
        {
            CustomFilters cfs = new CustomFilters();
            if (And != null && And.Value) cfs.And = And.Value;

            CustomFilter cf;
            if (OneCustomFilter)
            {
                cf = new CustomFilter();
                if (HasFirstOperator) cf.Operator = FirstOperator;
                cf.Val = FirstVal;
                cfs.Append(cf);
            }
            else
            {
                cf = new CustomFilter();
                if (HasFirstOperator) cf.Operator = FirstOperator;
                cf.Val = FirstVal;
                cfs.Append(cf);

                cf = new CustomFilter();
                if (HasSecondOperator) cf.Operator = SecondOperator;
                cf.Val = SecondVal;
                cfs.Append(cf);
            }

            return cfs;
        }

        internal SLCustomFilters Clone()
        {
            SLCustomFilters cfs = new SLCustomFilters
            {
                OneCustomFilter = OneCustomFilter,
                HasFirstOperator = HasFirstOperator,
                vFirstOperator = vFirstOperator,
                FirstVal = FirstVal,
                HasSecondOperator = HasSecondOperator,
                vSecondOperator = vSecondOperator,
                SecondVal = SecondVal,
                And = And
            };

            return cfs;
        }
    }
}