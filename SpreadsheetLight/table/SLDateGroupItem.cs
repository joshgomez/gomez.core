﻿using DocumentFormat.OpenXml.Spreadsheet;
using System;

namespace SpreadsheetLight
{
    internal class SLDateGroupItem
    {
        internal ushort Year { get; set; }
        internal ushort? Month { get; set; }
        internal ushort? Day { get; set; }
        internal ushort? Hour { get; set; }
        internal ushort? Minute { get; set; }
        internal ushort? Second { get; set; }
        internal DateTimeGroupingValues DateTimeGrouping { get; set; }

        internal SLDateGroupItem()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            Year = (ushort)DateTime.Now.Year;
            Month = null;
            Day = null;
            Hour = null;
            Minute = null;
            Second = null;
            DateTimeGrouping = DateTimeGroupingValues.Year;
        }

        internal void FromDateGroupItem(DateGroupItem dgi)
        {
            SetAllNull();

            Year = dgi.Year.Value;
            if (dgi.Month != null) Month = dgi.Month.Value;
            if (dgi.Day != null) Day = dgi.Day.Value;
            if (dgi.Hour != null) Hour = dgi.Hour.Value;
            if (dgi.Minute != null) Minute = dgi.Minute.Value;
            if (dgi.Second != null) Second = dgi.Second.Value;
            DateTimeGrouping = dgi.DateTimeGrouping.Value;
        }

        internal DateGroupItem ToDateGroupItem()
        {
            DateGroupItem dgi = new DateGroupItem
            {
                Year = Year
            };
            if (Month != null) dgi.Month = Month.Value;
            if (Day != null) dgi.Day = Day.Value;
            if (Hour != null) dgi.Hour = Hour.Value;
            if (Minute != null) dgi.Minute = Minute.Value;
            if (Second != null) dgi.Second = Second.Value;
            dgi.DateTimeGrouping = DateTimeGrouping;

            return dgi;
        }

        internal SLDateGroupItem Clone()
        {
            SLDateGroupItem dgi = new SLDateGroupItem
            {
                Year = Year,
                Month = Month,
                Day = Day,
                Hour = Hour,
                Minute = Minute,
                Second = Second,
                DateTimeGrouping = DateTimeGrouping
            };

            return dgi;
        }
    }
}