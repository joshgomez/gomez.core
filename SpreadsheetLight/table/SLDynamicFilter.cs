﻿using DocumentFormat.OpenXml.Spreadsheet;

namespace SpreadsheetLight
{
    internal class SLDynamicFilter
    {
        internal DynamicFilterValues Type { get; set; }
        internal double? Val { get; set; }
        internal double? MaxVal { get; set; }

        internal SLDynamicFilter()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            Type = DynamicFilterValues.Null;
            Val = null;
            MaxVal = null;
        }

        internal void FromDynamicFilter(DynamicFilter df)
        {
            SetAllNull();

            Type = df.Type.Value;
            if (df.Val != null) Val = df.Val.Value;
            if (df.MaxVal != null) MaxVal = df.MaxVal.Value;
        }

        internal DynamicFilter ToDynamicFilter()
        {
            DynamicFilter df = new DynamicFilter
            {
                Type = Type
            };
            if (Val != null) df.Val = Val.Value;
            if (MaxVal != null) df.MaxVal = MaxVal.Value;

            return df;
        }

        internal SLDynamicFilter Clone()
        {
            SLDynamicFilter df = new SLDynamicFilter
            {
                Type = Type,
                Val = Val,
                MaxVal = MaxVal
            };

            return df;
        }
    }
}