﻿using DocumentFormat.OpenXml.Spreadsheet;

namespace SpreadsheetLight
{
    internal class SLFilter
    {
        internal string Val { get; set; }

        internal SLFilter()
        {
            Val = string.Empty;
        }

        internal void FromFilter(Filter f)
        {
            Val = f.Val ?? string.Empty;
        }

        internal Filter ToFilter()
        {
            Filter f = new Filter
            {
                Val = Val
            };

            return f;
        }

        internal SLFilter Clone()
        {
            SLFilter f = new SLFilter
            {
                Val = Val
            };

            return f;
        }
    }
}