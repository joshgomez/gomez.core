﻿using DocumentFormat.OpenXml.Spreadsheet;

namespace SpreadsheetLight
{
    internal class SLIconFilter
    {
        internal IconSetValues IconSet { get; set; }
        internal uint? IconId { get; set; }

        internal SLIconFilter()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            IconSet = IconSetValues.ThreeArrows;
            IconId = null;
        }

        internal void FromIconFilter(IconFilter icf)
        {
            SetAllNull();

            IconSet = icf.IconSet.Value;
            if (icf.IconId != null) IconId = icf.IconId.Value;
        }

        internal IconFilter ToIconFilter()
        {
            IconFilter icf = new IconFilter
            {
                IconSet = IconSet
            };
            if (IconId != null) icf.IconId = IconId.Value;

            return icf;
        }

        internal SLIconFilter Clone()
        {
            SLIconFilter icf = new SLIconFilter
            {
                IconSet = IconSet,
                IconId = IconId
            };

            return icf;
        }
    }
}