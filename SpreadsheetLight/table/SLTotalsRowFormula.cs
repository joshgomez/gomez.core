﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;

namespace SpreadsheetLight
{
    internal class SLTotalsRowFormula
    {
        internal bool Array { get; set; }
        internal string Text { get; set; }

        internal SLTotalsRowFormula()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            Array = false;
            Text = string.Empty;
        }

        internal void FromTotalsRowFormula(TotalsRowFormula trf)
        {
            SetAllNull();

            if (trf.Array != null && trf.Array.Value) Array = true;
            Text = trf.Text;
        }

        internal TotalsRowFormula ToTotalsRowFormula()
        {
            TotalsRowFormula trf = new TotalsRowFormula();
            if (Array) trf.Array = Array;

            if (SLTool.ToPreserveSpace(Text))
            {
                trf.Space = SpaceProcessingModeValues.Preserve;
            }
            trf.Text = Text;

            return trf;
        }

        internal SLTotalsRowFormula Clone()
        {
            SLTotalsRowFormula trf = new SLTotalsRowFormula
            {
                Array = Array,
                Text = Text
            };

            return trf;
        }
    }
}