﻿using DocumentFormat.OpenXml.Spreadsheet;

namespace SpreadsheetLight
{
    internal class SLXmlColumnProperties
    {
        internal uint MapId { get; set; }
        internal string XPath { get; set; }
        internal bool? Denormalized { get; set; }
        internal XmlDataValues XmlDataType { get; set; }

        internal SLXmlColumnProperties()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            MapId = 0;
            XPath = string.Empty;
            Denormalized = null;
            XmlDataType = XmlDataValues.AnyType;
        }

        internal void FromXmlColumnProperties(XmlColumnProperties xcp)
        {
            SetAllNull();

            if (xcp.MapId != null) MapId = xcp.MapId.Value;
            if (xcp.XPath != null) XPath = xcp.XPath.Value;
            if (xcp.Denormalized != null && xcp.Denormalized.Value) Denormalized = xcp.Denormalized.Value;
            if (xcp.XmlDataType != null) XmlDataType = xcp.XmlDataType.Value;
        }

        internal XmlColumnProperties ToXmlColumnProperties()
        {
            XmlColumnProperties xcp = new XmlColumnProperties
            {
                MapId = MapId,
                XPath = XPath
            };
            if (Denormalized != null && Denormalized.Value) xcp.Denormalized = Denormalized.Value;
            xcp.XmlDataType = XmlDataType;

            return xcp;
        }

        internal SLXmlColumnProperties Clone()
        {
            SLXmlColumnProperties xcp = new SLXmlColumnProperties
            {
                MapId = MapId,
                XPath = XPath,
                Denormalized = Denormalized,
                XmlDataType = XmlDataType
            };

            return xcp;
        }
    }
}