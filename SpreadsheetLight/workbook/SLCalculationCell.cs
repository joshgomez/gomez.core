﻿using DocumentFormat.OpenXml.Spreadsheet;

namespace SpreadsheetLight
{
    internal class SLCalculationCell
    {
        internal int RowIndex { get; set; }
        internal int ColumnIndex { get; set; }
        internal int SheetId { get; set; }
        internal bool? InChildChain { get; set; }
        internal bool? NewLevel { get; set; }
        internal bool? NewThread { get; set; }
        internal bool? Array { get; set; }

        internal SLCalculationCell()
        {
            SetAllNull();
        }

        internal SLCalculationCell(string CellReference)
        {
            SetAllNull();

            int iRowIndex = -1;
            int iColumnIndex = -1;
            if (SLTool.FormatCellReferenceToRowColumnIndex(CellReference, out iRowIndex, out iColumnIndex))
            {
                RowIndex = iRowIndex;
                ColumnIndex = iColumnIndex;
            }
        }

        private void SetAllNull()
        {
            RowIndex = 1;
            ColumnIndex = 1;
            SheetId = 0;
            InChildChain = null;
            NewLevel = null;
            NewThread = null;
            Array = null;
        }

        internal void FromCalculationCell(CalculationCell cc)
        {
            SetAllNull();

            int iRowIndex = -1;
            int iColumnIndex = -1;
            if (SLTool.FormatCellReferenceToRowColumnIndex(cc.CellReference.Value, out iRowIndex, out iColumnIndex))
            {
                RowIndex = iRowIndex;
                ColumnIndex = iColumnIndex;
            }

            SheetId = cc.SheetId ?? 0;
            if (cc.InChildChain != null) InChildChain = cc.InChildChain.Value;
            if (cc.NewLevel != null) NewLevel = cc.NewLevel.Value;
            if (cc.NewThread != null) NewThread = cc.NewThread.Value;
            if (cc.Array != null) Array = cc.Array.Value;
        }

        internal CalculationCell ToCalculationCell()
        {
            CalculationCell cc = new CalculationCell
            {
                CellReference = SLTool.ToCellReference(RowIndex, ColumnIndex),
                SheetId = SheetId
            };
            if (InChildChain != null && InChildChain.Value) cc.InChildChain = InChildChain.Value;
            if (NewLevel != null && NewLevel.Value) cc.NewLevel = NewLevel.Value;
            if (NewThread != null && NewThread.Value) cc.NewThread = NewThread.Value;
            if (Array != null && Array.Value) cc.Array = Array.Value;

            return cc;
        }

        internal SLCalculationCell Clone()
        {
            SLCalculationCell cc = new SLCalculationCell
            {
                RowIndex = RowIndex,
                ColumnIndex = ColumnIndex,
                SheetId = SheetId,
                InChildChain = InChildChain,
                NewLevel = NewLevel,
                NewThread = NewThread,
                Array = Array
            };

            return cc;
        }
    }
}