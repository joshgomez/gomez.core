﻿using DocumentFormat.OpenXml.Spreadsheet;

namespace SpreadsheetLight
{
    internal class SLSheet
    {
        internal string Name { get; set; }
        internal uint SheetId { get; set; }
        internal SheetStateValues State { get; set; }
        internal string Id { get; set; }
        internal SLSheetType SheetType { get; set; }

        internal SLSheet(string Name, uint SheetId, string Id, SLSheetType SheetType)
        {
            this.Name = Name;
            this.SheetId = SheetId;
            State = SheetStateValues.Visible;
            this.Id = Id;
            this.SheetType = SheetType;
        }

        internal Sheet ToSheet()
        {
            Sheet s = new Sheet
            {
                Name = Name,
                SheetId = SheetId
            };
            if (State != SheetStateValues.Visible) s.State = State;
            s.Id = Id;

            return s;
        }

        internal SLSheet Clone()
        {
            SLSheet s = new SLSheet(Name, SheetId, Id, SheetType)
            {
                State = State
            };
            return s;
        }
    }
}