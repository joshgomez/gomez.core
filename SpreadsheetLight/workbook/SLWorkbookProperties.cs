﻿using DocumentFormat.OpenXml.Spreadsheet;

namespace SpreadsheetLight
{
    internal class SLWorkbookProperties
    {
        internal bool HasWorkbookProperties
        {
            get
            {
                return bDate1904 != null || bDateCompatibility != null || vShowObjects != null
                    || bShowBorderUnselectedTables != null || bFilterPrivacy != null || bPromptedSolutions != null
                    || bShowInkAnnotation != null || bBackupFile != null || bSaveExternalLinkValues != null
                    || vUpdateLinks != null || sCodeName != null || bHidePivotFieldList != null
                    || bShowPivotChartFilter != null || bAllowRefreshQuery != null || bPublishItems != null
                    || bCheckCompatibility != null || bAutoCompressPictures != null || bRefreshAllConnections != null
                    || iDefaultThemeVersion != null;
            }
        }

        private bool? bDate1904;

        internal bool Date1904
        {
            get { return bDate1904 ?? false; }
            set { bDate1904 = value; }
        }

        private bool? bDateCompatibility;

        internal bool DateCompatibility
        {
            get { return bDateCompatibility ?? true; }
            set { bDateCompatibility = value; }
        }

        private ObjectDisplayValues? vShowObjects;

        internal ObjectDisplayValues ShowObjects
        {
            get { return vShowObjects ?? ObjectDisplayValues.All; }
            set { vShowObjects = value; }
        }

        private bool? bShowBorderUnselectedTables;

        internal bool ShowBorderUnselectedTables
        {
            get { return bShowBorderUnselectedTables ?? true; }
            set { bShowBorderUnselectedTables = value; }
        }

        private bool? bFilterPrivacy;

        internal bool FilterPrivacy
        {
            get { return bFilterPrivacy ?? false; }
            set { bFilterPrivacy = value; }
        }

        private bool? bPromptedSolutions;

        internal bool PromptedSolutions
        {
            get { return bPromptedSolutions ?? false; }
            set { bPromptedSolutions = value; }
        }

        private bool? bShowInkAnnotation;

        internal bool ShowInkAnnotation
        {
            get { return bShowInkAnnotation ?? true; }
            set { bShowInkAnnotation = value; }
        }

        private bool? bBackupFile;

        internal bool BackupFile
        {
            get { return bBackupFile ?? false; }
            set { bBackupFile = value; }
        }

        private bool? bSaveExternalLinkValues;

        internal bool SaveExternalLinkValues
        {
            get { return bSaveExternalLinkValues ?? true; }
            set { bSaveExternalLinkValues = value; }
        }

        private UpdateLinksBehaviorValues? vUpdateLinks;

        internal UpdateLinksBehaviorValues UpdateLinks
        {
            get { return vUpdateLinks ?? UpdateLinksBehaviorValues.UserSet; }
            set { vUpdateLinks = value; }
        }

        private string sCodeName;

        internal string CodeName
        {
            get { return sCodeName ?? ""; }
            set { sCodeName = value; }
        }

        private bool? bHidePivotFieldList;

        internal bool HidePivotFieldList
        {
            get { return bHidePivotFieldList ?? false; }
            set { bHidePivotFieldList = value; }
        }

        private bool? bShowPivotChartFilter;

        internal bool ShowPivotChartFilter
        {
            get { return bShowPivotChartFilter ?? false; }
            set { bShowPivotChartFilter = value; }
        }

        private bool? bAllowRefreshQuery;

        internal bool AllowRefreshQuery
        {
            get { return bAllowRefreshQuery ?? false; }
            set { bAllowRefreshQuery = value; }
        }

        private bool? bPublishItems;

        internal bool PublishItems
        {
            get { return bPublishItems ?? false; }
            set { bPublishItems = value; }
        }

        private bool? bCheckCompatibility;

        internal bool CheckCompatibility
        {
            get { return bCheckCompatibility ?? false; }
            set { bCheckCompatibility = value; }
        }

        private bool? bAutoCompressPictures;

        internal bool AutoCompressPictures
        {
            get { return bAutoCompressPictures ?? true; }
            set { bAutoCompressPictures = value; }
        }

        private bool? bRefreshAllConnections;

        internal bool RefreshAllConnections
        {
            get { return bRefreshAllConnections ?? false; }
            set { bRefreshAllConnections = value; }
        }

        private uint? iDefaultThemeVersion;

        internal uint DefaultThemeVersion
        {
            get { return iDefaultThemeVersion ?? 0; }
            set { iDefaultThemeVersion = value; }
        }

        internal SLWorkbookProperties()
        {
            SetAllNull();
        }

        internal void SetAllNull()
        {
            bDate1904 = null;
            bDateCompatibility = null;
            vShowObjects = null;
            bShowBorderUnselectedTables = null;
            bFilterPrivacy = null;
            bPromptedSolutions = null;
            bShowInkAnnotation = null;
            bBackupFile = null;
            bSaveExternalLinkValues = null;
            vUpdateLinks = null;
            sCodeName = null;
            bHidePivotFieldList = null;
            bShowPivotChartFilter = null;
            bAllowRefreshQuery = null;
            bPublishItems = null;
            bCheckCompatibility = null;
            bAutoCompressPictures = null;
            bRefreshAllConnections = null;
            iDefaultThemeVersion = null;
        }

        internal void FromWorkbookProperties(WorkbookProperties wp)
        {
            SetAllNull();
            if (wp.Date1904 != null) Date1904 = wp.Date1904.Value;
            if (wp.DateCompatibility != null) DateCompatibility = wp.DateCompatibility.Value;
            if (wp.ShowObjects != null) ShowObjects = wp.ShowObjects.Value;
            if (wp.ShowBorderUnselectedTables != null) ShowBorderUnselectedTables = wp.ShowBorderUnselectedTables.Value;
            if (wp.FilterPrivacy != null) FilterPrivacy = wp.FilterPrivacy.Value;
            if (wp.PromptedSolutions != null) PromptedSolutions = wp.PromptedSolutions.Value;
            if (wp.ShowInkAnnotation != null) ShowInkAnnotation = wp.ShowInkAnnotation.Value;
            if (wp.BackupFile != null) BackupFile = wp.BackupFile.Value;
            if (wp.SaveExternalLinkValues != null) SaveExternalLinkValues = wp.SaveExternalLinkValues.Value;
            if (wp.UpdateLinks != null) UpdateLinks = wp.UpdateLinks.Value;
            if (wp.CodeName != null) CodeName = wp.CodeName.Value;
            if (wp.HidePivotFieldList != null) HidePivotFieldList = wp.HidePivotFieldList.Value;
            if (wp.ShowPivotChartFilter != null) ShowPivotChartFilter = wp.ShowPivotChartFilter.Value;
            if (wp.AllowRefreshQuery != null) AllowRefreshQuery = wp.AllowRefreshQuery.Value;
            if (wp.PublishItems != null) PublishItems = wp.PublishItems.Value;
            if (wp.CheckCompatibility != null) CheckCompatibility = wp.CheckCompatibility.Value;
            if (wp.AutoCompressPictures != null) AutoCompressPictures = wp.AutoCompressPictures.Value;
            if (wp.RefreshAllConnections != null) RefreshAllConnections = wp.RefreshAllConnections.Value;
            if (wp.DefaultThemeVersion != null) DefaultThemeVersion = wp.DefaultThemeVersion.Value;
        }

        internal WorkbookProperties ToWorkbookProperties()
        {
            WorkbookProperties wp = new WorkbookProperties();
            if (bDate1904 != null) wp.Date1904 = bDate1904.Value;
            if (bDateCompatibility != null) wp.DateCompatibility = bDateCompatibility.Value;
            if (vShowObjects != null) wp.ShowObjects = vShowObjects.Value;
            if (bShowBorderUnselectedTables != null) wp.ShowBorderUnselectedTables = bShowBorderUnselectedTables.Value;
            if (bFilterPrivacy != null) wp.FilterPrivacy = bFilterPrivacy.Value;
            if (bPromptedSolutions != null) wp.PromptedSolutions = bPromptedSolutions.Value;
            if (bShowInkAnnotation != null) wp.ShowInkAnnotation = bShowInkAnnotation.Value;
            if (bBackupFile != null) wp.BackupFile = bBackupFile.Value;
            if (bSaveExternalLinkValues != null) wp.SaveExternalLinkValues = bSaveExternalLinkValues.Value;
            if (vUpdateLinks != null) wp.UpdateLinks = vUpdateLinks.Value;
            if (sCodeName != null) wp.CodeName = sCodeName;
            if (bHidePivotFieldList != null) wp.HidePivotFieldList = bHidePivotFieldList.Value;
            if (bShowPivotChartFilter != null) wp.ShowPivotChartFilter = bShowPivotChartFilter.Value;
            if (bAllowRefreshQuery != null) wp.AllowRefreshQuery = bAllowRefreshQuery.Value;
            if (bPublishItems != null) wp.PublishItems = bPublishItems.Value;
            if (bCheckCompatibility != null) wp.CheckCompatibility = bCheckCompatibility.Value;
            if (bAutoCompressPictures != null) wp.AutoCompressPictures = bAutoCompressPictures.Value;
            if (bRefreshAllConnections != null) wp.RefreshAllConnections = bRefreshAllConnections.Value;
            if (iDefaultThemeVersion != null) wp.DefaultThemeVersion = iDefaultThemeVersion.Value;

            return wp;
        }
    }
}