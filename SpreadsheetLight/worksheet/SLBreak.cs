﻿using DocumentFormat.OpenXml.Spreadsheet;

namespace SpreadsheetLight
{
    internal class SLBreak
    {
        internal uint Id { get; set; }
        internal uint Min { get; set; }
        internal uint Max { get; set; }
        internal bool ManualPageBreak { get; set; }
        internal bool PivotTablePageBreak { get; set; }

        internal SLBreak()
        {
            SetAllNull();
        }

        internal void SetAllNull()
        {
            Id = 0;
            Min = 0;
            Max = 0;
            ManualPageBreak = false;
            PivotTablePageBreak = false;
        }

        internal void FromBreak(Break b)
        {
            SetAllNull();
            if (b.Id != null) Id = b.Id;
            if (b.Min != null) Min = b.Min;
            if (b.Max != null) Max = b.Max;
            if (b.ManualPageBreak != null) ManualPageBreak = b.ManualPageBreak;
            if (b.PivotTablePageBreak != null) PivotTablePageBreak = b.PivotTablePageBreak;
        }

        internal Break ToBreak()
        {
            Break b = new Break();
            if (Id != 0) b.Id = Id;
            if (Min != 0) b.Min = Min;
            if (Max != 0) b.Max = Max;
            if (ManualPageBreak != false) b.ManualPageBreak = ManualPageBreak;
            if (PivotTablePageBreak != false) b.PivotTablePageBreak = PivotTablePageBreak;

            return b;
        }

        internal SLBreak Clone()
        {
            SLBreak b = new SLBreak
            {
                Id = Id,
                Min = Min,
                Max = Max,
                ManualPageBreak = ManualPageBreak,
                PivotTablePageBreak = PivotTablePageBreak
            };

            return b;
        }
    }
}