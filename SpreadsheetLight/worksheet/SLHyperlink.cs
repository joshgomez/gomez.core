﻿using DocumentFormat.OpenXml.Spreadsheet;
using System;

namespace SpreadsheetLight
{
    internal class SLHyperlink
    {
        internal bool IsExternal;
        internal string HyperlinkUri;
        internal UriKind HyperlinkUriKind;
        internal bool IsNew;

        internal SLCellPointRange Reference { get; set; }
        internal string Id { get; set; }
        internal string Location { get; set; }
        internal string ToolTip { get; set; }
        internal string Display { get; set; }

        internal SLHyperlink()
        {
            SetAllNull();
        }

        private void SetAllNull()
        {
            IsExternal = false;
            HyperlinkUri = string.Empty;
            HyperlinkUriKind = UriKind.RelativeOrAbsolute;
            IsNew = true;

            Reference = new SLCellPointRange();
            Id = null;
            Location = null;
            ToolTip = null;
            Display = null;
        }

        internal void FromHyperlink(Hyperlink hl)
        {
            SetAllNull();

            IsNew = false;

            if (hl.Reference != null) Reference = SLTool.TranslateReferenceToCellPointRange(hl.Reference.Value);

            if (hl.Id != null)
            {
                // At least I think if there's a relationship ID, it's an external link.
                IsExternal = true;
                Id = hl.Id.Value;
            }

            if (hl.Location != null) Location = hl.Location.Value;
            if (hl.Tooltip != null) ToolTip = hl.Tooltip.Value;
            if (hl.Display != null) Display = hl.Display.Value;
        }

        internal Hyperlink ToHyperlink()
        {
            Hyperlink hl = new Hyperlink
            {
                Reference = SLTool.ToCellRange(Reference.StartRowIndex, Reference.StartColumnIndex, Reference.EndRowIndex, Reference.EndColumnIndex)
            };
            if (Id != null && Id.Length > 0) hl.Id = Id;
            if (Location != null && Location.Length > 0) hl.Location = Location;
            if (ToolTip != null && ToolTip.Length > 0) hl.Tooltip = ToolTip;
            if (Display != null && Display.Length > 0) hl.Display = Display;

            return hl;
        }

        internal SLHyperlink Clone()
        {
            SLHyperlink hl = new SLHyperlink
            {
                IsExternal = IsExternal,
                HyperlinkUri = HyperlinkUri,
                HyperlinkUriKind = HyperlinkUriKind,
                IsNew = IsNew,
                Reference = new SLCellPointRange(Reference.StartRowIndex, Reference.StartColumnIndex, Reference.EndRowIndex, Reference.EndColumnIndex),
                Id = Id,
                Location = Location,
                ToolTip = ToolTip,
                Display = Display
            };

            return hl;
        }
    }
}