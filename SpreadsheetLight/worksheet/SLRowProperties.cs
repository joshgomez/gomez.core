﻿using DocumentFormat.OpenXml.Spreadsheet;
using System;

namespace SpreadsheetLight
{
    internal class SLRowProperties
    {
        internal bool IsEmpty
        {
            get
            {
                return (StyleIndex == 0) && !HasHeight && !Hidden
                    && (OutlineLevel == 0) && !Collapsed
                    && !ThickTop && !ThickBottom && !ShowPhonetic;
            }
        }

        internal double DefaultRowHeight { get; set; }

        internal uint StyleIndex { get; set; }

        internal bool CustomHeight;

        internal bool HasHeight;
        private double fHeight;

        // The row height in points.
        internal double Height
        {
            get { return fHeight; }
            set
            {
                double fModifiedRowHeight = value / SLDocument.RowHeightMultiple;
                fModifiedRowHeight = Math.Ceiling(fModifiedRowHeight) * SLDocument.RowHeightMultiple;

                lHeightInEMU = (long)(fModifiedRowHeight * SLConstants.PointToEMU);

                fHeight = fModifiedRowHeight;
                CustomHeight = true;
                HasHeight = true;
            }
        }

        private long lHeightInEMU;

        internal long HeightInEMU
        {
            get { return lHeightInEMU; }
        }

        internal bool Hidden { get; set; }
        internal byte OutlineLevel { get; set; }
        internal bool Collapsed { get; set; }
        internal bool ThickTop { get; set; }
        internal bool ThickBottom { get; set; }
        internal bool ShowPhonetic { get; set; }

        internal SLRowProperties(double DefaultRowHeight)
        {
            this.DefaultRowHeight = DefaultRowHeight;
            StyleIndex = 0;
            Height = DefaultRowHeight;
            CustomHeight = false;
            HasHeight = false;
            Hidden = false;
            OutlineLevel = 0;
            Collapsed = false;
            ThickTop = false;
            ThickBottom = false;
            ShowPhonetic = false;
        }

        internal Row ToRow()
        {
            Row r = new Row();
            if (StyleIndex > 0)
            {
                r.StyleIndex = StyleIndex;
                r.CustomFormat = true;
            }
            if (HasHeight)
            {
                r.Height = Height;
            }
            if (CustomHeight)
            {
                r.CustomHeight = true;
            }

            if (Hidden != false) r.Hidden = Hidden;
            if (OutlineLevel > 0) r.OutlineLevel = OutlineLevel;
            if (Collapsed != false) r.Collapsed = Collapsed;
            if (ThickTop != false) r.ThickTop = ThickTop;
            if (ThickBottom != false) r.ThickBot = ThickBottom;
            if (ShowPhonetic != false) r.ShowPhonetic = ShowPhonetic;

            return r;
        }

        internal SLRowProperties Clone()
        {
            SLRowProperties rp = new SLRowProperties(DefaultRowHeight)
            {
                DefaultRowHeight = DefaultRowHeight,
                StyleIndex = StyleIndex,
                HasHeight = HasHeight,
                fHeight = fHeight,
                lHeightInEMU = lHeightInEMU,
                Hidden = Hidden,
                OutlineLevel = OutlineLevel,
                Collapsed = Collapsed,
                ThickTop = ThickTop,
                ThickBottom = ThickBottom,
                ShowPhonetic = ShowPhonetic
            };

            return rp;
        }
    }
}